#!/usr/bin/python
# -*- coding: utf-8 -*-

import datetime
import matplotlib.pyplot as plt


import copy

import matplotlib as mpl
import os, sys

import time, sys
import numpy as np
import matplotlib
matplotlib.use('QT4Agg')

from matplotlib.font_manager import FontProperties
import cmath as mth
import math
import time
import _pickle as cPickle
import pickle as Pickle

import matplotlib.mlab as mlab
import scipy
import scipy.fftpack
import scipy.stats.stats
import scipy.special
import scipy.linalg
import scipy.signal

import ctypes
import random

import locale
import operator
from matplotlib.ticker import ScalarFormatter
import re
import decimal
locale.setlocale(locale.LC_NUMERIC, "")
import urllib
import string
import pandas as pd
from scipy import stats

import os

from matplotlib import rcParams
import datetime


from cmath import *

from collections import Counter

import copy

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.colors as colors


import numpy as np
from scipy import ndimage


from scipy.optimize import minimize

from numpy import linalg as LA


import scipy as scipy
import numpy
print(numpy.version.version)
print("numpy.version.version")


import json

from os import listdir
from os.path import isfile, join
print('--------------')
print(scipy.__version__)
print("scipy.__version__")
print('--------------')
import datetime      

try:
    import cPickle as pickle
except ImportError:  # python 3.x
    import pickle




#########################################################################################
def maximum_absolute_value_of_einstein_tensor_components___myfunc(G__t_t,\
                                                                  G__t_rho,\
                                                                  G__t_x,\
                                                                  G__x_rho,\
                                                                  G__x_x,\
                                                                  G__phi_phi,\
                                                                  G__rho_rho,\
):
    return np.max(np.abs([G__t_t,\
                          G__t_rho,\
                          G__t_x,\
                          G__x_rho,\
                          G__x_x,\
                          G__phi_phi,\
                          G__rho_rho]))

#########################################################################################################################
def compute_each_space_box_minimum_nec___myfunc(epsilon_plus__p_x,epsilon_plus__p_rho,epsilon_plus__p_phi):
    return np.min([0,epsilon_plus__p_x,epsilon_plus__p_rho,epsilon_plus__p_phi])

#########################################################################################################################




################################################################################################
def choose_tW_matrix(meshed_f_r__ones_theta_real,sqrt_lambda_minus,meshed_cos_theta,meshed_sin_theta,sqrt_lambda_plus,meshed_f_r_over_r__ones_theta_real,second_basis_choice,big_N):


    # this is not the real theta /// meshed_cos_theta /// not meshed_cos_theta_real because: this rather is the theta by which we rotate in the eigenspace of g_{mu,nu}
                                         
    if second_basis_choice==0:
        #the following line gives the tW_4 but for the boost:
        tW_4_boost=np.array([[1,0                                                        ,0                                                        ,0],\
                             [0,np.cosh(meshed_f_r__ones_theta_real)                     ,np.sinh(meshed_f_r__ones_theta_real)*1./sqrt_lambda_minus,0],\
                             [0,np.sinh(meshed_f_r__ones_theta_real)*1.*sqrt_lambda_minus,np.cosh(meshed_f_r__ones_theta_real)                     ,0],\
                             [0,0                                                        ,0                                                        ,1],\
                             ])
        tW_4_second_fluid=tW_4_boost
    elif second_basis_choice==1:
        #the following line gives the tW_4 but instead of theta for (theta+pi/4):
        tW_4_pi_over_4=np.array([[np.sqrt(2)/2.*(meshed_cos_theta-meshed_sin_theta),0,np.sqrt(2)/2.*(meshed_sin_theta+meshed_cos_theta)*1./sqrt_lambda_plus,0],\
                                 [0,1,0,0],\
                                 [-np.sqrt(2)/2.*(meshed_sin_theta+meshed_cos_theta)*sqrt_lambda_plus,0,np.sqrt(2)/2.*(meshed_cos_theta-meshed_sin_theta),0],\
                                 [0,0,0,1],\
        ])
        tW_4_second_fluid=tW_4_pi_over_4
    elif second_basis_choice==2:
        ####################################
        tW_4_boost=np.array([[1,0                                                        ,0                                                        ,0],\
                             [0,np.cosh(meshed_f_r__ones_theta_real)                     ,np.sinh(meshed_f_r__ones_theta_real)*1./sqrt_lambda_minus,0],\
                             [0,np.sinh(meshed_f_r__ones_theta_real)*1.*sqrt_lambda_minus,np.cosh(meshed_f_r__ones_theta_real)                     ,0],\
                             [0,0                                                        ,0                                                        ,1],\
        ])
        tW_4_pi_over_4=np.array([[np.sqrt(2)/2.*(meshed_cos_theta-meshed_sin_theta),0,np.sqrt(2)/2.*(meshed_sin_theta+meshed_cos_theta)*1./sqrt_lambda_plus,0],\
                                 [0,1,0,0],\
                                 [-np.sqrt(2)/2.*(meshed_sin_theta+meshed_cos_theta)*sqrt_lambda_plus,0,np.sqrt(2)/2.*(meshed_cos_theta-meshed_sin_theta),0],\
                                 [0,0,0,1],\
        ])

        tW_4_second_fluid=np.dot(tW_4_pi_over_4,tW_4_boost)

        ####################################
    elif second_basis_choice==3:
        ####################################
        tW_4_boost=np.array([[1,0                                                        ,0                                                        ,0],\
                             [0,np.cosh(10*meshed_f_r__ones_theta_real)                     ,np.sinh(10*meshed_f_r__ones_theta_real)*1./sqrt_lambda_minus,0],\
                             [0,np.sinh(10*meshed_f_r__ones_theta_real)*1.*sqrt_lambda_minus,np.cosh(10*meshed_f_r__ones_theta_real)                     ,0],\
                             [0,0                                                        ,0                                                        ,1],\
        ])
        tW_4_pi_over_4=np.array([[np.sqrt(2)/2.*(meshed_cos_theta-meshed_sin_theta),0,np.sqrt(2)/2.*(meshed_sin_theta+meshed_cos_theta)*1./sqrt_lambda_plus,0],\
                                 [0,1,0,0],\
                                 [-np.sqrt(2)/2.*(meshed_sin_theta+meshed_cos_theta)*sqrt_lambda_plus,0,np.sqrt(2)/2.*(meshed_cos_theta-meshed_sin_theta),0],\
                                 [0,0,0,1],\
        ])

        tW_4_second_fluid=np.dot(tW_4_pi_over_4,tW_4_boost)

        ####################################
    elif second_basis_choice==4:
        ####################################
        tW_4_boost=np.array([[1,0                                                        ,0                                                        ,0],\
                             [0,np.cosh(10*meshed_f_r__ones_theta_real)                     ,np.sinh(10*meshed_f_r__ones_theta_real)*1./sqrt_lambda_minus,0],\
                             [0,np.sinh(10*meshed_f_r__ones_theta_real)*1.*sqrt_lambda_minus,np.cosh(10*meshed_f_r__ones_theta_real)                     ,0],\
                             [0,0                                                        ,0                                                        ,1],\
        ])
        tW_4_pi_over_3=np.array([[0.5*(meshed_cos_theta-np.sqrt(3)*meshed_sin_theta),0,0.5*(meshed_sin_theta+np.sqrt(3)*meshed_cos_theta)*1./sqrt_lambda_plus,0],\
                                 [0,1,0,0],\
                                 [-0.5*(meshed_sin_theta+np.sqrt(3)*meshed_cos_theta)*sqrt_lambda_plus,0,0.5*(meshed_cos_theta-np.sqrt(3)*meshed_sin_theta),0],\
                                 [0,0,0,1],\
        ])

        tW_4_second_fluid=np.dot(tW_4_pi_over_3,tW_4_boost)

        ####################################
    elif second_basis_choice==5:

        ####################################
        tW_4_boost=np.array([[1,0                                                        ,0                                                        ,0],\
                             [0,np.cosh(10*meshed_f_r_over_r__ones_theta_real)                     ,np.sinh(10*meshed_f_r_over_r__ones_theta_real)*1./sqrt_lambda_minus,0],\
                             [0,np.sinh(10*meshed_f_r_over_r__ones_theta_real)*1.*sqrt_lambda_minus,np.cosh(10*meshed_f_r_over_r__ones_theta_real)                     ,0],\
                             [0,0                                                        ,0                                                        ,1],\
        ])


        cos_theta_0=np.cos(np.pi/big_N)
        sin_theta_0=np.sin(np.pi/big_N)

        tW_4_pi_over_n=np.array([[(cos_theta_0*meshed_cos_theta-sin_theta_0*meshed_sin_theta),0,(cos_theta_0*meshed_sin_theta+sin_theta_0*meshed_cos_theta)*1./sqrt_lambda_plus,0],\
                                 [0,1,0,0],\
                                 [-(cos_theta_0*meshed_sin_theta+sin_theta_0*meshed_cos_theta)*sqrt_lambda_plus,0,(cos_theta_0*meshed_cos_theta-sin_theta_0*meshed_sin_theta),0],\
                                 [0,0,0,1],\
        ])

        tW_4_second_fluid=np.dot(tW_4_pi_over_n,tW_4_boost)

        ####################################
    return tW_4_second_fluid
############################################################
def print_np_array(array,txt):
                            
    np.set_printoptions(linewidth=150)#[source]
    print(array)
    print("\n -----\n "+txt+'\n -------------\n ')
    
    np.set_printoptions(linewidth=75)#[source]
    return
########################################################################
##################################################
def compute_big_matrix_for_fluids___display_properties(basis_vectors,G_einst_mixed_plus_Lambda_delta):


    # HERE we want to compute rho and pressure etc
    # thus we beforehand compute
    #terms like (e_beta,alpha)^mu    *      (e_beta,alpha)^nu , for each fluid

    nb_of_fluids=len(basis_vectors)


    big_matrix=[]
    vect_u_cov=[]
    
    for mu in range(4):

        for nu in range(4):

            
            row=[] # row for big matrix
            
            for i in range(nb_of_fluids):
                # on va former une matrice 4*nb_of_fluids x 16 (pour 2 fluides: 8 x 16 ;pour 3 fluides 12*16)

                

                #basis vector indexing (so far)
                #fluid nb / cov 0 -- contra 1 / component mu--nu/ vector index (a time vector , a X vector, a rho vector etc.)
                
                for j in range(4): # this is the vector index
                    ##ini


                    #en fait que contravariante e / big matrix
                    row.append(basis_vectors[i][1][nu,j]*basis_vectors[i][1][mu,j])
                    
            big_matrix.append(row)
    matrix__e_t__or__u__fluids=[]
    for i in range(nb_of_fluids):
        row=[]
        for mu in range(4):
        
            #basis vector indexing (so far)
            #fluid nb / cov 0 -- contra 1 / component mu--nu/ vector index (a time vector , a X vector, a rho vector etc.)
            
            
            #en fait que cov
            row.append(basis_vectors[i][0][mu,0])
        matrix__e_t__or__u__fluids.append(row)
    
    return big_matrix,matrix__e_t__or__u__fluids
###################################################

##################################################
def compute_big_matrix_for_fluids(basis_vectors,G_einst_mixed_plus_Lambda_delta):
    # HERE we compute
    #(e_beta,alpha)^mu    *      (e_beta,alpha)_nu

    nb_of_fluids=len(basis_vectors)


    big_matrix=[]
    
    Gee_vector=[]
    #G_einst_mixed

    overflow_big_mat=0
    G_plus_lambda_delta__over_8_pi=G_einst_mixed_plus_Lambda_delta/(8*np.pi)
    for mu in range(4):
        for nu in range(mu+1):
            Gee_vector.append(G_plus_lambda_delta__over_8_pi[mu,nu])
            
            row=[] # row for big matrix
            for i in range(nb_of_fluids):
                # on va former une matrice 4*nb_of_fluids x 10 (pour 2 fluides: 8 x 10 ;pour 3 fluides 12*10)

                

                #basis vector indexing (so far)
                #fluid nb / cov 0 -- contra 1 / component mu--nu/ vector index (a time vector , a X vector, a rho vector etc.)
                
                for j in range(4): # this is the vector index
                    try:
                        row.append(basis_vectors[i][0][nu,j]*basis_vectors[i][1][mu,j])
                    except:
                        row.append(1)
                        overflow_big_mat+=1

            big_matrix.append(row)
            
    
        
        
    #input()
    return big_matrix,Gee_vector,overflow_big_mat
###################################################


##################################################
#def compute_big_matrix_for_fluids(basis_vectors,G_einst_mixed_plus_Lambda_delta):

#big_matrix_3flu__errors,gee_vector_3flu__errors,overflow_big_mat__errors=
def compute_big_matrix_for_fluids__errors(basis_vectors,basis_vectors_delta_errors,\
                                          G_einst_mixed_plus_Lambda_delta):

    #
    #G_einst_mixed_plus_lambda_delta)
    #def    compute _ b i g _ ma  t rix_for_fluids__errors(basis_vectors,
    
    # HERE we compute
    #(DELTA e_beta,alpha)^mu    *      (e_beta,alpha)_nu  +  ( e_beta,alpha)^mu    *      (DELTA e_beta,alpha)_nu
    

    nb_of_fluids=len(basis_vectors)


    big_matrix=[]
    
    Gee_vector=[]
    #G_einst_mixed

    overflow_big_mat=0
    G_plus_lambda_delta__over_8_pi=G_einst_mixed_plus_Lambda_delta/(8*np.pi)
    for mu in range(4):
        for nu in range(mu+1):
            Gee_vector.append(G_plus_lambda_delta__over_8_pi[mu,nu])
            
            row=[] # row for big matrix
            for i in range(nb_of_fluids):
                # on va former une matrice 4*nb_of_fluids x 10 (pour 2 fluides: 8 x 10 ;pour 3 fluides 12*10)

                

                #basis vector indexing (so far)
                #fluid nb / cov 0 -- contra 1 / component mu--nu/ vector index (a time vector , a X vector, a rho vector etc.)
                
                for j in range(4): # this is the vector index
                    try:

                        row.append(basis_vectors[i][0][nu,j]*basis_vectors_delta_errors[i][1][mu,j]+basis_vectors[i][1][mu,j]*basis_vectors_delta_errors[i][0][nu,j])
                    except:
                        row.append(1)
                        overflow_big_mat+=1

            big_matrix.append(row)
            
    
        
        

    return big_matrix,Gee_vector,overflow_big_mat
###################################################


##################################################
def compute_close_to_identity_matrix_for_fluids(basis_vectors,G_einst_mixed):
    # (e_beta0,alpha0)_mu * (e_beta,alpha)^mu    +      (e_beta,alpha)_nu    *   (e_beta0,alpha0)^nu   

    nb_of_fluids=len(basis_vectors)


    big_matrix=[]
    
    Gee_vector=[]
    #G_einst_mixed
    
    for mu in range(4):
        for nu in range(mu+1):
            Gee_vector.append(G_einst_mixed[mu,nu])
            
            row=[] # row for big matrix
            for i in range(nb_of_fluids):
                # on va former une matrice 4*nb_of_fluids x 10 (pour 2 fluides: 8 x 10 ;pour 3 fluides 12*10)

                

                #basis vector indexing (so far)
                #fluid nb / cov 0 -- contra 1 / component mu--nu/ vector index (a time vector , a X vector, a rho vector etc.)
                
                for j in range(4): # this is the vector index

                    row.append(basis_vectors[i][0][nu,j]*basis_vectors[i][1][mu,j])
            big_matrix.append(row)
            
    return big_matrix,Gee_vector
###################################################

if 1:
        if 1:
                if 1:
                        if 1:
                                #if 1:
                                        

                                #############################################################################################
                                def complete_by_continuity(mask_where_we_can_replace_by_whatever_we_like,epsilon_minus_less_nan):
                        
                                        original_nans=np.isnan(epsilon_minus_less_nan)
                                        should_still_be_nans= ((original_nans==1) & (mask_where_we_can_replace_by_whatever_we_like==0))



                                        epsilon_minus_less_nan__mean_continuity = ndimage.generic_filter(epsilon_minus_less_nan, np.nanmean, size=3, mode='constant', cval=np.NaN)
                                        epsilon_minus_less_nan__mean_continuity[should_still_be_nans==1]=np.float('NaN')
                                        return epsilon_minus_less_nan__mean_continuity

                        
                                #####################################################################################################################
                                def plot_array(quantity,theta_mesh, phi_mesh,v_max,v_min,v,r_max,r_min,r,quantity_txt='',supplementary_array=[],\
                                               supplementary_array_txt=''):
                                        
                                        fig = plt.figure(num=None, figsize=(15, 13), dpi=80, facecolor='w', edgecolor='k')
                                        ax = fig.gca(projection='3d')
                                        surf = ax.plot_surface(theta_mesh, phi_mesh, quantity, rstride=1, cstride=1, cmap='hot', linewidth=0, antialiased=False)
                                        if len(supplementary_array)!=0:
                                                
                                                surf2 = ax.plot_surface(theta_mesh, phi_mesh, supplementary_array, rstride=1, cstride=1, cmap='cool', linewidth=0, antialiased=False)
                                        ax.set_xlabel('theta')
                                        ax.set_ylabel('phi')
                                        ax.set_zlabel(quantity_txt)

                                        text_to_show='v_max = '+v_max.__str__()+"\n v = "+v.__str__()+'\n v_min = '+v_min.__str__()+\
                                                '\n \n r_max = '+r_max.__str__()+' \n r = '+r.__str__()+'\n r_min = '+r_min.__str__()

                                        text_to_show+='\n \n '+quantity_txt+'___MAX = '+np.max(quantity.flatten()).__str__()+'\n '+quantity_txt+'___MIN = '+np.min(quantity.flatten()).__str__()
                                        ax.text2D(0.05, 0.95, text_to_show, transform=ax.transAxes)
                                        plt.title(quantity_txt)

                                        fig.colorbar(surf, shrink=0.5, aspect=5)
                                        if len(supplementary_array)!=0:
                                                fig.colorbar(surf2, shrink=0.5, aspect=5)
                                        plt.show()
                                        return
                                #############################################################################################
                                #####################################################################################################################
                                def plot_array__theta_r(quantity,theta_mesh, r_mesh,v_max,v_min,v,r_max,r_min,r,quantity_txt='',supplementary_array=[],\
                                                        supplementary_array_txt='', symlog_z=False):
                                        
                                        fig = plt.figure(num=None, figsize=(15, 13), dpi=80, facecolor='w', edgecolor='k')
                                        ax = fig.gca(projection='3d')
                                        surf = ax.plot_surface(theta_mesh, r_mesh, quantity, rstride=1, cstride=1, cmap='hot', linewidth=0, antialiased=False)
                                        if len(supplementary_array)!=0:
                                                
                                                surf2 = ax.plot_surface(theta_mesh, r_mesh, supplementary_array, rstride=1, cstride=1, cmap='cool', linewidth=0, antialiased=False)
                                        ax.set_xlabel('theta')
                                        ax.set_ylabel('r')
                                        ax.set_zlabel(quantity_txt)

                                        if symlog_z:
                                            ax.set_zscale('symlog')
                                        text_to_show='v_max = '+v_max.__str__()+"\n v = "+v.__str__()+'\n v_min = '+v_min.__str__()
                                        if 'varies' in str(r):
                                            text_to_show+='\n \n r_max = '+r_max.__str__()+'\n r_min = '+r_min.__str__()
                                        else:
                                            text_to_show+='\n \n r_max = '+r_max.__str__()+' \n r = '+r.__str__()+'\n r_min = '+r_min.__str__()

                                        text_to_show+='\n \n '+quantity_txt+'___MAX = '+np.max(quantity.flatten()).__str__()+'\n '+quantity_txt+'___MIN = '+np.min(quantity.flatten()).__str__()
                                        ax.text2D(0.05, 0.95, text_to_show, transform=ax.transAxes)
                                        plt.title(quantity_txt)

                                        fig.colorbar(surf, shrink=0.5, aspect=5)
                                        if len(supplementary_array)!=0:
                                                fig.colorbar(surf2, shrink=0.5, aspect=5)
                                        plt.show()
                                        return
                                #############################################################################################

                                def complete_by_continuity(mask_where_we_can_replace_by_whatever_we_like,epsilon_minus_less_nan):
                        
                                        original_nans=np.isnan(epsilon_minus_less_nan)
                                        should_still_be_nans= ((original_nans==1) & (mask_where_we_can_replace_by_whatever_we_like==0))



                                        epsilon_minus_less_nan__mean_continuity = ndimage.generic_filter(epsilon_minus_less_nan, np.nanmean, size=3, mode='constant', cval=np.NaN)
                                        epsilon_minus_less_nan__mean_continuity[should_still_be_nans==1]=np.float('NaN')
                                        return epsilon_minus_less_nan__mean_continuity



###################################################################
def compute_things_with_r_and_thetas(theta,r_range):
    
    ones_theta=np.ones(theta.shape) 

    ones_r=np.ones(r_range.shape) 

    

    r_max=np.max(r_range)
    r_min=np.min(r_range)

    
    theta_mesh,r_mesh=np.meshgrid(theta,r_range)
    
    

    

    cos_theta=np.cos(theta)
    sin_theta=np.sin(theta)
    sin_theta_squared=sin_theta**2
    cos_theta_squared=cos_theta**2
    sin_half_theta=np.sin(theta/2.)
    sin_half_theta_squared=sin_half_theta**2
    

    cos_theta_squared=cos_theta_squared[:,np.newaxis]
    sin_theta_squared=sin_theta_squared[:,np.newaxis]
    sin_theta=sin_theta[:,np.newaxis]
    cos_theta=cos_theta[:,np.newaxis]
    sin_half_theta=sin_half_theta[:,np.newaxis]
    sin_half_theta_squared=sin_half_theta_squared[:,np.newaxis]



    sin_2theta=np.sin(2*theta)[:,np.newaxis]

    ones_theta=ones_theta[:,np.newaxis]
    ones_r=ones_r[:,np.newaxis]

    ones_theta_and_r=np.dot(ones_r,ones_theta.T)
    meshed_sin_half_theta_squared=np.dot(ones_r,sin_half_theta_squared.T)
    meshed_sin_theta=np.dot(ones_r,sin_theta.T)
    meshed_cos_theta=np.dot(ones_r,cos_theta.T)

    return sin_theta,cos_theta,ones_theta,r_mesh,cos_theta_squared,sin_theta_squared,sin_half_theta,sin_half_theta_squared,ones_theta_and_r,\
            meshed_sin_half_theta_squared,meshed_sin_theta,meshed_cos_theta,theta_mesh



###################################################################

def compute_f_and_derivatives(r,R,sigma):

    T_plus=np.tanh(sigma*(r+R))
    T_minu=np.tanh(sigma*(r-R))
    tanh_sigma_R=np.tanh(sigma*R)
    f=(T_plus-T_minu)/(2*tanh_sigma_R)
    f_r=-sigma/(2*tanh_sigma_R)*(T_plus**2-T_minu**2)
    f_rr=-sigma**2/tanh_sigma_R*(T_plus*(1-T_plus**2)-T_minu*(1-T_minu**2))

    f_r_over_r=f_r/r

    f=f[:,np.newaxis]
    f_r=f_r[:,np.newaxis]
    f_rr=f_rr[:,np.newaxis]
    f_r_over_r=f_r_over_r[:,np.newaxis]
    return f,f_r,f_rr,f_r_over_r

####################################################################################

def mix_derivatives_of_f__and_theta(f,f_r,f_rr,f_r_over_r,sin_theta,cos_theta,ones_theta,cos_theta_squared,sin_theta_squared):
        


            
    sin_theta_dot_f_r=np.dot(f_r,sin_theta.T)
    cos_theta_dot_f_r=np.dot(f_r,cos_theta.T)
    meshed_f_r=np.dot(f_r,ones_theta.T)
    meshed_f_r_over_r=np.dot(f_r_over_r,ones_theta.T)
    meshed_f_rr=np.dot(f_rr,ones_theta.T)


    cos_theta_dot_sin_theta=cos_theta*sin_theta

    cos_theta_dot_sin_theta_dot__f_rr_minus_f_r_over_r=np.dot(f_rr-f_r_over_r,cos_theta_dot_sin_theta.T)
    

    cos_theta_squared_dot__f_rr_minus_f_r_over_r=np.dot(f_rr-f_r_over_r,cos_theta_squared.T)
    sin_theta_squared_dot__f_rr_minus_f_r_over_r=np.dot(f_rr-f_r_over_r,sin_theta_squared.T)



    ##################################
    # on nomme g_{\mu,\nu}*e'^{\mu}=e'_{\nu}, avec e' une base orthonormée de vecteurs au sens e'^\mu=e'_{\alpha}^\mu with \alpha \in \{t,X,\rho,\phi\}
    #and <e'_{\alpha} | e'_{\alpha'}>= <  e'_{\alpha}^\mu  g_{\mu\nu} e'_{\alpha'}^\nu  > =\eta^{\alpha \alpha'} = [the minkowski metric]


    meshed_f=np.dot(f,ones_theta.T)






    return meshed_f,cos_theta_squared_dot__f_rr_minus_f_r_over_r,sin_theta_squared_dot__f_rr_minus_f_r_over_r,meshed_f_r,meshed_f_r_over_r,meshed_f_rr
###########################################################################
def e_prime_computation(sin_half_theta_squared_over_N_plus_squared,\
                        lambda_plus,sqrt_lambda_plus,A,v,meshed_f,sin_theta_over_N_plus,meshed_f_r,meshed_cos_theta,theta_real_mesh,r_mesh):


                
    #e_p stands for e_prime /// e_p_t stands for the time vector then there is the component e_p_t_mu with mu in {t,X,rho,phi}
    # these are covariant components
    e_p_t_t=1+2*np.multiply(np.multiply(sin_half_theta_squared_over_N_plus_squared,-lambda_plus+2-A),np.multiply(A,lambda_plus-1)+A-1)
    e_p_t_X=-2*np.multiply(np.multiply(sin_half_theta_squared_over_N_plus_squared,-lambda_plus+2-A),  np.multiply(v*(1-meshed_f),lambda_plus  ))
    e_p_t_rho=np.multiply(np.multiply(sin_theta_over_N_plus,-lambda_plus+2-A),  -sqrt_lambda_plus)
    e_p_t_phi=0*meshed_f_r
    
    
    
    
    e_p_X_t=np.multiply(v*(1-meshed_f),(1-2*np.multiply(sin_half_theta_squared_over_N_plus_squared,-np.multiply(A,lambda_plus-1)-A+1)))
    e_p_X_X=1.-2.*np.multiply(np.multiply(sin_half_theta_squared_over_N_plus_squared,1.-A),lambda_plus)
    e_p_X_rho=np.multiply(np.multiply(sin_theta_over_N_plus,  v*(1-meshed_f)   ),  -sqrt_lambda_plus)
    e_p_X_phi=0*meshed_f_r
    
    
    e_p_rho_t=np.multiply(sin_theta_over_N_plus,np.multiply(1./sqrt_lambda_plus,-np.multiply(A,lambda_plus-1.)-A+1))
    e_p_rho_X=np.multiply(np.multiply(sin_theta_over_N_plus,v*(1-meshed_f)),sqrt_lambda_plus)
    e_p_rho_rho=meshed_cos_theta
    e_p_rho_phi=0*meshed_f_r
    
    
    # to compute the real values of rho (coordinate)
    real_rho_values=np.multiply(np.sin(theta_real_mesh),r_mesh)

    
    e_p_phi_t=0*meshed_f_r
    e_p_phi_X=0*meshed_f_r
    e_p_phi_rho=0*meshed_f_r
    e_p_phi_phi=real_rho_values
    ###############################################
    return e_p_t_t,\
        e_p_t_X,\
        e_p_t_rho,\
        e_p_t_phi,\
        e_p_X_t,\
        e_p_X_X,\
        e_p_X_rho,\
        e_p_X_phi,\
        e_p_rho_t,\
        e_p_rho_X,\
        e_p_rho_rho,\
        e_p_rho_phi,\
        e_p_phi_t,\
        e_p_phi_X,\
        e_p_phi_rho,\
        e_p_phi_phi,\
        real_rho_values
        
#######################################################################
def compute_composites_warp_v__meshed_f__and_theta(v,meshed_f,meshed_sin_half_theta_squared,meshed_sin_theta):

                
    A=1-v**2*(1-meshed_f)**2
    #########################################
    #ci dessous on écrit e'_t les 4 composantes d'un vecteur de genre temps (selon lequel sera choisi la vitesse du fluide probablement)

    lambda_plus= (-A+1+np.sqrt((A-1)**2+4))/2.
    sqrt_lambda_plus=np.sqrt(lambda_plus)
    lambda_minus= (-A+1-np.sqrt((A-1)**2+4))/2.
    sqrt_lambda_minus=np.sqrt(-lambda_minus)

    N_plus_squared=(lambda_plus-1)**2+v**2*(1-meshed_f)**2
    N_plus=np.sqrt(N_plus_squared)

    #important part of the function below 
    sin_half_theta_squared_over_N_plus_squared=np.multiply(1./N_plus_squared,meshed_sin_half_theta_squared)
    sin_theta_over_N_plus=np.multiply(1./N_plus,meshed_sin_theta)
    return A, lambda_plus,sqrt_lambda_plus,sqrt_lambda_minus,N_plus_squared,N_plus,sin_half_theta_squared_over_N_plus_squared,sin_theta_over_N_plus


####################################################################
def compute_doublycov_doublycontra_metrics_comp(ones_theta_and_r,v,meshed_f,A,real_rho_values):

    #doubly contravariant below
    g_n_t_t=-1.0*ones_theta_and_r
    g_n_t_X=v*(1.0-meshed_f)
    g_n_X_X=A
    g_n_rho_rho=ones_theta_and_r


    g_n_phi_phi=compute_inv_or_zero___vfunc(real_rho_values)**2
        
    #doubly covariant below
    g_v_t_t=-A
    g_v_t_X=v*(1-meshed_f)
    g_v_X_X=ones_theta_and_r
    g_v_rho_rho=ones_theta_and_r

    g_v_phi_phi=real_rho_values**2

    return g_n_t_t,\
        g_n_t_X,\
        g_n_X_X,\
        g_n_rho_rho,\
        g_n_phi_phi,\
        g_v_t_t,\
        g_v_t_X,\
        g_v_X_X,\
        g_v_rho_rho,\
        g_v_phi_phi
####################################################################
def compute_einstein_tensor_components_doublyCov(meshed_f_r_over_r__ones_theta_real,\
                                                 sin_theta_real_squared_dot__f_rr_minus_f_r_over_r,\
                                                 meshed_sin_squared_theta_real,\
                                                 meshed_f_r__ones_theta_real,\
                                                 v,\
                                                 meshed_f__ones_theta_real,\
                                                 meshed_cos_theta_real_times_meshed_sin_theta_real,\
                                                 meshed_f_rr__ones_theta_real,\
                                                 v_t,\
                                                 meshed_sin_theta_real,\
                                                 meshed_cos_theta_real,\
                                                 cos_theta_real_squared_dot__f_rr_minus_f_r_over_r,\
                                                 real_rho_values,\
):
    ###################""

                    
    two_f_r_over_r__plus_sin_squared_theta_real__dot_f_rr_minus_f_r_over_r=2*meshed_f_r_over_r__ones_theta_real+sin_theta_real_squared_dot__f_rr_minus_f_r_over_r
    
    G__t_t=-1.0/4.0*np.multiply(np.multiply(meshed_sin_squared_theta_real,meshed_f_r__ones_theta_real**2),v**2+3*v**4*np.multiply(1-meshed_f__ones_theta_real,1-meshed_f__ones_theta_real))+\
        v**2*np.multiply(1-meshed_f__ones_theta_real,two_f_r_over_r__plus_sin_squared_theta_real__dot_f_rr_minus_f_r_over_r)
    
    G__t_x=1.0/2.0*v*two_f_r_over_r__plus_sin_squared_theta_real__dot_f_rr_minus_f_r_over_r-3.0/4.0*v**3*np.multiply(1-meshed_f__ones_theta_real,\
                                                                                                                     np.multiply(meshed_sin_squared_theta_real,meshed_f_r__ones_theta_real))
    
    G__x_x=-3.0/4.0*v**2*np.multiply(meshed_sin_squared_theta_real,meshed_f_r__ones_theta_real)
    
    G__t_rho=np.multiply(meshed_cos_theta_real_times_meshed_sin_theta_real,\
                         np.multiply(np.multiply(meshed_f_r__ones_theta_real,meshed_f_r__ones_theta_real)*v**3,1-meshed_f__ones_theta_real)+\
                         (-1/2.0*v)*np.multiply(meshed_f_rr__ones_theta_real-meshed_f_r_over_r__ones_theta_real,1+v**2*(1-meshed_f__ones_theta_real)**2))+\
                         1/2.0*v*v_t*np.multiply(np.multiply(meshed_sin_theta_real,meshed_f_r__ones_theta_real),1-meshed_f__ones_theta_real)
    G__x_rho=np.multiply(meshed_cos_theta_real_times_meshed_sin_theta_real,\
                        np.multiply(meshed_f_r__ones_theta_real,meshed_f_r__ones_theta_real)*v**2+\
                        (-1/2.0)*np.multiply(meshed_f_rr__ones_theta_real-meshed_f_r_over_r__ones_theta_real,v**2*(1-meshed_f__ones_theta_real)**2))+\
                        1/2.0*v_t*np.multiply(meshed_sin_theta_real,meshed_f_r__ones_theta_real)
    G__rho_rho=np.multiply((-1/4.-3/4.*np.multiply(meshed_cos_theta_real,meshed_cos_theta_real))*v**2,np.multiply(meshed_f_r__ones_theta_real,meshed_f_r__ones_theta_real))+\
               v**2*np.multiply(1-meshed_f__ones_theta_real,meshed_f_r_over_r__ones_theta_real+cos_theta_real_squared_dot__f_rr_minus_f_r_over_r)\
               -v_t*np.multiply(meshed_cos_theta_real,meshed_f_r__ones_theta_real)
    G__phi_phi=np.multiply(real_rho_values**2,G__rho_rho)
    return  G__t_t,\
        G__t_x,\
        G__x_x,\
        G__t_rho,\
        G__x_rho,\
        G__rho_rho,\
        G__phi_phi
#############################################################################################
def choose_filename_saving(prefix,suffix):


    mypath='/home/wilwil/Documents/2016-09-18-important-folders/to-the-stars-or-not/2018-08-20--three--fluids_NEC'
    #Fuurai no Shiren GB2 - Going to Tabigarasu
    
    good_numbers = [int(f.split(prefix)[1].split(suffix)[0]) for f in listdir(mypath) if isfile(join(mypath, f)) and prefix in f]

    index=1
    while index in good_numbers:
        index+=1
    saving_filename= prefix+index.__str__()+suffix
        
    return saving_filename


#################################################################################################
################################################################################################
def two_fluids___rotation_euc_hyp__g_v__choice_tW_matrix___OUTPUT_GAMMAS_and_relations(hyp_2_flu3_x_t,\
                                            hyp_2_flu3_x_x,\
                                            hyp_2_flu3_rho_rho,\
                                            hyp_2_flu3_t_t,\
                                            hyp_2_flu3_cstt,\
                                            hyp_2_flu3_phi_phi,\
                                            hyp_flu3_x_t,\
                                            hyp_flu3_x_x,\
                                            hyp_flu3_rho_rho,\
                                            hyp_flu3_t_t,\
                                            hyp_flu3_cstt,\
                                            hyp_flu3_phi_phi,\
                                            euc_flu3_x_t,\
                                            euc_flu3_x_x,\
                                            euc_flu3_rho_rho,\
                                            euc_flu3_t_t,\
                                            euc_flu3_cstt,\
                                            euc_flu3_phi_phi,\
                                            inputs,\
):

    gamma_2=inputs["g_v_t_t"]*hyp_2_flu3_t_t\
           +inputs["g_v_t_X"]*hyp_2_flu3_x_t\
           +inputs["g_v_X_X"]*hyp_2_flu3_x_x\
           +inputs["g_v_rho_rho"]*hyp_2_flu3_rho_rho\
           +inputs["g_v_phi_phi"]*hyp_2_flu3_phi_phi\
           +hyp_2_flu3_cstt
    


    
    gamma=inputs["g_v_t_t"]*hyp_flu3_t_t\
           +inputs["g_v_t_X"]*hyp_flu3_x_t\
           +inputs["g_v_X_X"]*hyp_flu3_x_x\
           +inputs["g_v_rho_rho"]*hyp_flu3_rho_rho\
           +inputs["g_v_phi_phi"]*hyp_flu3_phi_phi\
           +hyp_flu3_cstt

    
    

    np_cosh_gamma=np.cosh(gamma)
    np_cosh_gamma_2=np.cosh(gamma_2)

    iter_overflow=0
    if np.isinf(np_cosh_gamma):
        if np.isinf(np.cosh(gamma)):
            iter_overflow+=1
            gamma=gamma/100.
    
    if np.isinf(np_cosh_gamma_2):
        if np.isinf(np.cosh(gamma_2)):
            iter_overflow+=1
            gamma_2=gamma_2/100.


    gamma_max=np.max([gamma,gamma_2])

    relation=np.cosh(gamma_max)**2-np.sinh(gamma_max)**2-1
    return gamma_max,relation,iter_overflow

####################################################################

###############################################################################################################
################################################################################################
def two_fluids___rotation_euc_hyp__g_v__choice_tW_matrix(hyp_2_flu3_x_t,\
                                            hyp_2_flu3_x_x,\
                                            hyp_2_flu3_rho_rho,\
                                            hyp_2_flu3_t_t,\
                                            hyp_2_flu3_cstt,\
                                            hyp_2_flu3_phi_phi,\
                                            hyp_flu3_x_t,\
                                            hyp_flu3_x_x,\
                                            hyp_flu3_rho_rho,\
                                            hyp_flu3_t_t,\
                                            hyp_flu3_cstt,\
                                            hyp_flu3_phi_phi,\
                                            euc_flu3_x_t,\
                                            euc_flu3_x_x,\
                                            euc_flu3_rho_rho,\
                                            euc_flu3_t_t,\
                                            euc_flu3_cstt,\
                                            euc_flu3_phi_phi,\
                                            inputs,\
):

    gamma_2=inputs["g_v_t_t"]*hyp_2_flu3_t_t\
           +inputs["g_v_t_X"]*hyp_2_flu3_x_t\
           +inputs["g_v_X_X"]*hyp_2_flu3_x_x\
           +inputs["g_v_rho_rho"]*hyp_2_flu3_rho_rho\
           +inputs["g_v_phi_phi"]*hyp_2_flu3_phi_phi\
           +hyp_2_flu3_cstt
    


    
    gamma=inputs["g_v_t_t"]*hyp_flu3_t_t\
           +inputs["g_v_t_X"]*hyp_flu3_x_t\
           +inputs["g_v_X_X"]*hyp_flu3_x_x\
           +inputs["g_v_rho_rho"]*hyp_flu3_rho_rho\
           +inputs["g_v_phi_phi"]*hyp_flu3_phi_phi\
           +hyp_flu3_cstt

    
    
    theta=inputs["g_v_t_t"]*euc_flu3_t_t\
           +inputs["g_v_t_X"]*euc_flu3_x_t\
           +inputs["g_v_X_X"]*euc_flu3_x_x\
           +inputs["g_v_rho_rho"]*euc_flu3_rho_rho\
           +inputs["g_v_phi_phi"]*euc_flu3_phi_phi\
           +euc_flu3_cstt

    
    
    ############ 
    sqrt_lambda_minus=inputs["sqrt_lambda_minus"]
    sqrt_lambda_plus=inputs["sqrt_lambda_plus"]
    ############""


    np_cosh_gamma=np.cosh(gamma)
    np_cosh_gamma_2=np.cosh(gamma_2)

    iter_overflow=0
    if np.isinf(np_cosh_gamma):
        if np.isinf(np.cosh(gamma)):
            iter_overflow+=1
            gamma=gamma/100.
    
    if np.isinf(np_cosh_gamma_2):
        if np.isinf(np.cosh(gamma_2)):
            iter_overflow+=1
            gamma_2=gamma_2/100.



    tW_4_after_2boost__1rot=two_fluids____compute_matrices_rotation_euc_hyp__matrices(sqrt_lambda_plus,sqrt_lambda_minus,gamma_2,gamma,theta,inputs=inputs)
    
    if np.any(np.isinf(tW_4_after_2boost__1rot.flatten())):
        if np.any(np.isinf(tW_4_after_2boost__1rot.flatten())):
            iter_overflow+=1
            gamma_2=gamma_2/100.
            gamma=gamma_2/100.
            tW_4_after_2boost__1rot=two_fluids____compute_matrices_rotation_euc_hyp__matrices(sqrt_lambda_plus,sqrt_lambda_minus,gamma_2,gamma,theta)
            
    return tW_4_after_2boost__1rot,iter_overflow

####################################################################

##############################################################################################################
def two_fluids____compute_matrices_rotation_euc_hyp__matrices(sqrt_lambda_plus,sqrt_lambda_minus,gamma_2,gamma,theta,inputs=[]):

    ################"
    ##########################"
    
    tW_4_boost=np.array([[1,0                                  ,0                                  ,0],\
                         [0,np.cosh(gamma)                     ,np.sinh(gamma)*1./sqrt_lambda_minus,0],\
                         [0,np.sinh(gamma)*1.*sqrt_lambda_minus,np.cosh(gamma)                     ,0],\
                         [0,0                                  ,0                                  ,1],\
    ])

    #special for two fluids (at first) below
    tW_4_boost_2=np.array([[np.cosh(gamma_2)                                      ,     np.sinh(gamma_2)*1.*sqrt_lambda_minus/sqrt_lambda_plus    ,0                                  ,0],\
                           [np.sinh(gamma_2)*1.*sqrt_lambda_plus/sqrt_lambda_minus,     np.cosh(gamma_2)                                          ,0                                  ,0],\
                           [0                                                     ,     0                                                         ,1                                  ,0],\
                           [0                                                     ,     0                                                         ,0                                  ,1],\
    ])


    


    tW_4_rotat=np.array([[np.cos(theta),0,(np.sin(theta))*1./sqrt_lambda_plus,0],\
                         [0,1,0,0],\
                         [-(np.sin(theta))*sqrt_lambda_plus,0,(np.cos(theta)),0],\
                         [0,0,0,1],\
    ])



    tW_4_fluid=np.dot(tW_4_rotat,tW_4_boost)

    tW_4_after_2boost__1rot=np.dot(tW_4_fluid,tW_4_boost_2)
    return tW_4_after_2boost__1rot
################################################################################
################################################################################
################################################################################
def three_fluids___rotation_euc_hyp__g_v__choice_tW_matrix__delta_error(hyp_flu3_x_t,\
                                            hyp_flu3_x_x,\
                                            hyp_flu3_rho_rho,\
                                            hyp_flu3_t_t,\
                                            hyp_flu3_cstt,\
                                            hyp_flu3_phi_phi,\
                                            euc_flu3_x_t,\
                                            euc_flu3_x_x,\
                                            euc_flu3_rho_rho,\
                                            euc_flu3_t_t,\
                                            euc_flu3_cstt,\
                                            euc_flu3_phi_phi,\
                                            inputs,\
):

    
                                            
    gamma=inputs["g_v_t_t"]*hyp_flu3_t_t\
           +inputs["g_v_t_X"]*hyp_flu3_x_t\
           +inputs["g_v_X_X"]*hyp_flu3_x_x\
           +inputs["g_v_rho_rho"]*hyp_flu3_rho_rho\
           +inputs["g_v_phi_phi"]*hyp_flu3_phi_phi\
           +hyp_flu3_cstt

    
    
    theta=inputs["g_v_t_t"]*euc_flu3_t_t\
           +inputs["g_v_t_X"]*euc_flu3_x_t\
           +inputs["g_v_X_X"]*euc_flu3_x_x\
           +inputs["g_v_rho_rho"]*euc_flu3_rho_rho\
           +inputs["g_v_phi_phi"]*euc_flu3_phi_phi\
           +euc_flu3_cstt

    
    
    ############ 
    sqrt_lambda_minus=inputs["sqrt_lambda_minus"]
    sqrt_lambda_plus=inputs["sqrt_lambda_plus"]
    ############""


    np_cosh_gamma=np.cosh(gamma)
    iter_overflow=0
    if np.isinf(np_cosh_gamma):
        if  np.isinf(np.cosh(gamma)):
            iter_overflow+=1
            gamma=gamma/100.
    

    #below we compute the delta

    tW_4_boost=np.array([[1,0                                  ,0                                  ,0],\
                         [0,np.exp(-np.abs(gamma))/2.0                     ,(-1.0)*np.exp(-np.abs(gamma))/2.0*1./sqrt_lambda_minus,0],\
                         [0,(-1.0)*np.exp(-np.abs(gamma))/2.0*1.*sqrt_lambda_minus,np.exp(-np.abs(gamma))/2.0                     ,0],\
                         [0,0                                  ,0                                  ,1],\
    ])



    


    tW_4_rotat=np.array([[np.cos(theta),0,(np.sin(theta))*1./sqrt_lambda_plus,0],\
                         [0,1,0,0],\
                         [-(np.sin(theta))*sqrt_lambda_plus,0,(np.cos(theta)),0],\
                         [0,0,0,1],\
    ])



    tW_4_fluid=np.dot(tW_4_rotat,tW_4_boost)

    return tW_4_fluid,iter_overflow

################################################################################################
def three_fluids___rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu3_x_t,\
                                            hyp_flu3_x_x,\
                                            hyp_flu3_rho_rho,\
                                            hyp_flu3_t_t,\
                                            hyp_flu3_cstt,\
                                            hyp_flu3_phi_phi,\
                                            euc_flu3_x_t,\
                                            euc_flu3_x_x,\
                                            euc_flu3_rho_rho,\
                                            euc_flu3_t_t,\
                                            euc_flu3_cstt,\
                                            euc_flu3_phi_phi,\
                                            inputs,\
):

    
                                            
    gamma=inputs["g_v_t_t"]*hyp_flu3_t_t\
           +inputs["g_v_t_X"]*hyp_flu3_x_t\
           +inputs["g_v_X_X"]*hyp_flu3_x_x\
           +inputs["g_v_rho_rho"]*hyp_flu3_rho_rho\
           +inputs["g_v_phi_phi"]*hyp_flu3_phi_phi\
           +hyp_flu3_cstt

    
    
    theta=inputs["g_v_t_t"]*euc_flu3_t_t\
           +inputs["g_v_t_X"]*euc_flu3_x_t\
           +inputs["g_v_X_X"]*euc_flu3_x_x\
           +inputs["g_v_rho_rho"]*euc_flu3_rho_rho\
           +inputs["g_v_phi_phi"]*euc_flu3_phi_phi\
           +euc_flu3_cstt

    
    
    ############ 
    sqrt_lambda_minus=inputs["sqrt_lambda_minus"]
    sqrt_lambda_plus=inputs["sqrt_lambda_plus"]
    ############""


    np_cosh_gamma=np.cosh(gamma)
    iter_overflow=0
    if np.isinf(np_cosh_gamma):
        if  np.isinf(np.cosh(gamma)):
            iter_overflow+=1
            gamma=gamma/100.
    
    
    tW_4_boost=np.array([[1,0                                  ,0                                  ,0],\
                         [0,np.cosh(gamma)                     ,np.sinh(gamma)*1./sqrt_lambda_minus,0],\
                         [0,np.sinh(gamma)*1.*sqrt_lambda_minus,np.cosh(gamma)                     ,0],\
                         [0,0                                  ,0                                  ,1],\
    ])

    tW_4_boost_no_curv=np.array([[1,0                                  ,0                                  ,0],\
                                 [0,np.cosh(gamma)                     ,np.sinh(gamma),0],\
                                 [0,np.sinh(gamma)*1.,np.cosh(gamma)                     ,0],\
                                 [0,0                                  ,0                                  ,1],\
    ])


    


    tW_4_rotat=np.array([[np.cos(theta),0,(np.sin(theta))*1./sqrt_lambda_plus,0],\
                         [0,1,0,0],\
                         [-(np.sin(theta))*sqrt_lambda_plus,0,(np.cos(theta)),0],\
                         [0,0,0,1],\
    ])


    tW_4_rotat_no_curv=np.array([[np.cos(theta),0,(np.sin(theta))*1.,0],\
                                 [0,1,0,0],\
                                 [-(np.sin(theta)),0,(np.cos(theta)),0],\
                                 [0,0,0,1],\
    ])


    tW_4_fluid=np.dot(tW_4_rotat,tW_4_boost)

    return tW_4_fluid,tW_4_rotat_no_curv,tW_4_boost_no_curv,iter_overflow

####################################################################
################################################################################################
def three_fluids___rotation_euc_hyp__g_v__choice_tW_matrix___OUTPUT_GAMMAS_and_relations(hyp_flu3_x_t,\
                                            hyp_flu3_x_x,\
                                            hyp_flu3_rho_rho,\
                                            hyp_flu3_t_t,\
                                            hyp_flu3_cstt,\
                                            hyp_flu3_phi_phi,\
                                            euc_flu3_x_t,\
                                            euc_flu3_x_x,\
                                            euc_flu3_rho_rho,\
                                            euc_flu3_t_t,\
                                            euc_flu3_cstt,\
                                            euc_flu3_phi_phi,\
                                            inputs,\
):

    
                                            
    gamma=inputs["g_v_t_t"]*hyp_flu3_t_t\
           +inputs["g_v_t_X"]*hyp_flu3_x_t\
           +inputs["g_v_X_X"]*hyp_flu3_x_x\
           +inputs["g_v_rho_rho"]*hyp_flu3_rho_rho\
           +inputs["g_v_phi_phi"]*hyp_flu3_phi_phi\
           +hyp_flu3_cstt

    
    
    np_cosh_gamma=np.cosh(gamma)
    iter_overflow=0
    if np.isinf(np_cosh_gamma):
        if  np.isinf(np.cosh(gamma)):
            iter_overflow+=1
            gamma=gamma/100.

    relation=np.cosh(gamma)**2-np.sinh(gamma)**2-1

    return gamma,relation,iter_overflow

####################################################################

#############################################################################################################
def extract_right_dicts_for_analysis(list_of_old_dict_res,v,v_t,sigma=8):



    exact_v_dict_mean=[dict_b for dict_b in list_of_old_dict_res if 'warp_speed' in dict_b and 'metric-fun' in dict_b \
                       and dict_b['warp_acceleration']==v_t and dict_b['sigma']==sigma and 'rank' in dict_b and "LAMBDA_into_account" in dict_b and dict_b['warp_speed']==v \
    ]


    return exact_v_dict_mean
#####################################################################
def  plot_warpspeed_against_NEC_violation(list_of_old_dict_res):
    #############
    v_list=[]
    v_t_list=[]
    fun_min=[]
    LAMBDA_into_account=[]
    for dict_b in list_of_old_dict_res:
        for key in dict_b.keys():

            
            if "warp" in key.lower() or "fun" in key.lower(): 
                print(key+'   :   '+str(dict_b[key]))
        if 'LAMBDA_into_account' in dict_b.keys() and 'timestamp' in dict_b.keys() and 'sigma' in dict_b.keys():
            if dict_b['sigma']==8 and  dict_b['warp_acceleration']==1.1e-16:
        
                v_list.append(dict_b['warp_speed'])

                v_t_list.append(dict_b['warp_acceleration'])
                fun_min.append(dict_b['fun'])
                LAMBDA_into_account.append(dict_b['LAMBDA_into_account'])

    data = {'v': v_list, 'v_t': v_t_list,'fun_min':fun_min,\
            "LAMBDA_into_account": LAMBDA_into_account,\
    }
    df=pd.DataFrame.from_dict(data)
    new_series=df.groupby('v')['fun_min'].min()
    new_df=pd.DataFrame(new_series)
    

    plt.figure()

    new_df.plot(logx=True,logy=True)
    plt.show()
    return

##########################################################################
def filter_out_useless(list_of_old_dict_res2):
    #check the key of last saved data
    # keep only data wth the same keys (not less)
    
    keys=list_of_old_dict_res2[0].keys()
    output_list_of_old_dict=[list_of_old_dict_res2[a] for a in range(len(list_of_old_dict_res2)) if 'warp_speed' in list_of_old_dict_res2[a].keys() and \
                             'warp_acceleration' in list_of_old_dict_res2[a].keys() and \
                             'LAMBDA_into_account' in list_of_old_dict_res2[a].keys() and \
                             'timestamp' in list_of_old_dict_res2[a].keys() and \
                             'sigma' in list_of_old_dict_res2[a].keys() and \
                             'rank' in list_of_old_dict_res2[a].keys() and \
                             'metric-fun' in  list_of_old_dict_res2[a].keys()]
    return output_list_of_old_dict

###############
#######################################################################################
def seek_for_the_correct_adresses_of_files(cwd=''):
    #we search for a specific arborescence
    list_of_dir__bits=['/three-fluids-accel-data',\
                       '/two-fluids-accel-data',\
                       '/two-fluids-decel-data',\
                       '/three-fluids-decel-data',\
    ]
    list_of_absolute_files_adresses=[]
    for dir__bit in list_of_dir__bits:

        list_of_files=os.listdir(cwd+dir__bit)
        list_of_files__param_warpdrive_=[el for el in list_of_files if "param_warpdrive_" in el]
        for el in list_of_files__param_warpdrive_:
            
            list_of_absolute_files_adresses.append(cwd+dir__bit+'/'+el)
    return list_of_absolute_files_adresses
###################################################################################
##############################################################################
def extract_the_various_parameters(list_of_absolute_files_adresses,threshold_timestamp=''):

    complete_dataframe=[]
    complete_list_of_dicts=[]
    iteration=0
    print("concatenate and drop duplicates...")
    for el in list_of_absolute_files_adresses:
        iteration+=1
        print(str(iteration)+'//'+str(len(list_of_absolute_files_adresses)))
        loading_filename=el

        #we read each data file
        with open(loading_filename, 'r') as fd:
            list_of_old_dict_res=json.load(fd)

        #first filtering step
        list_of_old_dict_res__filtered=filter_out_useless(list_of_old_dict_res)

        #second filtering step
        if len(threshold_timestamp)!=0:
            list_of_old_dict_res__filtered2=[ el for el in list_of_old_dict_res__filtered if el["timestamp"]>=threshold_timestamp]
        else:
            list_of_old_dict_res__filtered2=list_of_old_dict_res__filtered

        #we collect dicts that could be useful
        complete_list_of_dicts+=list_of_old_dict_res__filtered2
        #########################################################
        #### first we need to extract a dataframe with at least the following keys

        # and then remove the dulicates
        the_keys=['fun','nfev','warp_speed', 'warp_acceleration', 'LAMBDA_into_account', 'timestamp', 'sigma', 'rank', 'metric-fun']

        dict_for_creating_a_dataframe={}
        for key in the_keys:
            dict_for_creating_a_dataframe[key]=[list_of_old_dict_res__filtered2[a][key] for a in range(len(list_of_old_dict_res__filtered2))]
            
        df_partial=pd.DataFrame(dict_for_creating_a_dataframe)

        if isinstance(complete_dataframe,list):
            complete_dataframe=df_partial
        elif isinstance(complete_dataframe,pd.DataFrame):
            complete_dataframe=complete_dataframe.append(df_partial)
            complete_dataframe.drop_duplicates(inplace=True)
        else:
            print("isinstance(complete_dataframe,pd.DataFrame): not true")
            print("isinstance(complete_dataframe,list): not true")
            print('line 10486')
            raise
    
        
        



    # in the end we just want the dictionaries purged
    complete_dict_of_dicts=complete_dataframe.to_dict('index')
    complete_dict_of_dicts_keys_index=complete_dict_of_dicts.keys()

    #large (too large )list of complete dicts -> that will be restrained below
    complete_list_of_dicts__for_a_few_keys=[{key:di[key] for key in the_keys } for di in complete_list_of_dicts ]

    the_final_list=[]
    iterat=0
    for index in complete_dict_of_dicts_keys_index:
        iterat+=1
        if iterat%10==0:
            print(str(iterat)+'//'+str(len(complete_dict_of_dicts_keys_index)))

        #too small dict in term of keys
        in_question_dict=complete_dict_of_dicts[index]

        #we search for perfect match in term of a small number of keys
        the_list_where_identity_is_found=[in_question_dict==restr_dict for restr_dict in  complete_list_of_dicts__for_a_few_keys]

        #where there is a match we retrieve the 1st case of match
        argmax_ind=np.argmax(the_list_where_identity_is_found)

        #we collect the full result
        the_final_list.append(complete_list_of_dicts[argmax_ind])
        the_interesting_dict=complete_list_of_dicts[argmax_ind]
        the_new_interesting_dict={key:the_interesting_dict[key] for key in the_keys }
    return the_final_list


###########################################################################################################################
def extract_components_from__vector_basis(e_prime_cov):

    ###################################
    e_pr_cov_t_t=e_prime_cov[0,0]
    e_pr_cov_t_X=e_prime_cov[1,0]
    e_pr_cov_t_rho=e_prime_cov[2,0]
    e_pr_cov_t_phi=e_prime_cov[3,0]

    e_pr_cov_X_t=e_prime_cov[0,1]
    e_pr_cov_X_X=e_prime_cov[1,1]
    e_pr_cov_X_rho=e_prime_cov[2,1]
    e_pr_cov_X_phi=e_prime_cov[3,1]

    e_pr_cov_rho_t=e_prime_cov[0,2]
    e_pr_cov_rho_X=e_prime_cov[1,2]
    e_pr_cov_rho_rho=e_prime_cov[2,2]
    e_pr_cov_rho_phi=e_prime_cov[3,2]

    e_pr_cov_phi_t=e_prime_cov[0,3]
    e_pr_cov_phi_X=e_prime_cov[1,3]
    e_pr_cov_phi_rho=e_prime_cov[2,3]
    e_pr_cov_phi_phi=e_prime_cov[3,3]

    return e_pr_cov_t_t,\
        e_pr_cov_t_X,\
        e_pr_cov_t_rho,\
        e_pr_cov_t_phi,\
        e_pr_cov_X_t,\
        e_pr_cov_X_X,\
        e_pr_cov_X_rho,\
        e_pr_cov_X_phi,\
        e_pr_cov_rho_t,\
        e_pr_cov_rho_X,\
        e_pr_cov_rho_rho,\
        e_pr_cov_rho_phi,\
        e_pr_cov_phi_t,\
        e_pr_cov_phi_X,\
        e_pr_cov_phi_rho,\
        e_pr_cov_phi_phi
################################################################################################################################

def two_fluids__EOS_NEC_RANK___myfunc(g_n_t__t_jX,\
                                                       g_n__rho2_jX2,\
                                                       g__v_jn__phiphi,\
                                                       g_v_t__t_jX,\
                                                       g_v__rho2_jX2,\
                                                       meshed__cos_jsin__theta,\
                                                       sqrt_lambda__pl_jmin,\
                                                       G__t_t_jA,\
                                                       G__t__x_jrho,\
                                                       G__x__x_jrho,\
                                                       G_x__rho2_jphi2,\
                                                       hyp_coef_flu1_x__x_jt,\
                                                       hyp_coef_flu1__tt_jrhorho,\
                                                       hyp_coef_flu1__phiphi_jcstt,\
                                                       euc_coef_flu1_x__x_jt,\
                                                       euc_coef_flu1__tt_jrhorho,\
                                                       euc_coef_flu1__phiphi_jcstt,\
                                                       hyp_coef_flu2_x__x_jt,\
                                                       hyp_coef_flu2__tt_jrhorho,\
                                                       hyp_coef_flu2__phiphi_jcstt,\
                                                       euc_coef_flu2_x__x_jt,\
                                                       euc_coef_flu2__tt_jrhorho,\
                                                       euc_coef_flu2__phiphi_jcstt,\
                                                       hyp_coef_flu3_x__x_jt,\
                                                       hyp_coef_flu3__tt_jrhorho,\
                                                       hyp_coef_flu3__phiphi_jcstt,\
                                                       euc_coef_flu3_x__x_jt,\
                                                       euc_coef_flu3__tt_jrhorho,\
                                                       euc_coef_flu3__phiphi_jcstt,\
                                                       res_batch__1j_LAMBDA,\
):


    

        do_plot=False

        #1
        g_n_t_X=g_n_t__t_jX.imag
        g_n_t_t=g_n_t__t_jX.real

        #2
        g_n_X_X=g_n__rho2_jX2.imag
        g_n_rho_rho=g_n__rho2_jX2.real


        #3
        g_n_phi_phi=g__v_jn__phiphi.imag
        g_v_phi_phi=g__v_jn__phiphi.real



        #4
        g_v_t_X=g_v_t__t_jX.imag
        g_v_t_t=g_v_t__t_jX.real

        #5
        g_v_X_X=g_v__rho2_jX2.imag
        g_v_rho_rho=g_v__rho2_jX2.real

        #5
        meshed_sin_theta=meshed__cos_jsin__theta.imag
        meshed_cos_theta=meshed__cos_jsin__theta.real

        #6
        sqrt_lambda_minus=sqrt_lambda__pl_jmin.imag
        sqrt_lambda_plus=sqrt_lambda__pl_jmin.real

        #7
        A=G__t_t_jA.imag
        G__t_t=G__t_t_jA.real

        #8
        G__t_rho=G__t__x_jrho.imag
        G__t_x=G__t__x_jrho.real

        #9
        G__x_rho=G__x__x_jrho.imag
        G__x_x=G__x__x_jrho.real

        #10
        G__phi_phi=G_x__rho2_jphi2.imag
        G__rho_rho=G_x__rho2_jphi2.real

        # there are seven tt tx trho , xx xrho, rhorho, phiphi
        #11


        hyp_flu1_x_t=hyp_coef_flu1_x__x_jt.imag
        hyp_flu1_x_x=hyp_coef_flu1_x__x_jt.real

        #12
        hyp_flu1_rho_rho=hyp_coef_flu1__tt_jrhorho.imag
        hyp_flu1_t_t=hyp_coef_flu1__tt_jrhorho.real

        #13
        hyp_flu1_cstt=hyp_coef_flu1__phiphi_jcstt.imag
        hyp_flu1_phiphi=hyp_coef_flu1__phiphi_jcstt.real

        #14

        euc_flu1_x_t=euc_coef_flu1_x__x_jt.imag
        euc_flu1_x_x=euc_coef_flu1_x__x_jt.real


        euc_flu1_rho_rho=euc_coef_flu1__tt_jrhorho.imag
        euc_flu1_t_t=euc_coef_flu1__tt_jrhorho.real


        euc_flu1_cstt=euc_coef_flu1__phiphi_jcstt.imag
        euc_flu1_phiphi=euc_coef_flu1__phiphi_jcstt.real
        ################################
        hyp_flu2_x_t=hyp_coef_flu2_x__x_jt.imag
        hyp_flu2_x_x=hyp_coef_flu2_x__x_jt.real


        hyp_flu2_rho_rho=hyp_coef_flu2__tt_jrhorho.imag
        hyp_flu2_t_t=hyp_coef_flu2__tt_jrhorho.real


        hyp_flu2_cstt=hyp_coef_flu2__phiphi_jcstt.imag
        hyp_flu2_phiphi=hyp_coef_flu2__phiphi_jcstt.real



        euc_flu2_x_t=euc_coef_flu2_x__x_jt.imag
        euc_flu2_x_x=euc_coef_flu2_x__x_jt.real


        euc_flu2_rho_rho=euc_coef_flu2__tt_jrhorho.imag
        euc_flu2_t_t=euc_coef_flu2__tt_jrhorho.real


        euc_flu2_cstt=euc_coef_flu2__phiphi_jcstt.imag
        euc_flu2_phiphi=euc_coef_flu2__phiphi_jcstt.real

        ##################################
        hyp_flu3_x_t=hyp_coef_flu3_x__x_jt.imag
        hyp_flu3_x_x=hyp_coef_flu3_x__x_jt.real


        hyp_flu3_rho_rho=hyp_coef_flu3__tt_jrhorho.imag
        hyp_flu3_t_t=hyp_coef_flu3__tt_jrhorho.real


        hyp_flu3_cstt=hyp_coef_flu3__phiphi_jcstt.imag
        hyp_flu3_phiphi=hyp_coef_flu3__phiphi_jcstt.real



        euc_flu3_x_t=euc_coef_flu3_x__x_jt.imag
        euc_flu3_x_x=euc_coef_flu3_x__x_jt.real


        euc_flu3_rho_rho=euc_coef_flu3__tt_jrhorho.imag
        euc_flu3_t_t=euc_coef_flu3__tt_jrhorho.real


        euc_flu3_cstt=euc_coef_flu3__phiphi_jcstt.imag
        euc_flu3_phiphi=euc_coef_flu3__phiphi_jcstt.real





        #29
        res_batch=res_batch__1j_LAMBDA.real
        Lambda_cosmological_constant_=res_batch__1j_LAMBDA.imag








        #do_print_stuff=True
        do_print_stuff=False

        #do_print_stuff2=True
        do_print_stuff2=False


        do_print_stuff3=False
        inputs={}

        do_print_stuff4=False

        do_print_stuff5=False
        #do_print_stuff5=True
        ######################################################
        # correction on g_phi,phi
        #below
        if g_n_phi_phi==0 or np.isinf(g_n_phi_phi):
            g_n_phi_phi=1

        if g_v_phi_phi==0 or np.isinf(g_v_phi_phi):
            g_v_phi_phi=1

        if np.isnan(g_v_phi_phi) or np.isnan(g_n_phi_phi):
            g_n_phi_phi=1
            g_v_phi_phi=1



        inputs["g_n_t_t"]=g_n_t_t
        inputs["g_n_t_X"]=g_n_t_X
        inputs["g_n_X_X"]=g_n_X_X
        inputs["g_n_rho_rho"]=g_n_rho_rho
        inputs["g_n_phi_phi"]=g_n_phi_phi


        inputs["g_v_t_t"]=g_v_t_t
        inputs["g_v_t_X"]=g_v_t_X
        inputs["g_v_X_X"]=g_v_X_X
        inputs["g_v_rho_rho"]=g_v_rho_rho
        inputs["g_v_phi_phi"]=g_v_phi_phi

        inputs["sqrt_lambda_minus"]=sqrt_lambda_minus
        inputs["sqrt_lambda_plus"]=sqrt_lambda_plus



        ########################################################
        # we compute G_doubly covariant!!
        G_doubly_cov=np.array( [ [ G__t_t   , G__t_x   , G__t_rho   , 0 ],\
                                 [ G__t_x   , G__x_x   , G__x_rho   , 0 ],\
                                 [ G__t_rho , G__x_rho , G__rho_rho , 0 ],\
                                 [ 0        , 0        , 0          , G__phi_phi ],\
        ])
        #######################################################
        # below are the numpy equivalents of lines / rows

        g_n_mat=np.array([[inputs["g_n_t_t"],inputs["g_n_t_X"],0,0],\
                          [inputs["g_n_t_X"],inputs["g_n_X_X"],0,0],\
                          [0                ,0                ,inputs["g_n_rho_rho"],0],\
                          [0                ,0                ,0                    ,inputs["g_n_phi_phi"]]])

        g_v_mat=np.array([[inputs["g_v_t_t"],inputs["g_v_t_X"],0,0],\
                          [inputs["g_v_t_X"],inputs["g_v_X_X"],0,0],\
                          [0                ,0                ,inputs["g_v_rho_rho"],0],\
                          [0                ,0                ,0                    ,inputs["g_v_phi_phi"]]])



        lambda_plus=(-(A-1)+np.sqrt((A-1)**2+4))/2.
        lambda_minus=(-(A-1)-np.sqrt((A-1)**2+4))/2.
        tW=np.array([[meshed_cos_theta,0,meshed_sin_theta*1./sqrt_lambda_plus],\
                     [0,1,0],\
                     [-meshed_sin_theta*sqrt_lambda_plus,0,meshed_cos_theta],\
                     ])

        return_to_starting_block=True
        overflow_counting=0

        if 1:
            #print('choose tw4 ---> make a function which creates such matrices \n line 1640')



            tW_4,\
                iter_overflow=two_fluids___rotation_euc_hyp__g_v__choice_tW_matrix(euc_flu3_x_t,\
                                                                      euc_flu3_x_x,\
                                                                      euc_flu3_rho_rho,\
                                                                      euc_flu3_t_t,\
                                                                      euc_flu3_cstt,\
                                                                      euc_flu3_phiphi,\
                                                                      hyp_flu1_x_t,\
                                                                      hyp_flu1_x_x,\
                                                                      hyp_flu1_rho_rho,\
                                                                      hyp_flu1_t_t,\
                                                                      hyp_flu1_cstt,\
                                                                      hyp_flu1_phiphi,\
                                                                      euc_flu1_x_t,\
                                                                      euc_flu1_x_x,\
                                                                      euc_flu1_rho_rho,\
                                                                      euc_flu1_t_t,\
                                                                      euc_flu1_cstt,\
                                                                      euc_flu1_phiphi,\
                                                                      inputs,\
            )

            tW_4_second_fluid,\
                iter_overflow_second=two_fluids___rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu3_x_t,\
                                                                             hyp_flu3_x_x,\
                                                                             hyp_flu3_rho_rho,\
                                                                             hyp_flu3_t_t,\
                                                                             hyp_flu3_cstt,\
                                                                             hyp_flu3_phiphi,\
                                                                             hyp_flu2_x_t,\
                                                                             hyp_flu2_x_x,\
                                                                             hyp_flu2_rho_rho,\
                                                                             hyp_flu2_t_t,\
                                                                             hyp_flu2_cstt,\
                                                                             hyp_flu2_phiphi,\
                                                                             euc_flu2_x_t,\
                                                                             euc_flu2_x_x,\
                                                                             euc_flu2_rho_rho,\
                                                                             euc_flu2_t_t,\
                                                                             euc_flu2_cstt,\
                                                                             euc_flu2_phiphi,\
                                                                             inputs,\
            )


 





            #interesting line below!!
            ####    g_v_t_X=v*(1-meshed_f)

            N_plus=np.sqrt((lambda_plus-1)**2+g_v_t_X**2)
            N_minus=np.sqrt((lambda_minus-1)**2+g_v_t_X**2)

            #################################
            
            #eigen_vect_g_doublcovariant=EVGDCov
            #{EVGDCov=theta matrix} is the matrix of eigen vectors of g_{mu,nu}

            EVGDCov=np.array([[(lambda_plus-1.0)/N_plus,(lambda_minus-1.0)/N_minus,0,0],\
                             [g_v_t_X/(1.0*N_plus),g_v_t_X/(1.0*N_minus),0,0],\
                             [0,0,1.,0],\
                             [0,0,0,1.]])
            the_product_should_be_diagonal=np.dot(EVGDCov.T,np.dot(g_v_mat,EVGDCov))

            ##################################

            # the following is contravariant (each nested array is not a desirable vector rather this is component 1 of the first array and then comp 1 of the second, etc.
            e_not_prime=np.array([[-1,0,0,0],\
                                  [g_v_t_X,1,0,0],\
                                  [0,0,1,0],\
                                  [0,0,0,1/np.sqrt(g_v_phi_phi) ],\
            ])

            minkowski_metric=np.diag([-1,1,1,1])
            e_prime=np.dot(EVGDCov,np.dot(tW_4,np.dot(EVGDCov.T,e_not_prime)))
            e_prime_2nd=np.dot(EVGDCov,np.dot(tW_4_second_fluid,np.dot(EVGDCov.T,e_not_prime)))
            e_prime_2nd_cov=np.dot(g_v_mat,e_prime_2nd)# in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures



            # e_prime is described as sets of lines , each dissecting interesting vectors (vectors as columns)
            #e_prime here is contravariant
            #below we compute the covariant vectors in a single matrix


            e_prime_cov=np.dot(g_v_mat,e_prime)# in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures



            # we compute the mixed components of Einstein tensor
            G_einst_mixed_plus_lambda_delta=np.dot(g_n_mat,G_doubly_cov+Lambda_cosmological_constant_*g_v_mat )






            #############################################
 
            basis_vectors_2fluids=[[e_prime_cov,e_prime],[e_prime_2nd_cov,e_prime_2nd]]
            big_matrix_2flu,gee_vector_2flu,overflow_big_mat=compute_big_matrix_for_fluids(basis_vectors_2fluids,G_einst_mixed_plus_lambda_delta)



            if np.any(np.isnan(big_matrix_2flu)) or np.any(np.isinf(big_matrix_2flu)) or \
               np.any(np.isnan(gee_vector_2flu)) or np.any(np.isinf(gee_vector_2flu)):
                return_to_starting_block=True

                hyp_flu1_x_t=hyp_flu1_x_t/3.
                hyp_flu1_x_x=hyp_flu1_x_x/3.
                hyp_flu1_rho_rho=hyp_flu1_rho_rho/3.
                hyp_flu1_t_t=hyp_flu1_t_t/3.
                hyp_flu1_cstt=hyp_flu1_cstt/3.
                hyp_flu1_phiphi=hyp_flu1_phiphi/3.

                hyp_flu2_x_t=hyp_flu2_x_t/3.
                hyp_flu2_x_x=hyp_flu2_x_x/3.
                hyp_flu2_rho_rho=hyp_flu2_rho_rho/3.
                hyp_flu2_t_t=hyp_flu2_t_t/3.
                hyp_flu2_cstt=hyp_flu2_cstt/3.
                hyp_flu2_phiphi=hyp_flu2_phiphi/3.

                #this below is only for two fluids as the 3rd fluid parameters are actually for 2 fluids parameterization of the second boost
                hyp_flu3_x_t=hyp_flu3_x_t/3.
                hyp_flu3_x_x=hyp_flu3_x_x/3.
                hyp_flu3_rho_rho=hyp_flu3_rho_rho/3.
                hyp_flu3_t_t=hyp_flu3_t_t/3.
                hyp_flu3_cstt=hyp_flu3_cstt/3.
                hyp_flu3_phiphi=hyp_flu3_phiphi/3.

                euc_flu3_x_t=euc_flu3_x_t/3.
                euc_flu3_x_x=euc_flu3_x_x/3.
                euc_flu3_rho_rho=euc_flu3_rho_rho/3.
                euc_flu3_t_t=euc_flu3_t_t/3.
                euc_flu3_cstt=euc_flu3_cstt/3.
                euc_flu3_phiphi=euc_flu3_phiphi/3.


                overflow_counting+=2
                # above si inf ou nan on diminue la valeur de tous les coefficients
                densities_pressures_2fluids=[-11,-11,-11,-11,-11,-11,-11,-11]
                matrix_rank_2fluids=-1


            else:
                return_to_starting_block=False
                densities_pressures_2fluids,\
                    residuals_2fluids,\
                    matrix_rank_2fluids,\
                    sing_values_matrix_2fluids=np.linalg.lstsq(big_matrix_2flu, gee_vector_2flu,rcond=None)

        ##############################################################

        

        ##############################################################################################################################





        res=list(densities_pressures_2fluids)



        ######################
        # we compute equation of states below
        if res[0]!=0:
            w_1=(res[1]+res[2]+res[3])/(1.0*res[0])
        else:
            w_1=0

        if res[4]!=0:
            w_2=(res[5]+res[6]+res[7])/(1.0*res[4])
        else:
            w_2=0



        MW=np.max(np.abs([w_1,w_2])) # max of equation of states



        density_plus_pressure=[res[0]+res[1],res[0]+res[2],res[0]+res[3],\
                               res[4]+res[5],res[4]+res[6],res[4]+res[7],\
        ]


        min__density_plus_pressure=np.min(density_plus_pressure)
        if min__density_plus_pressure<0:
            pass
        else:
            min__density_plus_pressure=0

        
        return MW+1j*0,min__density_plus_pressure+1j*(matrix_rank_2fluids-8*(overflow_counting+iter_overflow_second+iter_overflow+overflow_big_mat))
#####################################################################################################################################################


#########################################################################################################################################

def two_fluids____element_wise__compute_p_rho__p_phi_pressures__output_them___myfunc(g_n_t__t_jX,\
                                                       g_n__rho2_jX2,\
                                                       g__v_jn__phiphi,\
                                                       g_v_t__t_jX,\
                                                       g_v__rho2_jX2,\
                                                       meshed__cos_jsin__theta,\
                                                       sqrt_lambda__pl_jmin,\
                                                       G__t_t_jA,\
                                                       G__t__x_jrho,\
                                                       G__x__x_jrho,\
                                                       G_x__rho2_jphi2,\
                                                       hyp_coef_flu1_x__x_jt,\
                                                       hyp_coef_flu1__tt_jrhorho,\
                                                       hyp_coef_flu1__phiphi_jcstt,\
                                                       euc_coef_flu1_x__x_jt,\
                                                       euc_coef_flu1__tt_jrhorho,\
                                                       euc_coef_flu1__phiphi_jcstt,\
                                                       hyp_coef_flu2_x__x_jt,\
                                                       hyp_coef_flu2__tt_jrhorho,\
                                                       hyp_coef_flu2__phiphi_jcstt,\
                                                       euc_coef_flu2_x__x_jt,\
                                                       euc_coef_flu2__tt_jrhorho,\
                                                       euc_coef_flu2__phiphi_jcstt,\
                                                       hyp_coef_flu3_x__x_jt,\
                                                       hyp_coef_flu3__tt_jrhorho,\
                                                       hyp_coef_flu3__phiphi_jcstt,\
                                                       euc_coef_flu3_x__x_jt,\
                                                       euc_coef_flu3__tt_jrhorho,\
                                                       euc_coef_flu3__phiphi_jcstt,\
):


    

        do_plot=False
        Lambda_cosmological_constant_=0
        #1.11e-52 #(m^(-2))

        #1
        g_n_t_X=g_n_t__t_jX.imag
        g_n_t_t=g_n_t__t_jX.real

        #2
        g_n_X_X=g_n__rho2_jX2.imag
        g_n_rho_rho=g_n__rho2_jX2.real


        #3
        g_n_phi_phi=g__v_jn__phiphi.imag
        g_v_phi_phi=g__v_jn__phiphi.real



        #4
        g_v_t_X=g_v_t__t_jX.imag
        g_v_t_t=g_v_t__t_jX.real

        #5
        g_v_X_X=g_v__rho2_jX2.imag
        g_v_rho_rho=g_v__rho2_jX2.real

        #5
        meshed_sin_theta=meshed__cos_jsin__theta.imag
        meshed_cos_theta=meshed__cos_jsin__theta.real

        #6
        sqrt_lambda_minus=sqrt_lambda__pl_jmin.imag
        sqrt_lambda_plus=sqrt_lambda__pl_jmin.real

        #7
        A=G__t_t_jA.imag
        G__t_t=G__t_t_jA.real

        #8
        G__t_rho=G__t__x_jrho.imag
        G__t_x=G__t__x_jrho.real

        #9
        G__x_rho=G__x__x_jrho.imag
        G__x_x=G__x__x_jrho.real

        #10
        G__phi_phi=G_x__rho2_jphi2.imag
        G__rho_rho=G_x__rho2_jphi2.real

        # there are seven tt tx trho , xx xrho, rhorho, phiphi
        #11


        hyp_flu1_x_t=hyp_coef_flu1_x__x_jt.imag
        hyp_flu1_x_x=hyp_coef_flu1_x__x_jt.real

        #12
        hyp_flu1_rho_rho=hyp_coef_flu1__tt_jrhorho.imag
        hyp_flu1_t_t=hyp_coef_flu1__tt_jrhorho.real

        #13
        hyp_flu1_cstt=hyp_coef_flu1__phiphi_jcstt.imag
        hyp_flu1_phiphi=hyp_coef_flu1__phiphi_jcstt.real

        #14

        euc_flu1_x_t=euc_coef_flu1_x__x_jt.imag
        euc_flu1_x_x=euc_coef_flu1_x__x_jt.real


        euc_flu1_rho_rho=euc_coef_flu1__tt_jrhorho.imag
        euc_flu1_t_t=euc_coef_flu1__tt_jrhorho.real


        euc_flu1_cstt=euc_coef_flu1__phiphi_jcstt.imag
        euc_flu1_phiphi=euc_coef_flu1__phiphi_jcstt.real
        ################################
        hyp_flu2_x_t=hyp_coef_flu2_x__x_jt.imag
        hyp_flu2_x_x=hyp_coef_flu2_x__x_jt.real


        hyp_flu2_rho_rho=hyp_coef_flu2__tt_jrhorho.imag
        hyp_flu2_t_t=hyp_coef_flu2__tt_jrhorho.real


        hyp_flu2_cstt=hyp_coef_flu2__phiphi_jcstt.imag
        hyp_flu2_phiphi=hyp_coef_flu2__phiphi_jcstt.real



        euc_flu2_x_t=euc_coef_flu2_x__x_jt.imag
        euc_flu2_x_x=euc_coef_flu2_x__x_jt.real


        euc_flu2_rho_rho=euc_coef_flu2__tt_jrhorho.imag
        euc_flu2_t_t=euc_coef_flu2__tt_jrhorho.real


        euc_flu2_cstt=euc_coef_flu2__phiphi_jcstt.imag
        euc_flu2_phiphi=euc_coef_flu2__phiphi_jcstt.real

        ##################################
        hyp_flu3_x_t=hyp_coef_flu3_x__x_jt.imag
        hyp_flu3_x_x=hyp_coef_flu3_x__x_jt.real


        hyp_flu3_rho_rho=hyp_coef_flu3__tt_jrhorho.imag
        hyp_flu3_t_t=hyp_coef_flu3__tt_jrhorho.real


        hyp_flu3_cstt=hyp_coef_flu3__phiphi_jcstt.imag
        hyp_flu3_phiphi=hyp_coef_flu3__phiphi_jcstt.real



        euc_flu3_x_t=euc_coef_flu3_x__x_jt.imag
        euc_flu3_x_x=euc_coef_flu3_x__x_jt.real


        euc_flu3_rho_rho=euc_coef_flu3__tt_jrhorho.imag
        euc_flu3_t_t=euc_coef_flu3__tt_jrhorho.real


        euc_flu3_cstt=euc_coef_flu3__phiphi_jcstt.imag
        euc_flu3_phiphi=euc_coef_flu3__phiphi_jcstt.real





        #29
        # res_batch=res_batch__1j_LAMBDA.real
        # Lambda_cosmological_constant_=res_batch__1j_LAMBDA.imag

        #do_print_stuff=True
        do_print_stuff=False

        #do_print_stuff2=True
        do_print_stuff2=False


        do_print_stuff3=False
        inputs={}

        do_print_stuff4=False

        do_print_stuff5=False
        #do_print_stuff5=True
        ######################################################
        # correction on g_phi,phi
        #below
        if g_n_phi_phi==0 or np.isinf(g_n_phi_phi):
            g_n_phi_phi=1

        if g_v_phi_phi==0 or np.isinf(g_v_phi_phi):
            g_v_phi_phi=1

        if np.isnan(g_v_phi_phi) or np.isnan(g_n_phi_phi):
            g_n_phi_phi=1
            g_v_phi_phi=1



        inputs["g_n_t_t"]=g_n_t_t
        inputs["g_n_t_X"]=g_n_t_X
        inputs["g_n_X_X"]=g_n_X_X
        inputs["g_n_rho_rho"]=g_n_rho_rho
        inputs["g_n_phi_phi"]=g_n_phi_phi


        inputs["g_v_t_t"]=g_v_t_t
        inputs["g_v_t_X"]=g_v_t_X
        inputs["g_v_X_X"]=g_v_X_X
        inputs["g_v_rho_rho"]=g_v_rho_rho
        inputs["g_v_phi_phi"]=g_v_phi_phi

        inputs["sqrt_lambda_minus"]=sqrt_lambda_minus
        inputs["sqrt_lambda_plus"]=sqrt_lambda_plus



        ########################################################
        # we compute G_doubly covariant!!
        G_doubly_cov=np.array( [ [ G__t_t   , G__t_x   , G__t_rho   , 0 ],\
                                 [ G__t_x   , G__x_x   , G__x_rho   , 0 ],\
                                 [ G__t_rho , G__x_rho , G__rho_rho , 0 ],\
                                 [ 0        , 0        , 0          , G__phi_phi ],\
        ])
        #######################################################
        # below are the numpy equivalents of lines / rows

        g_n_mat=np.array([[inputs["g_n_t_t"],inputs["g_n_t_X"],0,0],\
                          [inputs["g_n_t_X"],inputs["g_n_X_X"],0,0],\
                          [0                ,0                ,inputs["g_n_rho_rho"],0],\
                          [0                ,0                ,0                    ,inputs["g_n_phi_phi"]]])

        g_v_mat=np.array([[inputs["g_v_t_t"],inputs["g_v_t_X"],0,0],\
                          [inputs["g_v_t_X"],inputs["g_v_X_X"],0,0],\
                          [0                ,0                ,inputs["g_v_rho_rho"],0],\
                          [0                ,0                ,0                    ,inputs["g_v_phi_phi"]]])



        lambda_plus=(-(A-1)+np.sqrt((A-1)**2+4))/2.
        lambda_minus=(-(A-1)-np.sqrt((A-1)**2+4))/2.
        tW=np.array([[meshed_cos_theta,0,meshed_sin_theta*1./sqrt_lambda_plus],\
                     [0,1,0],\
                     [-meshed_sin_theta*sqrt_lambda_plus,0,meshed_cos_theta],\
                     ])

        return_to_starting_block=True
        overflow_counting=0

        if 1:
            #print('choose tw4 ---> make a function which creates such matrices \n line 1640')



            tW_4,\
                iter_overflow=two_fluids___rotation_euc_hyp__g_v__choice_tW_matrix(euc_flu3_x_t,\
                                                                      euc_flu3_x_x,\
                                                                      euc_flu3_rho_rho,\
                                                                      euc_flu3_t_t,\
                                                                      euc_flu3_cstt,\
                                                                      euc_flu3_phiphi,\
                                                                      hyp_flu1_x_t,\
                                                                      hyp_flu1_x_x,\
                                                                      hyp_flu1_rho_rho,\
                                                                      hyp_flu1_t_t,\
                                                                      hyp_flu1_cstt,\
                                                                      hyp_flu1_phiphi,\
                                                                      euc_flu1_x_t,\
                                                                      euc_flu1_x_x,\
                                                                      euc_flu1_rho_rho,\
                                                                      euc_flu1_t_t,\
                                                                      euc_flu1_cstt,\
                                                                      euc_flu1_phiphi,\
                                                                      inputs,\
            )

            tW_4_second_fluid,\
                iter_overflow_second=two_fluids___rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu3_x_t,\
                                                                             hyp_flu3_x_x,\
                                                                             hyp_flu3_rho_rho,\
                                                                             hyp_flu3_t_t,\
                                                                             hyp_flu3_cstt,\
                                                                             hyp_flu3_phiphi,\
                                                                             hyp_flu2_x_t,\
                                                                             hyp_flu2_x_x,\
                                                                             hyp_flu2_rho_rho,\
                                                                             hyp_flu2_t_t,\
                                                                             hyp_flu2_cstt,\
                                                                             hyp_flu2_phiphi,\
                                                                             euc_flu2_x_t,\
                                                                             euc_flu2_x_x,\
                                                                             euc_flu2_rho_rho,\
                                                                             euc_flu2_t_t,\
                                                                             euc_flu2_cstt,\
                                                                             euc_flu2_phiphi,\
                                                                             inputs,\
            )


 





            #interesting line below!!
            ####    g_v_t_X=v*(1-meshed_f)

            N_plus=np.sqrt((lambda_plus-1)**2+g_v_t_X**2)
            N_minus=np.sqrt((lambda_minus-1)**2+g_v_t_X**2)

            #################################
            # now we just examine the theta matrix
            #eigen_vect_g_doublcovariant=EVGDCov
            #{EVGDCov=theta matrix} is the matrix of eigen vectors of g_{mu,nu}

            EVGDCov=np.array([[(lambda_plus-1.0)/N_plus,(lambda_minus-1.0)/N_minus,0,0],\
                             [g_v_t_X/(1.0*N_plus),g_v_t_X/(1.0*N_minus),0,0],\
                             [0,0,1.,0],\
                             [0,0,0,1.]])
            the_product_should_be_diagonal=np.dot(EVGDCov.T,np.dot(g_v_mat,EVGDCov))

            ##################################

            # the following is contravariant (each nested array is not a desirable vector rather this is component 1 of the first array and then comp 1 of the second, etc.
            e_not_prime=np.array([[-1,0,0,0],\
                                  [g_v_t_X,1,0,0],\
                                  [0,0,1,0],\
                                  [0,0,0,1/np.sqrt(g_v_phi_phi) ],\
            ])

            minkowski_metric=np.diag([-1,1,1,1])
            e_prime=np.dot(EVGDCov,np.dot(tW_4,np.dot(EVGDCov.T,e_not_prime)))
            e_prime_2nd=np.dot(EVGDCov,np.dot(tW_4_second_fluid,np.dot(EVGDCov.T,e_not_prime)))

            # these two vectors are contravariant components


            e_prime_2nd_cov=np.dot(g_v_mat,e_prime_2nd)# in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures



            # e_prime is described as sets of lines , each dissecting interesting vectors (vectors as columns)
            #e_prime here is contravariant
            #below we compute the covariant vectors in a single matrix


            e_prime_cov=np.dot(g_v_mat,e_prime)# in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures



            # we compute the mixed components of Einstein tensor
            G_einst_mixed_plus_lambda_delta=np.dot(g_n_mat,G_doubly_cov+Lambda_cosmological_constant_*g_v_mat )






            #############################################
 
            basis_vectors_2fluids=[[e_prime_cov,e_prime],[e_prime_2nd_cov,e_prime_2nd]]
            big_matrix_2flu,gee_vector_2flu,overflow_big_mat=compute_big_matrix_for_fluids(basis_vectors_2fluids,G_einst_mixed_plus_lambda_delta)



            if np.any(np.isnan(big_matrix_2flu)) or np.any(np.isinf(big_matrix_2flu)) or \
               np.any(np.isnan(gee_vector_2flu)) or np.any(np.isinf(gee_vector_2flu)):
                return_to_starting_block=True

                hyp_flu1_x_t=hyp_flu1_x_t/3.
                hyp_flu1_x_x=hyp_flu1_x_x/3.
                hyp_flu1_rho_rho=hyp_flu1_rho_rho/3.
                hyp_flu1_t_t=hyp_flu1_t_t/3.
                hyp_flu1_cstt=hyp_flu1_cstt/3.
                hyp_flu1_phiphi=hyp_flu1_phiphi/3.

                hyp_flu2_x_t=hyp_flu2_x_t/3.
                hyp_flu2_x_x=hyp_flu2_x_x/3.
                hyp_flu2_rho_rho=hyp_flu2_rho_rho/3.
                hyp_flu2_t_t=hyp_flu2_t_t/3.
                hyp_flu2_cstt=hyp_flu2_cstt/3.
                hyp_flu2_phiphi=hyp_flu2_phiphi/3.

                #this below is only for two fluids as the 3rd fluid parameters are actually for 2 fluids parameterization of the second boost
                hyp_flu3_x_t=hyp_flu3_x_t/3.
                hyp_flu3_x_x=hyp_flu3_x_x/3.
                hyp_flu3_rho_rho=hyp_flu3_rho_rho/3.
                hyp_flu3_t_t=hyp_flu3_t_t/3.
                hyp_flu3_cstt=hyp_flu3_cstt/3.
                hyp_flu3_phiphi=hyp_flu3_phiphi/3.

                euc_flu3_x_t=euc_flu3_x_t/3.
                euc_flu3_x_x=euc_flu3_x_x/3.
                euc_flu3_rho_rho=euc_flu3_rho_rho/3.
                euc_flu3_t_t=euc_flu3_t_t/3.
                euc_flu3_cstt=euc_flu3_cstt/3.
                euc_flu3_phiphi=euc_flu3_phiphi/3.


                overflow_counting+=2
                # above if inf or nan we decrease the value of all coefficients
                densities_pressures_2fluids=[-11,-11,-11,-11,-11,-11,-11,-11]
                matrix_rank_2fluids=-1


            else:
                return_to_starting_block=False
                densities_pressures_2fluids,\
                    residuals_2fluids,\
                    matrix_rank_2fluids,\
                    sing_values_matrix_2fluids=np.linalg.lstsq(big_matrix_2flu, gee_vector_2flu,rcond=None)

        ##############################################################

        

        ##############################################################################################################################





        res=list(densities_pressures_2fluids)



        ######################
        # we compute equation of states below
        if res[0]!=0:
            w_1=(res[1]+res[2]+res[3])/(1.0*res[0])
        else:
            w_1=0

        if res[4]!=0:
            w_2=(res[5]+res[6]+res[7])/(1.0*res[4])
        else:
            w_2=0



        MW=np.max(np.abs([w_1,w_2])) # max of equation of states



        density_plus_pressure=[res[0]+res[1],res[0]+res[2],res[0]+res[3],\
                               res[4]+res[5],res[4]+res[6],res[4]+res[7],\
        ]


        min__density_plus_pressure=np.min(density_plus_pressure)
        if min__density_plus_pressure<0:
            pass
        else:
            min__density_plus_pressure=0

        return res[2]+1j*res[3],\
            res[6]+1j*res[7],\
        

#############################################################################################################################################
##############################################################################################################################
def two_fluids__element_wise__compute__pressures_pX__densities__output_them___myfunc(g_n_t__t_jX,\
                                                       g_n__rho2_jX2,\
                                                       g__v_jn__phiphi,\
                                                       g_v_t__t_jX,\
                                                       g_v__rho2_jX2,\
                                                       meshed__cos_jsin__theta,\
                                                       sqrt_lambda__pl_jmin,\
                                                       G__t_t_jA,\
                                                       G__t__x_jrho,\
                                                       G__x__x_jrho,\
                                                       G_x__rho2_jphi2,\
                                                       hyp_coef_flu1_x__x_jt,\
                                                       hyp_coef_flu1__tt_jrhorho,\
                                                       hyp_coef_flu1__phiphi_jcstt,\
                                                       euc_coef_flu1_x__x_jt,\
                                                       euc_coef_flu1__tt_jrhorho,\
                                                       euc_coef_flu1__phiphi_jcstt,\
                                                       hyp_coef_flu2_x__x_jt,\
                                                       hyp_coef_flu2__tt_jrhorho,\
                                                       hyp_coef_flu2__phiphi_jcstt,\
                                                       euc_coef_flu2_x__x_jt,\
                                                       euc_coef_flu2__tt_jrhorho,\
                                                       euc_coef_flu2__phiphi_jcstt,\
                                                       hyp_coef_flu3_x__x_jt,\
                                                       hyp_coef_flu3__tt_jrhorho,\
                                                       hyp_coef_flu3__phiphi_jcstt,\
                                                       euc_coef_flu3_x__x_jt,\
                                                       euc_coef_flu3__tt_jrhorho,\
                                                       euc_coef_flu3__phiphi_jcstt,\
):


    

        do_plot=False
        Lambda_cosmological_constant_=0
        #1.11e-52 #(m^(-2))

        #1
        g_n_t_X=g_n_t__t_jX.imag
        g_n_t_t=g_n_t__t_jX.real

        #2
        g_n_X_X=g_n__rho2_jX2.imag
        g_n_rho_rho=g_n__rho2_jX2.real


        #3
        g_n_phi_phi=g__v_jn__phiphi.imag
        g_v_phi_phi=g__v_jn__phiphi.real



        #4
        g_v_t_X=g_v_t__t_jX.imag
        g_v_t_t=g_v_t__t_jX.real

        #5
        g_v_X_X=g_v__rho2_jX2.imag
        g_v_rho_rho=g_v__rho2_jX2.real

        #5
        meshed_sin_theta=meshed__cos_jsin__theta.imag
        meshed_cos_theta=meshed__cos_jsin__theta.real

        #6
        sqrt_lambda_minus=sqrt_lambda__pl_jmin.imag
        sqrt_lambda_plus=sqrt_lambda__pl_jmin.real

        #7
        A=G__t_t_jA.imag
        G__t_t=G__t_t_jA.real

        #8
        G__t_rho=G__t__x_jrho.imag
        G__t_x=G__t__x_jrho.real

        #9
        G__x_rho=G__x__x_jrho.imag
        G__x_x=G__x__x_jrho.real

        #10
        G__phi_phi=G_x__rho2_jphi2.imag
        G__rho_rho=G_x__rho2_jphi2.real

        # there are seven tt tx trho , xx xrho, rhorho, phiphi
        #11


        hyp_flu1_x_t=hyp_coef_flu1_x__x_jt.imag
        hyp_flu1_x_x=hyp_coef_flu1_x__x_jt.real

        #12
        hyp_flu1_rho_rho=hyp_coef_flu1__tt_jrhorho.imag
        hyp_flu1_t_t=hyp_coef_flu1__tt_jrhorho.real

        #13
        hyp_flu1_cstt=hyp_coef_flu1__phiphi_jcstt.imag
        hyp_flu1_phiphi=hyp_coef_flu1__phiphi_jcstt.real

        #14

        euc_flu1_x_t=euc_coef_flu1_x__x_jt.imag
        euc_flu1_x_x=euc_coef_flu1_x__x_jt.real


        euc_flu1_rho_rho=euc_coef_flu1__tt_jrhorho.imag
        euc_flu1_t_t=euc_coef_flu1__tt_jrhorho.real


        euc_flu1_cstt=euc_coef_flu1__phiphi_jcstt.imag
        euc_flu1_phiphi=euc_coef_flu1__phiphi_jcstt.real
        ################################
        hyp_flu2_x_t=hyp_coef_flu2_x__x_jt.imag
        hyp_flu2_x_x=hyp_coef_flu2_x__x_jt.real


        hyp_flu2_rho_rho=hyp_coef_flu2__tt_jrhorho.imag
        hyp_flu2_t_t=hyp_coef_flu2__tt_jrhorho.real


        hyp_flu2_cstt=hyp_coef_flu2__phiphi_jcstt.imag
        hyp_flu2_phiphi=hyp_coef_flu2__phiphi_jcstt.real



        euc_flu2_x_t=euc_coef_flu2_x__x_jt.imag
        euc_flu2_x_x=euc_coef_flu2_x__x_jt.real


        euc_flu2_rho_rho=euc_coef_flu2__tt_jrhorho.imag
        euc_flu2_t_t=euc_coef_flu2__tt_jrhorho.real


        euc_flu2_cstt=euc_coef_flu2__phiphi_jcstt.imag
        euc_flu2_phiphi=euc_coef_flu2__phiphi_jcstt.real

        ##################################
        hyp_flu3_x_t=hyp_coef_flu3_x__x_jt.imag
        hyp_flu3_x_x=hyp_coef_flu3_x__x_jt.real


        hyp_flu3_rho_rho=hyp_coef_flu3__tt_jrhorho.imag
        hyp_flu3_t_t=hyp_coef_flu3__tt_jrhorho.real


        hyp_flu3_cstt=hyp_coef_flu3__phiphi_jcstt.imag
        hyp_flu3_phiphi=hyp_coef_flu3__phiphi_jcstt.real



        euc_flu3_x_t=euc_coef_flu3_x__x_jt.imag
        euc_flu3_x_x=euc_coef_flu3_x__x_jt.real


        euc_flu3_rho_rho=euc_coef_flu3__tt_jrhorho.imag
        euc_flu3_t_t=euc_coef_flu3__tt_jrhorho.real


        euc_flu3_cstt=euc_coef_flu3__phiphi_jcstt.imag
        euc_flu3_phiphi=euc_coef_flu3__phiphi_jcstt.real





        #29
        # res_batch=res_batch__1j_LAMBDA.real
        # Lambda_cosmological_constant_=res_batch__1j_LAMBDA.imag

        #do_print_stuff=True
        do_print_stuff=False

        #do_print_stuff2=True
        do_print_stuff2=False


        do_print_stuff3=False
        inputs={}

        do_print_stuff4=False

        do_print_stuff5=False
        #do_print_stuff5=True
        ######################################################
        # correction on g_phi,phi
        #below
        if g_n_phi_phi==0 or np.isinf(g_n_phi_phi):
            g_n_phi_phi=1

        if g_v_phi_phi==0 or np.isinf(g_v_phi_phi):
            g_v_phi_phi=1

        if np.isnan(g_v_phi_phi) or np.isnan(g_n_phi_phi):
            g_n_phi_phi=1
            g_v_phi_phi=1



        inputs["g_n_t_t"]=g_n_t_t
        inputs["g_n_t_X"]=g_n_t_X
        inputs["g_n_X_X"]=g_n_X_X
        inputs["g_n_rho_rho"]=g_n_rho_rho
        inputs["g_n_phi_phi"]=g_n_phi_phi


        inputs["g_v_t_t"]=g_v_t_t
        inputs["g_v_t_X"]=g_v_t_X
        inputs["g_v_X_X"]=g_v_X_X
        inputs["g_v_rho_rho"]=g_v_rho_rho
        inputs["g_v_phi_phi"]=g_v_phi_phi

        inputs["sqrt_lambda_minus"]=sqrt_lambda_minus
        inputs["sqrt_lambda_plus"]=sqrt_lambda_plus



        ########################################################
        # we compute G_doubly covariant!!
        G_doubly_cov=np.array( [ [ G__t_t   , G__t_x   , G__t_rho   , 0 ],\
                                 [ G__t_x   , G__x_x   , G__x_rho   , 0 ],\
                                 [ G__t_rho , G__x_rho , G__rho_rho , 0 ],\
                                 [ 0        , 0        , 0          , G__phi_phi ],\
        ])
        #######################################################
        # below are the numpy equivalents of lines / rows

        g_n_mat=np.array([[inputs["g_n_t_t"],inputs["g_n_t_X"],0,0],\
                          [inputs["g_n_t_X"],inputs["g_n_X_X"],0,0],\
                          [0                ,0                ,inputs["g_n_rho_rho"],0],\
                          [0                ,0                ,0                    ,inputs["g_n_phi_phi"]]])

        g_v_mat=np.array([[inputs["g_v_t_t"],inputs["g_v_t_X"],0,0],\
                          [inputs["g_v_t_X"],inputs["g_v_X_X"],0,0],\
                          [0                ,0                ,inputs["g_v_rho_rho"],0],\
                          [0                ,0                ,0                    ,inputs["g_v_phi_phi"]]])



        lambda_plus=(-(A-1)+np.sqrt((A-1)**2+4))/2.
        lambda_minus=(-(A-1)-np.sqrt((A-1)**2+4))/2.
        tW=np.array([[meshed_cos_theta,0,meshed_sin_theta*1./sqrt_lambda_plus],\
                     [0,1,0],\
                     [-meshed_sin_theta*sqrt_lambda_plus,0,meshed_cos_theta],\
                     ])

        return_to_starting_block=True
        overflow_counting=0

        if 1:
            #print('choose tw4 ---> make a function which creates such matrices \n line 1640')



            tW_4,\
                iter_overflow=two_fluids___rotation_euc_hyp__g_v__choice_tW_matrix(euc_flu3_x_t,\
                                                                      euc_flu3_x_x,\
                                                                      euc_flu3_rho_rho,\
                                                                      euc_flu3_t_t,\
                                                                      euc_flu3_cstt,\
                                                                      euc_flu3_phiphi,\
                                                                      hyp_flu1_x_t,\
                                                                      hyp_flu1_x_x,\
                                                                      hyp_flu1_rho_rho,\
                                                                      hyp_flu1_t_t,\
                                                                      hyp_flu1_cstt,\
                                                                      hyp_flu1_phiphi,\
                                                                      euc_flu1_x_t,\
                                                                      euc_flu1_x_x,\
                                                                      euc_flu1_rho_rho,\
                                                                      euc_flu1_t_t,\
                                                                      euc_flu1_cstt,\
                                                                      euc_flu1_phiphi,\
                                                                      inputs,\
            )

            tW_4_second_fluid,\
                iter_overflow_second=two_fluids___rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu3_x_t,\
                                                                             hyp_flu3_x_x,\
                                                                             hyp_flu3_rho_rho,\
                                                                             hyp_flu3_t_t,\
                                                                             hyp_flu3_cstt,\
                                                                             hyp_flu3_phiphi,\
                                                                             hyp_flu2_x_t,\
                                                                             hyp_flu2_x_x,\
                                                                             hyp_flu2_rho_rho,\
                                                                             hyp_flu2_t_t,\
                                                                             hyp_flu2_cstt,\
                                                                             hyp_flu2_phiphi,\
                                                                             euc_flu2_x_t,\
                                                                             euc_flu2_x_x,\
                                                                             euc_flu2_rho_rho,\
                                                                             euc_flu2_t_t,\
                                                                             euc_flu2_cstt,\
                                                                             euc_flu2_phiphi,\
                                                                             inputs,\
            )


 





            #interesting line below!!
            ####    g_v_t_X=v*(1-meshed_f)

            N_plus=np.sqrt((lambda_plus-1)**2+g_v_t_X**2)
            N_minus=np.sqrt((lambda_minus-1)**2+g_v_t_X**2)

            #################################
            # now we just examine the theta matrix
            #eigen_vect_g_doublcovariant=EVGDCov
            #{EVGDCov=theta matrix} is the matrix of eigen vectors of g_{mu,nu}

            EVGDCov=np.array([[(lambda_plus-1.0)/N_plus,(lambda_minus-1.0)/N_minus,0,0],\
                             [g_v_t_X/(1.0*N_plus),g_v_t_X/(1.0*N_minus),0,0],\
                             [0,0,1.,0],\
                             [0,0,0,1.]])
            the_product_should_be_diagonal=np.dot(EVGDCov.T,np.dot(g_v_mat,EVGDCov))

            ##################################

            # the following is contravariant (each nested array is not a desirable vector rather this is component 1 of the first array and then comp 1 of the second, etc.
            e_not_prime=np.array([[-1,0,0,0],\
                                  [g_v_t_X,1,0,0],\
                                  [0,0,1,0],\
                                  [0,0,0,1/np.sqrt(g_v_phi_phi) ],\
            ])

            minkowski_metric=np.diag([-1,1,1,1])
            e_prime=np.dot(EVGDCov,np.dot(tW_4,np.dot(EVGDCov.T,e_not_prime)))
            e_prime_2nd=np.dot(EVGDCov,np.dot(tW_4_second_fluid,np.dot(EVGDCov.T,e_not_prime)))

            # these two vectors are contravariant components


            e_prime_2nd_cov=np.dot(g_v_mat,e_prime_2nd)# in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures



            # e_prime is described as sets of lines , each dissecting interesting vectors (vectors as columns)
            #e_prime here is contravariant
            #below we compute the covariant vectors in a single matrix


            e_prime_cov=np.dot(g_v_mat,e_prime)# in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures



            # we compute the mixed components of Einstein tensor
            G_einst_mixed_plus_lambda_delta=np.dot(g_n_mat,G_doubly_cov+Lambda_cosmological_constant_*g_v_mat )






            #############################################
 
            basis_vectors_2fluids=[[e_prime_cov,e_prime],[e_prime_2nd_cov,e_prime_2nd]]
            big_matrix_2flu,gee_vector_2flu,overflow_big_mat=compute_big_matrix_for_fluids(basis_vectors_2fluids,G_einst_mixed_plus_lambda_delta)



            if np.any(np.isnan(big_matrix_2flu)) or np.any(np.isinf(big_matrix_2flu)) or \
               np.any(np.isnan(gee_vector_2flu)) or np.any(np.isinf(gee_vector_2flu)):
                return_to_starting_block=True

                hyp_flu1_x_t=hyp_flu1_x_t/3.
                hyp_flu1_x_x=hyp_flu1_x_x/3.
                hyp_flu1_rho_rho=hyp_flu1_rho_rho/3.
                hyp_flu1_t_t=hyp_flu1_t_t/3.
                hyp_flu1_cstt=hyp_flu1_cstt/3.
                hyp_flu1_phiphi=hyp_flu1_phiphi/3.

                hyp_flu2_x_t=hyp_flu2_x_t/3.
                hyp_flu2_x_x=hyp_flu2_x_x/3.
                hyp_flu2_rho_rho=hyp_flu2_rho_rho/3.
                hyp_flu2_t_t=hyp_flu2_t_t/3.
                hyp_flu2_cstt=hyp_flu2_cstt/3.
                hyp_flu2_phiphi=hyp_flu2_phiphi/3.

                #this below is only for two fluids as the 3rd fluid parameters are actually for 2 fluids parameterization of the second boost
                hyp_flu3_x_t=hyp_flu3_x_t/3.
                hyp_flu3_x_x=hyp_flu3_x_x/3.
                hyp_flu3_rho_rho=hyp_flu3_rho_rho/3.
                hyp_flu3_t_t=hyp_flu3_t_t/3.
                hyp_flu3_cstt=hyp_flu3_cstt/3.
                hyp_flu3_phiphi=hyp_flu3_phiphi/3.

                euc_flu3_x_t=euc_flu3_x_t/3.
                euc_flu3_x_x=euc_flu3_x_x/3.
                euc_flu3_rho_rho=euc_flu3_rho_rho/3.
                euc_flu3_t_t=euc_flu3_t_t/3.
                euc_flu3_cstt=euc_flu3_cstt/3.
                euc_flu3_phiphi=euc_flu3_phiphi/3.


                overflow_counting+=2
                # above if inf or nan we decrease the value of all coefficients
                densities_pressures_2fluids=[-11,-11,-11,-11,-11,-11,-11,-11]
                matrix_rank_2fluids=-1


            else:
                return_to_starting_block=False
                densities_pressures_2fluids,\
                    residuals_2fluids,\
                    matrix_rank_2fluids,\
                    sing_values_matrix_2fluids=np.linalg.lstsq(big_matrix_2flu, gee_vector_2flu,rcond=None)

        ##############################################################

        

        ##############################################################################################################################





        res=list(densities_pressures_2fluids)


        return res[0]+1j*res[1],\
            res[4]+1j*res[5],\
#####################################################################################################################################################


#########################################################################################################################################

        
##############################################################################################################################
def three_fluids__element_wise__compute__pressures_pX__densities__output_them___myfunc(g_n_t__t_jX,\
                                                       g_n__rho2_jX2,\
                                                       g__v_jn__phiphi,\
                                                       g_v_t__t_jX,\
                                                       g_v__rho2_jX2,\
                                                       meshed__cos_jsin__theta,\
                                                       sqrt_lambda__pl_jmin,\
                                                       G__t_t_jA,\
                                                       G__t__x_jrho,\
                                                       G__x__x_jrho,\
                                                       G_x__rho2_jphi2,\
                                                       hyp_coef_flu1_x__x_jt,\
                                                       hyp_coef_flu1__tt_jrhorho,\
                                                       hyp_coef_flu1__phiphi_jcstt,\
                                                       euc_coef_flu1_x__x_jt,\
                                                       euc_coef_flu1__tt_jrhorho,\
                                                       euc_coef_flu1__phiphi_jcstt,\
                                                       hyp_coef_flu2_x__x_jt,\
                                                       hyp_coef_flu2__tt_jrhorho,\
                                                       hyp_coef_flu2__phiphi_jcstt,\
                                                       euc_coef_flu2_x__x_jt,\
                                                       euc_coef_flu2__tt_jrhorho,\
                                                       euc_coef_flu2__phiphi_jcstt,\
                                                       hyp_coef_flu3_x__x_jt,\
                                                       hyp_coef_flu3__tt_jrhorho,\
                                                       hyp_coef_flu3__phiphi_jcstt,\
                                                       euc_coef_flu3_x__x_jt,\
                                                       euc_coef_flu3__tt_jrhorho,\
                                                       euc_coef_flu3__phiphi_jcstt,\
):
    Lambda_cosmological_constant_=0
    #1.11e-52 #(m^(-2))
    bad_inf_nan=0

    do_plot=False

    #1
    g_n_t_X=g_n_t__t_jX.imag
    g_n_t_t=g_n_t__t_jX.real

    #2
    g_n_X_X=g_n__rho2_jX2.imag
    g_n_rho_rho=g_n__rho2_jX2.real


    #3
    g_n_phi_phi=g__v_jn__phiphi.imag
    g_v_phi_phi=g__v_jn__phiphi.real



    #4
    g_v_t_X=g_v_t__t_jX.imag
    g_v_t_t=g_v_t__t_jX.real

    #5
    g_v_X_X=g_v__rho2_jX2.imag
    g_v_rho_rho=g_v__rho2_jX2.real

    #5
    meshed_sin_theta=meshed__cos_jsin__theta.imag
    meshed_cos_theta=meshed__cos_jsin__theta.real

    #6
    sqrt_lambda_minus=sqrt_lambda__pl_jmin.imag
    sqrt_lambda_plus=sqrt_lambda__pl_jmin.real

    #7
    A=G__t_t_jA.imag
    G__t_t=G__t_t_jA.real

    #8
    G__t_rho=G__t__x_jrho.imag
    G__t_x=G__t__x_jrho.real

    #9
    G__x_rho=G__x__x_jrho.imag
    G__x_x=G__x__x_jrho.real

    #10
    G__phi_phi=G_x__rho2_jphi2.imag
    G__rho_rho=G_x__rho2_jphi2.real

    # there are seven tt tx trho , xx xrho, rhorho, phiphi
    #11


    hyp_flu1_x_t=hyp_coef_flu1_x__x_jt.imag
    hyp_flu1_x_x=hyp_coef_flu1_x__x_jt.real

    #12
    hyp_flu1_rho_rho=hyp_coef_flu1__tt_jrhorho.imag
    hyp_flu1_t_t=hyp_coef_flu1__tt_jrhorho.real

    #13
    hyp_flu1_cstt=hyp_coef_flu1__phiphi_jcstt.imag
    hyp_flu1_phiphi=hyp_coef_flu1__phiphi_jcstt.real

    #14

    euc_flu1_x_t=euc_coef_flu1_x__x_jt.imag
    euc_flu1_x_x=euc_coef_flu1_x__x_jt.real


    euc_flu1_rho_rho=euc_coef_flu1__tt_jrhorho.imag
    euc_flu1_t_t=euc_coef_flu1__tt_jrhorho.real


    euc_flu1_cstt=euc_coef_flu1__phiphi_jcstt.imag
    euc_flu1_phiphi=euc_coef_flu1__phiphi_jcstt.real
    ################################
    hyp_flu2_x_t=hyp_coef_flu2_x__x_jt.imag
    hyp_flu2_x_x=hyp_coef_flu2_x__x_jt.real


    hyp_flu2_rho_rho=hyp_coef_flu2__tt_jrhorho.imag
    hyp_flu2_t_t=hyp_coef_flu2__tt_jrhorho.real


    hyp_flu2_cstt=hyp_coef_flu2__phiphi_jcstt.imag
    hyp_flu2_phiphi=hyp_coef_flu2__phiphi_jcstt.real



    euc_flu2_x_t=euc_coef_flu2_x__x_jt.imag
    euc_flu2_x_x=euc_coef_flu2_x__x_jt.real


    euc_flu2_rho_rho=euc_coef_flu2__tt_jrhorho.imag
    euc_flu2_t_t=euc_coef_flu2__tt_jrhorho.real


    euc_flu2_cstt=euc_coef_flu2__phiphi_jcstt.imag
    euc_flu2_phiphi=euc_coef_flu2__phiphi_jcstt.real

    ##################################
    hyp_flu3_x_t=hyp_coef_flu3_x__x_jt.imag
    hyp_flu3_x_x=hyp_coef_flu3_x__x_jt.real


    hyp_flu3_rho_rho=hyp_coef_flu3__tt_jrhorho.imag
    hyp_flu3_t_t=hyp_coef_flu3__tt_jrhorho.real


    hyp_flu3_cstt=hyp_coef_flu3__phiphi_jcstt.imag
    hyp_flu3_phiphi=hyp_coef_flu3__phiphi_jcstt.real



    euc_flu3_x_t=euc_coef_flu3_x__x_jt.imag
    euc_flu3_x_x=euc_coef_flu3_x__x_jt.real


    euc_flu3_rho_rho=euc_coef_flu3__tt_jrhorho.imag
    euc_flu3_t_t=euc_coef_flu3__tt_jrhorho.real


    euc_flu3_cstt=euc_coef_flu3__phiphi_jcstt.imag
    euc_flu3_phiphi=euc_coef_flu3__phiphi_jcstt.real













    #do_print_stuff=True
    do_print_stuff=False

    #do_print_stuff2=True
    do_print_stuff2=False


    do_print_stuff3=False
    inputs={}

    do_print_stuff4=False

    do_print_stuff5=False
    ######################################################
    # correction sur g_phi,phi
    #below
    if g_n_phi_phi==0 or np.isinf(g_n_phi_phi):
        g_n_phi_phi=1

    if g_v_phi_phi==0 or np.isinf(g_v_phi_phi):
        g_v_phi_phi=1

    if np.isnan(g_v_phi_phi) or np.isnan(g_n_phi_phi):
        g_n_phi_phi=1
        g_v_phi_phi=1



    inputs["g_n_t_t"]=g_n_t_t
    inputs["g_n_t_X"]=g_n_t_X
    inputs["g_n_X_X"]=g_n_X_X
    inputs["g_n_rho_rho"]=g_n_rho_rho
    inputs["g_n_phi_phi"]=g_n_phi_phi


    inputs["g_v_t_t"]=g_v_t_t
    inputs["g_v_t_X"]=g_v_t_X
    inputs["g_v_X_X"]=g_v_X_X
    inputs["g_v_rho_rho"]=g_v_rho_rho
    inputs["g_v_phi_phi"]=g_v_phi_phi

    inputs["sqrt_lambda_minus"]=sqrt_lambda_minus
    inputs["sqrt_lambda_plus"]=sqrt_lambda_plus



    ########################################################
    # we compute G_doubly covariant!!
    G_doubly_cov=np.array( [ [ G__t_t   , G__t_x   , G__t_rho   , 0 ],\
                             [ G__t_x   , G__x_x   , G__x_rho   , 0 ],\
                             [ G__t_rho , G__x_rho , G__rho_rho , 0 ],\
                             [ 0        , 0        , 0          , G__phi_phi ],\
    ])
    g_n_mat=np.array([[inputs["g_n_t_t"],inputs["g_n_t_X"],0,0],\
                      [inputs["g_n_t_X"],inputs["g_n_X_X"],0,0],\
                      [0                ,0                ,inputs["g_n_rho_rho"],0],\
                      [0                ,0                ,0                    ,inputs["g_n_phi_phi"]]])

    g_v_mat=np.array([[inputs["g_v_t_t"],inputs["g_v_t_X"],0,0],\
                      [inputs["g_v_t_X"],inputs["g_v_X_X"],0,0],\
                      [0                ,0                ,inputs["g_v_rho_rho"],0],\
                      [0                ,0                ,0                    ,inputs["g_v_phi_phi"]]])



    lambda_plus=(-(A-1)+np.sqrt((A-1)**2+4))/2.
    lambda_minus=(-(A-1)-np.sqrt((A-1)**2+4))/2.
    tW=np.array([[meshed_cos_theta,0,meshed_sin_theta*1./sqrt_lambda_plus],\
                 [0,1,0],\
                 [-meshed_sin_theta*sqrt_lambda_plus,0,meshed_cos_theta],\
                 ])

    return_to_starting_block=True
    if 1:


        #print('choose tw4 ---> make a function which creates such matrices \n line 1640')



        tW_4,\
            tW_4_rot_no_curv,\
            tW_4_boo_no_curv,\
            iter_overflow=three_fluids___rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu1_x_t,\
                                                                     hyp_flu1_x_x,\
                                                                     hyp_flu1_rho_rho,\
                                                                     hyp_flu1_t_t,\
                                                                     hyp_flu1_cstt,\
                                                                     hyp_flu1_phiphi,\
                                                                     euc_flu1_x_t,\
                                                                     euc_flu1_x_x,\
                                                                     euc_flu1_rho_rho,\
                                                                     euc_flu1_t_t,\
                                                                     euc_flu1_cstt,\
                                                                     euc_flu1_phiphi,\
                                                                     inputs,\
        )

        tW_4_second_fluid,\
            tW_4_second_fluid_rot_no_curv,\
            tW_4_second_fluid_boo_no_curv,\
            iter_overflow_second=three_fluids___rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu2_x_t,\
                                                                                  hyp_flu2_x_x,\
                                                                                  hyp_flu2_rho_rho,\
                                                                                  hyp_flu2_t_t,\
                                                                                  hyp_flu2_cstt,\
                                                                                  hyp_flu2_phiphi,\
                                                                                  euc_flu2_x_t,\
                                                                                  euc_flu2_x_x,\
                                                                                  euc_flu2_rho_rho,\
                                                                                  euc_flu2_t_t,\
                                                                                  euc_flu2_cstt,\
                                                                                  euc_flu2_phiphi,\
                                                                                  inputs,\
        )

        tW_4_third_fluid,\
            tW_4_third_fluid_rot_no_curv,\
            tW_4_third_fluid_boo_no_curv,\
            iter_overflow_third=three_fluids___rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu3_x_t,\
                                                                                 hyp_flu3_x_x,\
                                                                                 hyp_flu3_rho_rho,\
                                                                                 hyp_flu3_t_t,\
                                                                                 hyp_flu3_cstt,\
                                                                                 hyp_flu3_phiphi,\
                                                                                 euc_flu3_x_t,\
                                                                                 euc_flu3_x_x,\
                                                                                 euc_flu3_rho_rho,\
                                                                                 euc_flu3_t_t,\
                                                                                 euc_flu3_cstt,\
                                                                                 euc_flu3_phiphi,\
                                                                                 inputs,\
        )



        #below is the g_munu diagonal version
        thediag=np.diag([lambda_plus,lambda_minus,1])
        thediag_4=np.diag([lambda_plus,lambda_minus,1,g_v_phi_phi])
        thediag_4_no_curve=np.diag([1,-1,1,1])#no_curv

        product=np.dot(tW.T,np.dot(thediag,tW))

        product_4_4__ini_fluid__tw4=np.dot(tW_4.T,np.dot(thediag_4,tW_4)) # this should be the same as the_diag_4
        product_4_4__2nd_fluid__tw4=np.dot(tW_4_second_fluid.T,np.dot(thediag_4,tW_4_second_fluid)) # this should be the same as the_diag_4
        product_4_4__3rd_fluid__tw4=np.dot(tW_4_third_fluid.T,np.dot(thediag_4,tW_4_third_fluid)) # this should be the same as the_diag_4


        #interesting line below!!
        ####    g_v_t_X=v*(1-meshed_f)

        N_plus=np.sqrt((lambda_plus-1)**2+g_v_t_X**2)
        N_minus=np.sqrt((lambda_minus-1)**2+g_v_t_X**2)

        #################################
        # now we just examine the theta matrix
        #eigen_vect_g_doublcovariant=EVGDCov
        #{EVGDCov=theta matrix} is the matrix of eigen vectors of g_{mu,nu}

        EVGDCov=np.array([[(lambda_plus-1.0)/N_plus,(lambda_minus-1.0)/N_minus,0,0],\
                         [g_v_t_X/(1.0*N_plus),g_v_t_X/(1.0*N_minus),0,0],\
                         [0,0,1.,0],\
                         [0,0,0,1.]])
        the_product_should_be_diagonal=np.dot(EVGDCov.T,np.dot(g_v_mat,EVGDCov))

        # the following is contravariant (each nested array is not a desirable vector rather this is component 1 of the first array and then comp 1 of the second, etc.
        e_not_prime=np.array([[-1,0,0,0],\
                              [g_v_t_X,1,0,0],\
                              [0,0,1,0],\
                              [0,0,0,1/np.sqrt(g_v_phi_phi) ],\
        ])

        the_product_should_be_minkowski_metric=np.dot(e_not_prime.T,np.dot(g_v_mat,e_not_prime))


        minkowski_metric=np.diag([-1,1,1,1])
        e_prime=np.dot(EVGDCov,np.dot(tW_4,np.dot(EVGDCov.T,e_not_prime)))
        e_prime_2nd=np.dot(EVGDCov,np.dot(tW_4_second_fluid,np.dot(EVGDCov.T,e_not_prime)))
        e_prime_3rd=np.dot(EVGDCov,np.dot(tW_4_third_fluid,np.dot(EVGDCov.T,e_not_prime))) 
        # these 3 vectors have contravariant components

        e_prime_3rd_cov=np.dot(g_v_mat,e_prime_3rd)# in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures
        e_prime_2nd_cov=np.dot(g_v_mat,e_prime_2nd)# same as above



        # e_prime is described as sets of lines , each dissecting interesting vectors (vectors as columns)
        #e_prime here is contravariant
        #below we compute the covariant vectors in a single matrix


        e_prime_cov=np.dot(g_v_mat,e_prime)# in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures



        # we compute the mixed components of Einstein tensor
        G_einst_mixed_plus_lambda_delta=np.dot(g_n_mat,G_doubly_cov+Lambda_cosmological_constant_*g_v_mat )






        ####################################################################





        ################################################


        #################################################
        #################################################



        basis_vectors_3fluids=[[e_prime_cov,e_prime],[e_prime_2nd_cov,e_prime_2nd],[e_prime_3rd_cov,e_prime_3rd]]
        big_matrix_3flu,gee_vector_3flu,overflow_big_mat=compute_big_matrix_for_fluids(basis_vectors_3fluids,G_einst_mixed_plus_lambda_delta)


        if np.any(np.isnan(big_matrix_3flu)) or np.any(np.isinf(big_matrix_3flu)) or \
           np.any(np.isnan(gee_vector_3flu)) or np.any(np.isinf(gee_vector_3flu)):
            return_to_starting_block=True

            hyp_flu1_x_t=hyp_flu1_x_t/3.
            hyp_flu1_x_x=hyp_flu1_x_x/3.
            hyp_flu1_rho_rho=hyp_flu1_rho_rho/3.
            hyp_flu1_t_t=hyp_flu1_t_t/3.
            hyp_flu1_cstt=hyp_flu1_cstt/3.
            hyp_flu1_phiphi=hyp_flu1_phiphi/3.

            hyp_flu2_x_t=hyp_flu2_x_t/3.
            hyp_flu2_x_x=hyp_flu2_x_x/3.
            hyp_flu2_rho_rho=hyp_flu2_rho_rho/3.
            hyp_flu2_t_t=hyp_flu2_t_t/3.
            hyp_flu2_cstt=hyp_flu2_cstt/3.
            hyp_flu2_phiphi=hyp_flu2_phiphi/3.

            hyp_flu3_x_t=hyp_flu3_x_t/3.
            hyp_flu3_x_x=hyp_flu3_x_x/3.
            hyp_flu3_rho_rho=hyp_flu3_rho_rho/3.
            hyp_flu3_t_t=hyp_flu3_t_t/3.
            hyp_flu3_cstt=hyp_flu3_cstt/3.
            hyp_flu3_phiphi=hyp_flu3_phiphi/3.
            # above if inf or nan we decrease the values of all coefficients

            bad_inf_nan+=1
            
            densities_pressures_3fluids=[-11,-11,-11,-11,-11,-11,-11,-11,-11,-11,-11,-11]
            
            matrix_rank_3fluids=-1
            

        else:
            return_to_starting_block=False
            densities_pressures_3fluids,\
                residuals_3fluids,\
                matrix_rank_3fluids,\
                sing_values_matrix_3fluids=np.linalg.lstsq(big_matrix_3flu, gee_vector_3flu,rcond=None)

    ##############################################################


    ##############################################################################################################################




    res=list(densities_pressures_3fluids)



    return res[0]+1j*res[1],\
        res[4]+1j*res[5],\
        res[8]+1j*res[9]

#################################################################################################


####################################################################################################


##############################################################################################################################################

def any_nb_of_fluids__einst_mixed_largest_value___myfunc(g_n_t__t_jX,\
                                                       g_n__rho2_jX2,\
                                                       g__v_jn__phiphi,\
                                                       g_v_t__t_jX,\
                                                       g_v__rho2_jX2,\
                                                       meshed__cos_jsin__theta,\
                                                       sqrt_lambda__pl_jmin,\
                                                       G__t_t_jA,\
                                                       G__t__x_jrho,\
                                                       G__x__x_jrho,\
                                                       G_x__rho2_jphi2,\
                                                       hyp_coef_flu1_x__x_jt,\
                                                       hyp_coef_flu1__tt_jrhorho,\
                                                       hyp_coef_flu1__phiphi_jcstt,\
                                                       euc_coef_flu1_x__x_jt,\
                                                       euc_coef_flu1__tt_jrhorho,\
                                                       euc_coef_flu1__phiphi_jcstt,\
                                                       hyp_coef_flu2_x__x_jt,\
                                                       hyp_coef_flu2__tt_jrhorho,\
                                                       hyp_coef_flu2__phiphi_jcstt,\
                                                       euc_coef_flu2_x__x_jt,\
                                                       euc_coef_flu2__tt_jrhorho,\
                                                       euc_coef_flu2__phiphi_jcstt,\
                                                       hyp_coef_flu3_x__x_jt,\
                                                       hyp_coef_flu3__tt_jrhorho,\
                                                       hyp_coef_flu3__phiphi_jcstt,\
                                                       euc_coef_flu3_x__x_jt,\
                                                       euc_coef_flu3__tt_jrhorho,\
                                                       euc_coef_flu3__phiphi_jcstt,\
):



    bad_inf_nan=0


    do_plot=False

    #1
    g_n_t_X=g_n_t__t_jX.imag
    g_n_t_t=g_n_t__t_jX.real

    #2
    g_n_X_X=g_n__rho2_jX2.imag
    g_n_rho_rho=g_n__rho2_jX2.real


    #3
    g_n_phi_phi=g__v_jn__phiphi.imag
    g_v_phi_phi=g__v_jn__phiphi.real



    #4
    g_v_t_X=g_v_t__t_jX.imag
    g_v_t_t=g_v_t__t_jX.real

    #5
    g_v_X_X=g_v__rho2_jX2.imag
    g_v_rho_rho=g_v__rho2_jX2.real

    #5
    meshed_sin_theta=meshed__cos_jsin__theta.imag
    meshed_cos_theta=meshed__cos_jsin__theta.real

    #6
    sqrt_lambda_minus=sqrt_lambda__pl_jmin.imag
    sqrt_lambda_plus=sqrt_lambda__pl_jmin.real

    #7
    A=G__t_t_jA.imag
    G__t_t=G__t_t_jA.real

    #8
    G__t_rho=G__t__x_jrho.imag
    G__t_x=G__t__x_jrho.real

    #9
    G__x_rho=G__x__x_jrho.imag
    G__x_x=G__x__x_jrho.real

    #10
    G__phi_phi=G_x__rho2_jphi2.imag
    G__rho_rho=G_x__rho2_jphi2.real

    # there are seven tt tx trho , xx xrho, rhorho, phiphi
    #11


    hyp_flu1_x_t=hyp_coef_flu1_x__x_jt.imag
    hyp_flu1_x_x=hyp_coef_flu1_x__x_jt.real

    #12
    hyp_flu1_rho_rho=hyp_coef_flu1__tt_jrhorho.imag
    hyp_flu1_t_t=hyp_coef_flu1__tt_jrhorho.real

    #13
    hyp_flu1_cstt=hyp_coef_flu1__phiphi_jcstt.imag
    hyp_flu1_phiphi=hyp_coef_flu1__phiphi_jcstt.real

    #14

    euc_flu1_x_t=euc_coef_flu1_x__x_jt.imag
    euc_flu1_x_x=euc_coef_flu1_x__x_jt.real


    euc_flu1_rho_rho=euc_coef_flu1__tt_jrhorho.imag
    euc_flu1_t_t=euc_coef_flu1__tt_jrhorho.real


    euc_flu1_cstt=euc_coef_flu1__phiphi_jcstt.imag
    euc_flu1_phiphi=euc_coef_flu1__phiphi_jcstt.real
    ################################
    hyp_flu2_x_t=hyp_coef_flu2_x__x_jt.imag
    hyp_flu2_x_x=hyp_coef_flu2_x__x_jt.real


    hyp_flu2_rho_rho=hyp_coef_flu2__tt_jrhorho.imag
    hyp_flu2_t_t=hyp_coef_flu2__tt_jrhorho.real


    hyp_flu2_cstt=hyp_coef_flu2__phiphi_jcstt.imag
    hyp_flu2_phiphi=hyp_coef_flu2__phiphi_jcstt.real



    euc_flu2_x_t=euc_coef_flu2_x__x_jt.imag
    euc_flu2_x_x=euc_coef_flu2_x__x_jt.real


    euc_flu2_rho_rho=euc_coef_flu2__tt_jrhorho.imag
    euc_flu2_t_t=euc_coef_flu2__tt_jrhorho.real


    euc_flu2_cstt=euc_coef_flu2__phiphi_jcstt.imag
    euc_flu2_phiphi=euc_coef_flu2__phiphi_jcstt.real

    ##################################
    hyp_flu3_x_t=hyp_coef_flu3_x__x_jt.imag
    hyp_flu3_x_x=hyp_coef_flu3_x__x_jt.real


    hyp_flu3_rho_rho=hyp_coef_flu3__tt_jrhorho.imag
    hyp_flu3_t_t=hyp_coef_flu3__tt_jrhorho.real


    hyp_flu3_cstt=hyp_coef_flu3__phiphi_jcstt.imag
    hyp_flu3_phiphi=hyp_coef_flu3__phiphi_jcstt.real



    euc_flu3_x_t=euc_coef_flu3_x__x_jt.imag
    euc_flu3_x_x=euc_coef_flu3_x__x_jt.real


    euc_flu3_rho_rho=euc_coef_flu3__tt_jrhorho.imag
    euc_flu3_t_t=euc_coef_flu3__tt_jrhorho.real


    euc_flu3_cstt=euc_coef_flu3__phiphi_jcstt.imag
    euc_flu3_phiphi=euc_coef_flu3__phiphi_jcstt.real





    #29



    Lambda_cosmological_constant_=0
    #1.11e-52 #(m^(-2))
    







    #do_print_stuff=True
    do_print_stuff=False

    #do_print_stuff2=True
    do_print_stuff2=False


    do_print_stuff3=False
    inputs={}

    do_print_stuff4=False

    do_print_stuff5=False
    ######################################################
    # correction sur g_phi,phi
    #below
    if g_n_phi_phi==0 or np.isinf(g_n_phi_phi):
        g_n_phi_phi=1

    if g_v_phi_phi==0 or np.isinf(g_v_phi_phi):
        g_v_phi_phi=1

    if np.isnan(g_v_phi_phi) or np.isnan(g_n_phi_phi):
        g_n_phi_phi=1
        g_v_phi_phi=1



    inputs["g_n_t_t"]=g_n_t_t
    inputs["g_n_t_X"]=g_n_t_X
    inputs["g_n_X_X"]=g_n_X_X
    inputs["g_n_rho_rho"]=g_n_rho_rho
    inputs["g_n_phi_phi"]=g_n_phi_phi


    inputs["g_v_t_t"]=g_v_t_t
    inputs["g_v_t_X"]=g_v_t_X
    inputs["g_v_X_X"]=g_v_X_X
    inputs["g_v_rho_rho"]=g_v_rho_rho
    inputs["g_v_phi_phi"]=g_v_phi_phi

    inputs["sqrt_lambda_minus"]=sqrt_lambda_minus
    inputs["sqrt_lambda_plus"]=sqrt_lambda_plus



    ########################################################
    # we compute G_doubly covariant!!
    G_doubly_cov=np.array( [ [ G__t_t   , G__t_x   , G__t_rho   , 0 ],\
                             [ G__t_x   , G__x_x   , G__x_rho   , 0 ],\
                             [ G__t_rho , G__x_rho , G__rho_rho , 0 ],\
                             [ 0        , 0        , 0          , G__phi_phi ],\
    ])
    #######################################################
    # below are the numpy equivalents of lines / rows
    g_n_mat=np.array([[inputs["g_n_t_t"],inputs["g_n_t_X"],0,0],\
                      [inputs["g_n_t_X"],inputs["g_n_X_X"],0,0],\
                      [0                ,0                ,inputs["g_n_rho_rho"],0],\
                      [0                ,0                ,0                    ,inputs["g_n_phi_phi"]]])

    g_v_mat=np.array([[inputs["g_v_t_t"],inputs["g_v_t_X"],0,0],\
                      [inputs["g_v_t_X"],inputs["g_v_X_X"],0,0],\
                      [0                ,0                ,inputs["g_v_rho_rho"],0],\
                      [0                ,0                ,0                    ,inputs["g_v_phi_phi"]]])



    lambda_plus=(-(A-1)+np.sqrt((A-1)**2+4))/2.
    lambda_minus=(-(A-1)-np.sqrt((A-1)**2+4))/2.
    tW=np.array([[meshed_cos_theta,0,meshed_sin_theta*1./sqrt_lambda_plus],\
                 [0,1,0],\
                 [-meshed_sin_theta*sqrt_lambda_plus,0,meshed_cos_theta],\
                 ])

    return_to_starting_block=True
    if 1:


        #print('choose tw4 ---> make a function which creates such matrices \n line 1640')



        tW_4,\
            tW_4_rot_no_curv,\
            tW_4_boo_no_curv,\
            iter_overflow=three_fluids___rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu1_x_t,\
                                                                     hyp_flu1_x_x,\
                                                                     hyp_flu1_rho_rho,\
                                                                     hyp_flu1_t_t,\
                                                                     hyp_flu1_cstt,\
                                                                     hyp_flu1_phiphi,\
                                                                     euc_flu1_x_t,\
                                                                     euc_flu1_x_x,\
                                                                     euc_flu1_rho_rho,\
                                                                     euc_flu1_t_t,\
                                                                     euc_flu1_cstt,\
                                                                     euc_flu1_phiphi,\
                                                                     inputs,\
        )

        tW_4_second_fluid,\
            tW_4_second_fluid_rot_no_curv,\
            tW_4_second_fluid_boo_no_curv,\
            iter_overflow_second=three_fluids___rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu2_x_t,\
                                                                                  hyp_flu2_x_x,\
                                                                                  hyp_flu2_rho_rho,\
                                                                                  hyp_flu2_t_t,\
                                                                                  hyp_flu2_cstt,\
                                                                                  hyp_flu2_phiphi,\
                                                                                  euc_flu2_x_t,\
                                                                                  euc_flu2_x_x,\
                                                                                  euc_flu2_rho_rho,\
                                                                                  euc_flu2_t_t,\
                                                                                  euc_flu2_cstt,\
                                                                                  euc_flu2_phiphi,\
                                                                                  inputs,\
        )

        tW_4_third_fluid,\
            tW_4_third_fluid_rot_no_curv,\
            tW_4_third_fluid_boo_no_curv,\
            iter_overflow_third=three_fluids___rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu3_x_t,\
                                                                                 hyp_flu3_x_x,\
                                                                                 hyp_flu3_rho_rho,\
                                                                                 hyp_flu3_t_t,\
                                                                                 hyp_flu3_cstt,\
                                                                                 hyp_flu3_phiphi,\
                                                                                 euc_flu3_x_t,\
                                                                                 euc_flu3_x_x,\
                                                                                 euc_flu3_rho_rho,\
                                                                                 euc_flu3_t_t,\
                                                                                 euc_flu3_cstt,\
                                                                                 euc_flu3_phiphi,\
                                                                                 inputs,\
        )



        #below is the g_munu diagonal version
        thediag=np.diag([lambda_plus,lambda_minus,1])
        thediag_4=np.diag([lambda_plus,lambda_minus,1,g_v_phi_phi])
        thediag_4_no_curve=np.diag([1,-1,1,1])#no_curv

        product=np.dot(tW.T,np.dot(thediag,tW))

        product_4_4__ini_fluid__tw4=np.dot(tW_4.T,np.dot(thediag_4,tW_4)) # this should be the same as the_diag_4
        product_4_4__2nd_fluid__tw4=np.dot(tW_4_second_fluid.T,np.dot(thediag_4,tW_4_second_fluid)) # this should be the same as the_diag_4
        product_4_4__3rd_fluid__tw4=np.dot(tW_4_third_fluid.T,np.dot(thediag_4,tW_4_third_fluid)) # this should be the same as the_diag_4


        #interesting line below!!
        ####    g_v_t_X=v*(1-meshed_f)

        N_plus=np.sqrt((lambda_plus-1)**2+g_v_t_X**2)
        N_minus=np.sqrt((lambda_minus-1)**2+g_v_t_X**2)

        #################################
        # now we just examine the theta matrix
        #eigen_vect_g_doublcovariant=EVGDCov
        #{EVGDCov=theta matrix} is the matrix of eigen vectors of g_{mu,nu}

        EVGDCov=np.array([[(lambda_plus-1.0)/N_plus,(lambda_minus-1.0)/N_minus,0,0],\
                         [g_v_t_X/(1.0*N_plus),g_v_t_X/(1.0*N_minus),0,0],\
                         [0,0,1.,0],\
                         [0,0,0,1.]])
        the_product_should_be_diagonal=np.dot(EVGDCov.T,np.dot(g_v_mat,EVGDCov))

        ##################################

        # the following is contravariant (each nested array is not a desirable vector rather this is component 1 of the first array and then comp 1 of the second, etc.
        e_not_prime=np.array([[-1,0,0,0],\
                              [g_v_t_X,1,0,0],\
                              [0,0,1,0],\
                              [0,0,0,1/np.sqrt(g_v_phi_phi) ],\
        ])

        the_product_should_be_minkowski_metric=np.dot(e_not_prime.T,np.dot(g_v_mat,e_not_prime))


        minkowski_metric=np.diag([-1,1,1,1])
        e_prime=np.dot(EVGDCov,np.dot(tW_4,np.dot(EVGDCov.T,e_not_prime)))
        e_prime_2nd=np.dot(EVGDCov,np.dot(tW_4_second_fluid,np.dot(EVGDCov.T,e_not_prime)))
        e_prime_3rd=np.dot(EVGDCov,np.dot(tW_4_third_fluid,np.dot(EVGDCov.T,e_not_prime))) 
        # these 3 vectors have contravariant components

        e_prime_3rd_cov=np.dot(g_v_mat,e_prime_3rd)# in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures
        e_prime_2nd_cov=np.dot(g_v_mat,e_prime_2nd)# same as above



        # e_prime is described as sets of lines , each dissecting interesting vectors (vectors as columns)
        #e_prime here is contravariant
        #below we compute the covariant vectors in a single matrix


        e_prime_cov=np.dot(g_v_mat,e_prime)# in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures



        # we compute the mixed components of Einstein tensor
        G_einst_mixed_plus_lambda_delta=np.dot(g_n_mat,G_doubly_cov+Lambda_cosmological_constant_*g_v_mat )

        return np.max(np.abs(G_einst_mixed_plus_lambda_delta))


#################################################################################################
#
####################################################################################################

##############################################################################################################################################
def three_fluids__EOS_NEC_RANK___myfunc(g_n_t__t_jX,\
                                                       g_n__rho2_jX2,\
                                                       g__v_jn__phiphi,\
                                                       g_v_t__t_jX,\
                                                       g_v__rho2_jX2,\
                                                       meshed__cos_jsin__theta,\
                                                       sqrt_lambda__pl_jmin,\
                                                       G__t_t_jA,\
                                                       G__t__x_jrho,\
                                                       G__x__x_jrho,\
                                                       G_x__rho2_jphi2,\
                                                       hyp_coef_flu1_x__x_jt,\
                                                       hyp_coef_flu1__tt_jrhorho,\
                                                       hyp_coef_flu1__phiphi_jcstt,\
                                                       euc_coef_flu1_x__x_jt,\
                                                       euc_coef_flu1__tt_jrhorho,\
                                                       euc_coef_flu1__phiphi_jcstt,\
                                                       hyp_coef_flu2_x__x_jt,\
                                                       hyp_coef_flu2__tt_jrhorho,\
                                                       hyp_coef_flu2__phiphi_jcstt,\
                                                       euc_coef_flu2_x__x_jt,\
                                                       euc_coef_flu2__tt_jrhorho,\
                                                       euc_coef_flu2__phiphi_jcstt,\
                                                       hyp_coef_flu3_x__x_jt,\
                                                       hyp_coef_flu3__tt_jrhorho,\
                                                       hyp_coef_flu3__phiphi_jcstt,\
                                                       euc_coef_flu3_x__x_jt,\
                                                       euc_coef_flu3__tt_jrhorho,\
                                                       euc_coef_flu3__phiphi_jcstt,\
                                                       res_batch__1j_LAMBDA,\
):
    bad_inf_nan=0


    do_plot=False

    #1
    g_n_t_X=g_n_t__t_jX.imag
    g_n_t_t=g_n_t__t_jX.real

    #2
    g_n_X_X=g_n__rho2_jX2.imag
    g_n_rho_rho=g_n__rho2_jX2.real


    #3
    g_n_phi_phi=g__v_jn__phiphi.imag
    g_v_phi_phi=g__v_jn__phiphi.real



    #4
    g_v_t_X=g_v_t__t_jX.imag
    g_v_t_t=g_v_t__t_jX.real

    #5
    g_v_X_X=g_v__rho2_jX2.imag
    g_v_rho_rho=g_v__rho2_jX2.real

    #5
    meshed_sin_theta=meshed__cos_jsin__theta.imag
    meshed_cos_theta=meshed__cos_jsin__theta.real

    #6
    sqrt_lambda_minus=sqrt_lambda__pl_jmin.imag
    sqrt_lambda_plus=sqrt_lambda__pl_jmin.real

    #7
    A=G__t_t_jA.imag
    G__t_t=G__t_t_jA.real

    #8
    G__t_rho=G__t__x_jrho.imag
    G__t_x=G__t__x_jrho.real

    #9
    G__x_rho=G__x__x_jrho.imag
    G__x_x=G__x__x_jrho.real

    #10
    G__phi_phi=G_x__rho2_jphi2.imag
    G__rho_rho=G_x__rho2_jphi2.real

    # there are seven tt tx trho , xx xrho, rhorho, phiphi
    #11


    hyp_flu1_x_t=hyp_coef_flu1_x__x_jt.imag
    hyp_flu1_x_x=hyp_coef_flu1_x__x_jt.real

    #12
    hyp_flu1_rho_rho=hyp_coef_flu1__tt_jrhorho.imag
    hyp_flu1_t_t=hyp_coef_flu1__tt_jrhorho.real

    #13
    hyp_flu1_cstt=hyp_coef_flu1__phiphi_jcstt.imag
    hyp_flu1_phiphi=hyp_coef_flu1__phiphi_jcstt.real

    #14

    euc_flu1_x_t=euc_coef_flu1_x__x_jt.imag
    euc_flu1_x_x=euc_coef_flu1_x__x_jt.real


    euc_flu1_rho_rho=euc_coef_flu1__tt_jrhorho.imag
    euc_flu1_t_t=euc_coef_flu1__tt_jrhorho.real


    euc_flu1_cstt=euc_coef_flu1__phiphi_jcstt.imag
    euc_flu1_phiphi=euc_coef_flu1__phiphi_jcstt.real
    ################################
    hyp_flu2_x_t=hyp_coef_flu2_x__x_jt.imag
    hyp_flu2_x_x=hyp_coef_flu2_x__x_jt.real


    hyp_flu2_rho_rho=hyp_coef_flu2__tt_jrhorho.imag
    hyp_flu2_t_t=hyp_coef_flu2__tt_jrhorho.real


    hyp_flu2_cstt=hyp_coef_flu2__phiphi_jcstt.imag
    hyp_flu2_phiphi=hyp_coef_flu2__phiphi_jcstt.real



    euc_flu2_x_t=euc_coef_flu2_x__x_jt.imag
    euc_flu2_x_x=euc_coef_flu2_x__x_jt.real


    euc_flu2_rho_rho=euc_coef_flu2__tt_jrhorho.imag
    euc_flu2_t_t=euc_coef_flu2__tt_jrhorho.real


    euc_flu2_cstt=euc_coef_flu2__phiphi_jcstt.imag
    euc_flu2_phiphi=euc_coef_flu2__phiphi_jcstt.real

    ##################################
    hyp_flu3_x_t=hyp_coef_flu3_x__x_jt.imag
    hyp_flu3_x_x=hyp_coef_flu3_x__x_jt.real


    hyp_flu3_rho_rho=hyp_coef_flu3__tt_jrhorho.imag
    hyp_flu3_t_t=hyp_coef_flu3__tt_jrhorho.real


    hyp_flu3_cstt=hyp_coef_flu3__phiphi_jcstt.imag
    hyp_flu3_phiphi=hyp_coef_flu3__phiphi_jcstt.real



    euc_flu3_x_t=euc_coef_flu3_x__x_jt.imag
    euc_flu3_x_x=euc_coef_flu3_x__x_jt.real


    euc_flu3_rho_rho=euc_coef_flu3__tt_jrhorho.imag
    euc_flu3_t_t=euc_coef_flu3__tt_jrhorho.real


    euc_flu3_cstt=euc_coef_flu3__phiphi_jcstt.imag
    euc_flu3_phiphi=euc_coef_flu3__phiphi_jcstt.real





    #29
    res_batch=res_batch__1j_LAMBDA.real
    Lambda_cosmological_constant_=res_batch__1j_LAMBDA.imag







    #do_print_stuff=True
    do_print_stuff=False

    #do_print_stuff2=True
    do_print_stuff2=False


    do_print_stuff3=False
    inputs={}

    do_print_stuff4=False

    do_print_stuff5=False
    ######################################################
    # correction sur g_phi,phi
    #below
    if g_n_phi_phi==0 or np.isinf(g_n_phi_phi):
        g_n_phi_phi=1

    if g_v_phi_phi==0 or np.isinf(g_v_phi_phi):
        g_v_phi_phi=1

    if np.isnan(g_v_phi_phi) or np.isnan(g_n_phi_phi):
        g_n_phi_phi=1
        g_v_phi_phi=1



    inputs["g_n_t_t"]=g_n_t_t
    inputs["g_n_t_X"]=g_n_t_X
    inputs["g_n_X_X"]=g_n_X_X
    inputs["g_n_rho_rho"]=g_n_rho_rho
    inputs["g_n_phi_phi"]=g_n_phi_phi


    inputs["g_v_t_t"]=g_v_t_t
    inputs["g_v_t_X"]=g_v_t_X
    inputs["g_v_X_X"]=g_v_X_X
    inputs["g_v_rho_rho"]=g_v_rho_rho
    inputs["g_v_phi_phi"]=g_v_phi_phi

    inputs["sqrt_lambda_minus"]=sqrt_lambda_minus
    inputs["sqrt_lambda_plus"]=sqrt_lambda_plus



    ########################################################
    # we compute G_doubly covariant!!
    G_doubly_cov=np.array( [ [ G__t_t   , G__t_x   , G__t_rho   , 0 ],\
                             [ G__t_x   , G__x_x   , G__x_rho   , 0 ],\
                             [ G__t_rho , G__x_rho , G__rho_rho , 0 ],\
                             [ 0        , 0        , 0          , G__phi_phi ],\
    ])
    #######################################################
    # below are the numpy equivalents of lines / rows
    g_n_mat=np.array([[inputs["g_n_t_t"],inputs["g_n_t_X"],0,0],\
                      [inputs["g_n_t_X"],inputs["g_n_X_X"],0,0],\
                      [0                ,0                ,inputs["g_n_rho_rho"],0],\
                      [0                ,0                ,0                    ,inputs["g_n_phi_phi"]]])

    g_v_mat=np.array([[inputs["g_v_t_t"],inputs["g_v_t_X"],0,0],\
                      [inputs["g_v_t_X"],inputs["g_v_X_X"],0,0],\
                      [0                ,0                ,inputs["g_v_rho_rho"],0],\
                      [0                ,0                ,0                    ,inputs["g_v_phi_phi"]]])



    lambda_plus=(-(A-1)+np.sqrt((A-1)**2+4))/2.
    lambda_minus=(-(A-1)-np.sqrt((A-1)**2+4))/2.
    tW=np.array([[meshed_cos_theta,0,meshed_sin_theta*1./sqrt_lambda_plus],\
                 [0,1,0],\
                 [-meshed_sin_theta*sqrt_lambda_plus,0,meshed_cos_theta],\
                 ])

    return_to_starting_block=True
    if 1:


        #print('choose tw4 ---> make a function which creates such matrices \n line 1640')



        tW_4,\
            tW_4_rot_no_curv,\
            tW_4_boo_no_curv,\
            iter_overflow=three_fluids___rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu1_x_t,\
                                                                     hyp_flu1_x_x,\
                                                                     hyp_flu1_rho_rho,\
                                                                     hyp_flu1_t_t,\
                                                                     hyp_flu1_cstt,\
                                                                     hyp_flu1_phiphi,\
                                                                     euc_flu1_x_t,\
                                                                     euc_flu1_x_x,\
                                                                     euc_flu1_rho_rho,\
                                                                     euc_flu1_t_t,\
                                                                     euc_flu1_cstt,\
                                                                     euc_flu1_phiphi,\
                                                                     inputs,\
        )

        tW_4_second_fluid,\
            tW_4_second_fluid_rot_no_curv,\
            tW_4_second_fluid_boo_no_curv,\
            iter_overflow_second=three_fluids___rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu2_x_t,\
                                                                                  hyp_flu2_x_x,\
                                                                                  hyp_flu2_rho_rho,\
                                                                                  hyp_flu2_t_t,\
                                                                                  hyp_flu2_cstt,\
                                                                                  hyp_flu2_phiphi,\
                                                                                  euc_flu2_x_t,\
                                                                                  euc_flu2_x_x,\
                                                                                  euc_flu2_rho_rho,\
                                                                                  euc_flu2_t_t,\
                                                                                  euc_flu2_cstt,\
                                                                                  euc_flu2_phiphi,\
                                                                                  inputs,\
        )

        tW_4_third_fluid,\
            tW_4_third_fluid_rot_no_curv,\
            tW_4_third_fluid_boo_no_curv,\
            iter_overflow_third=three_fluids___rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu3_x_t,\
                                                                                 hyp_flu3_x_x,\
                                                                                 hyp_flu3_rho_rho,\
                                                                                 hyp_flu3_t_t,\
                                                                                 hyp_flu3_cstt,\
                                                                                 hyp_flu3_phiphi,\
                                                                                 euc_flu3_x_t,\
                                                                                 euc_flu3_x_x,\
                                                                                 euc_flu3_rho_rho,\
                                                                                 euc_flu3_t_t,\
                                                                                 euc_flu3_cstt,\
                                                                                 euc_flu3_phiphi,\
                                                                                 inputs,\
        )



        #below is the g_munu diagonal version
        thediag=np.diag([lambda_plus,lambda_minus,1])
        thediag_4=np.diag([lambda_plus,lambda_minus,1,g_v_phi_phi])
        thediag_4_no_curve=np.diag([1,-1,1,1])#no_curv

        product=np.dot(tW.T,np.dot(thediag,tW))

        product_4_4__ini_fluid__tw4=np.dot(tW_4.T,np.dot(thediag_4,tW_4)) # this should be the same as the_diag_4
        product_4_4__2nd_fluid__tw4=np.dot(tW_4_second_fluid.T,np.dot(thediag_4,tW_4_second_fluid)) # this should be the same as the_diag_4
        product_4_4__3rd_fluid__tw4=np.dot(tW_4_third_fluid.T,np.dot(thediag_4,tW_4_third_fluid)) # this should be the same as the_diag_4


        #interesting line below!!
        ####    g_v_t_X=v*(1-meshed_f)

        N_plus=np.sqrt((lambda_plus-1)**2+g_v_t_X**2)
        N_minus=np.sqrt((lambda_minus-1)**2+g_v_t_X**2)

        #################################
        # now we just examine the theta matrix
        #eigen_vect_g_doublcovariant=EVGDCov
        #{EVGDCov=theta matrix} is the matrix of eigen vectors of g_{mu,nu}

        EVGDCov=np.array([[(lambda_plus-1.0)/N_plus,(lambda_minus-1.0)/N_minus,0,0],\
                         [g_v_t_X/(1.0*N_plus),g_v_t_X/(1.0*N_minus),0,0],\
                         [0,0,1.,0],\
                         [0,0,0,1.]])
        the_product_should_be_diagonal=np.dot(EVGDCov.T,np.dot(g_v_mat,EVGDCov))

        ##################################

        # the following is contravariant (each nested array is not a desirable vector rather this is component 1 of the first array and then comp 1 of the second, etc.
        e_not_prime=np.array([[-1,0,0,0],\
                              [g_v_t_X,1,0,0],\
                              [0,0,1,0],\
                              [0,0,0,1/np.sqrt(g_v_phi_phi) ],\
        ])

        the_product_should_be_minkowski_metric=np.dot(e_not_prime.T,np.dot(g_v_mat,e_not_prime))


        minkowski_metric=np.diag([-1,1,1,1])
        e_prime=np.dot(EVGDCov,np.dot(tW_4,np.dot(EVGDCov.T,e_not_prime)))
        e_prime_2nd=np.dot(EVGDCov,np.dot(tW_4_second_fluid,np.dot(EVGDCov.T,e_not_prime)))
        e_prime_3rd=np.dot(EVGDCov,np.dot(tW_4_third_fluid,np.dot(EVGDCov.T,e_not_prime))) 
        # these 3 vectors have contravariant components

        e_prime_3rd_cov=np.dot(g_v_mat,e_prime_3rd)# in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures
        e_prime_2nd_cov=np.dot(g_v_mat,e_prime_2nd)# same as above



        # e_prime is described as sets of lines , each dissecting interesting vectors (vectors as columns)
        #e_prime here is contravariant
        #below we compute the covariant vectors in a single matrix


        e_prime_cov=np.dot(g_v_mat,e_prime)# in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures



        # we compute the mixed components of Einstein tensor
        G_einst_mixed_plus_lambda_delta=np.dot(g_n_mat,G_doubly_cov+Lambda_cosmological_constant_*g_v_mat )






        ####################################################################





        ################################################


        #################################################
        #################################################



        basis_vectors_3fluids=[[e_prime_cov,e_prime],[e_prime_2nd_cov,e_prime_2nd],[e_prime_3rd_cov,e_prime_3rd]]
        big_matrix_3flu,gee_vector_3flu,overflow_big_mat=compute_big_matrix_for_fluids(basis_vectors_3fluids,G_einst_mixed_plus_lambda_delta)


        if np.any(np.isnan(big_matrix_3flu)) or np.any(np.isinf(big_matrix_3flu)) or \
           np.any(np.isnan(gee_vector_3flu)) or np.any(np.isinf(gee_vector_3flu)):
            return_to_starting_block=True

            hyp_flu1_x_t=hyp_flu1_x_t/3.
            hyp_flu1_x_x=hyp_flu1_x_x/3.
            hyp_flu1_rho_rho=hyp_flu1_rho_rho/3.
            hyp_flu1_t_t=hyp_flu1_t_t/3.
            hyp_flu1_cstt=hyp_flu1_cstt/3.
            hyp_flu1_phiphi=hyp_flu1_phiphi/3.

            hyp_flu2_x_t=hyp_flu2_x_t/3.
            hyp_flu2_x_x=hyp_flu2_x_x/3.
            hyp_flu2_rho_rho=hyp_flu2_rho_rho/3.
            hyp_flu2_t_t=hyp_flu2_t_t/3.
            hyp_flu2_cstt=hyp_flu2_cstt/3.
            hyp_flu2_phiphi=hyp_flu2_phiphi/3.

            hyp_flu3_x_t=hyp_flu3_x_t/3.
            hyp_flu3_x_x=hyp_flu3_x_x/3.
            hyp_flu3_rho_rho=hyp_flu3_rho_rho/3.
            hyp_flu3_t_t=hyp_flu3_t_t/3.
            hyp_flu3_cstt=hyp_flu3_cstt/3.
            hyp_flu3_phiphi=hyp_flu3_phiphi/3.
            # above if inf or nan we decrease the values of all coefficients

            bad_inf_nan+=1
            
            densities_pressures_3fluids=[-11,-11,-11,-11,-11,-11,-11,-11,-11,-11,-11,-11]
            
            matrix_rank_3fluids=-1
            

        else:
            return_to_starting_block=False
            densities_pressures_3fluids,\
                residuals_3fluids,\
                matrix_rank_3fluids,\
                sing_values_matrix_3fluids=np.linalg.lstsq(big_matrix_3flu, gee_vector_3flu,rcond=None)

    ##############################################################


    ##############################################################################################################################




    res=list(densities_pressures_3fluids)



    ######################
    # we compute equation of states below
    if res[0]!=0:
        w_1=(res[1]+res[2]+res[3])/(1.0*res[0])
    else:
        w_1=0

    if res[4]!=0:
        w_2=(res[5]+res[6]+res[7])/(1.0*res[4])
    else:
        w_2=0

    if res[8]!=0:
        w_3=(res[9]+res[10]+res[11])/(1.0*res[8])
    else:
        w_3=0
    MW=np.max(np.abs([w_1,w_2,w_3])) # max of equation of states (w= EOS)

    

    density_plus_pressure=[res[0]+res[1],res[0]+res[2],res[0]+res[3],\
                           res[4]+res[5],res[4]+res[6],res[4]+res[7],\
                           res[8]+res[9],res[8]+res[10],res[8]+res[11],\
    ]

    min__density_plus_pressure=np.min(density_plus_pressure)
    if min__density_plus_pressure<0:
        pass
    else:
        min__density_plus_pressure=0

    
    
    return MW+1j*0,min__density_plus_pressure+1j*(matrix_rank_3fluids-8*(iter_overflow_third+iter_overflow_second+iter_overflow+bad_inf_nan+overflow_big_mat))
#################################################################################################






#############################################################################################################################################


def two_fluids__gammas_and_gamma_relation___myfunc(g_n_t__t_jX,\
                                                       g_n__rho2_jX2,\
                                                       g__v_jn__phiphi,\
                                                       g_v_t__t_jX,\
                                                       g_v__rho2_jX2,\
                                                       meshed__cos_jsin__theta,\
                                                       sqrt_lambda__pl_jmin,\
                                                       G__t_t_jA,\
                                                       G__t__x_jrho,\
                                                       G__x__x_jrho,\
                                                       G_x__rho2_jphi2,\
                                                       hyp_coef_flu1_x__x_jt,\
                                                       hyp_coef_flu1__tt_jrhorho,\
                                                       hyp_coef_flu1__phiphi_jcstt,\
                                                       euc_coef_flu1_x__x_jt,\
                                                       euc_coef_flu1__tt_jrhorho,\
                                                       euc_coef_flu1__phiphi_jcstt,\
                                                       hyp_coef_flu2_x__x_jt,\
                                                       hyp_coef_flu2__tt_jrhorho,\
                                                       hyp_coef_flu2__phiphi_jcstt,\
                                                       euc_coef_flu2_x__x_jt,\
                                                       euc_coef_flu2__tt_jrhorho,\
                                                       euc_coef_flu2__phiphi_jcstt,\
                                                       hyp_coef_flu3_x__x_jt,\
                                                       hyp_coef_flu3__tt_jrhorho,\
                                                       hyp_coef_flu3__phiphi_jcstt,\
                                                       euc_coef_flu3_x__x_jt,\
                                                       euc_coef_flu3__tt_jrhorho,\
                                                       euc_coef_flu3__phiphi_jcstt,\
):


    

        do_plot=False

        #1
        Lambda_cosmological_constant_=0
        #1.11e-52 #(m^(-2))

        g_n_t_X=g_n_t__t_jX.imag
        g_n_t_t=g_n_t__t_jX.real

        #2
        g_n_X_X=g_n__rho2_jX2.imag
        g_n_rho_rho=g_n__rho2_jX2.real


        #3
        g_n_phi_phi=g__v_jn__phiphi.imag
        g_v_phi_phi=g__v_jn__phiphi.real



        #4
        g_v_t_X=g_v_t__t_jX.imag
        g_v_t_t=g_v_t__t_jX.real

        #5
        g_v_X_X=g_v__rho2_jX2.imag
        g_v_rho_rho=g_v__rho2_jX2.real

        #5
        meshed_sin_theta=meshed__cos_jsin__theta.imag
        meshed_cos_theta=meshed__cos_jsin__theta.real

        #6
        sqrt_lambda_minus=sqrt_lambda__pl_jmin.imag
        sqrt_lambda_plus=sqrt_lambda__pl_jmin.real

        #7
        A=G__t_t_jA.imag
        G__t_t=G__t_t_jA.real

        #8
        G__t_rho=G__t__x_jrho.imag
        G__t_x=G__t__x_jrho.real

        #9
        G__x_rho=G__x__x_jrho.imag
        G__x_x=G__x__x_jrho.real

        #10
        G__phi_phi=G_x__rho2_jphi2.imag
        G__rho_rho=G_x__rho2_jphi2.real

        # there are seven tt tx trho , xx xrho, rhorho, phiphi
        #11


        hyp_flu1_x_t=hyp_coef_flu1_x__x_jt.imag
        hyp_flu1_x_x=hyp_coef_flu1_x__x_jt.real

        #12
        hyp_flu1_rho_rho=hyp_coef_flu1__tt_jrhorho.imag
        hyp_flu1_t_t=hyp_coef_flu1__tt_jrhorho.real

        #13
        hyp_flu1_cstt=hyp_coef_flu1__phiphi_jcstt.imag
        hyp_flu1_phiphi=hyp_coef_flu1__phiphi_jcstt.real

        #14

        euc_flu1_x_t=euc_coef_flu1_x__x_jt.imag
        euc_flu1_x_x=euc_coef_flu1_x__x_jt.real


        euc_flu1_rho_rho=euc_coef_flu1__tt_jrhorho.imag
        euc_flu1_t_t=euc_coef_flu1__tt_jrhorho.real


        euc_flu1_cstt=euc_coef_flu1__phiphi_jcstt.imag
        euc_flu1_phiphi=euc_coef_flu1__phiphi_jcstt.real
        ################################
        hyp_flu2_x_t=hyp_coef_flu2_x__x_jt.imag
        hyp_flu2_x_x=hyp_coef_flu2_x__x_jt.real


        hyp_flu2_rho_rho=hyp_coef_flu2__tt_jrhorho.imag
        hyp_flu2_t_t=hyp_coef_flu2__tt_jrhorho.real


        hyp_flu2_cstt=hyp_coef_flu2__phiphi_jcstt.imag
        hyp_flu2_phiphi=hyp_coef_flu2__phiphi_jcstt.real



        euc_flu2_x_t=euc_coef_flu2_x__x_jt.imag
        euc_flu2_x_x=euc_coef_flu2_x__x_jt.real


        euc_flu2_rho_rho=euc_coef_flu2__tt_jrhorho.imag
        euc_flu2_t_t=euc_coef_flu2__tt_jrhorho.real


        euc_flu2_cstt=euc_coef_flu2__phiphi_jcstt.imag
        euc_flu2_phiphi=euc_coef_flu2__phiphi_jcstt.real

        ##################################
        hyp_flu3_x_t=hyp_coef_flu3_x__x_jt.imag
        hyp_flu3_x_x=hyp_coef_flu3_x__x_jt.real


        hyp_flu3_rho_rho=hyp_coef_flu3__tt_jrhorho.imag
        hyp_flu3_t_t=hyp_coef_flu3__tt_jrhorho.real


        hyp_flu3_cstt=hyp_coef_flu3__phiphi_jcstt.imag
        hyp_flu3_phiphi=hyp_coef_flu3__phiphi_jcstt.real



        euc_flu3_x_t=euc_coef_flu3_x__x_jt.imag
        euc_flu3_x_x=euc_coef_flu3_x__x_jt.real


        euc_flu3_rho_rho=euc_coef_flu3__tt_jrhorho.imag
        euc_flu3_t_t=euc_coef_flu3__tt_jrhorho.real


        euc_flu3_cstt=euc_coef_flu3__phiphi_jcstt.imag
        euc_flu3_phiphi=euc_coef_flu3__phiphi_jcstt.real





        #29
        # res_batch=res_batch__1j_LAMBDA.real
        # Lambda_cosmological_constant_=res_batch__1j_LAMBDA.imag

        #do_print_stuff=True
        do_print_stuff=False

        #do_print_stuff2=True
        do_print_stuff2=False


        do_print_stuff3=False
        inputs={}

        do_print_stuff4=False

        do_print_stuff5=False
        #do_print_stuff5=True
        ######################################################
        # correction on g_phi,phi
        #below
        if g_n_phi_phi==0 or np.isinf(g_n_phi_phi):
            g_n_phi_phi=1

        if g_v_phi_phi==0 or np.isinf(g_v_phi_phi):
            g_v_phi_phi=1

        if np.isnan(g_v_phi_phi) or np.isnan(g_n_phi_phi):
            g_n_phi_phi=1
            g_v_phi_phi=1



        inputs["g_n_t_t"]=g_n_t_t
        inputs["g_n_t_X"]=g_n_t_X
        inputs["g_n_X_X"]=g_n_X_X
        inputs["g_n_rho_rho"]=g_n_rho_rho
        inputs["g_n_phi_phi"]=g_n_phi_phi


        inputs["g_v_t_t"]=g_v_t_t
        inputs["g_v_t_X"]=g_v_t_X
        inputs["g_v_X_X"]=g_v_X_X
        inputs["g_v_rho_rho"]=g_v_rho_rho
        inputs["g_v_phi_phi"]=g_v_phi_phi

        inputs["sqrt_lambda_minus"]=sqrt_lambda_minus
        inputs["sqrt_lambda_plus"]=sqrt_lambda_plus



        ########################################################
        # we compute G_doubly covariant!!
        G_doubly_cov=np.array( [ [ G__t_t   , G__t_x   , G__t_rho   , 0 ],\
                                 [ G__t_x   , G__x_x   , G__x_rho   , 0 ],\
                                 [ G__t_rho , G__x_rho , G__rho_rho , 0 ],\
                                 [ 0        , 0        , 0          , G__phi_phi ],\
        ])
        #######################################################
        # below are the numpy equivalents of lines / rows

        g_n_mat=np.array([[inputs["g_n_t_t"],inputs["g_n_t_X"],0,0],\
                          [inputs["g_n_t_X"],inputs["g_n_X_X"],0,0],\
                          [0                ,0                ,inputs["g_n_rho_rho"],0],\
                          [0                ,0                ,0                    ,inputs["g_n_phi_phi"]]])

        g_v_mat=np.array([[inputs["g_v_t_t"],inputs["g_v_t_X"],0,0],\
                          [inputs["g_v_t_X"],inputs["g_v_X_X"],0,0],\
                          [0                ,0                ,inputs["g_v_rho_rho"],0],\
                          [0                ,0                ,0                    ,inputs["g_v_phi_phi"]]])



        lambda_plus=(-(A-1)+np.sqrt((A-1)**2+4))/2.
        lambda_minus=(-(A-1)-np.sqrt((A-1)**2+4))/2.
        tW=np.array([[meshed_cos_theta,0,meshed_sin_theta*1./sqrt_lambda_plus],\
                     [0,1,0],\
                     [-meshed_sin_theta*sqrt_lambda_plus,0,meshed_cos_theta],\
                     ])

        return_to_starting_block=True
        overflow_counting=0

        if 1:
            #print('choose tw4 ---> make a function which creates such matrices \n line 1640')



            biggest_gamma_1st_fluid,\
                biggest_gamma_relation_1st_fluid,\
                iter_overflow_1st_fluid=two_fluids___rotation_euc_hyp__g_v__choice_tW_matrix___OUTPUT_GAMMAS_and_relations(euc_flu3_x_t,\
                                                                      euc_flu3_x_x,\
                                                                      euc_flu3_rho_rho,\
                                                                      euc_flu3_t_t,\
                                                                      euc_flu3_cstt,\
                                                                      euc_flu3_phiphi,\
                                                                      hyp_flu1_x_t,\
                                                                      hyp_flu1_x_x,\
                                                                      hyp_flu1_rho_rho,\
                                                                      hyp_flu1_t_t,\
                                                                      hyp_flu1_cstt,\
                                                                      hyp_flu1_phiphi,\
                                                                      euc_flu1_x_t,\
                                                                      euc_flu1_x_x,\
                                                                      euc_flu1_rho_rho,\
                                                                      euc_flu1_t_t,\
                                                                      euc_flu1_cstt,\
                                                                      euc_flu1_phiphi,\
                                                                      inputs,\
            )

            biggest_gamma_2nd_fluid,\
                biggest_gamma_relation_2nd_fluid,\
                iter_overflow_2nd_fluid=two_fluids___rotation_euc_hyp__g_v__choice_tW_matrix___OUTPUT_GAMMAS_and_relations(hyp_flu3_x_t,\
                                                                             hyp_flu3_x_x,\
                                                                             hyp_flu3_rho_rho,\
                                                                             hyp_flu3_t_t,\
                                                                             hyp_flu3_cstt,\
                                                                             hyp_flu3_phiphi,\
                                                                             hyp_flu2_x_t,\
                                                                             hyp_flu2_x_x,\
                                                                             hyp_flu2_rho_rho,\
                                                                             hyp_flu2_t_t,\
                                                                             hyp_flu2_cstt,\
                                                                             hyp_flu2_phiphi,\
                                                                             euc_flu2_x_t,\
                                                                             euc_flu2_x_x,\
                                                                             euc_flu2_rho_rho,\
                                                                             euc_flu2_t_t,\
                                                                             euc_flu2_cstt,\
                                                                             euc_flu2_phiphi,\
                                                                             inputs,\
            )


 





            
    
            return biggest_gamma_1st_fluid+1j*biggest_gamma_relation_1st_fluid,\
                biggest_gamma_2nd_fluid+1j*biggest_gamma_relation_2nd_fluid

        
        print('line 438-6--')
        raise
#################################################################################################

#############################################################################################################################################



def two_fluids__extract_velocity_unitarity___myfunc(g_n_t__t_jX,\
                                                       g_n__rho2_jX2,\
                                                       g__v_jn__phiphi,\
                                                       g_v_t__t_jX,\
                                                       g_v__rho2_jX2,\
                                                       meshed__cos_jsin__theta,\
                                                       sqrt_lambda__pl_jmin,\
                                                       G__t_t_jA,\
                                                       G__t__x_jrho,\
                                                       G__x__x_jrho,\
                                                       G_x__rho2_jphi2,\
                                                       hyp_coef_flu1_x__x_jt,\
                                                       hyp_coef_flu1__tt_jrhorho,\
                                                       hyp_coef_flu1__phiphi_jcstt,\
                                                       euc_coef_flu1_x__x_jt,\
                                                       euc_coef_flu1__tt_jrhorho,\
                                                       euc_coef_flu1__phiphi_jcstt,\
                                                       hyp_coef_flu2_x__x_jt,\
                                                       hyp_coef_flu2__tt_jrhorho,\
                                                       hyp_coef_flu2__phiphi_jcstt,\
                                                       euc_coef_flu2_x__x_jt,\
                                                       euc_coef_flu2__tt_jrhorho,\
                                                       euc_coef_flu2__phiphi_jcstt,\
                                                       hyp_coef_flu3_x__x_jt,\
                                                       hyp_coef_flu3__tt_jrhorho,\
                                                       hyp_coef_flu3__phiphi_jcstt,\
                                                       euc_coef_flu3_x__x_jt,\
                                                       euc_coef_flu3__tt_jrhorho,\
                                                       euc_coef_flu3__phiphi_jcstt,\
):


    

        do_plot=False

        #1
        Lambda_cosmological_constant_=0
        #1.11e-52 #(m^(-2))
        g_n_t_X=g_n_t__t_jX.imag
        g_n_t_t=g_n_t__t_jX.real

        #2
        g_n_X_X=g_n__rho2_jX2.imag
        g_n_rho_rho=g_n__rho2_jX2.real


        #3
        g_n_phi_phi=g__v_jn__phiphi.imag
        g_v_phi_phi=g__v_jn__phiphi.real



        #4
        g_v_t_X=g_v_t__t_jX.imag
        g_v_t_t=g_v_t__t_jX.real

        #5
        g_v_X_X=g_v__rho2_jX2.imag
        g_v_rho_rho=g_v__rho2_jX2.real

        #5
        meshed_sin_theta=meshed__cos_jsin__theta.imag
        meshed_cos_theta=meshed__cos_jsin__theta.real

        #6
        sqrt_lambda_minus=sqrt_lambda__pl_jmin.imag
        sqrt_lambda_plus=sqrt_lambda__pl_jmin.real

        #7
        A=G__t_t_jA.imag
        G__t_t=G__t_t_jA.real

        #8
        G__t_rho=G__t__x_jrho.imag
        G__t_x=G__t__x_jrho.real

        #9
        G__x_rho=G__x__x_jrho.imag
        G__x_x=G__x__x_jrho.real

        #10
        G__phi_phi=G_x__rho2_jphi2.imag
        G__rho_rho=G_x__rho2_jphi2.real

        # there are seven tt tx trho , xx xrho, rhorho, phiphi
        #11


        hyp_flu1_x_t=hyp_coef_flu1_x__x_jt.imag
        hyp_flu1_x_x=hyp_coef_flu1_x__x_jt.real

        #12
        hyp_flu1_rho_rho=hyp_coef_flu1__tt_jrhorho.imag
        hyp_flu1_t_t=hyp_coef_flu1__tt_jrhorho.real

        #13
        hyp_flu1_cstt=hyp_coef_flu1__phiphi_jcstt.imag
        hyp_flu1_phiphi=hyp_coef_flu1__phiphi_jcstt.real

        #14

        euc_flu1_x_t=euc_coef_flu1_x__x_jt.imag
        euc_flu1_x_x=euc_coef_flu1_x__x_jt.real


        euc_flu1_rho_rho=euc_coef_flu1__tt_jrhorho.imag
        euc_flu1_t_t=euc_coef_flu1__tt_jrhorho.real


        euc_flu1_cstt=euc_coef_flu1__phiphi_jcstt.imag
        euc_flu1_phiphi=euc_coef_flu1__phiphi_jcstt.real
        ################################
        hyp_flu2_x_t=hyp_coef_flu2_x__x_jt.imag
        hyp_flu2_x_x=hyp_coef_flu2_x__x_jt.real


        hyp_flu2_rho_rho=hyp_coef_flu2__tt_jrhorho.imag
        hyp_flu2_t_t=hyp_coef_flu2__tt_jrhorho.real


        hyp_flu2_cstt=hyp_coef_flu2__phiphi_jcstt.imag
        hyp_flu2_phiphi=hyp_coef_flu2__phiphi_jcstt.real



        euc_flu2_x_t=euc_coef_flu2_x__x_jt.imag
        euc_flu2_x_x=euc_coef_flu2_x__x_jt.real


        euc_flu2_rho_rho=euc_coef_flu2__tt_jrhorho.imag
        euc_flu2_t_t=euc_coef_flu2__tt_jrhorho.real


        euc_flu2_cstt=euc_coef_flu2__phiphi_jcstt.imag
        euc_flu2_phiphi=euc_coef_flu2__phiphi_jcstt.real

        ##################################
        hyp_flu3_x_t=hyp_coef_flu3_x__x_jt.imag
        hyp_flu3_x_x=hyp_coef_flu3_x__x_jt.real


        hyp_flu3_rho_rho=hyp_coef_flu3__tt_jrhorho.imag
        hyp_flu3_t_t=hyp_coef_flu3__tt_jrhorho.real


        hyp_flu3_cstt=hyp_coef_flu3__phiphi_jcstt.imag
        hyp_flu3_phiphi=hyp_coef_flu3__phiphi_jcstt.real



        euc_flu3_x_t=euc_coef_flu3_x__x_jt.imag
        euc_flu3_x_x=euc_coef_flu3_x__x_jt.real


        euc_flu3_rho_rho=euc_coef_flu3__tt_jrhorho.imag
        euc_flu3_t_t=euc_coef_flu3__tt_jrhorho.real


        euc_flu3_cstt=euc_coef_flu3__phiphi_jcstt.imag
        euc_flu3_phiphi=euc_coef_flu3__phiphi_jcstt.real





        #29
        # res_batch=res_batch__1j_LAMBDA.real
        # Lambda_cosmological_constant_=res_batch__1j_LAMBDA.imag

        #do_print_stuff=True
        do_print_stuff=False

        #do_print_stuff2=True
        do_print_stuff2=False


        do_print_stuff3=False
        inputs={}

        do_print_stuff4=False

        do_print_stuff5=False
        #do_print_stuff5=True
        ######################################################
        # correction on g_phi,phi
        #below
        if g_n_phi_phi==0 or np.isinf(g_n_phi_phi):
            g_n_phi_phi=1

        if g_v_phi_phi==0 or np.isinf(g_v_phi_phi):
            g_v_phi_phi=1

        if np.isnan(g_v_phi_phi) or np.isnan(g_n_phi_phi):
            g_n_phi_phi=1
            g_v_phi_phi=1



        inputs["g_n_t_t"]=g_n_t_t
        inputs["g_n_t_X"]=g_n_t_X
        inputs["g_n_X_X"]=g_n_X_X
        inputs["g_n_rho_rho"]=g_n_rho_rho
        inputs["g_n_phi_phi"]=g_n_phi_phi


        inputs["g_v_t_t"]=g_v_t_t
        inputs["g_v_t_X"]=g_v_t_X
        inputs["g_v_X_X"]=g_v_X_X
        inputs["g_v_rho_rho"]=g_v_rho_rho
        inputs["g_v_phi_phi"]=g_v_phi_phi

        inputs["sqrt_lambda_minus"]=sqrt_lambda_minus
        inputs["sqrt_lambda_plus"]=sqrt_lambda_plus



        ########################################################
        # we compute G_doubly covariant!!
        G_doubly_cov=np.array( [ [ G__t_t   , G__t_x   , G__t_rho   , 0 ],\
                                 [ G__t_x   , G__x_x   , G__x_rho   , 0 ],\
                                 [ G__t_rho , G__x_rho , G__rho_rho , 0 ],\
                                 [ 0        , 0        , 0          , G__phi_phi ],\
        ])
        #######################################################
        # below are the numpy equivalents of lines / rows

        g_n_mat=np.array([[inputs["g_n_t_t"],inputs["g_n_t_X"],0,0],\
                          [inputs["g_n_t_X"],inputs["g_n_X_X"],0,0],\
                          [0                ,0                ,inputs["g_n_rho_rho"],0],\
                          [0                ,0                ,0                    ,inputs["g_n_phi_phi"]]])

        g_v_mat=np.array([[inputs["g_v_t_t"],inputs["g_v_t_X"],0,0],\
                          [inputs["g_v_t_X"],inputs["g_v_X_X"],0,0],\
                          [0                ,0                ,inputs["g_v_rho_rho"],0],\
                          [0                ,0                ,0                    ,inputs["g_v_phi_phi"]]])



        lambda_plus=(-(A-1)+np.sqrt((A-1)**2+4))/2.
        lambda_minus=(-(A-1)-np.sqrt((A-1)**2+4))/2.
        tW=np.array([[meshed_cos_theta,0,meshed_sin_theta*1./sqrt_lambda_plus],\
                     [0,1,0],\
                     [-meshed_sin_theta*sqrt_lambda_plus,0,meshed_cos_theta],\
                     ])

        return_to_starting_block=True
        overflow_counting=0


        inputs['g_v_mat']=g_v_mat



        #interesting line below!!
        ####    g_v_t_X=v*(1-meshed_f)

        N_plus=np.sqrt((lambda_plus-1)**2+g_v_t_X**2)
        N_minus=np.sqrt((lambda_minus-1)**2+g_v_t_X**2)

        #################################
        # now we just examine the theta matrix
        #eigen_vect_g_doublcovariant=EVGDCov
        #{EVGDCov=theta matrix} is the matrix of eigen vectors of g_{mu,nu}

        EVGDCov=np.array([[(lambda_plus-1.0)/N_plus,(lambda_minus-1.0)/N_minus,0 ,0],\
                          [g_v_t_X/(1.0*N_plus)    ,g_v_t_X/(1.0*N_minus)     ,0 ,0],\
                          [0                       ,0                         ,1.,0],\
                          [0                       ,0                         ,0 ,1.]])
        inputs['EVGDCov']=EVGDCov

        
        the_product_should_be_diagonal=np.dot(EVGDCov.T,np.dot(g_v_mat,EVGDCov))


        
        if 1:
            #print('choose tw4 ---> make a function which creates such matrices \n line 1640')



            tW_4,\
                iter_overflow=two_fluids___rotation_euc_hyp__g_v__choice_tW_matrix(euc_flu3_x_t,\
                                                                      euc_flu3_x_x,\
                                                                      euc_flu3_rho_rho,\
                                                                      euc_flu3_t_t,\
                                                                      euc_flu3_cstt,\
                                                                      euc_flu3_phiphi,\
                                                                      hyp_flu1_x_t,\
                                                                      hyp_flu1_x_x,\
                                                                      hyp_flu1_rho_rho,\
                                                                      hyp_flu1_t_t,\
                                                                      hyp_flu1_cstt,\
                                                                      hyp_flu1_phiphi,\
                                                                      euc_flu1_x_t,\
                                                                      euc_flu1_x_x,\
                                                                      euc_flu1_rho_rho,\
                                                                      euc_flu1_t_t,\
                                                                      euc_flu1_cstt,\
                                                                      euc_flu1_phiphi,\
                                                                      inputs,\
            )

            tW_4_second_fluid,\
                iter_overflow_second=two_fluids___rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu3_x_t,\
                                                                             hyp_flu3_x_x,\
                                                                             hyp_flu3_rho_rho,\
                                                                             hyp_flu3_t_t,\
                                                                             hyp_flu3_cstt,\
                                                                             hyp_flu3_phiphi,\
                                                                             hyp_flu2_x_t,\
                                                                             hyp_flu2_x_x,\
                                                                             hyp_flu2_rho_rho,\
                                                                             hyp_flu2_t_t,\
                                                                             hyp_flu2_cstt,\
                                                                             hyp_flu2_phiphi,\
                                                                             euc_flu2_x_t,\
                                                                             euc_flu2_x_x,\
                                                                             euc_flu2_rho_rho,\
                                                                             euc_flu2_t_t,\
                                                                             euc_flu2_cstt,\
                                                                             euc_flu2_phiphi,\
                                                                             inputs,\
            )


 






            ##################################

            # the following is contravariant (each nested array is not a desirable vector rather this is component 1 of the first array and then comp 1 of the second, etc.
            e_not_prime=np.array([[-1,0,0,0],\
                                  [g_v_t_X,1,0,0],\
                                  [0,0,1,0],\
                                  [0,0,0,1/np.sqrt(g_v_phi_phi) ],\
            ])

            minkowski_metric=np.diag([-1,1,1,1])
            e_prime=np.dot(EVGDCov,np.dot(tW_4,np.dot(EVGDCov.T,e_not_prime)))
            e_prime_2nd=np.dot(EVGDCov,np.dot(tW_4_second_fluid,np.dot(EVGDCov.T,e_not_prime)))

            # these two vectors are contravariant components


            e_prime_2nd_cov=np.dot(g_v_mat,e_prime_2nd)# in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures



            # e_prime is described as sets of lines , each dissecting interesting vectors (vectors as columns)
            #e_prime here is contravariant
            #below we compute the covariant vectors in a single matrix


            e_prime_cov=np.dot(g_v_mat,e_prime)# in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures



            #######################################################################
            the_product_should_be_minkowski_metric_1=np.dot(e_prime.T,np.dot(g_v_mat,e_prime))
            the_product_should_be_minkowski_metric_2=np.dot(e_prime_2nd.T,np.dot(g_v_mat,e_prime_2nd))
            
            

            max_abs_u_squared_plus_one=np.max(np.abs([the_product_should_be_minkowski_metric_1[0][0]+1,\
                                                  the_product_should_be_minkowski_metric_2[0][0]+1]))
            max_abs_of_velocities_components=np.max([np.max(np.abs(e_prime[0])),np.max(np.abs(e_prime_2nd[0]))])
        
    
    
            return max_abs_u_squared_plus_one,max_abs_of_velocities_components
        
        print('line 4386')
        raise
#################################################################################################


#####################################################################################################################################################

##############################################################################################################################################

def three_fluids__gammas_and_gamma_relation___myfunc(g_n_t__t_jX,\
                                                       g_n__rho2_jX2,\
                                                       g__v_jn__phiphi,\
                                                       g_v_t__t_jX,\
                                                       g_v__rho2_jX2,\
                                                       meshed__cos_jsin__theta,\
                                                       sqrt_lambda__pl_jmin,\
                                                       G__t_t_jA,\
                                                       G__t__x_jrho,\
                                                       G__x__x_jrho,\
                                                       G_x__rho2_jphi2,\
                                                       hyp_coef_flu1_x__x_jt,\
                                                       hyp_coef_flu1__tt_jrhorho,\
                                                       hyp_coef_flu1__phiphi_jcstt,\
                                                       euc_coef_flu1_x__x_jt,\
                                                       euc_coef_flu1__tt_jrhorho,\
                                                       euc_coef_flu1__phiphi_jcstt,\
                                                       hyp_coef_flu2_x__x_jt,\
                                                       hyp_coef_flu2__tt_jrhorho,\
                                                       hyp_coef_flu2__phiphi_jcstt,\
                                                       euc_coef_flu2_x__x_jt,\
                                                       euc_coef_flu2__tt_jrhorho,\
                                                       euc_coef_flu2__phiphi_jcstt,\
                                                       hyp_coef_flu3_x__x_jt,\
                                                       hyp_coef_flu3__tt_jrhorho,\
                                                       hyp_coef_flu3__phiphi_jcstt,\
                                                       euc_coef_flu3_x__x_jt,\
                                                       euc_coef_flu3__tt_jrhorho,\
                                                       euc_coef_flu3__phiphi_jcstt,\
):
    bad_inf_nan=0
    Lambda_cosmological_constant_=0
    #1.11e-52 #(m^(-2))
    


    do_plot=False

    #1
    g_n_t_X=g_n_t__t_jX.imag
    g_n_t_t=g_n_t__t_jX.real

    #2
    g_n_X_X=g_n__rho2_jX2.imag
    g_n_rho_rho=g_n__rho2_jX2.real


    #3
    g_n_phi_phi=g__v_jn__phiphi.imag
    g_v_phi_phi=g__v_jn__phiphi.real



    #4
    g_v_t_X=g_v_t__t_jX.imag
    g_v_t_t=g_v_t__t_jX.real

    #5
    g_v_X_X=g_v__rho2_jX2.imag
    g_v_rho_rho=g_v__rho2_jX2.real

    #5
    meshed_sin_theta=meshed__cos_jsin__theta.imag
    meshed_cos_theta=meshed__cos_jsin__theta.real

    #6
    sqrt_lambda_minus=sqrt_lambda__pl_jmin.imag
    sqrt_lambda_plus=sqrt_lambda__pl_jmin.real

    #7
    A=G__t_t_jA.imag
    G__t_t=G__t_t_jA.real

    #8
    G__t_rho=G__t__x_jrho.imag
    G__t_x=G__t__x_jrho.real

    #9
    G__x_rho=G__x__x_jrho.imag
    G__x_x=G__x__x_jrho.real

    #10
    G__phi_phi=G_x__rho2_jphi2.imag
    G__rho_rho=G_x__rho2_jphi2.real

    # there are seven tt tx trho , xx xrho, rhorho, phiphi
    #11


    hyp_flu1_x_t=hyp_coef_flu1_x__x_jt.imag
    hyp_flu1_x_x=hyp_coef_flu1_x__x_jt.real

    #12
    hyp_flu1_rho_rho=hyp_coef_flu1__tt_jrhorho.imag
    hyp_flu1_t_t=hyp_coef_flu1__tt_jrhorho.real

    #13
    hyp_flu1_cstt=hyp_coef_flu1__phiphi_jcstt.imag
    hyp_flu1_phiphi=hyp_coef_flu1__phiphi_jcstt.real

    #14

    euc_flu1_x_t=euc_coef_flu1_x__x_jt.imag
    euc_flu1_x_x=euc_coef_flu1_x__x_jt.real


    euc_flu1_rho_rho=euc_coef_flu1__tt_jrhorho.imag
    euc_flu1_t_t=euc_coef_flu1__tt_jrhorho.real


    euc_flu1_cstt=euc_coef_flu1__phiphi_jcstt.imag
    euc_flu1_phiphi=euc_coef_flu1__phiphi_jcstt.real
    ################################
    hyp_flu2_x_t=hyp_coef_flu2_x__x_jt.imag
    hyp_flu2_x_x=hyp_coef_flu2_x__x_jt.real


    hyp_flu2_rho_rho=hyp_coef_flu2__tt_jrhorho.imag
    hyp_flu2_t_t=hyp_coef_flu2__tt_jrhorho.real


    hyp_flu2_cstt=hyp_coef_flu2__phiphi_jcstt.imag
    hyp_flu2_phiphi=hyp_coef_flu2__phiphi_jcstt.real



    euc_flu2_x_t=euc_coef_flu2_x__x_jt.imag
    euc_flu2_x_x=euc_coef_flu2_x__x_jt.real


    euc_flu2_rho_rho=euc_coef_flu2__tt_jrhorho.imag
    euc_flu2_t_t=euc_coef_flu2__tt_jrhorho.real


    euc_flu2_cstt=euc_coef_flu2__phiphi_jcstt.imag
    euc_flu2_phiphi=euc_coef_flu2__phiphi_jcstt.real

    ##################################
    hyp_flu3_x_t=hyp_coef_flu3_x__x_jt.imag
    hyp_flu3_x_x=hyp_coef_flu3_x__x_jt.real


    hyp_flu3_rho_rho=hyp_coef_flu3__tt_jrhorho.imag
    hyp_flu3_t_t=hyp_coef_flu3__tt_jrhorho.real


    hyp_flu3_cstt=hyp_coef_flu3__phiphi_jcstt.imag
    hyp_flu3_phiphi=hyp_coef_flu3__phiphi_jcstt.real



    euc_flu3_x_t=euc_coef_flu3_x__x_jt.imag
    euc_flu3_x_x=euc_coef_flu3_x__x_jt.real


    euc_flu3_rho_rho=euc_coef_flu3__tt_jrhorho.imag
    euc_flu3_t_t=euc_coef_flu3__tt_jrhorho.real


    euc_flu3_cstt=euc_coef_flu3__phiphi_jcstt.imag
    euc_flu3_phiphi=euc_coef_flu3__phiphi_jcstt.real






    #






    #do_print_stuff=True
    do_print_stuff=False

    #do_print_stuff2=True
    do_print_stuff2=False


    do_print_stuff3=False
    inputs={}

    do_print_stuff4=False

    do_print_stuff5=False
    ######################################################
    # correction on g_phi,phi
    #below
    if g_n_phi_phi==0 or np.isinf(g_n_phi_phi):
        g_n_phi_phi=1

    if g_v_phi_phi==0 or np.isinf(g_v_phi_phi):
        g_v_phi_phi=1

    if np.isnan(g_v_phi_phi) or np.isnan(g_n_phi_phi):
        g_n_phi_phi=1
        g_v_phi_phi=1



    inputs["g_n_t_t"]=g_n_t_t
    inputs["g_n_t_X"]=g_n_t_X
    inputs["g_n_X_X"]=g_n_X_X
    inputs["g_n_rho_rho"]=g_n_rho_rho
    inputs["g_n_phi_phi"]=g_n_phi_phi


    inputs["g_v_t_t"]=g_v_t_t
    inputs["g_v_t_X"]=g_v_t_X
    inputs["g_v_X_X"]=g_v_X_X
    inputs["g_v_rho_rho"]=g_v_rho_rho
    inputs["g_v_phi_phi"]=g_v_phi_phi

    inputs["sqrt_lambda_minus"]=sqrt_lambda_minus
    inputs["sqrt_lambda_plus"]=sqrt_lambda_plus



    ########################################################
    # we compute G_doubly covariant!!
    G_doubly_cov=np.array( [ [ G__t_t   , G__t_x   , G__t_rho   , 0 ],\
                             [ G__t_x   , G__x_x   , G__x_rho   , 0 ],\
                             [ G__t_rho , G__x_rho , G__rho_rho , 0 ],\
                             [ 0        , 0        , 0          , G__phi_phi ],\
    ])
    #######################################################
    # below are the numpy equivalents of lines / rows
    g_n_mat=np.array([[inputs["g_n_t_t"],inputs["g_n_t_X"],0,0],\
                      [inputs["g_n_t_X"],inputs["g_n_X_X"],0,0],\
                      [0                ,0                ,inputs["g_n_rho_rho"],0],\
                      [0                ,0                ,0                    ,inputs["g_n_phi_phi"]]])

    g_v_mat=np.array([[inputs["g_v_t_t"],inputs["g_v_t_X"],0,0],\
                      [inputs["g_v_t_X"],inputs["g_v_X_X"],0,0],\
                      [0                ,0                ,inputs["g_v_rho_rho"],0],\
                      [0                ,0                ,0                    ,inputs["g_v_phi_phi"]]])



    lambda_plus=(-(A-1)+np.sqrt((A-1)**2+4))/2.
    lambda_minus=(-(A-1)-np.sqrt((A-1)**2+4))/2.
    tW=np.array([[meshed_cos_theta,0,meshed_sin_theta*1./sqrt_lambda_plus],\
                 [0,1,0],\
                 [-meshed_sin_theta*sqrt_lambda_plus,0,meshed_cos_theta],\
                 ])

    return_to_starting_block=True
    if 1:


        #print('choose tw4 ---> make a function which creates such matrices \n line 1640')




        gamma_1st_fluid,\
            relation__1st_fluid,\
            iterflow__1st_fluid=three_fluids___rotation_euc_hyp__g_v__choice_tW_matrix___OUTPUT_GAMMAS_and_relations(hyp_flu1_x_t,\
                                                                     hyp_flu1_x_x,\
                                                                     hyp_flu1_rho_rho,\
                                                                     hyp_flu1_t_t,\
                                                                     hyp_flu1_cstt,\
                                                                     hyp_flu1_phiphi,\
                                                                     euc_flu1_x_t,\
                                                                     euc_flu1_x_x,\
                                                                     euc_flu1_rho_rho,\
                                                                     euc_flu1_t_t,\
                                                                     euc_flu1_cstt,\
                                                                     euc_flu1_phiphi,\
                                                                     inputs,\
        )
        
        gamma_2nd_fluid,\
            relation__2nd_fluid,\
            iterflow__2nd_fluid=three_fluids___rotation_euc_hyp__g_v__choice_tW_matrix___OUTPUT_GAMMAS_and_relations(hyp_flu2_x_t,\
                                                                                  hyp_flu2_x_x,\
                                                                                  hyp_flu2_rho_rho,\
                                                                                  hyp_flu2_t_t,\
                                                                                  hyp_flu2_cstt,\
                                                                                  hyp_flu2_phiphi,\
                                                                                  euc_flu2_x_t,\
                                                                                  euc_flu2_x_x,\
                                                                                  euc_flu2_rho_rho,\
                                                                                  euc_flu2_t_t,\
                                                                                  euc_flu2_cstt,\
                                                                                  euc_flu2_phiphi,\
                                                                                  inputs,\
        )

        gamma_3rd_fluid,\
            relation__3rd_fluid,\
            iterflow__3rd_fluid=three_fluids___rotation_euc_hyp__g_v__choice_tW_matrix___OUTPUT_GAMMAS_and_relations(hyp_flu3_x_t,\
                                                                                 hyp_flu3_x_x,\
                                                                                 hyp_flu3_rho_rho,\
                                                                                 hyp_flu3_t_t,\
                                                                                 hyp_flu3_cstt,\
                                                                                 hyp_flu3_phiphi,\
                                                                                 euc_flu3_x_t,\
                                                                                 euc_flu3_x_x,\
                                                                                 euc_flu3_rho_rho,\
                                                                                 euc_flu3_t_t,\
                                                                                 euc_flu3_cstt,\
                                                                                 euc_flu3_phiphi,\
                                                                                 inputs,\
        )



    
        
    return gamma_1st_fluid+1j*relation__1st_fluid,\
        gamma_2nd_fluid+1j*relation__2nd_fluid,\
        gamma_3rd_fluid+1j*relation__3rd_fluid,\
        
#max_abs_u_squared_plus_one,max_abs_of_velocities_components
#biduke+1j*0,min__density_plus_pressure+1j*(matrix_rank_3fluids-8*(iter_overflow_third+iter_overflow_second+iter_overflow+bad_inf_nan+overflow_big_mat))
#################################################################################################
#line 5113

##############################################################################################################################################

def three_fluids__extract_velocity_unitarity___myfunc(g_n_t__t_jX,\
                                                       g_n__rho2_jX2,\
                                                       g__v_jn__phiphi,\
                                                       g_v_t__t_jX,\
                                                       g_v__rho2_jX2,\
                                                       meshed__cos_jsin__theta,\
                                                       sqrt_lambda__pl_jmin,\
                                                       G__t_t_jA,\
                                                       G__t__x_jrho,\
                                                       G__x__x_jrho,\
                                                       G_x__rho2_jphi2,\
                                                       hyp_coef_flu1_x__x_jt,\
                                                       hyp_coef_flu1__tt_jrhorho,\
                                                       hyp_coef_flu1__phiphi_jcstt,\
                                                       euc_coef_flu1_x__x_jt,\
                                                       euc_coef_flu1__tt_jrhorho,\
                                                       euc_coef_flu1__phiphi_jcstt,\
                                                       hyp_coef_flu2_x__x_jt,\
                                                       hyp_coef_flu2__tt_jrhorho,\
                                                       hyp_coef_flu2__phiphi_jcstt,\
                                                       euc_coef_flu2_x__x_jt,\
                                                       euc_coef_flu2__tt_jrhorho,\
                                                       euc_coef_flu2__phiphi_jcstt,\
                                                       hyp_coef_flu3_x__x_jt,\
                                                       hyp_coef_flu3__tt_jrhorho,\
                                                       hyp_coef_flu3__phiphi_jcstt,\
                                                       euc_coef_flu3_x__x_jt,\
                                                       euc_coef_flu3__tt_jrhorho,\
                                                       euc_coef_flu3__phiphi_jcstt,\
):
    bad_inf_nan=0
    Lambda_cosmological_constant_=0
    #1.11e-52 #(m^(-2))
    


    do_plot=False

    #1
    g_n_t_X=g_n_t__t_jX.imag
    g_n_t_t=g_n_t__t_jX.real

    #2
    g_n_X_X=g_n__rho2_jX2.imag
    g_n_rho_rho=g_n__rho2_jX2.real


    #3
    g_n_phi_phi=g__v_jn__phiphi.imag
    g_v_phi_phi=g__v_jn__phiphi.real



    #4
    g_v_t_X=g_v_t__t_jX.imag
    g_v_t_t=g_v_t__t_jX.real

    #5
    g_v_X_X=g_v__rho2_jX2.imag
    g_v_rho_rho=g_v__rho2_jX2.real

    #5
    meshed_sin_theta=meshed__cos_jsin__theta.imag
    meshed_cos_theta=meshed__cos_jsin__theta.real

    #6
    sqrt_lambda_minus=sqrt_lambda__pl_jmin.imag
    sqrt_lambda_plus=sqrt_lambda__pl_jmin.real

    #7
    A=G__t_t_jA.imag
    G__t_t=G__t_t_jA.real

    #8
    G__t_rho=G__t__x_jrho.imag
    G__t_x=G__t__x_jrho.real

    #9
    G__x_rho=G__x__x_jrho.imag
    G__x_x=G__x__x_jrho.real

    #10
    G__phi_phi=G_x__rho2_jphi2.imag
    G__rho_rho=G_x__rho2_jphi2.real

    # there are seven tt tx trho , xx xrho, rhorho, phiphi
    #11


    hyp_flu1_x_t=hyp_coef_flu1_x__x_jt.imag
    hyp_flu1_x_x=hyp_coef_flu1_x__x_jt.real

    #12
    hyp_flu1_rho_rho=hyp_coef_flu1__tt_jrhorho.imag
    hyp_flu1_t_t=hyp_coef_flu1__tt_jrhorho.real

    #13
    hyp_flu1_cstt=hyp_coef_flu1__phiphi_jcstt.imag
    hyp_flu1_phiphi=hyp_coef_flu1__phiphi_jcstt.real

    #14

    euc_flu1_x_t=euc_coef_flu1_x__x_jt.imag
    euc_flu1_x_x=euc_coef_flu1_x__x_jt.real


    euc_flu1_rho_rho=euc_coef_flu1__tt_jrhorho.imag
    euc_flu1_t_t=euc_coef_flu1__tt_jrhorho.real


    euc_flu1_cstt=euc_coef_flu1__phiphi_jcstt.imag
    euc_flu1_phiphi=euc_coef_flu1__phiphi_jcstt.real
    ################################
    hyp_flu2_x_t=hyp_coef_flu2_x__x_jt.imag
    hyp_flu2_x_x=hyp_coef_flu2_x__x_jt.real


    hyp_flu2_rho_rho=hyp_coef_flu2__tt_jrhorho.imag
    hyp_flu2_t_t=hyp_coef_flu2__tt_jrhorho.real


    hyp_flu2_cstt=hyp_coef_flu2__phiphi_jcstt.imag
    hyp_flu2_phiphi=hyp_coef_flu2__phiphi_jcstt.real



    euc_flu2_x_t=euc_coef_flu2_x__x_jt.imag
    euc_flu2_x_x=euc_coef_flu2_x__x_jt.real


    euc_flu2_rho_rho=euc_coef_flu2__tt_jrhorho.imag
    euc_flu2_t_t=euc_coef_flu2__tt_jrhorho.real


    euc_flu2_cstt=euc_coef_flu2__phiphi_jcstt.imag
    euc_flu2_phiphi=euc_coef_flu2__phiphi_jcstt.real

    ##################################
    hyp_flu3_x_t=hyp_coef_flu3_x__x_jt.imag
    hyp_flu3_x_x=hyp_coef_flu3_x__x_jt.real


    hyp_flu3_rho_rho=hyp_coef_flu3__tt_jrhorho.imag
    hyp_flu3_t_t=hyp_coef_flu3__tt_jrhorho.real


    hyp_flu3_cstt=hyp_coef_flu3__phiphi_jcstt.imag
    hyp_flu3_phiphi=hyp_coef_flu3__phiphi_jcstt.real



    euc_flu3_x_t=euc_coef_flu3_x__x_jt.imag
    euc_flu3_x_x=euc_coef_flu3_x__x_jt.real


    euc_flu3_rho_rho=euc_coef_flu3__tt_jrhorho.imag
    euc_flu3_t_t=euc_coef_flu3__tt_jrhorho.real


    euc_flu3_cstt=euc_coef_flu3__phiphi_jcstt.imag
    euc_flu3_phiphi=euc_coef_flu3__phiphi_jcstt.real






    #






    #do_print_stuff=True
    do_print_stuff=False

    #do_print_stuff2=True
    do_print_stuff2=False


    do_print_stuff3=False
    inputs={}

    do_print_stuff4=False

    do_print_stuff5=False
    ######################################################
    # correction on g_phi,phi
    #below
    if g_n_phi_phi==0 or np.isinf(g_n_phi_phi):
        g_n_phi_phi=1

    if g_v_phi_phi==0 or np.isinf(g_v_phi_phi):
        g_v_phi_phi=1

    if np.isnan(g_v_phi_phi) or np.isnan(g_n_phi_phi):
        g_n_phi_phi=1
        g_v_phi_phi=1



    inputs["g_n_t_t"]=g_n_t_t
    inputs["g_n_t_X"]=g_n_t_X
    inputs["g_n_X_X"]=g_n_X_X
    inputs["g_n_rho_rho"]=g_n_rho_rho
    inputs["g_n_phi_phi"]=g_n_phi_phi


    inputs["g_v_t_t"]=g_v_t_t
    inputs["g_v_t_X"]=g_v_t_X
    inputs["g_v_X_X"]=g_v_X_X
    inputs["g_v_rho_rho"]=g_v_rho_rho
    inputs["g_v_phi_phi"]=g_v_phi_phi

    inputs["sqrt_lambda_minus"]=sqrt_lambda_minus
    inputs["sqrt_lambda_plus"]=sqrt_lambda_plus



    ########################################################
    # we compute G_doubly covariant!!
    G_doubly_cov=np.array( [ [ G__t_t   , G__t_x   , G__t_rho   , 0 ],\
                             [ G__t_x   , G__x_x   , G__x_rho   , 0 ],\
                             [ G__t_rho , G__x_rho , G__rho_rho , 0 ],\
                             [ 0        , 0        , 0          , G__phi_phi ],\
    ])
    #######################################################
    # below are the numpy equivalents of lines / rows
    g_n_mat=np.array([[inputs["g_n_t_t"],inputs["g_n_t_X"],0,0],\
                      [inputs["g_n_t_X"],inputs["g_n_X_X"],0,0],\
                      [0                ,0                ,inputs["g_n_rho_rho"],0],\
                      [0                ,0                ,0                    ,inputs["g_n_phi_phi"]]])

    g_v_mat=np.array([[inputs["g_v_t_t"],inputs["g_v_t_X"],0,0],\
                      [inputs["g_v_t_X"],inputs["g_v_X_X"],0,0],\
                      [0                ,0                ,inputs["g_v_rho_rho"],0],\
                      [0                ,0                ,0                    ,inputs["g_v_phi_phi"]]])



    lambda_plus=(-(A-1)+np.sqrt((A-1)**2+4))/2.
    lambda_minus=(-(A-1)-np.sqrt((A-1)**2+4))/2.
    tW=np.array([[meshed_cos_theta,0,meshed_sin_theta*1./sqrt_lambda_plus],\
                 [0,1,0],\
                 [-meshed_sin_theta*sqrt_lambda_plus,0,meshed_cos_theta],\
                 ])

    return_to_starting_block=True
    if 1:


        #print('choose tw4 ---> make a function which creates such matrices \n line 1640')




        tW_4,\
            tW_4_rot_no_curv,\
            tW_4_boo_no_curv,\
            iter_overflow=three_fluids___rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu1_x_t,\
                                                                     hyp_flu1_x_x,\
                                                                     hyp_flu1_rho_rho,\
                                                                     hyp_flu1_t_t,\
                                                                     hyp_flu1_cstt,\
                                                                     hyp_flu1_phiphi,\
                                                                     euc_flu1_x_t,\
                                                                     euc_flu1_x_x,\
                                                                     euc_flu1_rho_rho,\
                                                                     euc_flu1_t_t,\
                                                                     euc_flu1_cstt,\
                                                                     euc_flu1_phiphi,\
                                                                     inputs,\
        )

        tW_4_second_fluid,\
            tW_4_second_fluid_rot_no_curv,\
            tW_4_second_fluid_boo_no_curv,\
            iter_overflow_second=three_fluids___rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu2_x_t,\
                                                                                  hyp_flu2_x_x,\
                                                                                  hyp_flu2_rho_rho,\
                                                                                  hyp_flu2_t_t,\
                                                                                  hyp_flu2_cstt,\
                                                                                  hyp_flu2_phiphi,\
                                                                                  euc_flu2_x_t,\
                                                                                  euc_flu2_x_x,\
                                                                                  euc_flu2_rho_rho,\
                                                                                  euc_flu2_t_t,\
                                                                                  euc_flu2_cstt,\
                                                                                  euc_flu2_phiphi,\
                                                                                  inputs,\
        )

        tW_4_third_fluid,\
            tW_4_third_fluid_rot_no_curv,\
            tW_4_third_fluid_boo_no_curv,\
            iter_overflow_third=three_fluids___rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu3_x_t,\
                                                                                 hyp_flu3_x_x,\
                                                                                 hyp_flu3_rho_rho,\
                                                                                 hyp_flu3_t_t,\
                                                                                 hyp_flu3_cstt,\
                                                                                 hyp_flu3_phiphi,\
                                                                                 euc_flu3_x_t,\
                                                                                 euc_flu3_x_x,\
                                                                                 euc_flu3_rho_rho,\
                                                                                 euc_flu3_t_t,\
                                                                                 euc_flu3_cstt,\
                                                                                 euc_flu3_phiphi,\
                                                                                 inputs,\
        )



        #below is the g_munu diagonal version
        thediag=np.diag([lambda_plus,lambda_minus,1])
        thediag_4=np.diag([lambda_plus,lambda_minus,1,g_v_phi_phi])
        thediag_4_no_curve=np.diag([1,-1,1,1])#no_curv

        product=np.dot(tW.T,np.dot(thediag,tW))

        product_4_4__ini_fluid__tw4=np.dot(tW_4.T,np.dot(thediag_4,tW_4)) # this should be the same as the_diag_4
        product_4_4__2nd_fluid__tw4=np.dot(tW_4_second_fluid.T,np.dot(thediag_4,tW_4_second_fluid)) # this should be the same as the_diag_4
        product_4_4__3rd_fluid__tw4=np.dot(tW_4_third_fluid.T,np.dot(thediag_4,tW_4_third_fluid)) # this should be the same as the_diag_4


        #interesting line below!!
        ####    g_v_t_X=v*(1-meshed_f)

        N_plus=np.sqrt((lambda_plus-1)**2+g_v_t_X**2)
        N_minus=np.sqrt((lambda_minus-1)**2+g_v_t_X**2)

        #################################
        # now we just examine the theta matrix
        #eigen_vect_g_doublcovariant=EVGDCov
        #{EVGDCov=theta matrix} is the matrix of eigen vectors of g_{mu,nu}

        EVGDCov=np.array([[(lambda_plus-1.0)/N_plus,(lambda_minus-1.0)/N_minus,0,0],\
                         [g_v_t_X/(1.0*N_plus),g_v_t_X/(1.0*N_minus),0,0],\
                         [0,0,1.,0],\
                         [0,0,0,1.]])
        the_product_should_be_diagonal=np.dot(EVGDCov.T,np.dot(g_v_mat,EVGDCov))

        ##################################

        # the following is contravariant (each nested array is not a desirable vector rather this is component 1 of the first array and then comp 1 of the second, etc.
        e_not_prime=np.array([[-1,0,0,0],\
                              [g_v_t_X,1,0,0],\
                              [0,0,1,0],\
                              [0,0,0,1/np.sqrt(g_v_phi_phi) ],\
        ])

        #######################"
        minkowski_metric=np.diag([-1,1,1,1])
        e_prime=np.dot(EVGDCov,np.dot(tW_4,np.dot(EVGDCov.T,e_not_prime)))
        e_prime_2nd=np.dot(EVGDCov,np.dot(tW_4_second_fluid,np.dot(EVGDCov.T,e_not_prime)))
        e_prime_3rd=np.dot(EVGDCov,np.dot(tW_4_third_fluid,np.dot(EVGDCov.T,e_not_prime)))


        the_product_should_be_minkowski_metric_1=np.dot(e_prime.T,np.dot(g_v_mat,e_prime))
        the_product_should_be_minkowski_metric_2=np.dot(e_prime_2nd.T,np.dot(g_v_mat,e_prime_2nd))
        the_product_should_be_minkowski_metric_3=np.dot(e_prime_3rd.T,np.dot(g_v_mat,e_prime_3rd))

        max_abs_u_squared_plus_one=np.max(np.abs([the_product_should_be_minkowski_metric_1[0][0]+1,\
                                                  the_product_should_be_minkowski_metric_2[0][0]+1,\
                                                  the_product_should_be_minkowski_metric_3[0][0]+1]))
        max_abs_of_velocities_components=np.max([np.max(np.abs(e_prime[0])),np.max(np.abs(e_prime_2nd[0])),np.max(np.abs(e_prime_3rd[0]))])
        
    
    
    return max_abs_u_squared_plus_one,max_abs_of_velocities_components

#################################################################################################


def two_fluids__element_wise__compute_relat_error__norm2__output_them___myfunc(g_n_t__t_jX,\
                                                       g_n__rho2_jX2,\
                                                       g__v_jn__phiphi,\
                                                       g_v_t__t_jX,\
                                                       g_v__rho2_jX2,\
                                                       meshed__cos_jsin__theta,\
                                                       sqrt_lambda__pl_jmin,\
                                                       G__t_t_jA,\
                                                       G__t__x_jrho,\
                                                       G__x__x_jrho,\
                                                       G_x__rho2_jphi2,\
                                                       hyp_coef_flu1_x__x_jt,\
                                                       hyp_coef_flu1__tt_jrhorho,\
                                                       hyp_coef_flu1__phiphi_jcstt,\
                                                       euc_coef_flu1_x__x_jt,\
                                                       euc_coef_flu1__tt_jrhorho,\
                                                       euc_coef_flu1__phiphi_jcstt,\
                                                       hyp_coef_flu2_x__x_jt,\
                                                       hyp_coef_flu2__tt_jrhorho,\
                                                       hyp_coef_flu2__phiphi_jcstt,\
                                                       euc_coef_flu2_x__x_jt,\
                                                       euc_coef_flu2__tt_jrhorho,\
                                                       euc_coef_flu2__phiphi_jcstt,\
                                                       hyp_coef_flu3_x__x_jt,\
                                                       hyp_coef_flu3__tt_jrhorho,\
                                                       hyp_coef_flu3__phiphi_jcstt,\
                                                       euc_coef_flu3_x__x_jt,\
                                                       euc_coef_flu3__tt_jrhorho,\
                                                       euc_coef_flu3__phiphi_jcstt,\
):


    

    


        #1
        g_n_t_X=g_n_t__t_jX.imag
        g_n_t_t=g_n_t__t_jX.real
        do_plot=False
        Lambda_cosmological_constant_=0
        #1.11e-52 #(m^(-2))

        #2
        g_n_X_X=g_n__rho2_jX2.imag
        g_n_rho_rho=g_n__rho2_jX2.real


        #3
        g_n_phi_phi=g__v_jn__phiphi.imag
        g_v_phi_phi=g__v_jn__phiphi.real



        #4
        g_v_t_X=g_v_t__t_jX.imag
        g_v_t_t=g_v_t__t_jX.real

        #5
        g_v_X_X=g_v__rho2_jX2.imag
        g_v_rho_rho=g_v__rho2_jX2.real

        #5
        meshed_sin_theta=meshed__cos_jsin__theta.imag
        meshed_cos_theta=meshed__cos_jsin__theta.real

        #6
        sqrt_lambda_minus=sqrt_lambda__pl_jmin.imag
        sqrt_lambda_plus=sqrt_lambda__pl_jmin.real

        #7
        A=G__t_t_jA.imag
        G__t_t=G__t_t_jA.real

        #8
        G__t_rho=G__t__x_jrho.imag
        G__t_x=G__t__x_jrho.real

        #9
        G__x_rho=G__x__x_jrho.imag
        G__x_x=G__x__x_jrho.real

        #10
        G__phi_phi=G_x__rho2_jphi2.imag
        G__rho_rho=G_x__rho2_jphi2.real

        # there are seven tt tx trho , xx xrho, rhorho, phiphi
        #11


        hyp_flu1_x_t=hyp_coef_flu1_x__x_jt.imag
        hyp_flu1_x_x=hyp_coef_flu1_x__x_jt.real

        #12
        hyp_flu1_rho_rho=hyp_coef_flu1__tt_jrhorho.imag
        hyp_flu1_t_t=hyp_coef_flu1__tt_jrhorho.real

        #13
        hyp_flu1_cstt=hyp_coef_flu1__phiphi_jcstt.imag
        hyp_flu1_phiphi=hyp_coef_flu1__phiphi_jcstt.real

        #14

        euc_flu1_x_t=euc_coef_flu1_x__x_jt.imag
        euc_flu1_x_x=euc_coef_flu1_x__x_jt.real


        euc_flu1_rho_rho=euc_coef_flu1__tt_jrhorho.imag
        euc_flu1_t_t=euc_coef_flu1__tt_jrhorho.real


        euc_flu1_cstt=euc_coef_flu1__phiphi_jcstt.imag
        euc_flu1_phiphi=euc_coef_flu1__phiphi_jcstt.real
        ################################
        hyp_flu2_x_t=hyp_coef_flu2_x__x_jt.imag
        hyp_flu2_x_x=hyp_coef_flu2_x__x_jt.real


        hyp_flu2_rho_rho=hyp_coef_flu2__tt_jrhorho.imag
        hyp_flu2_t_t=hyp_coef_flu2__tt_jrhorho.real


        hyp_flu2_cstt=hyp_coef_flu2__phiphi_jcstt.imag
        hyp_flu2_phiphi=hyp_coef_flu2__phiphi_jcstt.real



        euc_flu2_x_t=euc_coef_flu2_x__x_jt.imag
        euc_flu2_x_x=euc_coef_flu2_x__x_jt.real


        euc_flu2_rho_rho=euc_coef_flu2__tt_jrhorho.imag
        euc_flu2_t_t=euc_coef_flu2__tt_jrhorho.real


        euc_flu2_cstt=euc_coef_flu2__phiphi_jcstt.imag
        euc_flu2_phiphi=euc_coef_flu2__phiphi_jcstt.real

        ##################################
        hyp_flu3_x_t=hyp_coef_flu3_x__x_jt.imag
        hyp_flu3_x_x=hyp_coef_flu3_x__x_jt.real


        hyp_flu3_rho_rho=hyp_coef_flu3__tt_jrhorho.imag
        hyp_flu3_t_t=hyp_coef_flu3__tt_jrhorho.real


        hyp_flu3_cstt=hyp_coef_flu3__phiphi_jcstt.imag
        hyp_flu3_phiphi=hyp_coef_flu3__phiphi_jcstt.real



        euc_flu3_x_t=euc_coef_flu3_x__x_jt.imag
        euc_flu3_x_x=euc_coef_flu3_x__x_jt.real


        euc_flu3_rho_rho=euc_coef_flu3__tt_jrhorho.imag
        euc_flu3_t_t=euc_coef_flu3__tt_jrhorho.real


        euc_flu3_cstt=euc_coef_flu3__phiphi_jcstt.imag
        euc_flu3_phiphi=euc_coef_flu3__phiphi_jcstt.real





    
        # res_batch=res_batch__1j_LAMBDA.real
        # Lambda_cosmological_constant_=res_batch__1j_LAMBDA.imag

        #do_print_stuff=True
        do_print_stuff=False

        #do_print_stuff2=True
        do_print_stuff2=False


        do_print_stuff3=False
        inputs={}

        do_print_stuff4=False

        do_print_stuff5=False
        #do_print_stuff5=True
        ######################################################
        # correction on g_phi,phi
        #below
        if g_n_phi_phi==0 or np.isinf(g_n_phi_phi):
            g_n_phi_phi=1

        if g_v_phi_phi==0 or np.isinf(g_v_phi_phi):
            g_v_phi_phi=1

        if np.isnan(g_v_phi_phi) or np.isnan(g_n_phi_phi):
            g_n_phi_phi=1
            g_v_phi_phi=1



        inputs["g_n_t_t"]=g_n_t_t
        inputs["g_n_t_X"]=g_n_t_X
        inputs["g_n_X_X"]=g_n_X_X
        inputs["g_n_rho_rho"]=g_n_rho_rho
        inputs["g_n_phi_phi"]=g_n_phi_phi


        inputs["g_v_t_t"]=g_v_t_t
        inputs["g_v_t_X"]=g_v_t_X
        inputs["g_v_X_X"]=g_v_X_X
        inputs["g_v_rho_rho"]=g_v_rho_rho
        inputs["g_v_phi_phi"]=g_v_phi_phi

        inputs["sqrt_lambda_minus"]=sqrt_lambda_minus
        inputs["sqrt_lambda_plus"]=sqrt_lambda_plus



        ########################################################
        # we compute G_doubly covariant!!
        G_doubly_cov=np.array( [ [ G__t_t   , G__t_x   , G__t_rho   , 0 ],\
                                 [ G__t_x   , G__x_x   , G__x_rho   , 0 ],\
                                 [ G__t_rho , G__x_rho , G__rho_rho , 0 ],\
                                 [ 0        , 0        , 0          , G__phi_phi ],\
        ])
        #######################################################
        # below are the numpy equivalents of lines / rows

        g_n_mat=np.array([[inputs["g_n_t_t"],inputs["g_n_t_X"],0,0],\
                          [inputs["g_n_t_X"],inputs["g_n_X_X"],0,0],\
                          [0                ,0                ,inputs["g_n_rho_rho"],0],\
                          [0                ,0                ,0                    ,inputs["g_n_phi_phi"]]])

        g_v_mat=np.array([[inputs["g_v_t_t"],inputs["g_v_t_X"],0,0],\
                          [inputs["g_v_t_X"],inputs["g_v_X_X"],0,0],\
                          [0                ,0                ,inputs["g_v_rho_rho"],0],\
                          [0                ,0                ,0                    ,inputs["g_v_phi_phi"]]])



        lambda_plus=(-(A-1)+np.sqrt((A-1)**2+4))/2.
        lambda_minus=(-(A-1)-np.sqrt((A-1)**2+4))/2.
        tW=np.array([[meshed_cos_theta,0,meshed_sin_theta*1./sqrt_lambda_plus],\
                     [0,1,0],\
                     [-meshed_sin_theta*sqrt_lambda_plus,0,meshed_cos_theta],\
                     ])

        return_to_starting_block=True
        overflow_counting=0

        if 1:
            #print('choose tw4 ---> make a function which creates such matrices \n line 1640')



            tW_4,\
                iter_overflow=two_fluids___rotation_euc_hyp__g_v__choice_tW_matrix(euc_flu3_x_t,\
                                                                      euc_flu3_x_x,\
                                                                      euc_flu3_rho_rho,\
                                                                      euc_flu3_t_t,\
                                                                      euc_flu3_cstt,\
                                                                      euc_flu3_phiphi,\
                                                                      hyp_flu1_x_t,\
                                                                      hyp_flu1_x_x,\
                                                                      hyp_flu1_rho_rho,\
                                                                      hyp_flu1_t_t,\
                                                                      hyp_flu1_cstt,\
                                                                      hyp_flu1_phiphi,\
                                                                      euc_flu1_x_t,\
                                                                      euc_flu1_x_x,\
                                                                      euc_flu1_rho_rho,\
                                                                      euc_flu1_t_t,\
                                                                      euc_flu1_cstt,\
                                                                      euc_flu1_phiphi,\
                                                                      inputs,\
            )

            tW_4_second_fluid,\
                iter_overflow_second=two_fluids___rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu3_x_t,\
                                                                             hyp_flu3_x_x,\
                                                                             hyp_flu3_rho_rho,\
                                                                             hyp_flu3_t_t,\
                                                                             hyp_flu3_cstt,\
                                                                             hyp_flu3_phiphi,\
                                                                             hyp_flu2_x_t,\
                                                                             hyp_flu2_x_x,\
                                                                             hyp_flu2_rho_rho,\
                                                                             hyp_flu2_t_t,\
                                                                             hyp_flu2_cstt,\
                                                                             hyp_flu2_phiphi,\
                                                                             euc_flu2_x_t,\
                                                                             euc_flu2_x_x,\
                                                                             euc_flu2_rho_rho,\
                                                                             euc_flu2_t_t,\
                                                                             euc_flu2_cstt,\
                                                                             euc_flu2_phiphi,\
                                                                             inputs,\
            )


 





            #interesting line below!!
            ####    g_v_t_X=v*(1-meshed_f)

            N_plus=np.sqrt((lambda_plus-1)**2+g_v_t_X**2)
            N_minus=np.sqrt((lambda_minus-1)**2+g_v_t_X**2)

            #################################
            # now we just examine the theta matrix
            #eigen_vect_g_doublcovariant=EVGDCov
            #{EVGDCov=theta matrix} is the matrix of eigen vectors of g_{mu,nu}

            EVGDCov=np.array([[(lambda_plus-1.0)/N_plus,(lambda_minus-1.0)/N_minus,0,0],\
                             [g_v_t_X/(1.0*N_plus),g_v_t_X/(1.0*N_minus),0,0],\
                             [0,0,1.,0],\
                             [0,0,0,1.]])
            the_product_should_be_diagonal=np.dot(EVGDCov.T,np.dot(g_v_mat,EVGDCov))

            ##################################

            # the following is contravariant (each nested array is not a desirable vector rather this is component 1 of the first array and then comp 1 of the second, etc.
            e_not_prime=np.array([[-1,0,0,0],\
                                  [g_v_t_X,1,0,0],\
                                  [0,0,1,0],\
                                  [0,0,0,1/np.sqrt(g_v_phi_phi) ],\
            ])

            minkowski_metric=np.diag([-1,1,1,1])
            e_prime=np.dot(EVGDCov,np.dot(tW_4,np.dot(EVGDCov.T,e_not_prime)))
            e_prime_2nd=np.dot(EVGDCov,np.dot(tW_4_second_fluid,np.dot(EVGDCov.T,e_not_prime)))

            # these two vectors are contravariant components


            e_prime_2nd_cov=np.dot(g_v_mat,e_prime_2nd)# in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures



            # e_prime is described as sets of lines , each dissecting interesting vectors (vectors as columns)
            #e_prime here is contravariant
            #below we compute the covariant vectors in a single matrix


            e_prime_cov=np.dot(g_v_mat,e_prime)# in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures



            # we compute the mixed components of Einstein tensor
            G_einst_mixed_plus_lambda_delta=np.dot(g_n_mat,G_doubly_cov+Lambda_cosmological_constant_*g_v_mat )






            #############################################
 
            basis_vectors_2fluids=[[e_prime_cov,e_prime],[e_prime_2nd_cov,e_prime_2nd]]
            big_matrix_2flu,gee_vector_2flu,overflow_big_mat=compute_big_matrix_for_fluids(basis_vectors_2fluids,G_einst_mixed_plus_lambda_delta)



            if np.any(np.isnan(big_matrix_2flu)) or np.any(np.isinf(big_matrix_2flu)) or \
               np.any(np.isnan(gee_vector_2flu)) or np.any(np.isinf(gee_vector_2flu)):
                return_to_starting_block=True

                hyp_flu1_x_t=hyp_flu1_x_t/3.
                hyp_flu1_x_x=hyp_flu1_x_x/3.
                hyp_flu1_rho_rho=hyp_flu1_rho_rho/3.
                hyp_flu1_t_t=hyp_flu1_t_t/3.
                hyp_flu1_cstt=hyp_flu1_cstt/3.
                hyp_flu1_phiphi=hyp_flu1_phiphi/3.

                hyp_flu2_x_t=hyp_flu2_x_t/3.
                hyp_flu2_x_x=hyp_flu2_x_x/3.
                hyp_flu2_rho_rho=hyp_flu2_rho_rho/3.
                hyp_flu2_t_t=hyp_flu2_t_t/3.
                hyp_flu2_cstt=hyp_flu2_cstt/3.
                hyp_flu2_phiphi=hyp_flu2_phiphi/3.

                #this below is only for two fluids as the 3rd fluid parameters are actually for 2 fluids parameterization of the second boost
                hyp_flu3_x_t=hyp_flu3_x_t/3.
                hyp_flu3_x_x=hyp_flu3_x_x/3.
                hyp_flu3_rho_rho=hyp_flu3_rho_rho/3.
                hyp_flu3_t_t=hyp_flu3_t_t/3.
                hyp_flu3_cstt=hyp_flu3_cstt/3.
                hyp_flu3_phiphi=hyp_flu3_phiphi/3.

                euc_flu3_x_t=euc_flu3_x_t/3.
                euc_flu3_x_x=euc_flu3_x_x/3.
                euc_flu3_rho_rho=euc_flu3_rho_rho/3.
                euc_flu3_t_t=euc_flu3_t_t/3.
                euc_flu3_cstt=euc_flu3_cstt/3.
                euc_flu3_phiphi=euc_flu3_phiphi/3.


                overflow_counting+=2
                # above if inf or nan we decrease the value of all coefficients
                densities_pressures_2fluids=[-11,-11,-11,-11,-11,-11,-11,-11]
                matrix_rank_2fluids=-1
                sing_values_matrix_2fluids=[0,0,0,0,0,0,0,0,0,0,0,0,0]

            else:
                return_to_starting_block=False
                densities_pressures_2fluids,\
                    residuals_2fluids,\
                    matrix_rank_2fluids,\
                    sing_values_matrix_2fluids=np.linalg.lstsq(big_matrix_2flu, gee_vector_2flu,rcond=None)

        ##############################################################

        

        ##############################################################################################################################





        res=list(densities_pressures_2fluids)

        v_p=np.dot(big_matrix_2flu,densities_pressures_2fluids) # vector predicted
        v_r=gee_vector_2flu

        
        denom=np.array([el if el !=0 else 1 for el in list(np.abs(v_p)+np.abs(v_r)) ])
        diff_sur_som=np.abs(v_p-v_r)/denom



        non_zero_sigmas=[el for el in sing_values_matrix_2fluids if el !=0]
        if sing_values_matrix_2fluids[6]==0:
            ratio_sigmas=0
        else:
            ratio_sigmas=sing_values_matrix_2fluids[0]/sing_values_matrix_2fluids[6]

        return numpy.linalg.norm(v_p-v_r)+1j*numpy.linalg.norm(v_r),\
            matrix_rank_2fluids+1j*sing_values_matrix_2fluids[6],\
            ratio_sigmas+1j*np.min(sing_values_matrix_2fluids)

###############################################################################################################################################
#line 4314-5796
#line 5797
def two_fluids__element_wise__compute_relat_error__dif_over_sum__output_them___myfunc(g_n_t__t_jX,\
                                                       g_n__rho2_jX2,\
                                                       g__v_jn__phiphi,\
                                                       g_v_t__t_jX,\
                                                       g_v__rho2_jX2,\
                                                       meshed__cos_jsin__theta,\
                                                       sqrt_lambda__pl_jmin,\
                                                       G__t_t_jA,\
                                                       G__t__x_jrho,\
                                                       G__x__x_jrho,\
                                                       G_x__rho2_jphi2,\
                                                       hyp_coef_flu1_x__x_jt,\
                                                       hyp_coef_flu1__tt_jrhorho,\
                                                       hyp_coef_flu1__phiphi_jcstt,\
                                                       euc_coef_flu1_x__x_jt,\
                                                       euc_coef_flu1__tt_jrhorho,\
                                                       euc_coef_flu1__phiphi_jcstt,\
                                                       hyp_coef_flu2_x__x_jt,\
                                                       hyp_coef_flu2__tt_jrhorho,\
                                                       hyp_coef_flu2__phiphi_jcstt,\
                                                       euc_coef_flu2_x__x_jt,\
                                                       euc_coef_flu2__tt_jrhorho,\
                                                       euc_coef_flu2__phiphi_jcstt,\
                                                       hyp_coef_flu3_x__x_jt,\
                                                       hyp_coef_flu3__tt_jrhorho,\
                                                       hyp_coef_flu3__phiphi_jcstt,\
                                                       euc_coef_flu3_x__x_jt,\
                                                       euc_coef_flu3__tt_jrhorho,\
                                                       euc_coef_flu3__phiphi_jcstt,\
):


    

    


        #1
        g_n_t_X=g_n_t__t_jX.imag
        g_n_t_t=g_n_t__t_jX.real
        do_plot=False
        Lambda_cosmological_constant_=0
        #1.11e-52 #(m^(-2))

        #2
        g_n_X_X=g_n__rho2_jX2.imag
        g_n_rho_rho=g_n__rho2_jX2.real


        #3
        g_n_phi_phi=g__v_jn__phiphi.imag
        g_v_phi_phi=g__v_jn__phiphi.real



        #4
        g_v_t_X=g_v_t__t_jX.imag
        g_v_t_t=g_v_t__t_jX.real

        #5
        g_v_X_X=g_v__rho2_jX2.imag
        g_v_rho_rho=g_v__rho2_jX2.real

        #5
        meshed_sin_theta=meshed__cos_jsin__theta.imag
        meshed_cos_theta=meshed__cos_jsin__theta.real

        #6
        sqrt_lambda_minus=sqrt_lambda__pl_jmin.imag
        sqrt_lambda_plus=sqrt_lambda__pl_jmin.real

        #7
        A=G__t_t_jA.imag
        G__t_t=G__t_t_jA.real

        #8
        G__t_rho=G__t__x_jrho.imag
        G__t_x=G__t__x_jrho.real

        #9
        G__x_rho=G__x__x_jrho.imag
        G__x_x=G__x__x_jrho.real

        #10
        G__phi_phi=G_x__rho2_jphi2.imag
        G__rho_rho=G_x__rho2_jphi2.real

        # there are seven tt tx trho , xx xrho, rhorho, phiphi
        #11


        hyp_flu1_x_t=hyp_coef_flu1_x__x_jt.imag
        hyp_flu1_x_x=hyp_coef_flu1_x__x_jt.real

        #12
        hyp_flu1_rho_rho=hyp_coef_flu1__tt_jrhorho.imag
        hyp_flu1_t_t=hyp_coef_flu1__tt_jrhorho.real

        #13
        hyp_flu1_cstt=hyp_coef_flu1__phiphi_jcstt.imag
        hyp_flu1_phiphi=hyp_coef_flu1__phiphi_jcstt.real

        #14

        euc_flu1_x_t=euc_coef_flu1_x__x_jt.imag
        euc_flu1_x_x=euc_coef_flu1_x__x_jt.real


        euc_flu1_rho_rho=euc_coef_flu1__tt_jrhorho.imag
        euc_flu1_t_t=euc_coef_flu1__tt_jrhorho.real


        euc_flu1_cstt=euc_coef_flu1__phiphi_jcstt.imag
        euc_flu1_phiphi=euc_coef_flu1__phiphi_jcstt.real
        ################################
        hyp_flu2_x_t=hyp_coef_flu2_x__x_jt.imag
        hyp_flu2_x_x=hyp_coef_flu2_x__x_jt.real


        hyp_flu2_rho_rho=hyp_coef_flu2__tt_jrhorho.imag
        hyp_flu2_t_t=hyp_coef_flu2__tt_jrhorho.real


        hyp_flu2_cstt=hyp_coef_flu2__phiphi_jcstt.imag
        hyp_flu2_phiphi=hyp_coef_flu2__phiphi_jcstt.real



        euc_flu2_x_t=euc_coef_flu2_x__x_jt.imag
        euc_flu2_x_x=euc_coef_flu2_x__x_jt.real


        euc_flu2_rho_rho=euc_coef_flu2__tt_jrhorho.imag
        euc_flu2_t_t=euc_coef_flu2__tt_jrhorho.real


        euc_flu2_cstt=euc_coef_flu2__phiphi_jcstt.imag
        euc_flu2_phiphi=euc_coef_flu2__phiphi_jcstt.real

        ##################################
        hyp_flu3_x_t=hyp_coef_flu3_x__x_jt.imag
        hyp_flu3_x_x=hyp_coef_flu3_x__x_jt.real


        hyp_flu3_rho_rho=hyp_coef_flu3__tt_jrhorho.imag
        hyp_flu3_t_t=hyp_coef_flu3__tt_jrhorho.real


        hyp_flu3_cstt=hyp_coef_flu3__phiphi_jcstt.imag
        hyp_flu3_phiphi=hyp_coef_flu3__phiphi_jcstt.real



        euc_flu3_x_t=euc_coef_flu3_x__x_jt.imag
        euc_flu3_x_x=euc_coef_flu3_x__x_jt.real


        euc_flu3_rho_rho=euc_coef_flu3__tt_jrhorho.imag
        euc_flu3_t_t=euc_coef_flu3__tt_jrhorho.real


        euc_flu3_cstt=euc_coef_flu3__phiphi_jcstt.imag
        euc_flu3_phiphi=euc_coef_flu3__phiphi_jcstt.real





    
        # res_batch=res_batch__1j_LAMBDA.real
        # Lambda_cosmological_constant_=res_batch__1j_LAMBDA.imag

        #do_print_stuff=True
        do_print_stuff=False

        #do_print_stuff2=True
        do_print_stuff2=False


        do_print_stuff3=False
        inputs={}

        do_print_stuff4=False

        do_print_stuff5=False
        #do_print_stuff5=True
        ######################################################
        # correction on g_phi,phi
        #below
        if g_n_phi_phi==0 or np.isinf(g_n_phi_phi):
            g_n_phi_phi=1

        if g_v_phi_phi==0 or np.isinf(g_v_phi_phi):
            g_v_phi_phi=1

        if np.isnan(g_v_phi_phi) or np.isnan(g_n_phi_phi):
            g_n_phi_phi=1
            g_v_phi_phi=1



        inputs["g_n_t_t"]=g_n_t_t
        inputs["g_n_t_X"]=g_n_t_X
        inputs["g_n_X_X"]=g_n_X_X
        inputs["g_n_rho_rho"]=g_n_rho_rho
        inputs["g_n_phi_phi"]=g_n_phi_phi


        inputs["g_v_t_t"]=g_v_t_t
        inputs["g_v_t_X"]=g_v_t_X
        inputs["g_v_X_X"]=g_v_X_X
        inputs["g_v_rho_rho"]=g_v_rho_rho
        inputs["g_v_phi_phi"]=g_v_phi_phi

        inputs["sqrt_lambda_minus"]=sqrt_lambda_minus
        inputs["sqrt_lambda_plus"]=sqrt_lambda_plus



        ########################################################
        # we compute G_doubly covariant!!
        G_doubly_cov=np.array( [ [ G__t_t   , G__t_x   , G__t_rho   , 0 ],\
                                 [ G__t_x   , G__x_x   , G__x_rho   , 0 ],\
                                 [ G__t_rho , G__x_rho , G__rho_rho , 0 ],\
                                 [ 0        , 0        , 0          , G__phi_phi ],\
        ])
        #######################################################
        # below are the numpy equivalents of lines / rows

        g_n_mat=np.array([[inputs["g_n_t_t"],inputs["g_n_t_X"],0,0],\
                          [inputs["g_n_t_X"],inputs["g_n_X_X"],0,0],\
                          [0                ,0                ,inputs["g_n_rho_rho"],0],\
                          [0                ,0                ,0                    ,inputs["g_n_phi_phi"]]])

        g_v_mat=np.array([[inputs["g_v_t_t"],inputs["g_v_t_X"],0,0],\
                          [inputs["g_v_t_X"],inputs["g_v_X_X"],0,0],\
                          [0                ,0                ,inputs["g_v_rho_rho"],0],\
                          [0                ,0                ,0                    ,inputs["g_v_phi_phi"]]])



        lambda_plus=(-(A-1)+np.sqrt((A-1)**2+4))/2.
        lambda_minus=(-(A-1)-np.sqrt((A-1)**2+4))/2.
        tW=np.array([[meshed_cos_theta,0,meshed_sin_theta*1./sqrt_lambda_plus],\
                     [0,1,0],\
                     [-meshed_sin_theta*sqrt_lambda_plus,0,meshed_cos_theta],\
                     ])

        return_to_starting_block=True
        overflow_counting=0

        if 1:
            #print('choose tw4 ---> make a function which creates such matrices \n line 1640')



            tW_4,\
                iter_overflow=two_fluids___rotation_euc_hyp__g_v__choice_tW_matrix(euc_flu3_x_t,\
                                                                      euc_flu3_x_x,\
                                                                      euc_flu3_rho_rho,\
                                                                      euc_flu3_t_t,\
                                                                      euc_flu3_cstt,\
                                                                      euc_flu3_phiphi,\
                                                                      hyp_flu1_x_t,\
                                                                      hyp_flu1_x_x,\
                                                                      hyp_flu1_rho_rho,\
                                                                      hyp_flu1_t_t,\
                                                                      hyp_flu1_cstt,\
                                                                      hyp_flu1_phiphi,\
                                                                      euc_flu1_x_t,\
                                                                      euc_flu1_x_x,\
                                                                      euc_flu1_rho_rho,\
                                                                      euc_flu1_t_t,\
                                                                      euc_flu1_cstt,\
                                                                      euc_flu1_phiphi,\
                                                                      inputs,\
            )

            tW_4_second_fluid,\
                iter_overflow_second=two_fluids___rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu3_x_t,\
                                                                             hyp_flu3_x_x,\
                                                                             hyp_flu3_rho_rho,\
                                                                             hyp_flu3_t_t,\
                                                                             hyp_flu3_cstt,\
                                                                             hyp_flu3_phiphi,\
                                                                             hyp_flu2_x_t,\
                                                                             hyp_flu2_x_x,\
                                                                             hyp_flu2_rho_rho,\
                                                                             hyp_flu2_t_t,\
                                                                             hyp_flu2_cstt,\
                                                                             hyp_flu2_phiphi,\
                                                                             euc_flu2_x_t,\
                                                                             euc_flu2_x_x,\
                                                                             euc_flu2_rho_rho,\
                                                                             euc_flu2_t_t,\
                                                                             euc_flu2_cstt,\
                                                                             euc_flu2_phiphi,\
                                                                             inputs,\
            )


 





            #interesting line below!!
            ####    g_v_t_X=v*(1-meshed_f)

            N_plus=np.sqrt((lambda_plus-1)**2+g_v_t_X**2)
            N_minus=np.sqrt((lambda_minus-1)**2+g_v_t_X**2)

            #################################
            # now we just examine the theta matrix
            #eigen_vect_g_doublcovariant=EVGDCov
            #{EVGDCov=theta matrix} is the matrix of eigen vectors of g_{mu,nu}

            EVGDCov=np.array([[(lambda_plus-1.0)/N_plus,(lambda_minus-1.0)/N_minus,0,0],\
                             [g_v_t_X/(1.0*N_plus),g_v_t_X/(1.0*N_minus),0,0],\
                             [0,0,1.,0],\
                             [0,0,0,1.]])
            the_product_should_be_diagonal=np.dot(EVGDCov.T,np.dot(g_v_mat,EVGDCov))

            ##################################

            # the following is contravariant (each nested array is not a desirable vector rather this is component 1 of the first array and then comp 1 of the second, etc.
            e_not_prime=np.array([[-1,0,0,0],\
                                  [g_v_t_X,1,0,0],\
                                  [0,0,1,0],\
                                  [0,0,0,1/np.sqrt(g_v_phi_phi) ],\
            ])

            minkowski_metric=np.diag([-1,1,1,1])
            e_prime=np.dot(EVGDCov,np.dot(tW_4,np.dot(EVGDCov.T,e_not_prime)))
            e_prime_2nd=np.dot(EVGDCov,np.dot(tW_4_second_fluid,np.dot(EVGDCov.T,e_not_prime)))

            # these two vectors are contravariant components


            e_prime_2nd_cov=np.dot(g_v_mat,e_prime_2nd)# in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures



            # e_prime is described as sets of lines , each dissecting interesting vectors (vectors as columns)
            #e_prime here is contravariant
            #below we compute the covariant vectors in a single matrix


            e_prime_cov=np.dot(g_v_mat,e_prime)# in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures



            # we compute the mixed components of Einstein tensor
            G_einst_mixed_plus_lambda_delta=np.dot(g_n_mat,G_doubly_cov+Lambda_cosmological_constant_*g_v_mat )






            #############################################
 
            basis_vectors_2fluids=[[e_prime_cov,e_prime],[e_prime_2nd_cov,e_prime_2nd]]
            big_matrix_2flu,gee_vector_2flu,overflow_big_mat=compute_big_matrix_for_fluids(basis_vectors_2fluids,G_einst_mixed_plus_lambda_delta)



            if np.any(np.isnan(big_matrix_2flu)) or np.any(np.isinf(big_matrix_2flu)) or \
               np.any(np.isnan(gee_vector_2flu)) or np.any(np.isinf(gee_vector_2flu)):
                return_to_starting_block=True

                hyp_flu1_x_t=hyp_flu1_x_t/3.
                hyp_flu1_x_x=hyp_flu1_x_x/3.
                hyp_flu1_rho_rho=hyp_flu1_rho_rho/3.
                hyp_flu1_t_t=hyp_flu1_t_t/3.
                hyp_flu1_cstt=hyp_flu1_cstt/3.
                hyp_flu1_phiphi=hyp_flu1_phiphi/3.

                hyp_flu2_x_t=hyp_flu2_x_t/3.
                hyp_flu2_x_x=hyp_flu2_x_x/3.
                hyp_flu2_rho_rho=hyp_flu2_rho_rho/3.
                hyp_flu2_t_t=hyp_flu2_t_t/3.
                hyp_flu2_cstt=hyp_flu2_cstt/3.
                hyp_flu2_phiphi=hyp_flu2_phiphi/3.

                #this below is only for two fluids as the 3rd fluid parameters are actually for 2 fluids parameterization of the second boost
                hyp_flu3_x_t=hyp_flu3_x_t/3.
                hyp_flu3_x_x=hyp_flu3_x_x/3.
                hyp_flu3_rho_rho=hyp_flu3_rho_rho/3.
                hyp_flu3_t_t=hyp_flu3_t_t/3.
                hyp_flu3_cstt=hyp_flu3_cstt/3.
                hyp_flu3_phiphi=hyp_flu3_phiphi/3.

                euc_flu3_x_t=euc_flu3_x_t/3.
                euc_flu3_x_x=euc_flu3_x_x/3.
                euc_flu3_rho_rho=euc_flu3_rho_rho/3.
                euc_flu3_t_t=euc_flu3_t_t/3.
                euc_flu3_cstt=euc_flu3_cstt/3.
                euc_flu3_phiphi=euc_flu3_phiphi/3.


                overflow_counting+=2
                # above if inf or nan we decrease the value of all coefficients
                densities_pressures_2fluids=[-11,-11,-11,-11,-11,-11,-11,-11]
                matrix_rank_2fluids=-1
                sing_values_matrix_2fluids=[0,0,0,0,0,0,0,0,0,0,0,0,0]

            else:
                return_to_starting_block=False
                densities_pressures_2fluids,\
                    residuals_2fluids,\
                    matrix_rank_2fluids,\
                    sing_values_matrix_2fluids=np.linalg.lstsq(big_matrix_2flu, gee_vector_2flu,rcond=None)

        ##############################################################

        

        ##############################################################################################################################





        res=list(densities_pressures_2fluids)

        v_p=np.dot(big_matrix_2flu,densities_pressures_2fluids) # vector predicted
        v_r=gee_vector_2flu
    
        denom=np.array([el if el !=0 else 1 for el in list(np.abs(v_p)+np.abs(v_r)) ])
        diff_sur_som=np.abs(v_p-v_r)/denom



        non_zero_sigmas=[el for el in sing_values_matrix_2fluids if el !=0]
        if sing_values_matrix_2fluids[6]==0:
            ratio_sigmas=0
        else:
            ratio_sigmas=sing_values_matrix_2fluids[0]/sing_values_matrix_2fluids[6]

        return diff_sur_som[np.argmax(np.abs(v_r))]+1j*sing_values_matrix_2fluids[0],\
            matrix_rank_2fluids+1j*sing_values_matrix_2fluids[6],\
            ratio_sigmas+1j*np.min(sing_values_matrix_2fluids)
    
#####################################################################################################################################################
###############################################################################################################################################

    
def two_fluids__element_wise__analyse__dif_over_sum__einst__tensor___myfunc(g_n_t__t_jX,\
                                                       g_n__rho2_jX2,\
                                                       g__v_jn__phiphi,\
                                                       g_v_t__t_jX,\
                                                       g_v__rho2_jX2,\
                                                       meshed__cos_jsin__theta,\
                                                       sqrt_lambda__pl_jmin,\
                                                       G__t_t_jA,\
                                                       G__t__x_jrho,\
                                                       G__x__x_jrho,\
                                                       G_x__rho2_jphi2,\
                                                       hyp_coef_flu1_x__x_jt,\
                                                       hyp_coef_flu1__tt_jrhorho,\
                                                       hyp_coef_flu1__phiphi_jcstt,\
                                                       euc_coef_flu1_x__x_jt,\
                                                       euc_coef_flu1__tt_jrhorho,\
                                                       euc_coef_flu1__phiphi_jcstt,\
                                                       hyp_coef_flu2_x__x_jt,\
                                                       hyp_coef_flu2__tt_jrhorho,\
                                                       hyp_coef_flu2__phiphi_jcstt,\
                                                       euc_coef_flu2_x__x_jt,\
                                                       euc_coef_flu2__tt_jrhorho,\
                                                       euc_coef_flu2__phiphi_jcstt,\
                                                       hyp_coef_flu3_x__x_jt,\
                                                       hyp_coef_flu3__tt_jrhorho,\
                                                       hyp_coef_flu3__phiphi_jcstt,\
                                                       euc_coef_flu3_x__x_jt,\
                                                       euc_coef_flu3__tt_jrhorho,\
                                                       euc_coef_flu3__phiphi_jcstt,\
):


    

    


        #1
        g_n_t_X=g_n_t__t_jX.imag
        g_n_t_t=g_n_t__t_jX.real
        do_plot=False
        Lambda_cosmological_constant_=0
        #1.11e-52 #(m^(-2))

        #2
        g_n_X_X=g_n__rho2_jX2.imag
        g_n_rho_rho=g_n__rho2_jX2.real


        #3
        g_n_phi_phi=g__v_jn__phiphi.imag
        g_v_phi_phi=g__v_jn__phiphi.real



        #4
        g_v_t_X=g_v_t__t_jX.imag
        g_v_t_t=g_v_t__t_jX.real

        #5
        g_v_X_X=g_v__rho2_jX2.imag
        g_v_rho_rho=g_v__rho2_jX2.real

        #5
        meshed_sin_theta=meshed__cos_jsin__theta.imag
        meshed_cos_theta=meshed__cos_jsin__theta.real

        #6
        sqrt_lambda_minus=sqrt_lambda__pl_jmin.imag
        sqrt_lambda_plus=sqrt_lambda__pl_jmin.real

        #7
        A=G__t_t_jA.imag
        G__t_t=G__t_t_jA.real

        #8
        G__t_rho=G__t__x_jrho.imag
        G__t_x=G__t__x_jrho.real

        #9
        G__x_rho=G__x__x_jrho.imag
        G__x_x=G__x__x_jrho.real

        #10
        G__phi_phi=G_x__rho2_jphi2.imag
        G__rho_rho=G_x__rho2_jphi2.real

        # there are seven tt tx trho , xx xrho, rhorho, phiphi
        #11


        hyp_flu1_x_t=hyp_coef_flu1_x__x_jt.imag
        hyp_flu1_x_x=hyp_coef_flu1_x__x_jt.real

        #12
        hyp_flu1_rho_rho=hyp_coef_flu1__tt_jrhorho.imag
        hyp_flu1_t_t=hyp_coef_flu1__tt_jrhorho.real

        #13
        hyp_flu1_cstt=hyp_coef_flu1__phiphi_jcstt.imag
        hyp_flu1_phiphi=hyp_coef_flu1__phiphi_jcstt.real

        #14

        euc_flu1_x_t=euc_coef_flu1_x__x_jt.imag
        euc_flu1_x_x=euc_coef_flu1_x__x_jt.real


        euc_flu1_rho_rho=euc_coef_flu1__tt_jrhorho.imag
        euc_flu1_t_t=euc_coef_flu1__tt_jrhorho.real


        euc_flu1_cstt=euc_coef_flu1__phiphi_jcstt.imag
        euc_flu1_phiphi=euc_coef_flu1__phiphi_jcstt.real
        ################################
        hyp_flu2_x_t=hyp_coef_flu2_x__x_jt.imag
        hyp_flu2_x_x=hyp_coef_flu2_x__x_jt.real


        hyp_flu2_rho_rho=hyp_coef_flu2__tt_jrhorho.imag
        hyp_flu2_t_t=hyp_coef_flu2__tt_jrhorho.real


        hyp_flu2_cstt=hyp_coef_flu2__phiphi_jcstt.imag
        hyp_flu2_phiphi=hyp_coef_flu2__phiphi_jcstt.real



        euc_flu2_x_t=euc_coef_flu2_x__x_jt.imag
        euc_flu2_x_x=euc_coef_flu2_x__x_jt.real


        euc_flu2_rho_rho=euc_coef_flu2__tt_jrhorho.imag
        euc_flu2_t_t=euc_coef_flu2__tt_jrhorho.real


        euc_flu2_cstt=euc_coef_flu2__phiphi_jcstt.imag
        euc_flu2_phiphi=euc_coef_flu2__phiphi_jcstt.real

        ##################################
        hyp_flu3_x_t=hyp_coef_flu3_x__x_jt.imag
        hyp_flu3_x_x=hyp_coef_flu3_x__x_jt.real


        hyp_flu3_rho_rho=hyp_coef_flu3__tt_jrhorho.imag
        hyp_flu3_t_t=hyp_coef_flu3__tt_jrhorho.real


        hyp_flu3_cstt=hyp_coef_flu3__phiphi_jcstt.imag
        hyp_flu3_phiphi=hyp_coef_flu3__phiphi_jcstt.real



        euc_flu3_x_t=euc_coef_flu3_x__x_jt.imag
        euc_flu3_x_x=euc_coef_flu3_x__x_jt.real


        euc_flu3_rho_rho=euc_coef_flu3__tt_jrhorho.imag
        euc_flu3_t_t=euc_coef_flu3__tt_jrhorho.real


        euc_flu3_cstt=euc_coef_flu3__phiphi_jcstt.imag
        euc_flu3_phiphi=euc_coef_flu3__phiphi_jcstt.real





    
        # res_batch=res_batch__1j_LAMBDA.real
        # Lambda_cosmological_constant_=res_batch__1j_LAMBDA.imag





        #do_print_stuff=True
        do_print_stuff=False

        #do_print_stuff2=True
        do_print_stuff2=False


        do_print_stuff3=False
        inputs={}

        do_print_stuff4=False

        do_print_stuff5=False
        #do_print_stuff5=True
        ######################################################
        # correction on g_phi,phi
        #below
        if g_n_phi_phi==0 or np.isinf(g_n_phi_phi):
            g_n_phi_phi=1

        if g_v_phi_phi==0 or np.isinf(g_v_phi_phi):
            g_v_phi_phi=1

        if np.isnan(g_v_phi_phi) or np.isnan(g_n_phi_phi):
            g_n_phi_phi=1
            g_v_phi_phi=1



        inputs["g_n_t_t"]=g_n_t_t
        inputs["g_n_t_X"]=g_n_t_X
        inputs["g_n_X_X"]=g_n_X_X
        inputs["g_n_rho_rho"]=g_n_rho_rho
        inputs["g_n_phi_phi"]=g_n_phi_phi


        inputs["g_v_t_t"]=g_v_t_t
        inputs["g_v_t_X"]=g_v_t_X
        inputs["g_v_X_X"]=g_v_X_X
        inputs["g_v_rho_rho"]=g_v_rho_rho
        inputs["g_v_phi_phi"]=g_v_phi_phi

        inputs["sqrt_lambda_minus"]=sqrt_lambda_minus
        inputs["sqrt_lambda_plus"]=sqrt_lambda_plus



        ########################################################
        # we compute G_doubly covariant!!
        G_doubly_cov=np.array( [ [ G__t_t   , G__t_x   , G__t_rho   , 0 ],\
                                 [ G__t_x   , G__x_x   , G__x_rho   , 0 ],\
                                 [ G__t_rho , G__x_rho , G__rho_rho , 0 ],\
                                 [ 0        , 0        , 0          , G__phi_phi ],\
        ])
        #######################################################
        # below are the numpy equivalents of lines / rows

        g_n_mat=np.array([[inputs["g_n_t_t"],inputs["g_n_t_X"],0,0],\
                          [inputs["g_n_t_X"],inputs["g_n_X_X"],0,0],\
                          [0                ,0                ,inputs["g_n_rho_rho"],0],\
                          [0                ,0                ,0                    ,inputs["g_n_phi_phi"]]])

        g_v_mat=np.array([[inputs["g_v_t_t"],inputs["g_v_t_X"],0,0],\
                          [inputs["g_v_t_X"],inputs["g_v_X_X"],0,0],\
                          [0                ,0                ,inputs["g_v_rho_rho"],0],\
                          [0                ,0                ,0                    ,inputs["g_v_phi_phi"]]])



        lambda_plus=(-(A-1)+np.sqrt((A-1)**2+4))/2.
        lambda_minus=(-(A-1)-np.sqrt((A-1)**2+4))/2.
        tW=np.array([[meshed_cos_theta,0,meshed_sin_theta*1./sqrt_lambda_plus],\
                     [0,1,0],\
                     [-meshed_sin_theta*sqrt_lambda_plus,0,meshed_cos_theta],\
                     ])

        return_to_starting_block=True
        overflow_counting=0

        if 1:
            #print('choose tw4 ---> make a function which creates such matrices \n line 1640')
    


            tW_4,\
                iter_overflow=two_fluids___rotation_euc_hyp__g_v__choice_tW_matrix(euc_flu3_x_t,\
                                                                      euc_flu3_x_x,\
                                                                      euc_flu3_rho_rho,\
                                                                      euc_flu3_t_t,\
                                                                      euc_flu3_cstt,\
                                                                      euc_flu3_phiphi,\
                                                                      hyp_flu1_x_t,\
                                                                      hyp_flu1_x_x,\
                                                                      hyp_flu1_rho_rho,\
                                                                      hyp_flu1_t_t,\
                                                                      hyp_flu1_cstt,\
                                                                      hyp_flu1_phiphi,\
                                                                      euc_flu1_x_t,\
                                                                      euc_flu1_x_x,\
                                                                      euc_flu1_rho_rho,\
                                                                      euc_flu1_t_t,\
                                                                      euc_flu1_cstt,\
                                                                      euc_flu1_phiphi,\
                                                                      inputs,\
            )

            tW_4_second_fluid,\
                iter_overflow_second=two_fluids___rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu3_x_t,\
                                                                             hyp_flu3_x_x,\
                                                                             hyp_flu3_rho_rho,\
                                                                             hyp_flu3_t_t,\
                                                                             hyp_flu3_cstt,\
                                                                             hyp_flu3_phiphi,\
                                                                             hyp_flu2_x_t,\
                                                                             hyp_flu2_x_x,\
                                                                             hyp_flu2_rho_rho,\
                                                                             hyp_flu2_t_t,\
                                                                             hyp_flu2_cstt,\
                                                                             hyp_flu2_phiphi,\
                                                                             euc_flu2_x_t,\
                                                                             euc_flu2_x_x,\
                                                                             euc_flu2_rho_rho,\
                                                                             euc_flu2_t_t,\
                                                                             euc_flu2_cstt,\
                                                                             euc_flu2_phiphi,\
                                                                             inputs,\
            )


 





            #interesting line below!!
            ####    g_v_t_X=v*(1-meshed_f)

            N_plus=np.sqrt((lambda_plus-1)**2+g_v_t_X**2)
            N_minus=np.sqrt((lambda_minus-1)**2+g_v_t_X**2)

            #################################
            # now we just examine the theta matrix
            #eigen_vect_g_doublcovariant=EVGDCov
            #{EVGDCov=theta matrix} is the matrix of eigen vectors of g_{mu,nu}

            EVGDCov=np.array([[(lambda_plus-1.0)/N_plus,(lambda_minus-1.0)/N_minus,0,0],\
                             [g_v_t_X/(1.0*N_plus),g_v_t_X/(1.0*N_minus),0,0],\
                             [0,0,1.,0],\
                             [0,0,0,1.]])
            the_product_should_be_diagonal=np.dot(EVGDCov.T,np.dot(g_v_mat,EVGDCov))

            ##################################

            # the following is contravariant (each nested array is not a desirable vector rather this is component 1 of the first array and then comp 1 of the second, etc.
            e_not_prime=np.array([[-1,0,0,0],\
                                  [g_v_t_X,1,0,0],\
                                  [0,0,1,0],\
                                  [0,0,0,1/np.sqrt(g_v_phi_phi) ],\
            ])

            minkowski_metric=np.diag([-1,1,1,1])
            e_prime=np.dot(EVGDCov,np.dot(tW_4,np.dot(EVGDCov.T,e_not_prime)))
            e_prime_2nd=np.dot(EVGDCov,np.dot(tW_4_second_fluid,np.dot(EVGDCov.T,e_not_prime)))

            # these two vectors are contravariant components


            e_prime_2nd_cov=np.dot(g_v_mat,e_prime_2nd)# in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures



            # e_prime is described as sets of lines , each dissecting interesting vectors (vectors as columns)
            #e_prime here is contravariant
            #below we compute the covariant vectors in a single matrix


            e_prime_cov=np.dot(g_v_mat,e_prime)# in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures



            # we compute the mixed components of Einstein tensor
            G_einst_mixed_plus_lambda_delta=np.dot(g_n_mat,G_doubly_cov+Lambda_cosmological_constant_*g_v_mat )






            #############################################
 
            basis_vectors_2fluids=[[e_prime_cov,e_prime],[e_prime_2nd_cov,e_prime_2nd]]
            big_matrix_2flu,gee_vector_2flu,overflow_big_mat=compute_big_matrix_for_fluids(basis_vectors_2fluids,G_einst_mixed_plus_lambda_delta)



            if np.any(np.isnan(big_matrix_2flu)) or np.any(np.isinf(big_matrix_2flu)) or \
               np.any(np.isnan(gee_vector_2flu)) or np.any(np.isinf(gee_vector_2flu)):
                return_to_starting_block=True

                hyp_flu1_x_t=hyp_flu1_x_t/3.
                hyp_flu1_x_x=hyp_flu1_x_x/3.
                hyp_flu1_rho_rho=hyp_flu1_rho_rho/3.
                hyp_flu1_t_t=hyp_flu1_t_t/3.
                hyp_flu1_cstt=hyp_flu1_cstt/3.
                hyp_flu1_phiphi=hyp_flu1_phiphi/3.

                hyp_flu2_x_t=hyp_flu2_x_t/3.
                hyp_flu2_x_x=hyp_flu2_x_x/3.
                hyp_flu2_rho_rho=hyp_flu2_rho_rho/3.
                hyp_flu2_t_t=hyp_flu2_t_t/3.
                hyp_flu2_cstt=hyp_flu2_cstt/3.
                hyp_flu2_phiphi=hyp_flu2_phiphi/3.

                #this below is only for two fluids as the 3rd fluid parameters are actually for 2 fluids parameterization of the second boost
                hyp_flu3_x_t=hyp_flu3_x_t/3.
                hyp_flu3_x_x=hyp_flu3_x_x/3.
                hyp_flu3_rho_rho=hyp_flu3_rho_rho/3.
                hyp_flu3_t_t=hyp_flu3_t_t/3.
                hyp_flu3_cstt=hyp_flu3_cstt/3.
                hyp_flu3_phiphi=hyp_flu3_phiphi/3.

                euc_flu3_x_t=euc_flu3_x_t/3.
                euc_flu3_x_x=euc_flu3_x_x/3.
                euc_flu3_rho_rho=euc_flu3_rho_rho/3.
                euc_flu3_t_t=euc_flu3_t_t/3.
                euc_flu3_cstt=euc_flu3_cstt/3.
                euc_flu3_phiphi=euc_flu3_phiphi/3.


                overflow_counting+=2
                # above if inf or nan we decrease the value of all coefficients
                densities_pressures_2fluids=[-11,-11,-11,-11,-11,-11,-11,-11]
                matrix_rank_2fluids=-1
                sing_values_matrix_2fluids=[0,0,0,0,0,0,0,0,0,0,0,0,0]

            else:
                return_to_starting_block=False
                densities_pressures_2fluids,\
                    residuals_2fluids,\
                    matrix_rank_2fluids,\
                    sing_values_matrix_2fluids=np.linalg.lstsq(big_matrix_2flu, gee_vector_2flu,rcond=None)

        ##############################################################

        

        ##############################################################################################################################





        res=list(densities_pressures_2fluids)

        v_p=np.dot(big_matrix_2flu,densities_pressures_2fluids) # vector predicted
        v_r=gee_vector_2flu
    
        #######################################################################################
        if np.linalg.norm(v_r)!=0:
            sin_t_h_e_t_a__error=np.linalg.norm(v_p-v_r)/np.linalg.norm(v_r)
        else:
            sin_t_h_e_t_a__error=0.


        denom=np.array([el if el !=0 else 1 for el in list(np.abs(v_p)+np.abs(v_r)) ])


        diff_sur_som=np.abs(v_p-v_r)/denom




        return sin_t_h_e_t_a__error,-1,matrix_rank_2fluids
#####################################################################################################################################################
    
def three_fluids__element_wise__analyse__dif_over_sum__einst__tensor___myfunc(g_n_t__t_jX,\
                                                                              g_n__rho2_jX2,\
                                                                              g__v_jn__phiphi,\
                                                                              g_v_t__t_jX,\
                                                                              g_v__rho2_jX2,\
                                                                              meshed__cos_jsin__theta,\
                                                                              sqrt_lambda__pl_jmin,\
                                                                              G__t_t_jA,\
                                                                              G__t__x_jrho,\
                                                                              G__x__x_jrho,\
                                                                              G_x__rho2_jphi2,\
                                                                              hyp_coef_flu1_x__x_jt,\
                                                                              hyp_coef_flu1__tt_jrhorho,\
                                                                              hyp_coef_flu1__phiphi_jcstt,\
                                                                              euc_coef_flu1_x__x_jt,\
                                                                              euc_coef_flu1__tt_jrhorho,\
                                                                              euc_coef_flu1__phiphi_jcstt,\
                                                                              hyp_coef_flu2_x__x_jt,\
                                                                              hyp_coef_flu2__tt_jrhorho,\
                                                                              hyp_coef_flu2__phiphi_jcstt,\
                                                                              euc_coef_flu2_x__x_jt,\
                                                                              euc_coef_flu2__tt_jrhorho,\
                                                                              euc_coef_flu2__phiphi_jcstt,\
                                                                              hyp_coef_flu3_x__x_jt,\
                                                                              hyp_coef_flu3__tt_jrhorho,\
                                                                              hyp_coef_flu3__phiphi_jcstt,\
                                                                              euc_coef_flu3_x__x_jt,\
                                                                              euc_coef_flu3__tt_jrhorho,\
                                                                              euc_coef_flu3__phiphi_jcstt,\
):
    bad_inf_nan=0


    do_plot=False

    #1
    g_n_t_X=g_n_t__t_jX.imag
    g_n_t_t=g_n_t__t_jX.real

    #2
    g_n_X_X=g_n__rho2_jX2.imag
    g_n_rho_rho=g_n__rho2_jX2.real


    #3
    g_n_phi_phi=g__v_jn__phiphi.imag
    g_v_phi_phi=g__v_jn__phiphi.real



    #4
    g_v_t_X=g_v_t__t_jX.imag
    g_v_t_t=g_v_t__t_jX.real

    #5
    g_v_X_X=g_v__rho2_jX2.imag
    g_v_rho_rho=g_v__rho2_jX2.real

    #5
    meshed_sin_theta=meshed__cos_jsin__theta.imag
    meshed_cos_theta=meshed__cos_jsin__theta.real

    #6
    sqrt_lambda_minus=sqrt_lambda__pl_jmin.imag
    sqrt_lambda_plus=sqrt_lambda__pl_jmin.real

    #7
    A=G__t_t_jA.imag
    G__t_t=G__t_t_jA.real

    #8
    G__t_rho=G__t__x_jrho.imag
    G__t_x=G__t__x_jrho.real

    #9
    G__x_rho=G__x__x_jrho.imag
    G__x_x=G__x__x_jrho.real

    #10
    G__phi_phi=G_x__rho2_jphi2.imag
    G__rho_rho=G_x__rho2_jphi2.real

    # there are seven tt tx trho , xx xrho, rhorho, phiphi
    #11


    hyp_flu1_x_t=hyp_coef_flu1_x__x_jt.imag
    hyp_flu1_x_x=hyp_coef_flu1_x__x_jt.real

    #12
    hyp_flu1_rho_rho=hyp_coef_flu1__tt_jrhorho.imag
    hyp_flu1_t_t=hyp_coef_flu1__tt_jrhorho.real

    #13
    hyp_flu1_cstt=hyp_coef_flu1__phiphi_jcstt.imag
    hyp_flu1_phiphi=hyp_coef_flu1__phiphi_jcstt.real

    #14

    euc_flu1_x_t=euc_coef_flu1_x__x_jt.imag
    euc_flu1_x_x=euc_coef_flu1_x__x_jt.real


    euc_flu1_rho_rho=euc_coef_flu1__tt_jrhorho.imag
    euc_flu1_t_t=euc_coef_flu1__tt_jrhorho.real


    euc_flu1_cstt=euc_coef_flu1__phiphi_jcstt.imag
    euc_flu1_phiphi=euc_coef_flu1__phiphi_jcstt.real
    ################################
    hyp_flu2_x_t=hyp_coef_flu2_x__x_jt.imag
    hyp_flu2_x_x=hyp_coef_flu2_x__x_jt.real


    hyp_flu2_rho_rho=hyp_coef_flu2__tt_jrhorho.imag
    hyp_flu2_t_t=hyp_coef_flu2__tt_jrhorho.real


    hyp_flu2_cstt=hyp_coef_flu2__phiphi_jcstt.imag
    hyp_flu2_phiphi=hyp_coef_flu2__phiphi_jcstt.real



    euc_flu2_x_t=euc_coef_flu2_x__x_jt.imag
    euc_flu2_x_x=euc_coef_flu2_x__x_jt.real


    euc_flu2_rho_rho=euc_coef_flu2__tt_jrhorho.imag
    euc_flu2_t_t=euc_coef_flu2__tt_jrhorho.real


    euc_flu2_cstt=euc_coef_flu2__phiphi_jcstt.imag
    euc_flu2_phiphi=euc_coef_flu2__phiphi_jcstt.real

    ##################################
    hyp_flu3_x_t=hyp_coef_flu3_x__x_jt.imag
    hyp_flu3_x_x=hyp_coef_flu3_x__x_jt.real


    hyp_flu3_rho_rho=hyp_coef_flu3__tt_jrhorho.imag
    hyp_flu3_t_t=hyp_coef_flu3__tt_jrhorho.real


    hyp_flu3_cstt=hyp_coef_flu3__phiphi_jcstt.imag
    hyp_flu3_phiphi=hyp_coef_flu3__phiphi_jcstt.real



    euc_flu3_x_t=euc_coef_flu3_x__x_jt.imag
    euc_flu3_x_x=euc_coef_flu3_x__x_jt.real


    euc_flu3_rho_rho=euc_coef_flu3__tt_jrhorho.imag
    euc_flu3_t_t=euc_coef_flu3__tt_jrhorho.real


    euc_flu3_cstt=euc_coef_flu3__phiphi_jcstt.imag
    euc_flu3_phiphi=euc_coef_flu3__phiphi_jcstt.real








    Lambda_cosmological_constant_=0
    #1.11e-52 #(m^(-2))






    #do_print_stuff=True
    do_print_stuff=False

    #do_print_stuff2=True
    do_print_stuff2=False


    do_print_stuff3=False
    inputs={}

    do_print_stuff4=False

    do_print_stuff5=False
    ######################################################
    # correction on g_phi,phi
    #below
    if g_n_phi_phi==0 or np.isinf(g_n_phi_phi):
        g_n_phi_phi=1

    if g_v_phi_phi==0 or np.isinf(g_v_phi_phi):
        g_v_phi_phi=1

    if np.isnan(g_v_phi_phi) or np.isnan(g_n_phi_phi):
        g_n_phi_phi=1
        g_v_phi_phi=1



    inputs["g_n_t_t"]=g_n_t_t
    inputs["g_n_t_X"]=g_n_t_X
    inputs["g_n_X_X"]=g_n_X_X
    inputs["g_n_rho_rho"]=g_n_rho_rho
    inputs["g_n_phi_phi"]=g_n_phi_phi


    inputs["g_v_t_t"]=g_v_t_t
    inputs["g_v_t_X"]=g_v_t_X
    inputs["g_v_X_X"]=g_v_X_X
    inputs["g_v_rho_rho"]=g_v_rho_rho
    inputs["g_v_phi_phi"]=g_v_phi_phi

    inputs["sqrt_lambda_minus"]=sqrt_lambda_minus
    inputs["sqrt_lambda_plus"]=sqrt_lambda_plus



    ########################################################
    # we compute G_doubly covariant!!
    G_doubly_cov=np.array( [ [ G__t_t   , G__t_x   , G__t_rho   , 0 ],\
                             [ G__t_x   , G__x_x   , G__x_rho   , 0 ],\
                             [ G__t_rho , G__x_rho , G__rho_rho , 0 ],\
                             [ 0        , 0        , 0          , G__phi_phi ],\
    ])
    #######################################################
    # below are the numpy equivalents of lines / rows
    g_n_mat=np.array([[inputs["g_n_t_t"],inputs["g_n_t_X"],0,0],\
                      [inputs["g_n_t_X"],inputs["g_n_X_X"],0,0],\
                      [0                ,0                ,inputs["g_n_rho_rho"],0],\
                      [0                ,0                ,0                    ,inputs["g_n_phi_phi"]]])

    g_v_mat=np.array([[inputs["g_v_t_t"],inputs["g_v_t_X"],0,0],\
                      [inputs["g_v_t_X"],inputs["g_v_X_X"],0,0],\
                      [0                ,0                ,inputs["g_v_rho_rho"],0],\
                      [0                ,0                ,0                    ,inputs["g_v_phi_phi"]]])



    lambda_plus=(-(A-1)+np.sqrt((A-1)**2+4))/2.
    lambda_minus=(-(A-1)-np.sqrt((A-1)**2+4))/2.
    tW=np.array([[meshed_cos_theta,0,meshed_sin_theta*1./sqrt_lambda_plus],\
                 [0,1,0],\
                 [-meshed_sin_theta*sqrt_lambda_plus,0,meshed_cos_theta],\
                 ])

    return_to_starting_block=True
    if 1:


        #print('choose tw4 ---> make a function which creates such matrices \n line 1640')



        tW_4,\
            tW_4_rot_no_curv,\
            tW_4_boo_no_curv,\
            iter_overflow=three_fluids___rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu1_x_t,\
                                                                     hyp_flu1_x_x,\
                                                                     hyp_flu1_rho_rho,\
                                                                     hyp_flu1_t_t,\
                                                                     hyp_flu1_cstt,\
                                                                     hyp_flu1_phiphi,\
                                                                     euc_flu1_x_t,\
                                                                     euc_flu1_x_x,\
                                                                     euc_flu1_rho_rho,\
                                                                     euc_flu1_t_t,\
                                                                     euc_flu1_cstt,\
                                                                     euc_flu1_phiphi,\
                                                                     inputs,\
        )

        tW_4_second_fluid,\
            tW_4_second_fluid_rot_no_curv,\
            tW_4_second_fluid_boo_no_curv,\
            iter_overflow_second=three_fluids___rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu2_x_t,\
                                                                                  hyp_flu2_x_x,\
                                                                                  hyp_flu2_rho_rho,\
                                                                                  hyp_flu2_t_t,\
                                                                                  hyp_flu2_cstt,\
                                                                                  hyp_flu2_phiphi,\
                                                                                  euc_flu2_x_t,\
                                                                                  euc_flu2_x_x,\
                                                                                  euc_flu2_rho_rho,\
                                                                                  euc_flu2_t_t,\
                                                                                  euc_flu2_cstt,\
                                                                                  euc_flu2_phiphi,\
                                                                                  inputs,\
        )

        tW_4_third_fluid,\
            tW_4_third_fluid_rot_no_curv,\
            tW_4_third_fluid_boo_no_curv,\
            iter_overflow_third=three_fluids___rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu3_x_t,\
                                                                                 hyp_flu3_x_x,\
                                                                                 hyp_flu3_rho_rho,\
                                                                                 hyp_flu3_t_t,\
                                                                                 hyp_flu3_cstt,\
                                                                                 hyp_flu3_phiphi,\
                                                                                 euc_flu3_x_t,\
                                                                                 euc_flu3_x_x,\
                                                                                 euc_flu3_rho_rho,\
                                                                                 euc_flu3_t_t,\
                                                                                 euc_flu3_cstt,\
                                                                                 euc_flu3_phiphi,\
                                                                                 inputs,\
        )

        ###############################################

        tW_4__delta_error,\
            iter_overflow__delta_error=three_fluids___rotation_euc_hyp__g_v__choice_tW_matrix__delta_error(hyp_flu1_x_t,\
                                                                     hyp_flu1_x_x,\
                                                                     hyp_flu1_rho_rho,\
                                                                     hyp_flu1_t_t,\
                                                                     hyp_flu1_cstt,\
                                                                     hyp_flu1_phiphi,\
                                                                     euc_flu1_x_t,\
                                                                     euc_flu1_x_x,\
                                                                     euc_flu1_rho_rho,\
                                                                     euc_flu1_t_t,\
                                                                     euc_flu1_cstt,\
                                                                     euc_flu1_phiphi,\
                                                                     inputs,\
        )

        tW_4_second_fluid__delta_error,\
            iter_overflow_second__delta_error=three_fluids___rotation_euc_hyp__g_v__choice_tW_matrix__delta_error(hyp_flu2_x_t,\
                                                                                  hyp_flu2_x_x,\
                                                                                  hyp_flu2_rho_rho,\
                                                                                  hyp_flu2_t_t,\
                                                                                  hyp_flu2_cstt,\
                                                                                  hyp_flu2_phiphi,\
                                                                                  euc_flu2_x_t,\
                                                                                  euc_flu2_x_x,\
                                                                                  euc_flu2_rho_rho,\
                                                                                  euc_flu2_t_t,\
                                                                                  euc_flu2_cstt,\
                                                                                  euc_flu2_phiphi,\
                                                                                  inputs,\
        )

        tW_4_third_fluid__delta_error,\
            iter_overflow_third__delta_error=three_fluids___rotation_euc_hyp__g_v__choice_tW_matrix__delta_error(hyp_flu3_x_t,\
                                                                                 hyp_flu3_x_x,\
                                                                                 hyp_flu3_rho_rho,\
                                                                                 hyp_flu3_t_t,\
                                                                                 hyp_flu3_cstt,\
                                                                                 hyp_flu3_phiphi,\
                                                                                 euc_flu3_x_t,\
                                                                                 euc_flu3_x_x,\
                                                                                 euc_flu3_rho_rho,\
                                                                                 euc_flu3_t_t,\
                                                                                 euc_flu3_cstt,\
                                                                                 euc_flu3_phiphi,\
                                                                                 inputs,\
        )
        ######################################################


        #below is the g_munu diagonal version
        thediag=np.diag([lambda_plus,lambda_minus,1])
        thediag_4=np.diag([lambda_plus,lambda_minus,1,g_v_phi_phi])
        thediag_4_no_curve=np.diag([1,-1,1,1])#no_curv

        product=np.dot(tW.T,np.dot(thediag,tW))

        product_4_4__ini_fluid__tw4=np.dot(tW_4.T,np.dot(thediag_4,tW_4)) # this should be the same as the_diag_4
        product_4_4__2nd_fluid__tw4=np.dot(tW_4_second_fluid.T,np.dot(thediag_4,tW_4_second_fluid)) # this should be the same as the_diag_4
        product_4_4__3rd_fluid__tw4=np.dot(tW_4_third_fluid.T,np.dot(thediag_4,tW_4_third_fluid)) # this should be the same as the_diag_4


        
        
        #interesting line below!!
        ####    g_v_t_X=v*(1-meshed_f)

        N_plus=np.sqrt((lambda_plus-1)**2+g_v_t_X**2)
        N_minus=np.sqrt((lambda_minus-1)**2+g_v_t_X**2)

        #################################
        # now we just examine the theta matrix
        #eigen_vect_g_doublcovariant=EVGDCov
        #{EVGDCov=theta matrix} is the matrix of eigen vectors of g_{mu,nu}

        EVGDCov=np.array([[(lambda_plus-1.0)/N_plus,(lambda_minus-1.0)/N_minus,0,0],\
                         [g_v_t_X/(1.0*N_plus),g_v_t_X/(1.0*N_minus),0,0],\
                         [0,0,1.,0],\
                         [0,0,0,1.]])
        the_product_should_be_diagonal=np.dot(EVGDCov.T,np.dot(g_v_mat,EVGDCov))

        ##################################

        # the following is contravariant (each nested array is not a desirable vector rather this is component 1 of the first array and then comp 1 of the second, etc.
        e_not_prime=np.array([[-1,0,0,0],\
                              [g_v_t_X,1,0,0],\
                              [0,0,1,0],\
                              [0,0,0,1/np.sqrt(g_v_phi_phi) ],\
        ])

        the_product_should_be_minkowski_metric=np.dot(e_not_prime.T,np.dot(g_v_mat,e_not_prime))


        minkowski_metric=np.diag([-1,1,1,1])

        ####################################################

        

        e_prime=np.dot(EVGDCov,np.dot(tW_4,np.dot(EVGDCov.T,e_not_prime)))
        e_prime_2nd=np.dot(EVGDCov,np.dot(tW_4_second_fluid,np.dot(EVGDCov.T,e_not_prime)))
        e_prime_3rd=np.dot(EVGDCov,np.dot(tW_4_third_fluid,np.dot(EVGDCov.T,e_not_prime))) 
        # these 3 vectors have contravariant components



        # errors are computed below
        DELTA_e_prime=np.dot(EVGDCov,np.dot(tW_4__delta_error,np.dot(EVGDCov.T,e_not_prime)))
        DELTA_e_prime_2nd=np.dot(EVGDCov,np.dot(tW_4_second_fluid__delta_error,np.dot(EVGDCov.T,e_not_prime)))
        DELTA_e_prime_3rd=np.dot(EVGDCov,np.dot(tW_4_third_fluid__delta_error,np.dot(EVGDCov.T,e_not_prime))) 

        e_prime_cov=np.dot(g_v_mat,e_prime)# in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures        
        e_prime_3rd_cov=np.dot(g_v_mat,e_prime_3rd)# in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures
        e_prime_2nd_cov=np.dot(g_v_mat,e_prime_2nd)# same as above


        DELTA_e_prime_cov=np.dot(g_v_mat,DELTA_e_prime)
        DELTA_e_prime_3rd_cov=np.dot(g_v_mat,DELTA_e_prime_2nd)
        DELTA_e_prime_2nd_cov=np.dot(g_v_mat,DELTA_e_prime_3rd)
        

        


        # e_prime is described as sets of lines , each dissecting interesting vectors (vectors as columns)
        #e_prime here is contravariant
        #below we compute the covariant vectors in a single matrix






        # we compute the mixed components of Einstein tensor
        G_einst_mixed_plus_lambda_delta=np.dot(g_n_mat,G_doubly_cov+Lambda_cosmological_constant_*g_v_mat )






        ####################################################################





        ################################################


        #################################################
        #################################################



        basis_vectors_3fluids=[[e_prime_cov,e_prime],[e_prime_2nd_cov,e_prime_2nd],[e_prime_3rd_cov,e_prime_3rd]]

        basis_vectors_3fluids__delta_errors=[[DELTA_e_prime_cov,DELTA_e_prime],[DELTA_e_prime_2nd_cov,DELTA_e_prime_2nd],[DELTA_e_prime_3rd_cov,DELTA_e_prime_3rd]]




        big_matrix_3flu,gee_vector_3flu,overflow_big_mat=compute_big_matrix_for_fluids(basis_vectors_3fluids,G_einst_mixed_plus_lambda_delta)


        big_matrix_3flu__errors,gee_vector_3flu__errors,overflow_big_mat__errors=compute_big_matrix_for_fluids__errors(basis_vectors_3fluids,basis_vectors_3fluids__delta_errors,\
                                                                                                                       G_einst_mixed_plus_lambda_delta)



        if np.any(np.isnan(big_matrix_3flu)) or np.any(np.isinf(big_matrix_3flu)) or \
           np.any(np.isnan(gee_vector_3flu)) or np.any(np.isinf(gee_vector_3flu)):
            return_to_starting_block=True

            hyp_flu1_x_t=hyp_flu1_x_t/3.
            hyp_flu1_x_x=hyp_flu1_x_x/3.
            hyp_flu1_rho_rho=hyp_flu1_rho_rho/3.
            hyp_flu1_t_t=hyp_flu1_t_t/3.
            hyp_flu1_cstt=hyp_flu1_cstt/3.
            hyp_flu1_phiphi=hyp_flu1_phiphi/3.

            hyp_flu2_x_t=hyp_flu2_x_t/3.
            hyp_flu2_x_x=hyp_flu2_x_x/3.
            hyp_flu2_rho_rho=hyp_flu2_rho_rho/3.
            hyp_flu2_t_t=hyp_flu2_t_t/3.
            hyp_flu2_cstt=hyp_flu2_cstt/3.
            hyp_flu2_phiphi=hyp_flu2_phiphi/3.

            hyp_flu3_x_t=hyp_flu3_x_t/3.
            hyp_flu3_x_x=hyp_flu3_x_x/3.
            hyp_flu3_rho_rho=hyp_flu3_rho_rho/3.
            hyp_flu3_t_t=hyp_flu3_t_t/3.
            hyp_flu3_cstt=hyp_flu3_cstt/3.
            hyp_flu3_phiphi=hyp_flu3_phiphi/3.
            # above if inf or nan we decrease the value of all coefficients

            bad_inf_nan+=1
            
            densities_pressures_3fluids=[-11,-11,-11,-11,-11,-11,-11,-11,-11,-11,-11,-11]
            
            matrix_rank_3fluids=-1
            

        else:
            return_to_starting_block=False
            densities_pressures_3fluids,\
                residuals_3fluids,\
                matrix_rank_3fluids,\
                sing_values_matrix_3fluids=np.linalg.lstsq(big_matrix_3flu, gee_vector_3flu,rcond=None)

    ##############################################################


    ##############################################################################################################################




    res=list(densities_pressures_3fluids)


    
    v_p_errors=np.dot(big_matrix_3flu__errors,densities_pressures_3fluids) # vector predicted


    
    v_p=np.dot(big_matrix_3flu,densities_pressures_3fluids) # vector predicted
    v_r=gee_vector_3flu


    if np.linalg.norm(v_r)!=0:
        sin_t_h_e_t_a__error=np.linalg.norm(v_p-v_r)/np.linalg.norm(v_r)
    else:
        sin_t_h_e_t_a__error=0.

        
    denom=np.array([el if el !=0 else 1 for el in list(np.abs(v_p)+np.abs(v_r)) ])


    diff_sur_som=np.abs(v_p-v_r)/denom

    diff_sur_som_errors=np.abs(v_p_errors)/denom


    return sin_t_h_e_t_a__error,diff_sur_som_errors[np.argmax(np.abs(v_r))],matrix_rank_3fluids
#################################################################################################



def three_fluids__element_wise__compute_relat_error__norm2__output_them___myfunc(g_n_t__t_jX,\
                                                       g_n__rho2_jX2,\
                                                       g__v_jn__phiphi,\
                                                       g_v_t__t_jX,\
                                                       g_v__rho2_jX2,\
                                                       meshed__cos_jsin__theta,\
                                                       sqrt_lambda__pl_jmin,\
                                                       G__t_t_jA,\
                                                       G__t__x_jrho,\
                                                       G__x__x_jrho,\
                                                       G_x__rho2_jphi2,\
                                                       hyp_coef_flu1_x__x_jt,\
                                                       hyp_coef_flu1__tt_jrhorho,\
                                                       hyp_coef_flu1__phiphi_jcstt,\
                                                       euc_coef_flu1_x__x_jt,\
                                                       euc_coef_flu1__tt_jrhorho,\
                                                       euc_coef_flu1__phiphi_jcstt,\
                                                       hyp_coef_flu2_x__x_jt,\
                                                       hyp_coef_flu2__tt_jrhorho,\
                                                       hyp_coef_flu2__phiphi_jcstt,\
                                                       euc_coef_flu2_x__x_jt,\
                                                       euc_coef_flu2__tt_jrhorho,\
                                                       euc_coef_flu2__phiphi_jcstt,\
                                                       hyp_coef_flu3_x__x_jt,\
                                                       hyp_coef_flu3__tt_jrhorho,\
                                                       hyp_coef_flu3__phiphi_jcstt,\
                                                       euc_coef_flu3_x__x_jt,\
                                                       euc_coef_flu3__tt_jrhorho,\
                                                       euc_coef_flu3__phiphi_jcstt,\
):
    bad_inf_nan=0


    do_plot=False

    #1
    g_n_t_X=g_n_t__t_jX.imag
    g_n_t_t=g_n_t__t_jX.real

    #2
    g_n_X_X=g_n__rho2_jX2.imag
    g_n_rho_rho=g_n__rho2_jX2.real


    #3
    g_n_phi_phi=g__v_jn__phiphi.imag
    g_v_phi_phi=g__v_jn__phiphi.real



    #4
    g_v_t_X=g_v_t__t_jX.imag
    g_v_t_t=g_v_t__t_jX.real

    #5
    g_v_X_X=g_v__rho2_jX2.imag
    g_v_rho_rho=g_v__rho2_jX2.real

    #5
    meshed_sin_theta=meshed__cos_jsin__theta.imag
    meshed_cos_theta=meshed__cos_jsin__theta.real

    #6
    sqrt_lambda_minus=sqrt_lambda__pl_jmin.imag
    sqrt_lambda_plus=sqrt_lambda__pl_jmin.real

    #7
    A=G__t_t_jA.imag
    G__t_t=G__t_t_jA.real

    #8
    G__t_rho=G__t__x_jrho.imag
    G__t_x=G__t__x_jrho.real

    #9
    G__x_rho=G__x__x_jrho.imag
    G__x_x=G__x__x_jrho.real

    #10
    G__phi_phi=G_x__rho2_jphi2.imag
    G__rho_rho=G_x__rho2_jphi2.real

    # there are seven tt tx trho , xx xrho, rhorho, phiphi
    #11


    hyp_flu1_x_t=hyp_coef_flu1_x__x_jt.imag
    hyp_flu1_x_x=hyp_coef_flu1_x__x_jt.real

    #12
    hyp_flu1_rho_rho=hyp_coef_flu1__tt_jrhorho.imag
    hyp_flu1_t_t=hyp_coef_flu1__tt_jrhorho.real

    #13
    hyp_flu1_cstt=hyp_coef_flu1__phiphi_jcstt.imag
    hyp_flu1_phiphi=hyp_coef_flu1__phiphi_jcstt.real

    #14

    euc_flu1_x_t=euc_coef_flu1_x__x_jt.imag
    euc_flu1_x_x=euc_coef_flu1_x__x_jt.real


    euc_flu1_rho_rho=euc_coef_flu1__tt_jrhorho.imag
    euc_flu1_t_t=euc_coef_flu1__tt_jrhorho.real


    euc_flu1_cstt=euc_coef_flu1__phiphi_jcstt.imag
    euc_flu1_phiphi=euc_coef_flu1__phiphi_jcstt.real
    ################################
    hyp_flu2_x_t=hyp_coef_flu2_x__x_jt.imag
    hyp_flu2_x_x=hyp_coef_flu2_x__x_jt.real


    hyp_flu2_rho_rho=hyp_coef_flu2__tt_jrhorho.imag
    hyp_flu2_t_t=hyp_coef_flu2__tt_jrhorho.real


    hyp_flu2_cstt=hyp_coef_flu2__phiphi_jcstt.imag
    hyp_flu2_phiphi=hyp_coef_flu2__phiphi_jcstt.real



    euc_flu2_x_t=euc_coef_flu2_x__x_jt.imag
    euc_flu2_x_x=euc_coef_flu2_x__x_jt.real


    euc_flu2_rho_rho=euc_coef_flu2__tt_jrhorho.imag
    euc_flu2_t_t=euc_coef_flu2__tt_jrhorho.real


    euc_flu2_cstt=euc_coef_flu2__phiphi_jcstt.imag
    euc_flu2_phiphi=euc_coef_flu2__phiphi_jcstt.real

    ##################################
    hyp_flu3_x_t=hyp_coef_flu3_x__x_jt.imag
    hyp_flu3_x_x=hyp_coef_flu3_x__x_jt.real


    hyp_flu3_rho_rho=hyp_coef_flu3__tt_jrhorho.imag
    hyp_flu3_t_t=hyp_coef_flu3__tt_jrhorho.real


    hyp_flu3_cstt=hyp_coef_flu3__phiphi_jcstt.imag
    hyp_flu3_phiphi=hyp_coef_flu3__phiphi_jcstt.real



    euc_flu3_x_t=euc_coef_flu3_x__x_jt.imag
    euc_flu3_x_x=euc_coef_flu3_x__x_jt.real


    euc_flu3_rho_rho=euc_coef_flu3__tt_jrhorho.imag
    euc_flu3_t_t=euc_coef_flu3__tt_jrhorho.real


    euc_flu3_cstt=euc_coef_flu3__phiphi_jcstt.imag
    euc_flu3_phiphi=euc_coef_flu3__phiphi_jcstt.real








    Lambda_cosmological_constant_=0
    #1.11e-52 #(m^(-2))






    #do_print_stuff=True
    do_print_stuff=False

    #do_print_stuff2=True
    do_print_stuff2=False


    do_print_stuff3=False
    inputs={}

    do_print_stuff4=False

    do_print_stuff5=False
    ######################################################
    # correction on g_phi,phi
    #below
    if g_n_phi_phi==0 or np.isinf(g_n_phi_phi):
        g_n_phi_phi=1

    if g_v_phi_phi==0 or np.isinf(g_v_phi_phi):
        g_v_phi_phi=1

    if np.isnan(g_v_phi_phi) or np.isnan(g_n_phi_phi):
        g_n_phi_phi=1
        g_v_phi_phi=1



    inputs["g_n_t_t"]=g_n_t_t
    inputs["g_n_t_X"]=g_n_t_X
    inputs["g_n_X_X"]=g_n_X_X
    inputs["g_n_rho_rho"]=g_n_rho_rho
    inputs["g_n_phi_phi"]=g_n_phi_phi


    inputs["g_v_t_t"]=g_v_t_t
    inputs["g_v_t_X"]=g_v_t_X
    inputs["g_v_X_X"]=g_v_X_X
    inputs["g_v_rho_rho"]=g_v_rho_rho
    inputs["g_v_phi_phi"]=g_v_phi_phi

    inputs["sqrt_lambda_minus"]=sqrt_lambda_minus
    inputs["sqrt_lambda_plus"]=sqrt_lambda_plus



    ########################################################
    # we compute G_doubly covariant!!
    G_doubly_cov=np.array( [ [ G__t_t   , G__t_x   , G__t_rho   , 0 ],\
                             [ G__t_x   , G__x_x   , G__x_rho   , 0 ],\
                             [ G__t_rho , G__x_rho , G__rho_rho , 0 ],\
                             [ 0        , 0        , 0          , G__phi_phi ],\
    ])
    #######################################################
    # below are the numpy equivalents of lines / rows
    g_n_mat=np.array([[inputs["g_n_t_t"],inputs["g_n_t_X"],0,0],\
                      [inputs["g_n_t_X"],inputs["g_n_X_X"],0,0],\
                      [0                ,0                ,inputs["g_n_rho_rho"],0],\
                      [0                ,0                ,0                    ,inputs["g_n_phi_phi"]]])

    g_v_mat=np.array([[inputs["g_v_t_t"],inputs["g_v_t_X"],0,0],\
                      [inputs["g_v_t_X"],inputs["g_v_X_X"],0,0],\
                      [0                ,0                ,inputs["g_v_rho_rho"],0],\
                      [0                ,0                ,0                    ,inputs["g_v_phi_phi"]]])



    lambda_plus=(-(A-1)+np.sqrt((A-1)**2+4))/2.
    lambda_minus=(-(A-1)-np.sqrt((A-1)**2+4))/2.
    tW=np.array([[meshed_cos_theta,0,meshed_sin_theta*1./sqrt_lambda_plus],\
                 [0,1,0],\
                 [-meshed_sin_theta*sqrt_lambda_plus,0,meshed_cos_theta],\
                 ])

    return_to_starting_block=True
    if 1:


        #print('choose tw4 ---> make a function which creates such matrices \n line 1640')



        tW_4,\
            tW_4_rot_no_curv,\
            tW_4_boo_no_curv,\
            iter_overflow=three_fluids___rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu1_x_t,\
                                                                     hyp_flu1_x_x,\
                                                                     hyp_flu1_rho_rho,\
                                                                     hyp_flu1_t_t,\
                                                                     hyp_flu1_cstt,\
                                                                     hyp_flu1_phiphi,\
                                                                     euc_flu1_x_t,\
                                                                     euc_flu1_x_x,\
                                                                     euc_flu1_rho_rho,\
                                                                     euc_flu1_t_t,\
                                                                     euc_flu1_cstt,\
                                                                     euc_flu1_phiphi,\
                                                                     inputs,\
        )

        tW_4_second_fluid,\
            tW_4_second_fluid_rot_no_curv,\
            tW_4_second_fluid_boo_no_curv,\
            iter_overflow_second=three_fluids___rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu2_x_t,\
                                                                                  hyp_flu2_x_x,\
                                                                                  hyp_flu2_rho_rho,\
                                                                                  hyp_flu2_t_t,\
                                                                                  hyp_flu2_cstt,\
                                                                                  hyp_flu2_phiphi,\
                                                                                  euc_flu2_x_t,\
                                                                                  euc_flu2_x_x,\
                                                                                  euc_flu2_rho_rho,\
                                                                                  euc_flu2_t_t,\
                                                                                  euc_flu2_cstt,\
                                                                                  euc_flu2_phiphi,\
                                                                                  inputs,\
        )

        tW_4_third_fluid,\
            tW_4_third_fluid_rot_no_curv,\
            tW_4_third_fluid_boo_no_curv,\
            iter_overflow_third=three_fluids___rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu3_x_t,\
                                                                                 hyp_flu3_x_x,\
                                                                                 hyp_flu3_rho_rho,\
                                                                                 hyp_flu3_t_t,\
                                                                                 hyp_flu3_cstt,\
                                                                                 hyp_flu3_phiphi,\
                                                                                 euc_flu3_x_t,\
                                                                                 euc_flu3_x_x,\
                                                                                 euc_flu3_rho_rho,\
                                                                                 euc_flu3_t_t,\
                                                                                 euc_flu3_cstt,\
                                                                                 euc_flu3_phiphi,\
                                                                                 inputs,\
        )



        #below is the g_munu diagonal version
        thediag=np.diag([lambda_plus,lambda_minus,1])
        thediag_4=np.diag([lambda_plus,lambda_minus,1,g_v_phi_phi])
        thediag_4_no_curve=np.diag([1,-1,1,1])#no_curv

        product=np.dot(tW.T,np.dot(thediag,tW))

        product_4_4__ini_fluid__tw4=np.dot(tW_4.T,np.dot(thediag_4,tW_4)) # this should be the same as the_diag_4
        product_4_4__2nd_fluid__tw4=np.dot(tW_4_second_fluid.T,np.dot(thediag_4,tW_4_second_fluid)) # this should be the same as the_diag_4
        product_4_4__3rd_fluid__tw4=np.dot(tW_4_third_fluid.T,np.dot(thediag_4,tW_4_third_fluid)) # this should be the same as the_diag_4


        #interesting line below!!
        ####    g_v_t_X=v*(1-meshed_f)

        N_plus=np.sqrt((lambda_plus-1)**2+g_v_t_X**2)
        N_minus=np.sqrt((lambda_minus-1)**2+g_v_t_X**2)

        #################################
        # now we just examine the theta matrix
        #eigen_vect_g_doublcovariant=EVGDCov
        #{EVGDCov=theta matrix} is the matrix of eigen vectors of g_{mu,nu}

        EVGDCov=np.array([[(lambda_plus-1.0)/N_plus,(lambda_minus-1.0)/N_minus,0,0],\
                         [g_v_t_X/(1.0*N_plus),g_v_t_X/(1.0*N_minus),0,0],\
                         [0,0,1.,0],\
                         [0,0,0,1.]])
        the_product_should_be_diagonal=np.dot(EVGDCov.T,np.dot(g_v_mat,EVGDCov))

        ##################################

        # the following is contravariant (each nested array is not a desirable vector rather this is component 1 of the first array and then comp 1 of the second, etc.
        e_not_prime=np.array([[-1,0,0,0],\
                              [g_v_t_X,1,0,0],\
                              [0,0,1,0],\
                              [0,0,0,1/np.sqrt(g_v_phi_phi) ],\
        ])

        the_product_should_be_minkowski_metric=np.dot(e_not_prime.T,np.dot(g_v_mat,e_not_prime))


        minkowski_metric=np.diag([-1,1,1,1])
        e_prime=np.dot(EVGDCov,np.dot(tW_4,np.dot(EVGDCov.T,e_not_prime)))
        e_prime_2nd=np.dot(EVGDCov,np.dot(tW_4_second_fluid,np.dot(EVGDCov.T,e_not_prime)))
        e_prime_3rd=np.dot(EVGDCov,np.dot(tW_4_third_fluid,np.dot(EVGDCov.T,e_not_prime))) 
        # these 3 vectors have contravariant components

        e_prime_3rd_cov=np.dot(g_v_mat,e_prime_3rd)# in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures
        e_prime_2nd_cov=np.dot(g_v_mat,e_prime_2nd)# same as above



        # e_prime is described as sets of lines , each dissecting interesting vectors (vectors as columns)
        #e_prime here is contravariant
        #below we compute the covariant vectors in a single matrix


        e_prime_cov=np.dot(g_v_mat,e_prime)# in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures



        # we compute the mixed components of Einstein tensor
        G_einst_mixed_plus_lambda_delta=np.dot(g_n_mat,G_doubly_cov+Lambda_cosmological_constant_*g_v_mat )






        ####################################################################





        ################################################


        #################################################
        #################################################



        basis_vectors_3fluids=[[e_prime_cov,e_prime],[e_prime_2nd_cov,e_prime_2nd],[e_prime_3rd_cov,e_prime_3rd]]
        big_matrix_3flu,gee_vector_3flu,overflow_big_mat=compute_big_matrix_for_fluids(basis_vectors_3fluids,G_einst_mixed_plus_lambda_delta)


        if np.any(np.isnan(big_matrix_3flu)) or np.any(np.isinf(big_matrix_3flu)) or \
           np.any(np.isnan(gee_vector_3flu)) or np.any(np.isinf(gee_vector_3flu)):
            return_to_starting_block=True

            hyp_flu1_x_t=hyp_flu1_x_t/3.
            hyp_flu1_x_x=hyp_flu1_x_x/3.
            hyp_flu1_rho_rho=hyp_flu1_rho_rho/3.
            hyp_flu1_t_t=hyp_flu1_t_t/3.
            hyp_flu1_cstt=hyp_flu1_cstt/3.
            hyp_flu1_phiphi=hyp_flu1_phiphi/3.

            hyp_flu2_x_t=hyp_flu2_x_t/3.
            hyp_flu2_x_x=hyp_flu2_x_x/3.
            hyp_flu2_rho_rho=hyp_flu2_rho_rho/3.
            hyp_flu2_t_t=hyp_flu2_t_t/3.
            hyp_flu2_cstt=hyp_flu2_cstt/3.
            hyp_flu2_phiphi=hyp_flu2_phiphi/3.

            hyp_flu3_x_t=hyp_flu3_x_t/3.
            hyp_flu3_x_x=hyp_flu3_x_x/3.
            hyp_flu3_rho_rho=hyp_flu3_rho_rho/3.
            hyp_flu3_t_t=hyp_flu3_t_t/3.
            hyp_flu3_cstt=hyp_flu3_cstt/3.
            hyp_flu3_phiphi=hyp_flu3_phiphi/3.
            # above if inf or nan we decrease the values of all coefficients

            bad_inf_nan+=1
            
            densities_pressures_3fluids=[-11,-11,-11,-11,-11,-11,-11,-11,-11,-11,-11,-11]
            
            matrix_rank_3fluids=-1
            

        else:
            return_to_starting_block=False
            densities_pressures_3fluids,\
                residuals_3fluids,\
                matrix_rank_3fluids,\
                sing_values_matrix_3fluids=np.linalg.lstsq(big_matrix_3flu, gee_vector_3flu,rcond=None)

    ##############################################################


    ##############################################################################################################################




    res=list(densities_pressures_3fluids)




    v_p=np.dot(big_matrix_3flu,densities_pressures_3fluids) # vector predicted
    v_r=gee_vector_3flu

    
    
    denom=np.array([el if el !=0 else 1 for el in list(np.abs(v_p)+np.abs(v_r)) ])
    diff_sur_som=np.abs(v_p-v_r)/denom
    non_zero_sigmas=[el for el in sing_values_matrix_3fluids if el !=0]

    
    return np.linalg.norm(v_p-v_r)+1j*np.linalg.norm(v_r),\
        matrix_rank_3fluids+1j*sing_values_matrix_3fluids[6],\
        sing_values_matrix_3fluids[0]/sing_values_matrix_3fluids[6]+1j*np.min(sing_values_matrix_3fluids)
#################################################################################################

##############################################################################################################################################


def three_fluids__element_wise__compute_relat_error__dif_over_sum__output_them___myfunc(g_n_t__t_jX,\
                                                       g_n__rho2_jX2,\
                                                       g__v_jn__phiphi,\
                                                       g_v_t__t_jX,\
                                                       g_v__rho2_jX2,\
                                                       meshed__cos_jsin__theta,\
                                                       sqrt_lambda__pl_jmin,\
                                                       G__t_t_jA,\
                                                       G__t__x_jrho,\
                                                       G__x__x_jrho,\
                                                       G_x__rho2_jphi2,\
                                                       hyp_coef_flu1_x__x_jt,\
                                                       hyp_coef_flu1__tt_jrhorho,\
                                                       hyp_coef_flu1__phiphi_jcstt,\
                                                       euc_coef_flu1_x__x_jt,\
                                                       euc_coef_flu1__tt_jrhorho,\
                                                       euc_coef_flu1__phiphi_jcstt,\
                                                       hyp_coef_flu2_x__x_jt,\
                                                       hyp_coef_flu2__tt_jrhorho,\
                                                       hyp_coef_flu2__phiphi_jcstt,\
                                                       euc_coef_flu2_x__x_jt,\
                                                       euc_coef_flu2__tt_jrhorho,\
                                                       euc_coef_flu2__phiphi_jcstt,\
                                                       hyp_coef_flu3_x__x_jt,\
                                                       hyp_coef_flu3__tt_jrhorho,\
                                                       hyp_coef_flu3__phiphi_jcstt,\
                                                       euc_coef_flu3_x__x_jt,\
                                                       euc_coef_flu3__tt_jrhorho,\
                                                       euc_coef_flu3__phiphi_jcstt,\
):
    bad_inf_nan=0


    do_plot=False

    #1
    g_n_t_X=g_n_t__t_jX.imag
    g_n_t_t=g_n_t__t_jX.real

    #2
    g_n_X_X=g_n__rho2_jX2.imag
    g_n_rho_rho=g_n__rho2_jX2.real


    #3
    g_n_phi_phi=g__v_jn__phiphi.imag
    g_v_phi_phi=g__v_jn__phiphi.real



    #4
    g_v_t_X=g_v_t__t_jX.imag
    g_v_t_t=g_v_t__t_jX.real

    #5
    g_v_X_X=g_v__rho2_jX2.imag
    g_v_rho_rho=g_v__rho2_jX2.real

    #5
    meshed_sin_theta=meshed__cos_jsin__theta.imag
    meshed_cos_theta=meshed__cos_jsin__theta.real

    #6
    sqrt_lambda_minus=sqrt_lambda__pl_jmin.imag
    sqrt_lambda_plus=sqrt_lambda__pl_jmin.real

    #7
    A=G__t_t_jA.imag
    G__t_t=G__t_t_jA.real

    #8
    G__t_rho=G__t__x_jrho.imag
    G__t_x=G__t__x_jrho.real

    #9
    G__x_rho=G__x__x_jrho.imag
    G__x_x=G__x__x_jrho.real

    #10
    G__phi_phi=G_x__rho2_jphi2.imag
    G__rho_rho=G_x__rho2_jphi2.real

    # there are seven tt tx trho , xx xrho, rhorho, phiphi
    #11


    hyp_flu1_x_t=hyp_coef_flu1_x__x_jt.imag
    hyp_flu1_x_x=hyp_coef_flu1_x__x_jt.real

    #12
    hyp_flu1_rho_rho=hyp_coef_flu1__tt_jrhorho.imag
    hyp_flu1_t_t=hyp_coef_flu1__tt_jrhorho.real

    #13
    hyp_flu1_cstt=hyp_coef_flu1__phiphi_jcstt.imag
    hyp_flu1_phiphi=hyp_coef_flu1__phiphi_jcstt.real

    #14

    euc_flu1_x_t=euc_coef_flu1_x__x_jt.imag
    euc_flu1_x_x=euc_coef_flu1_x__x_jt.real


    euc_flu1_rho_rho=euc_coef_flu1__tt_jrhorho.imag
    euc_flu1_t_t=euc_coef_flu1__tt_jrhorho.real


    euc_flu1_cstt=euc_coef_flu1__phiphi_jcstt.imag
    euc_flu1_phiphi=euc_coef_flu1__phiphi_jcstt.real
    ################################
    hyp_flu2_x_t=hyp_coef_flu2_x__x_jt.imag
    hyp_flu2_x_x=hyp_coef_flu2_x__x_jt.real


    hyp_flu2_rho_rho=hyp_coef_flu2__tt_jrhorho.imag
    hyp_flu2_t_t=hyp_coef_flu2__tt_jrhorho.real


    hyp_flu2_cstt=hyp_coef_flu2__phiphi_jcstt.imag
    hyp_flu2_phiphi=hyp_coef_flu2__phiphi_jcstt.real



    euc_flu2_x_t=euc_coef_flu2_x__x_jt.imag
    euc_flu2_x_x=euc_coef_flu2_x__x_jt.real


    euc_flu2_rho_rho=euc_coef_flu2__tt_jrhorho.imag
    euc_flu2_t_t=euc_coef_flu2__tt_jrhorho.real


    euc_flu2_cstt=euc_coef_flu2__phiphi_jcstt.imag
    euc_flu2_phiphi=euc_coef_flu2__phiphi_jcstt.real

    ##################################
    hyp_flu3_x_t=hyp_coef_flu3_x__x_jt.imag
    hyp_flu3_x_x=hyp_coef_flu3_x__x_jt.real


    hyp_flu3_rho_rho=hyp_coef_flu3__tt_jrhorho.imag
    hyp_flu3_t_t=hyp_coef_flu3__tt_jrhorho.real


    hyp_flu3_cstt=hyp_coef_flu3__phiphi_jcstt.imag
    hyp_flu3_phiphi=hyp_coef_flu3__phiphi_jcstt.real



    euc_flu3_x_t=euc_coef_flu3_x__x_jt.imag
    euc_flu3_x_x=euc_coef_flu3_x__x_jt.real


    euc_flu3_rho_rho=euc_coef_flu3__tt_jrhorho.imag
    euc_flu3_t_t=euc_coef_flu3__tt_jrhorho.real


    euc_flu3_cstt=euc_coef_flu3__phiphi_jcstt.imag
    euc_flu3_phiphi=euc_coef_flu3__phiphi_jcstt.real








    Lambda_cosmological_constant_=0
    #1.11e-52 #(m^(-2))






    #do_print_stuff=True
    do_print_stuff=False

    #do_print_stuff2=True
    do_print_stuff2=False


    do_print_stuff3=False
    inputs={}

    do_print_stuff4=False

    do_print_stuff5=False
    ######################################################
    # correction on g_phi,phi
    #below
    if g_n_phi_phi==0 or np.isinf(g_n_phi_phi):
        g_n_phi_phi=1

    if g_v_phi_phi==0 or np.isinf(g_v_phi_phi):
        g_v_phi_phi=1

    if np.isnan(g_v_phi_phi) or np.isnan(g_n_phi_phi):
        g_n_phi_phi=1
        g_v_phi_phi=1



    inputs["g_n_t_t"]=g_n_t_t
    inputs["g_n_t_X"]=g_n_t_X
    inputs["g_n_X_X"]=g_n_X_X
    inputs["g_n_rho_rho"]=g_n_rho_rho
    inputs["g_n_phi_phi"]=g_n_phi_phi


    inputs["g_v_t_t"]=g_v_t_t
    inputs["g_v_t_X"]=g_v_t_X
    inputs["g_v_X_X"]=g_v_X_X
    inputs["g_v_rho_rho"]=g_v_rho_rho
    inputs["g_v_phi_phi"]=g_v_phi_phi

    inputs["sqrt_lambda_minus"]=sqrt_lambda_minus
    inputs["sqrt_lambda_plus"]=sqrt_lambda_plus



    ########################################################
    # we compute G_doubly covariant!!
    G_doubly_cov=np.array( [ [ G__t_t   , G__t_x   , G__t_rho   , 0 ],\
                             [ G__t_x   , G__x_x   , G__x_rho   , 0 ],\
                             [ G__t_rho , G__x_rho , G__rho_rho , 0 ],\
                             [ 0        , 0        , 0          , G__phi_phi ],\
    ])
    #######################################################
    # below are the numpy equivalents of lines / rows
    g_n_mat=np.array([[inputs["g_n_t_t"],inputs["g_n_t_X"],0,0],\
                      [inputs["g_n_t_X"],inputs["g_n_X_X"],0,0],\
                      [0                ,0                ,inputs["g_n_rho_rho"],0],\
                      [0                ,0                ,0                    ,inputs["g_n_phi_phi"]]])

    g_v_mat=np.array([[inputs["g_v_t_t"],inputs["g_v_t_X"],0,0],\
                      [inputs["g_v_t_X"],inputs["g_v_X_X"],0,0],\
                      [0                ,0                ,inputs["g_v_rho_rho"],0],\
                      [0                ,0                ,0                    ,inputs["g_v_phi_phi"]]])



    lambda_plus=(-(A-1)+np.sqrt((A-1)**2+4))/2.
    lambda_minus=(-(A-1)-np.sqrt((A-1)**2+4))/2.
    tW=np.array([[meshed_cos_theta,0,meshed_sin_theta*1./sqrt_lambda_plus],\
                 [0,1,0],\
                 [-meshed_sin_theta*sqrt_lambda_plus,0,meshed_cos_theta],\
                 ])

    return_to_starting_block=True
    if 1:


        #print('choose tw4 ---> make a function which creates such matrices \n line 1640')



        tW_4,\
            tW_4_rot_no_curv,\
            tW_4_boo_no_curv,\
            iter_overflow=three_fluids___rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu1_x_t,\
                                                                     hyp_flu1_x_x,\
                                                                     hyp_flu1_rho_rho,\
                                                                     hyp_flu1_t_t,\
                                                                     hyp_flu1_cstt,\
                                                                     hyp_flu1_phiphi,\
                                                                     euc_flu1_x_t,\
                                                                     euc_flu1_x_x,\
                                                                     euc_flu1_rho_rho,\
                                                                     euc_flu1_t_t,\
                                                                     euc_flu1_cstt,\
                                                                     euc_flu1_phiphi,\
                                                                     inputs,\
        )

        tW_4_second_fluid,\
            tW_4_second_fluid_rot_no_curv,\
            tW_4_second_fluid_boo_no_curv,\
            iter_overflow_second=three_fluids___rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu2_x_t,\
                                                                                  hyp_flu2_x_x,\
                                                                                  hyp_flu2_rho_rho,\
                                                                                  hyp_flu2_t_t,\
                                                                                  hyp_flu2_cstt,\
                                                                                  hyp_flu2_phiphi,\
                                                                                  euc_flu2_x_t,\
                                                                                  euc_flu2_x_x,\
                                                                                  euc_flu2_rho_rho,\
                                                                                  euc_flu2_t_t,\
                                                                                  euc_flu2_cstt,\
                                                                                  euc_flu2_phiphi,\
                                                                                  inputs,\
        )

        tW_4_third_fluid,\
            tW_4_third_fluid_rot_no_curv,\
            tW_4_third_fluid_boo_no_curv,\
            iter_overflow_third=three_fluids___rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu3_x_t,\
                                                                                 hyp_flu3_x_x,\
                                                                                 hyp_flu3_rho_rho,\
                                                                                 hyp_flu3_t_t,\
                                                                                 hyp_flu3_cstt,\
                                                                                 hyp_flu3_phiphi,\
                                                                                 euc_flu3_x_t,\
                                                                                 euc_flu3_x_x,\
                                                                                 euc_flu3_rho_rho,\
                                                                                 euc_flu3_t_t,\
                                                                                 euc_flu3_cstt,\
                                                                                 euc_flu3_phiphi,\
                                                                                 inputs,\
        )



        #below is the g_munu diagonal version
        thediag=np.diag([lambda_plus,lambda_minus,1])
        thediag_4=np.diag([lambda_plus,lambda_minus,1,g_v_phi_phi])
        thediag_4_no_curve=np.diag([1,-1,1,1])#no_curv

        product=np.dot(tW.T,np.dot(thediag,tW))

        product_4_4__ini_fluid__tw4=np.dot(tW_4.T,np.dot(thediag_4,tW_4)) # this should be the same as the_diag_4
        product_4_4__2nd_fluid__tw4=np.dot(tW_4_second_fluid.T,np.dot(thediag_4,tW_4_second_fluid)) # this should be the same as the_diag_4
        product_4_4__3rd_fluid__tw4=np.dot(tW_4_third_fluid.T,np.dot(thediag_4,tW_4_third_fluid)) # this should be the same as the_diag_4


        #interesting line below!!
        ####    g_v_t_X=v*(1-meshed_f)

        N_plus=np.sqrt((lambda_plus-1)**2+g_v_t_X**2)
        N_minus=np.sqrt((lambda_minus-1)**2+g_v_t_X**2)

        #################################
        # now we just examine the theta matrix
        #eigen_vect_g_doublcovariant=EVGDCov
        #{EVGDCov=theta matrix} is the matrix of eigen vectors of g_{mu,nu}

        EVGDCov=np.array([[(lambda_plus-1.0)/N_plus,(lambda_minus-1.0)/N_minus,0,0],\
                         [g_v_t_X/(1.0*N_plus),g_v_t_X/(1.0*N_minus),0,0],\
                         [0,0,1.,0],\
                         [0,0,0,1.]])
        the_product_should_be_diagonal=np.dot(EVGDCov.T,np.dot(g_v_mat,EVGDCov))

        ##################################

        # the following is contravariant (each nested array is not a desirable vector rather this is component 1 of the first array and then comp 1 of the second, etc.
        e_not_prime=np.array([[-1,0,0,0],\
                              [g_v_t_X,1,0,0],\
                              [0,0,1,0],\
                              [0,0,0,1/np.sqrt(g_v_phi_phi) ],\
        ])

        the_product_should_be_minkowski_metric=np.dot(e_not_prime.T,np.dot(g_v_mat,e_not_prime))


        minkowski_metric=np.diag([-1,1,1,1])
        e_prime=np.dot(EVGDCov,np.dot(tW_4,np.dot(EVGDCov.T,e_not_prime)))
        e_prime_2nd=np.dot(EVGDCov,np.dot(tW_4_second_fluid,np.dot(EVGDCov.T,e_not_prime)))
        e_prime_3rd=np.dot(EVGDCov,np.dot(tW_4_third_fluid,np.dot(EVGDCov.T,e_not_prime))) 
        # these 3 vectors have contravariant components

        e_prime_3rd_cov=np.dot(g_v_mat,e_prime_3rd)# in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures
        e_prime_2nd_cov=np.dot(g_v_mat,e_prime_2nd)# same as above



        # e_prime is described as sets of lines , each dissecting interesting vectors (vectors as columns)
        #e_prime here is contravariant
        #below we compute the covariant vectors in a single matrix


        e_prime_cov=np.dot(g_v_mat,e_prime)# in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures



        # we compute the mixed components of Einstein tensor
        G_einst_mixed_plus_lambda_delta=np.dot(g_n_mat,G_doubly_cov+Lambda_cosmological_constant_*g_v_mat )






        ####################################################################





        ################################################


        #################################################
        #################################################



        basis_vectors_3fluids=[[e_prime_cov,e_prime],[e_prime_2nd_cov,e_prime_2nd],[e_prime_3rd_cov,e_prime_3rd]]
        big_matrix_3flu,gee_vector_3flu,overflow_big_mat=compute_big_matrix_for_fluids(basis_vectors_3fluids,G_einst_mixed_plus_lambda_delta)


        if np.any(np.isnan(big_matrix_3flu)) or np.any(np.isinf(big_matrix_3flu)) or \
           np.any(np.isnan(gee_vector_3flu)) or np.any(np.isinf(gee_vector_3flu)):
            return_to_starting_block=True

            hyp_flu1_x_t=hyp_flu1_x_t/3.
            hyp_flu1_x_x=hyp_flu1_x_x/3.
            hyp_flu1_rho_rho=hyp_flu1_rho_rho/3.
            hyp_flu1_t_t=hyp_flu1_t_t/3.
            hyp_flu1_cstt=hyp_flu1_cstt/3.
            hyp_flu1_phiphi=hyp_flu1_phiphi/3.

            hyp_flu2_x_t=hyp_flu2_x_t/3.
            hyp_flu2_x_x=hyp_flu2_x_x/3.
            hyp_flu2_rho_rho=hyp_flu2_rho_rho/3.
            hyp_flu2_t_t=hyp_flu2_t_t/3.
            hyp_flu2_cstt=hyp_flu2_cstt/3.
            hyp_flu2_phiphi=hyp_flu2_phiphi/3.

            hyp_flu3_x_t=hyp_flu3_x_t/3.
            hyp_flu3_x_x=hyp_flu3_x_x/3.
            hyp_flu3_rho_rho=hyp_flu3_rho_rho/3.
            hyp_flu3_t_t=hyp_flu3_t_t/3.
            hyp_flu3_cstt=hyp_flu3_cstt/3.
            hyp_flu3_phiphi=hyp_flu3_phiphi/3.
            # above if inf or nan we decrease the values of all coefficients

            bad_inf_nan+=1
            
            densities_pressures_3fluids=[-11,-11,-11,-11,-11,-11,-11,-11,-11,-11,-11,-11]
            
            matrix_rank_3fluids=-1
            

        else:
            return_to_starting_block=False
            densities_pressures_3fluids,\
                residuals_3fluids,\
                matrix_rank_3fluids,\
                sing_values_matrix_3fluids=np.linalg.lstsq(big_matrix_3flu, gee_vector_3flu,rcond=None)

    ##############################################################


    ##############################################################################################################################




    res=list(densities_pressures_3fluids)




    v_p=np.dot(big_matrix_3flu,densities_pressures_3fluids) # vector predicted
    v_r=gee_vector_3flu
    
    denom=np.array([el if el !=0 else 1 for el in list(np.abs(v_p)+np.abs(v_r)) ])
    diff_sur_som=np.abs(v_p-v_r)/denom
    non_zero_sigmas=[el for el in sing_values_matrix_3fluids if el !=0]

    
    return diff_sur_som[np.argmax(np.abs(v_r))]+1j*sing_values_matrix_3fluids[0],\
        matrix_rank_3fluids+1j*sing_values_matrix_3fluids[6],\
        sing_values_matrix_3fluids[0]/sing_values_matrix_3fluids[6]+1j*np.min(sing_values_matrix_3fluids)
#################################################################################################


##########################################################################################################################################


def three_fluids__element_wise__compute_p_rho__p_phi_pressures__output_them___myfunc(g_n_t__t_jX,\
                                                       g_n__rho2_jX2,\
                                                       g__v_jn__phiphi,\
                                                       g_v_t__t_jX,\
                                                       g_v__rho2_jX2,\
                                                       meshed__cos_jsin__theta,\
                                                       sqrt_lambda__pl_jmin,\
                                                       G__t_t_jA,\
                                                       G__t__x_jrho,\
                                                       G__x__x_jrho,\
                                                       G_x__rho2_jphi2,\
                                                       hyp_coef_flu1_x__x_jt,\
                                                       hyp_coef_flu1__tt_jrhorho,\
                                                       hyp_coef_flu1__phiphi_jcstt,\
                                                       euc_coef_flu1_x__x_jt,\
                                                       euc_coef_flu1__tt_jrhorho,\
                                                       euc_coef_flu1__phiphi_jcstt,\
                                                       hyp_coef_flu2_x__x_jt,\
                                                       hyp_coef_flu2__tt_jrhorho,\
                                                       hyp_coef_flu2__phiphi_jcstt,\
                                                       euc_coef_flu2_x__x_jt,\
                                                       euc_coef_flu2__tt_jrhorho,\
                                                       euc_coef_flu2__phiphi_jcstt,\
                                                       hyp_coef_flu3_x__x_jt,\
                                                       hyp_coef_flu3__tt_jrhorho,\
                                                       hyp_coef_flu3__phiphi_jcstt,\
                                                       euc_coef_flu3_x__x_jt,\
                                                       euc_coef_flu3__tt_jrhorho,\
                                                       euc_coef_flu3__phiphi_jcstt,\
):
    
    Lambda_cosmological_constant_=0
    #1.11e-52 #(m^(-2))

    bad_inf_nan=0


    do_plot=False

    #1
    g_n_t_X=g_n_t__t_jX.imag
    g_n_t_t=g_n_t__t_jX.real

    #2
    g_n_X_X=g_n__rho2_jX2.imag
    g_n_rho_rho=g_n__rho2_jX2.real


    #3
    g_n_phi_phi=g__v_jn__phiphi.imag
    g_v_phi_phi=g__v_jn__phiphi.real



    #4
    g_v_t_X=g_v_t__t_jX.imag
    g_v_t_t=g_v_t__t_jX.real

    #5
    g_v_X_X=g_v__rho2_jX2.imag
    g_v_rho_rho=g_v__rho2_jX2.real

    #5
    meshed_sin_theta=meshed__cos_jsin__theta.imag
    meshed_cos_theta=meshed__cos_jsin__theta.real

    #6
    sqrt_lambda_minus=sqrt_lambda__pl_jmin.imag
    sqrt_lambda_plus=sqrt_lambda__pl_jmin.real

    #7
    A=G__t_t_jA.imag
    G__t_t=G__t_t_jA.real

    #8
    G__t_rho=G__t__x_jrho.imag
    G__t_x=G__t__x_jrho.real

    #9
    G__x_rho=G__x__x_jrho.imag
    G__x_x=G__x__x_jrho.real

    #10
    G__phi_phi=G_x__rho2_jphi2.imag
    G__rho_rho=G_x__rho2_jphi2.real

    # there are seven tt tx trho , xx xrho, rhorho, phiphi
    #11


    hyp_flu1_x_t=hyp_coef_flu1_x__x_jt.imag
    hyp_flu1_x_x=hyp_coef_flu1_x__x_jt.real

    #12
    hyp_flu1_rho_rho=hyp_coef_flu1__tt_jrhorho.imag
    hyp_flu1_t_t=hyp_coef_flu1__tt_jrhorho.real

    #13
    hyp_flu1_cstt=hyp_coef_flu1__phiphi_jcstt.imag
    hyp_flu1_phiphi=hyp_coef_flu1__phiphi_jcstt.real

    #14

    euc_flu1_x_t=euc_coef_flu1_x__x_jt.imag
    euc_flu1_x_x=euc_coef_flu1_x__x_jt.real


    euc_flu1_rho_rho=euc_coef_flu1__tt_jrhorho.imag
    euc_flu1_t_t=euc_coef_flu1__tt_jrhorho.real


    euc_flu1_cstt=euc_coef_flu1__phiphi_jcstt.imag
    euc_flu1_phiphi=euc_coef_flu1__phiphi_jcstt.real
    ################################
    hyp_flu2_x_t=hyp_coef_flu2_x__x_jt.imag
    hyp_flu2_x_x=hyp_coef_flu2_x__x_jt.real


    hyp_flu2_rho_rho=hyp_coef_flu2__tt_jrhorho.imag
    hyp_flu2_t_t=hyp_coef_flu2__tt_jrhorho.real


    hyp_flu2_cstt=hyp_coef_flu2__phiphi_jcstt.imag
    hyp_flu2_phiphi=hyp_coef_flu2__phiphi_jcstt.real



    euc_flu2_x_t=euc_coef_flu2_x__x_jt.imag
    euc_flu2_x_x=euc_coef_flu2_x__x_jt.real


    euc_flu2_rho_rho=euc_coef_flu2__tt_jrhorho.imag
    euc_flu2_t_t=euc_coef_flu2__tt_jrhorho.real


    euc_flu2_cstt=euc_coef_flu2__phiphi_jcstt.imag
    euc_flu2_phiphi=euc_coef_flu2__phiphi_jcstt.real

    ##################################
    hyp_flu3_x_t=hyp_coef_flu3_x__x_jt.imag
    hyp_flu3_x_x=hyp_coef_flu3_x__x_jt.real


    hyp_flu3_rho_rho=hyp_coef_flu3__tt_jrhorho.imag
    hyp_flu3_t_t=hyp_coef_flu3__tt_jrhorho.real


    hyp_flu3_cstt=hyp_coef_flu3__phiphi_jcstt.imag
    hyp_flu3_phiphi=hyp_coef_flu3__phiphi_jcstt.real



    euc_flu3_x_t=euc_coef_flu3_x__x_jt.imag
    euc_flu3_x_x=euc_coef_flu3_x__x_jt.real


    euc_flu3_rho_rho=euc_coef_flu3__tt_jrhorho.imag
    euc_flu3_t_t=euc_coef_flu3__tt_jrhorho.real


    euc_flu3_cstt=euc_coef_flu3__phiphi_jcstt.imag
    euc_flu3_phiphi=euc_coef_flu3__phiphi_jcstt.real







    








    #do_print_stuff=True
    do_print_stuff=False

    #do_print_stuff2=True
    do_print_stuff2=False


    do_print_stuff3=False
    inputs={}

    do_print_stuff4=False

    do_print_stuff5=False
    ######################################################
    # correction on g_phi,phi
    #below
    if g_n_phi_phi==0 or np.isinf(g_n_phi_phi):
        g_n_phi_phi=1

    if g_v_phi_phi==0 or np.isinf(g_v_phi_phi):
        g_v_phi_phi=1

    if np.isnan(g_v_phi_phi) or np.isnan(g_n_phi_phi):
        g_n_phi_phi=1
        g_v_phi_phi=1



    inputs["g_n_t_t"]=g_n_t_t
    inputs["g_n_t_X"]=g_n_t_X
    inputs["g_n_X_X"]=g_n_X_X
    inputs["g_n_rho_rho"]=g_n_rho_rho
    inputs["g_n_phi_phi"]=g_n_phi_phi


    inputs["g_v_t_t"]=g_v_t_t
    inputs["g_v_t_X"]=g_v_t_X
    inputs["g_v_X_X"]=g_v_X_X
    inputs["g_v_rho_rho"]=g_v_rho_rho
    inputs["g_v_phi_phi"]=g_v_phi_phi

    inputs["sqrt_lambda_minus"]=sqrt_lambda_minus
    inputs["sqrt_lambda_plus"]=sqrt_lambda_plus



    ########################################################
    # we compute G_doubly covariant!!
    G_doubly_cov=np.array( [ [ G__t_t   , G__t_x   , G__t_rho   , 0 ],\
                             [ G__t_x   , G__x_x   , G__x_rho   , 0 ],\
                             [ G__t_rho , G__x_rho , G__rho_rho , 0 ],\
                             [ 0        , 0        , 0          , G__phi_phi ],\
    ])
    #######################################################
    # below are the numpy equivalents of lines / rows
    g_n_mat=np.array([[inputs["g_n_t_t"],inputs["g_n_t_X"],0,0],\
                      [inputs["g_n_t_X"],inputs["g_n_X_X"],0,0],\
                      [0                ,0                ,inputs["g_n_rho_rho"],0],\
                      [0                ,0                ,0                    ,inputs["g_n_phi_phi"]]])

    g_v_mat=np.array([[inputs["g_v_t_t"],inputs["g_v_t_X"],0,0],\
                      [inputs["g_v_t_X"],inputs["g_v_X_X"],0,0],\
                      [0                ,0                ,inputs["g_v_rho_rho"],0],\
                      [0                ,0                ,0                    ,inputs["g_v_phi_phi"]]])



    lambda_plus=(-(A-1)+np.sqrt((A-1)**2+4))/2.
    lambda_minus=(-(A-1)-np.sqrt((A-1)**2+4))/2.
    tW=np.array([[meshed_cos_theta,0,meshed_sin_theta*1./sqrt_lambda_plus],\
                 [0,1,0],\
                 [-meshed_sin_theta*sqrt_lambda_plus,0,meshed_cos_theta],\
                 ])

    return_to_starting_block=True
    if 1:


        #print('choose tw4 ---> make a function which creates such matrices \n line 1640')



        tW_4,\
            tW_4_rot_no_curv,\
            tW_4_boo_no_curv,\
            iter_overflow=three_fluids___rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu1_x_t,\
                                                                     hyp_flu1_x_x,\
                                                                     hyp_flu1_rho_rho,\
                                                                     hyp_flu1_t_t,\
                                                                     hyp_flu1_cstt,\
                                                                     hyp_flu1_phiphi,\
                                                                     euc_flu1_x_t,\
                                                                     euc_flu1_x_x,\
                                                                     euc_flu1_rho_rho,\
                                                                     euc_flu1_t_t,\
                                                                     euc_flu1_cstt,\
                                                                     euc_flu1_phiphi,\
                                                                     inputs,\
        )

        tW_4_second_fluid,\
            tW_4_second_fluid_rot_no_curv,\
            tW_4_second_fluid_boo_no_curv,\
            iter_overflow_second=three_fluids___rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu2_x_t,\
                                                                                  hyp_flu2_x_x,\
                                                                                  hyp_flu2_rho_rho,\
                                                                                  hyp_flu2_t_t,\
                                                                                  hyp_flu2_cstt,\
                                                                                  hyp_flu2_phiphi,\
                                                                                  euc_flu2_x_t,\
                                                                                  euc_flu2_x_x,\
                                                                                  euc_flu2_rho_rho,\
                                                                                  euc_flu2_t_t,\
                                                                                  euc_flu2_cstt,\
                                                                                  euc_flu2_phiphi,\
                                                                                  inputs,\
        )

        tW_4_third_fluid,\
            tW_4_third_fluid_rot_no_curv,\
            tW_4_third_fluid_boo_no_curv,\
            iter_overflow_third=three_fluids___rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu3_x_t,\
                                                                                 hyp_flu3_x_x,\
                                                                                 hyp_flu3_rho_rho,\
                                                                                 hyp_flu3_t_t,\
                                                                                 hyp_flu3_cstt,\
                                                                                 hyp_flu3_phiphi,\
                                                                                 euc_flu3_x_t,\
                                                                                 euc_flu3_x_x,\
                                                                                 euc_flu3_rho_rho,\
                                                                                 euc_flu3_t_t,\
                                                                                 euc_flu3_cstt,\
                                                                                 euc_flu3_phiphi,\
                                                                                 inputs,\
        )



        #below is the g_munu diagonal version
        thediag=np.diag([lambda_plus,lambda_minus,1])
        thediag_4=np.diag([lambda_plus,lambda_minus,1,g_v_phi_phi])
        thediag_4_no_curve=np.diag([1,-1,1,1])#no_curv

        product=np.dot(tW.T,np.dot(thediag,tW))

        product_4_4__ini_fluid__tw4=np.dot(tW_4.T,np.dot(thediag_4,tW_4)) # this should be the same as the_diag_4
        product_4_4__2nd_fluid__tw4=np.dot(tW_4_second_fluid.T,np.dot(thediag_4,tW_4_second_fluid)) # this should be the same as the_diag_4
        product_4_4__3rd_fluid__tw4=np.dot(tW_4_third_fluid.T,np.dot(thediag_4,tW_4_third_fluid)) # this should be the same as the_diag_4


        #interesting line below!!
        ####    g_v_t_X=v*(1-meshed_f)

        N_plus=np.sqrt((lambda_plus-1)**2+g_v_t_X**2)
        N_minus=np.sqrt((lambda_minus-1)**2+g_v_t_X**2)

        #################################
        # now we just examine the theta matrix
        #eigen_vect_g_doublcovariant=EVGDCov
        #{EVGDCov=theta matrix} is the matrix of eigen vectors of g_{mu,nu}

        EVGDCov=np.array([[(lambda_plus-1.0)/N_plus,(lambda_minus-1.0)/N_minus,0,0],\
                         [g_v_t_X/(1.0*N_plus),g_v_t_X/(1.0*N_minus),0,0],\
                         [0,0,1.,0],\
                         [0,0,0,1.]])
        the_product_should_be_diagonal=np.dot(EVGDCov.T,np.dot(g_v_mat,EVGDCov))

        ##################################

        # the following is contravariant (each nested array is not a desirable vector rather this is component 1 of the first array and then comp 1 of the second, etc.
        e_not_prime=np.array([[-1,0,0,0],\
                              [g_v_t_X,1,0,0],\
                              [0,0,1,0],\
                              [0,0,0,1/np.sqrt(g_v_phi_phi) ],\
        ])

        the_product_should_be_minkowski_metric=np.dot(e_not_prime.T,np.dot(g_v_mat,e_not_prime))


        minkowski_metric=np.diag([-1,1,1,1])
        e_prime=np.dot(EVGDCov,np.dot(tW_4,np.dot(EVGDCov.T,e_not_prime)))
        e_prime_2nd=np.dot(EVGDCov,np.dot(tW_4_second_fluid,np.dot(EVGDCov.T,e_not_prime)))
        e_prime_3rd=np.dot(EVGDCov,np.dot(tW_4_third_fluid,np.dot(EVGDCov.T,e_not_prime))) 
        # these 3 vectors have contravariant components

        e_prime_3rd_cov=np.dot(g_v_mat,e_prime_3rd)# in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures
        e_prime_2nd_cov=np.dot(g_v_mat,e_prime_2nd)# same as above



        # e_prime is described as sets of lines , each dissecting interesting vectors (vectors as columns)
        #e_prime here is contravariant
        #below we compute the covariant vectors in a single matrix


        e_prime_cov=np.dot(g_v_mat,e_prime)# in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures



        # we compute the mixed components of Einstein tensor
        G_einst_mixed_plus_lambda_delta=np.dot(g_n_mat,G_doubly_cov+Lambda_cosmological_constant_*g_v_mat )






        ####################################################################





        ################################################


        #################################################
        #################################################



        basis_vectors_3fluids=[[e_prime_cov,e_prime],[e_prime_2nd_cov,e_prime_2nd],[e_prime_3rd_cov,e_prime_3rd]]
        big_matrix_3flu,gee_vector_3flu,overflow_big_mat=compute_big_matrix_for_fluids(basis_vectors_3fluids,G_einst_mixed_plus_lambda_delta)


        if np.any(np.isnan(big_matrix_3flu)) or np.any(np.isinf(big_matrix_3flu)) or \
           np.any(np.isnan(gee_vector_3flu)) or np.any(np.isinf(gee_vector_3flu)):
            return_to_starting_block=True

            hyp_flu1_x_t=hyp_flu1_x_t/3.
            hyp_flu1_x_x=hyp_flu1_x_x/3.
            hyp_flu1_rho_rho=hyp_flu1_rho_rho/3.
            hyp_flu1_t_t=hyp_flu1_t_t/3.
            hyp_flu1_cstt=hyp_flu1_cstt/3.
            hyp_flu1_phiphi=hyp_flu1_phiphi/3.

            hyp_flu2_x_t=hyp_flu2_x_t/3.
            hyp_flu2_x_x=hyp_flu2_x_x/3.
            hyp_flu2_rho_rho=hyp_flu2_rho_rho/3.
            hyp_flu2_t_t=hyp_flu2_t_t/3.
            hyp_flu2_cstt=hyp_flu2_cstt/3.
            hyp_flu2_phiphi=hyp_flu2_phiphi/3.

            hyp_flu3_x_t=hyp_flu3_x_t/3.
            hyp_flu3_x_x=hyp_flu3_x_x/3.
            hyp_flu3_rho_rho=hyp_flu3_rho_rho/3.
            hyp_flu3_t_t=hyp_flu3_t_t/3.
            hyp_flu3_cstt=hyp_flu3_cstt/3.
            hyp_flu3_phiphi=hyp_flu3_phiphi/3.
            # above if inf or nan we decrease the values of all coefficients

            bad_inf_nan+=1
            
            densities_pressures_3fluids=[-11,-11,-11,-11,-11,-11,-11,-11,-11,-11,-11,-11]
            
            matrix_rank_3fluids=-1
            

        else:
            return_to_starting_block=False
            densities_pressures_3fluids,\
                residuals_3fluids,\
                matrix_rank_3fluids,\
                sing_values_matrix_3fluids=np.linalg.lstsq(big_matrix_3flu, gee_vector_3flu,rcond=None)

    ##############################################################


    v_p=np.dot(big_matrix_3flu,densities_pressures_3fluids) # vector predicted
    v_r=gee_vector_3flu
    
    denom=np.array([el if el !=0 else 1 for el in list(np.abs(v_p)+np.abs(v_r)) ])
    diff_sur_som=np.abs(v_p-v_r)/denom
    

    ##############################################################################################################################

    res=list(densities_pressures_3fluids)
    


    return res[2]+1j*res[3],\
        res[6]+1j*res[7],\
        res[10]+1j*res[11]


#############################################################################################

############################################################################################################################################
##########################################################################################################################
def maximum_absolute_value_of_einstein_tensor_components___myfunc(G__t_t,\
                                                                  G__t_rho,\
                                                                  G__t_x,\
                                                                  G__x_rho,\
                                                                  G__x_x,\
                                                                  G__phi_phi,\
                                                                  G__rho_rho,\
):
    return np.max(np.abs([G__t_t,\
                          G__t_rho,\
                          G__t_x,\
                          G__x_rho,\
                          G__x_x,\
                          G__phi_phi,\
                          G__rho_rho]))

#########################################################################################################################
#########################################################################################################################
#########################################################################################################################

################################################################################################################################
def finish_dict_reloaded__compute_meaningful_physical_quantities(dict_a,N__fluids_sol__output_dict):




    p_rho_1=N__fluids_sol__output_dict["p_rho__p_phi_1"].real
    p_phi_1=N__fluids_sol__output_dict["p_rho__p_phi_1"].imag
    p_rho_2=N__fluids_sol__output_dict["p_rho__p_phi_2"].real
    p_phi_2=N__fluids_sol__output_dict["p_rho__p_phi_2"].imag
    zeros_array=np.zeros(p_rho_1.shape)

    density_1=N__fluids_sol__output_dict["rho__p_x_1"].real
    p_X_1=N__fluids_sol__output_dict["rho__p_x_1"].imag
    density_2=N__fluids_sol__output_dict["rho__p_x_2"].real
    p_X_2=N__fluids_sol__output_dict["rho__p_x_2"].imag
    


    very_small_volumes=N__fluids_sol__output_dict['very_small_volumes']
    small_volumes=N__fluids_sol__output_dict['small_volumes']
    if (np.array(small_volumes)<0).any():
        print("(np.array(small_volumes)<0).any():")
        print("this is bad: volume are negative line 6307")
        raise


    


    vol_dens_1=np.multiply(small_volumes,density_1)
    vol_dens_1_pos_sum=np.sum(vol_dens_1[vol_dens_1>0])
    vol_dens_1_neg_sum=np.sum(vol_dens_1[vol_dens_1<0])
    vol_dens_1_TOT_sum=np.sum(vol_dens_1)

    vol_dens_2=np.multiply(small_volumes,density_2)
    vol_dens_2_pos_sum=np.sum(vol_dens_2[vol_dens_2>0])
    vol_dens_2_neg_sum=np.sum(vol_dens_2[vol_dens_2<0])
    vol_dens_2_TOT_sum=np.sum(vol_dens_2)

    
    dict_a["vol_dens_2_pos_sum"]=vol_dens_2_pos_sum
    dict_a["vol_dens_2_neg_sum"]=vol_dens_2_neg_sum
    dict_a["vol_dens_2_TOT_sum"]=vol_dens_2_TOT_sum

    dict_a["vol_dens_1_pos_sum"]=vol_dens_1_pos_sum
    dict_a["vol_dens_1_neg_sum"]=vol_dens_1_neg_sum
    dict_a["vol_dens_1_TOT_sum"]=vol_dens_1_TOT_sum

    R_very_small__spherical=np.power(very_small_volumes/(4.*np.pi/3.),1.0/3.0)
    if N__fluids_sol__output_dict["nb_of_fluids"]>2.5:



        
        p_rho_3=N__fluids_sol__output_dict["p_rho__p_phi_3"].real
        p_phi_3=N__fluids_sol__output_dict["p_rho__p_phi_3"].imag
        density_3=N__fluids_sol__output_dict["rho__p_x_3"].real
        p_X_3=N__fluids_sol__output_dict["rho__p_x_3"].imag

        #############
        #compute schwarzschild condition
        all_densities_positive_matrix=(density_1>0)&(density_2>0)&(density_3>0)

        # below we compute R_very small as the radius of a volume element of size each of elements  of V_very_small



        condition_schwarzschild=2*4.0/3.*np.pi*np.multiply(density_1+density_2+density_3,R_very_small__spherical**2)


        dict_a["np.mean(condition_schwarzschild)"]=np.mean(condition_schwarzschild)
        dict_a["np.max(condition_schwarzschild)"]=np.max(condition_schwarzschild)
        dict_a["np.min(condition_schwarzschild)"]=np.min(condition_schwarzschild)
        
        

        selected_data__cond_schwarz=np.select(all_densities_positive_matrix,condition_schwarzschild,default=np.nan)
        
        dict_a["np.nanmean(selected_data__cond_schwarz)"]=np.nanmean(selected_data__cond_schwarz)
        dict_a["np.nanmax(selected_data__cond_schwarz)"]=np.nanmax(selected_data__cond_schwarz)
        dict_a["np.nanmin(selected_data__cond_schwarz)"]=np.nanmin(selected_data__cond_schwarz)

        


        
        vol_dens_3=np.multiply(small_volumes,density_3)
        vol_dens_3_pos_sum=np.sum(vol_dens_3[vol_dens_3>0])
        vol_dens_3_neg_sum=np.sum(vol_dens_3[vol_dens_3<0])
        vol_dens_3_TOT_sum=np.sum(vol_dens_3)

        dict_a["vol_dens_3_pos_sum"]=vol_dens_3_pos_sum
        dict_a["vol_dens_3_neg_sum"]=vol_dens_3_neg_sum
        dict_a["vol_dens_3_TOT_sum"]=vol_dens_3_TOT_sum



        pressure_1=p_rho_1+p_phi_1+p_X_1
        pressure_2=p_rho_2+p_phi_2+p_X_2
        pressure_3=p_rho_3+p_phi_3+p_X_3

        pressure_max=np.max([pressure_1,pressure_2,pressure_3])
        pressure_mean=np.mean([pressure_1,pressure_2,pressure_3])
        pressure_std=np.std([pressure_1,pressure_2,pressure_3])
        pressure_min=np.min([pressure_1,pressure_2,pressure_3])

        
        p_rho_max=np.max([p_rho_1,p_rho_2,p_rho_3])
        p_rho_mean=np.mean([p_rho_1,p_rho_2,p_rho_3])
        p_rho_std=np.std([p_rho_1,p_rho_2,p_rho_3])
        p_rho_min=np.min([p_rho_1,p_rho_2,p_rho_3])
        
        p_phi_max=np.max([p_phi_1,p_phi_2,p_phi_3])
        p_phi_mean=np.mean([p_phi_1,p_phi_2,p_phi_3])
        p_phi_std=np.std([p_phi_1,p_phi_2,p_phi_3])
        p_phi_min=np.min([p_phi_1,p_phi_2,p_phi_3])

        p_X_max=np.max([p_X_1,p_X_2,p_X_3])
        p_X_mean=np.mean([p_X_1,p_X_2,p_X_3])
        p_X_std=np.std([p_X_1,p_X_2,p_X_3])
        p_X_min=np.min([p_X_1,p_X_2,p_X_3])
        
        density_max=np.max([density_1,density_2,density_3])
        density_mean=np.mean([density_1,density_2,density_3])
        density_std=np.std([density_1,density_2,density_3])
        density_min=np.min([density_1,density_2,density_3])
    else:




        all_densities_positive_matrix=(density_1>0)&(density_2>0)
        
        condition_schwarzschild=2*4.0/3.*np.pi*np.multiply(density_1+density_2,R_very_small__spherical**2)

        dict_a["np.mean(condition_schwarzschild)"]=np.mean(condition_schwarzschild)
        dict_a["np.max(condition_schwarzschild)"]=np.max(condition_schwarzschild)
        dict_a["np.min(condition_schwarzschild)"]=np.min(condition_schwarzschild)
        
        

        selected_data__cond_schwarz=np.select(all_densities_positive_matrix,condition_schwarzschild,default=np.nan)
        
        dict_a["np.nanmean(selected_data__cond_schwarz)"]=np.nanmean(selected_data__cond_schwarz)
        dict_a["np.nanmax(selected_data__cond_schwarz)"]=np.nanmax(selected_data__cond_schwarz)
        dict_a["np.nanmin(selected_data__cond_schwarz)"]=np.nanmin(selected_data__cond_schwarz)



        pressure_1=p_rho_1+p_phi_1+p_X_1
        pressure_2=p_rho_2+p_phi_2+p_X_2
        

        pressure_max=np.max([pressure_1,pressure_2])
        pressure_mean=np.mean([pressure_1,pressure_2])
        pressure_std=np.std([pressure_1,pressure_2])
        pressure_min=np.min([pressure_1,pressure_2])

        p_rho_max=np.max([p_rho_1,p_rho_2])
        p_rho_mean=np.mean([p_rho_1,p_rho_2])
        p_rho_std=np.std([p_rho_1,p_rho_2])
        p_rho_min=np.min([p_rho_1,p_rho_2])
        
        p_phi_max=np.max([p_phi_1,p_phi_2])
        p_phi_mean=np.mean([p_phi_1,p_phi_2])
        p_phi_std=np.std([p_phi_1,p_phi_2])
        p_phi_min=np.min([p_phi_1,p_phi_2])

        p_X_max=np.max([p_X_1,p_X_2])
        p_X_mean=np.mean([p_X_1,p_X_2])
        p_X_std=np.std([p_X_1,p_X_2])
        p_X_min=np.min([p_X_1,p_X_2])
        
        density_max=np.max([density_1,density_2])
        density_mean=np.mean([density_1,density_2])
        density_std=np.std([density_1,density_2])
        density_min=np.min([density_1,density_2])

    dict_a["pressure_max"]=pressure_max
    dict_a["pressure_mean"]=pressure_mean
    dict_a["pressure_std"]=pressure_std
    dict_a["pressure_min"]=pressure_min

    
    dict_a["p_rho_max"]=p_rho_max
    dict_a["p_rho_mean"]=p_rho_mean
    dict_a["p_rho_std"]=p_rho_std
    dict_a["p_rho_min"]=p_rho_min



    
    dict_a["p_phi_max"]=p_phi_max
    dict_a["p_phi_mean"]=p_phi_mean
    dict_a["p_phi_std"]=p_phi_std
    dict_a["p_phi_min"]=p_phi_min
    
    dict_a["p_X_max"]=p_X_max
    dict_a["p_X_mean"]=p_X_mean
    dict_a["p_X_std"]=p_X_std
    dict_a["p_X_min"]=p_X_min
    
    dict_a["density_max"]=density_max
    dict_a["density_mean"]=density_mean
    dict_a["density_std"]=density_std
    dict_a["density_min"]=density_min

    ##########################


    p_phi1=p_phi_1
    rho1=density_1
    px1=p_X_1
    p_rho1=p_rho_1

    p_phi2=p_phi_2
    rho2=density_2
    px2=p_X_2
    p_rho2=p_rho_2

    if N__fluids_sol__output_dict["nb_of_fluids"]>2.5:
        p_phi3=p_phi_3
        rho3=density_3
        px3=p_X_3
        p_rho3=p_rho_3

    ###################################
    
    dict_a['MAX-w--EOS']=N__fluids_sol__output_dict["current_EOS"]
    
    dict_a['MIN-p-plus-rho']=N__fluids_sol__output_dict["current_NEC"]
    dict_a['min-ranks']=N__fluids_sol__output_dict["ranks"]


    diff_sur_som=N__fluids_sol__output_dict["diff_over_sum"]
    dict_a['np.max(diff_sur_som)']=np.max(diff_sur_som)
    dict_a['np.mean(diff_sur_som)']=np.mean(diff_sur_som)
    dict_a['np.std(diff_sur_som)']=np.std(diff_sur_som)
    dict_a['np.min(diff_sur_som)']=np.min(diff_sur_som)


    rho_1_is_zero=(np.abs(rho1)<np.finfo(np.float).eps)
    w_1=np.divide(px1+p_rho1+p_phi1,rho1)
    corrected_for_zero_rho1__w_1=np.multiply(np.zeros(rho1.shape),rho_1_is_zero)+np.multiply(w_1,~rho_1_is_zero)

    rho_2_is_zero=(np.abs(rho2)<np.finfo(np.float).eps)
    w_2=np.divide(px2+p_rho2+p_phi2,rho2)
    corrected_for_zero_rho2__w_2=np.multiply(np.zeros(rho2.shape),rho_2_is_zero)+np.multiply(w_2,~rho_2_is_zero)

    if N__fluids_sol__output_dict["nb_of_fluids"]>2.5:
    
        rho_3_is_zero=(np.abs(rho3)<np.finfo(np.float).eps)
        w_3=np.divide(px3+p_rho3+p_phi3,rho3)
        corrected_for_zero_rho3__w_3=np.multiply(np.zeros(rho3.shape),rho_3_is_zero)+np.multiply(w_3,~rho_3_is_zero)

        max_abs_w_s__EOS=maximum_absolute_value_of_einstein_tensor_components___vfunc(corrected_for_zero_rho3__w_3,\
                                                                                      corrected_for_zero_rho2__w_2,\
                                                                                      corrected_for_zero_rho1__w_1,\
                                                                                      0,\
                                                                                      0,\
                                                                                      0,\
                                                                                      0,\
        )

    else :
        max_abs_w_s__EOS=maximum_absolute_value_of_einstein_tensor_components___vfunc(corrected_for_zero_rho2__w_2,\
                                                                                      corrected_for_zero_rho1__w_1,\
                                                                                      0,\
                                                                                      0,\
                                                                                      0,\
                                                                                      0,\
                                                                                      0,\
        )



    dict_a['np.max(max_abs_w_s__EOS)']=np.max(max_abs_w_s__EOS)
    dict_a['np.std(max_abs_w_s__EOS)']=np.std(max_abs_w_s__EOS)
    dict_a['np.mean(max_abs_w_s__EOS)']=np.mean(max_abs_w_s__EOS)
    dict_a['np.min(max_abs_w_s__EOS)']=np.min(max_abs_w_s__EOS)
    
    
    dict_a['np.sum(max_abs_w_s__EOS<1.0/3.0)']=np.float(np.sum(max_abs_w_s__EOS<1.0/3.0))
    dict_a['np.sum(max_abs_w_s__EOS<1.0)']=np.float(np.sum(max_abs_w_s__EOS<1.0))
    ################################

    epsilon__plus__p_x=rho1+px1
    epsilon__plus__p_rho=rho1+p_rho1
    epsilon__plus__p_phi=rho1+p_phi1
    minimum_NEC_1=compute_each_space_box_minimum_nec___vfunc(epsilon__plus__p_x,epsilon__plus__p_rho,epsilon__plus__p_phi)

    epsilon__plus__p_x=rho2+px2
    epsilon__plus__p_rho=rho2+p_rho2
    epsilon__plus__p_phi=rho2+p_phi2
    minimum_NEC_2=compute_each_space_box_minimum_nec___vfunc(epsilon__plus__p_x,epsilon__plus__p_rho,epsilon__plus__p_phi)
    if N__fluids_sol__output_dict["nb_of_fluids"]>2.5:
        epsilon__plus__p_x=rho3+px3
        
        epsilon__plus__p_rho=rho3+p_rho3
        epsilon__plus__p_phi=rho3+p_phi3
        minimum_NEC_3=compute_each_space_box_minimum_nec___vfunc(epsilon__plus__p_x,epsilon__plus__p_rho,epsilon__plus__p_phi)
    

        

        absolute_minimum_NEC=compute_each_space_box_minimum_nec___vfunc(minimum_NEC_1,minimum_NEC_2,minimum_NEC_3)
    else:
        absolute_minimum_NEC=compute_each_space_box_minimum_nec___vfunc(minimum_NEC_1,minimum_NEC_2,0)
        
    dict_a['np.max(absolute_minimum_NEC)']=np.max(absolute_minimum_NEC)
    dict_a['np.std(absolute_minimum_NEC)']=np.std(absolute_minimum_NEC)
    dict_a['np.mean(absolute_minimum_NEC)']=np.mean(absolute_minimum_NEC)
    dict_a['np.min(absolute_minimum_NEC)']=np.min(absolute_minimum_NEC)
    #################################


    #######################
    names=[el for el in list(N__fluids_sol__output_dict.keys()) if ('gamma___' in el.lower() and "_relation" in el.lower()) or \
           el=="max_abs_u_squared_plus_one" or \
           el=="max_abs_of_velocities_components" \
    ]
    for el in names:
        dict_a[el]=N__fluids_sol__output_dict[el]

    ####################################
    keys_dict=list(dict_a.keys())
    for key in keys_dict:
        if  np.iscomplexobj(dict_a[key]):
                dict_a[key+'___real']=dict_a[key].real
                dict_a[key+'___imag']=dict_a[key].imag
                dict_a.pop(key,None)
        
    for key in dict_a.keys():

        if 'fun'==key:
            
            print(str(key)+'   :   '+str(dict_a[key]))
            dict_a[key]=np.float(dict_a[key])
            
        if isinstance(dict_a[key], np.ndarray):
            dict_a[key]=dict_a[key].tolist()
        if key=='final_simplex':

            if not isinstance(dict_a[key],list):
                dict_a[key]=dict_a[key].tolist()
    
    

    return dict_a
###############################################################
def compute_inv_or_zero___myfunc(x):
    if x!=0:
        return 1.0/x
    else:
        return 0.
###########################################################################################################################

def Nfluids_solution__check__EOS_NEC_RANK(dict_reloaded,v_t,v,Lambda_cosmological_constant,nb_of_fluids,\
                                          two_fluids__EOS_NEC_RANK___vfunc,\
                                          three_fluids__EOS_NEC_RANK___vfunc,\
):

    #these are angles that will be explored
    DELTA_theta=np.pi/100.
    theta_real=np.arange(0.0,np.pi,DELTA_theta)

    theta=theta_real/4. # any continuous function of theta could be used for optim / but not when we are analyzing results !! 
    

    #this is the radial range that is explored
    DELTA_r=0.05
    r_range=np.arange(0.01,2.7,DELTA_r) 
    

    
    sin_theta,cos_theta,ones_theta,r_mesh,cos_theta_squared,sin_theta_squared,sin_half_theta,sin_half_theta_squared,ones_theta_and_r,\
        meshed_sin_half_theta_squared,meshed_sin_theta,meshed_cos_theta,theta_mesh=compute_things_with_r_and_thetas(theta,r_range)

    sin_theta_real,cos_theta_real,ones_theta_real,r_mesh__one_theta_real,cos_theta_real_squared,sin_theta_real_squared,sin_half_theta_real,sin_half_theta_real_squared,ones_theta_real_and_r,\
        meshed_sin_half_theta_real_squared,meshed_sin_theta_real,meshed_cos_theta_real,theta_real_mesh=compute_things_with_r_and_thetas(theta_real,r_range)
    ##########################"
    
    small_volumes=np.multiply(meshed_sin_theta_real,(r_mesh__one_theta_real+DELTA_r/2.)**2)*DELTA_theta*DELTA_r*2*np.pi
    # what is above has a meaning : just try to compute a corona volume 2 pi around an axis in spherical coordinates

    #corona-virus?
    very_small_volumes=(r_mesh__one_theta_real+DELTA_r/2.)**2*DELTA_theta**2*DELTA_r
    
    r=r_range

    
    sigma=8
    
    R=1

    f,f_r,f_rr,f_r_over_r=compute_f_and_derivatives(r,R,sigma)

    ######################################################################
    #necessary requirements for einstein tensor computation
    meshed_cos_theta_real_times_meshed_sin_theta_real=np.multiply(meshed_cos_theta_real,meshed_sin_theta_real)
    meshed_cos_squared_theta_real=np.multiply(meshed_cos_theta_real,meshed_cos_theta_real)
    meshed_sin_squared_theta_real=np.multiply(meshed_sin_theta_real,meshed_sin_theta_real)
    f_r__squared=np.multiply(f_r,f_r)
    f_rr_minus__f_r_over_r=f_rr-f_r_over_r

    #############################################################

    
    meshed_f,cos_theta_squared_dot__f_rr_minus_f_r_over_r,\
        sin_theta_squared_dot__f_rr_minus_f_r_over_r,meshed_f_r,meshed_f_r_over_r,meshed_f_rr=mix_derivatives_of_f__and_theta(f,f_r,f_rr,f_r_over_r,sin_theta,cos_theta,ones_theta,\
                                                                                                                              cos_theta_squared,sin_theta_squared)

    meshed_f__ones_theta_real,cos_theta_real_squared_dot__f_rr_minus_f_r_over_r,\
        sin_theta_real_squared_dot__f_rr_minus_f_r_over_r,\
        meshed_f_r__ones_theta_real,\
        meshed_f_r_over_r__ones_theta_real,\
        meshed_f_rr__ones_theta_real=mix_derivatives_of_f__and_theta(f,f_r,f_rr,f_r_over_r,sin_theta_real,cos_theta_real,ones_theta_real,cos_theta_real_squared,sin_theta_real_squared)

    ############################################################
    ############################################################
    if 1:


        print(' ------ ')
        print (' CURRENT warp speed : '+str(v))
        print(' -------------- \n')

        if 1:
            right_dict=[dict_reloaded]


            A, lambda_plus,sqrt_lambda_plus,sqrt_lambda_minus,N_plus_squared,N_plus,sin_half_theta_squared_over_N_plus_squared,\
                sin_theta_over_N_plus=compute_composites_warp_v__meshed_f__and_theta(v,meshed_f,meshed_sin_half_theta_squared,meshed_sin_theta)

            A__ones_theta_real, lambda_plus__ones_theta_real,sqrt_lambda_plus__ones_theta_real,sqrt_lambda_minus__ones_theta_real,N_plus_squared__ones_theta_real,N_plus__ones_theta_real,\
                sin_half_theta_real_squared_over_N_plus_squared,\
                sin_theta_real_over_N_plus=compute_composites_warp_v__meshed_f__and_theta(v,meshed_f__ones_theta_real,meshed_sin_half_theta_real_squared,meshed_sin_theta_real)

            e_p_t_t,\
                e_p_t_X,\
                e_p_t_rho,\
                e_p_t_phi,\
                e_p_X_t,\
                e_p_X_X,\
                e_p_X_rho,\
                e_p_X_phi,\
                e_p_rho_t,\
                e_p_rho_X,\
                e_p_rho_rho,\
                e_p_rho_phi,\
                e_p_phi_t,\
                e_p_phi_X,\
                e_p_phi_rho,\
                e_p_phi_phi,\
                real_rho_values=e_prime_computation(sin_half_theta_squared_over_N_plus_squared,\
                                                    lambda_plus,sqrt_lambda_plus,A,v,meshed_f,sin_theta_over_N_plus,meshed_f_r,meshed_cos_theta,theta_real_mesh,r_mesh)

            G__t_t,\
                G__t_x,\
                G__x_x,\
                G__t_rho,\
                G__x_rho,\
                G__rho_rho,\
                G__phi_phi=compute_einstein_tensor_components_doublyCov(meshed_f_r_over_r__ones_theta_real,\
                                                                        sin_theta_real_squared_dot__f_rr_minus_f_r_over_r,\
                                                                        meshed_sin_squared_theta_real,\
                                                                        meshed_f_r__ones_theta_real,\
                                                                        v,\
                                                                        meshed_f__ones_theta_real,\
                                                                        meshed_cos_theta_real_times_meshed_sin_theta_real,\
                                                                        meshed_f_rr__ones_theta_real,\
                                                                        v_t,\
                                                                        meshed_sin_theta_real,\
                                                                        meshed_cos_theta_real,\
                                                                        cos_theta_real_squared_dot__f_rr_minus_f_r_over_r,\
                                                                        real_rho_values,\
            )



            ################"

            g_n_t_t,\
                g_n_t_X,\
                g_n_X_X,\
                g_n_rho_rho,\
                g_n_phi_phi,\
                g_v_t_t,\
                g_v_t_X,\
                g_v_X_X,\
                g_v_rho_rho,\
                g_v_phi_phi=compute_doublycov_doublycontra_metrics_comp(ones_theta_and_r,v,meshed_f,A,real_rho_values,\
            )


            ##########################################################

            # et  m a i ntenant on calcule le tenseur d'einstein


            ######################################
    

            #######################################
    



            ##############################################################################################################################################
            #############################################################################################################

            ############################################################################################################

            
            
            full_outputs=[]

            not_mock_up___now=1
            if not_mock_up___now:
                batch_nb=1
                # these batch enabled to output the three parts of the results one at a time / for optimization of NEC we do not need to curtail/ tailor a unique solution
                # as a three headed solution because the outputs are not numerous


                res_batch=batch_nb*np.ones(meshed_f_rr__ones_theta_real.shape)


                #1
                g_n_t__t_jX=1j*g_n_t_X
                g_n_t__t_jX+=g_n_t_t

                #2
                g_n__rho2_jX2=1j*g_n_X_X
                g_n__rho2_jX2+=g_n_rho_rho


                #3
                #try:
                g__v_jn__phiphi=1j*g_n_phi_phi
                g__v_jn__phiphi+=g_v_phi_phi



                #4
                g_v_t__t_jX=1j*g_v_t_X
                g_v_t__t_jX+=g_v_t_t

                #5
                g_v__rho2_jX2=1j*g_v_X_X
                g_v__rho2_jX2+=g_v_rho_rho

                #5
                meshed__cos_jsin__theta=1j*meshed_sin_theta
                meshed__cos_jsin__theta+=meshed_cos_theta

                #6
                sqrt_lambda__pl_jmin=1j*sqrt_lambda_minus
                sqrt_lambda__pl_jmin+=sqrt_lambda_plus

                #7
                G__t_t_jA=1j*A
                G__t_t_jA+=G__t_t

                #8
                G__t__x_jrho=1j*G__t_rho
                G__t__x_jrho+=G__t_x

                #9
                G__x__x_jrho=1j*G__x_rho
                G__x__x_jrho+=G__x_x

                #10
                G_x__rho2_jphi2=1j*G__phi_phi
                G_x__rho2_jphi2+=G__rho_rho


                
                if len(right_dict)!=0 :

                    [hyp_flu1_x_t,\
                     hyp_flu1_x_x,\
                     hyp_flu1_rho_rho,\
                     hyp_flu1_t_t,\
                     hyp_flu1_cstt,\
                     hyp_flu1_phiphi,\
                     euc_flu1_x_t,\
                     euc_flu1_x_x,\
                     euc_flu1_rho_rho,\
                     euc_flu1_t_t,\
                     euc_flu1_cstt,\
                     euc_flu1_phiphi,\
                     hyp_flu2_x_t,\
                     hyp_flu2_x_x,\
                     hyp_flu2_rho_rho,\
                     hyp_flu2_t_t,\
                     hyp_flu2_cstt,\
                     hyp_flu2_phiphi,\
                     euc_flu2_x_t,\
                     euc_flu2_x_x,\
                     euc_flu2_rho_rho,\
                     euc_flu2_t_t,\
                     euc_flu2_cstt,\
                     euc_flu2_phiphi,\
                     hyp_flu3_x_t,\
                     hyp_flu3_x_x,\
                     hyp_flu3_rho_rho,\
                     hyp_flu3_t_t,\
                     hyp_flu3_cstt,\
                     hyp_flu3_phiphi,\
                     euc_flu3_x_t,\
                     euc_flu3_x_x,\
                     euc_flu3_rho_rho,\
                     euc_flu3_t_t,\
                     euc_flu3_cstt,\
                     euc_flu3_phiphi,\
                    ]=right_dict[0]['x']
                else:
                    print('no results whatsoever!')
                    #there should be some initialization HERE for optimization (but this is not optimization :) however when we analyze results (this is thecase here), one wishes to have results!
                    print('there is no initialisation here and there should be no need of this here \n  line 12191 \n big problem!')
                    raise

                x0=np.array([hyp_flu1_x_t,\
                             hyp_flu1_x_x,\
                             hyp_flu1_rho_rho,\
                             hyp_flu1_t_t,\
                             hyp_flu1_cstt,\
                             hyp_flu1_phiphi,\
                             euc_flu1_x_t,\
                             euc_flu1_x_x,\
                             euc_flu1_rho_rho,\
                             euc_flu1_t_t,\
                             euc_flu1_cstt,\
                             euc_flu1_phiphi,\
                             hyp_flu2_x_t,\
                             hyp_flu2_x_x,\
                             hyp_flu2_rho_rho,\
                             hyp_flu2_t_t,\
                             hyp_flu2_cstt,\
                             hyp_flu2_phiphi,\
                             euc_flu2_x_t,\
                             euc_flu2_x_x,\
                             euc_flu2_rho_rho,\
                             euc_flu2_t_t,\
                             euc_flu2_cstt,\
                             euc_flu2_phiphi,\
                             hyp_flu3_x_t,\
                             hyp_flu3_x_x,\
                             hyp_flu3_rho_rho,\
                             hyp_flu3_t_t,\
                             hyp_flu3_cstt,\
                             hyp_flu3_phiphi,\
                             euc_flu3_x_t,\
                             euc_flu3_x_x,\
                             euc_flu3_rho_rho,\
                             euc_flu3_t_t,\
                             euc_flu3_cstt,\
                             euc_flu3_phiphi,\
                             ])

                args=(g_n_t__t_jX,\
                      g_n__rho2_jX2,\
                      g__v_jn__phiphi,\
                      g_v_t__t_jX,\
                      g_v__rho2_jX2,\
                      meshed__cos_jsin__theta,\
                      sqrt_lambda__pl_jmin,\
                      G__t_t_jA,\
                      G__t__x_jrho,\
                      G__x__x_jrho,\
                      G_x__rho2_jphi2)

                ###################################################

                # there are seven tt tx trho , xx xrho, rhorho, phiphi
                #11
                hyp_coef_flu1_x__x_jt=1j*hyp_flu1_x_t
                hyp_coef_flu1_x__x_jt+=hyp_flu1_x_x

                #12
                hyp_coef_flu1__tt_jrhorho=1j*hyp_flu1_rho_rho
                hyp_coef_flu1__tt_jrhorho+=hyp_flu1_t_t

                #13
                hyp_coef_flu1__phiphi_jcstt=1j*hyp_flu1_cstt
                hyp_coef_flu1__phiphi_jcstt+=hyp_flu1_phiphi

                #14
                euc_coef_flu1_x__x_jt=1j*euc_flu1_x_t
                euc_coef_flu1_x__x_jt+=euc_flu1_x_x

                #15
                euc_coef_flu1__tt_jrhorho=1j*euc_flu1_rho_rho
                euc_coef_flu1__tt_jrhorho+=euc_flu1_t_t

                #16
                euc_coef_flu1__phiphi_jcstt=1j*euc_flu1_cstt
                euc_coef_flu1__phiphi_jcstt+=euc_flu1_phiphi

                ######################"
                #17
                hyp_coef_flu2_x__x_jt=1j*hyp_flu2_x_t
                hyp_coef_flu2_x__x_jt+=hyp_flu2_x_x

                #18
                hyp_coef_flu2__tt_jrhorho=1j*hyp_flu2_rho_rho
                hyp_coef_flu2__tt_jrhorho+=hyp_flu2_t_t

                #19
                hyp_coef_flu2__phiphi_jcstt=1j*hyp_flu2_cstt
                hyp_coef_flu2__phiphi_jcstt+=hyp_flu2_phiphi

                #20
                euc_coef_flu2_x__x_jt=1j*euc_flu2_x_t
                euc_coef_flu2_x__x_jt+=euc_flu2_x_x

                #21
                euc_coef_flu2__tt_jrhorho=1j*euc_flu2_rho_rho
                euc_coef_flu2__tt_jrhorho+=euc_flu2_t_t

                #22
                euc_coef_flu2__phiphi_jcstt=1j*euc_flu2_cstt
                euc_coef_flu2__phiphi_jcstt+=euc_flu2_phiphi
                ########################################

                ######################"
                #23
                hyp_coef_flu3_x__x_jt=1j*hyp_flu3_x_t
                hyp_coef_flu3_x__x_jt+=hyp_flu3_x_x

                #24
                hyp_coef_flu3__tt_jrhorho=1j*hyp_flu3_rho_rho
                hyp_coef_flu3__tt_jrhorho+=hyp_flu3_t_t

                #25
                hyp_coef_flu3__phiphi_jcstt=1j*hyp_flu3_cstt
                hyp_coef_flu3__phiphi_jcstt+=hyp_flu3_phiphi

                #26
                euc_coef_flu3_x__x_jt=1j*euc_flu3_x_t
                euc_coef_flu3_x__x_jt+=euc_flu3_x_x

                #27
                euc_coef_flu3__tt_jrhorho=1j*euc_flu3_rho_rho
                euc_coef_flu3__tt_jrhorho+=euc_flu3_t_t

                #28
                euc_coef_flu3__phiphi_jcstt=1j*euc_flu3_cstt
                euc_coef_flu3__phiphi_jcstt+=euc_flu3_phiphi
                ########################################



            

                res_batch=1
                res_batch__1j_LAMBDA=1j*Lambda_cosmological_constant
                res_batch__1j_LAMBDA+=res_batch



                ##########################################################################
                ##########################################################################

                #1

                g__v_jn__phiphi[(g__v_jn__phiphi.imag==0)|(g__v_jn__phiphi.imag==np.inf)]=1
                g__v_jn__phiphi[(g__v_jn__phiphi.real==0)|(g__v_jn__phiphi.real==np.inf)]=1

                nans_1_of_the_2=np.isnan(g__v_jn__phiphi.real)|np.isnan(g__v_jn__phiphi.imag)
                g__v_jn__phiphi[nans_1_of_the_2]=1+1j*1

                
                output_dict={}
        
                #############################################################


                einst_mixed_largest_value=any_nb_of_fluids__einst_mixed_largest_value___vfunc(g_n_t__t_jX,\
                                                                                                g_n__rho2_jX2,\
                                                                                                g__v_jn__phiphi,\
                                                                                                g_v_t__t_jX,\
                                                                                                g_v__rho2_jX2,\
                                                                                                meshed__cos_jsin__theta,\
                                                                                                sqrt_lambda__pl_jmin,\
                                                                                                G__t_t_jA,\
                                                                                                G__t__x_jrho,\
                                                                                                G__x__x_jrho,\
                                                                                                G_x__rho2_jphi2,\
                                                                                                hyp_coef_flu1_x__x_jt,\
                                                                                                hyp_coef_flu1__tt_jrhorho,\
                                                                                                hyp_coef_flu1__phiphi_jcstt,\
                                                                                                euc_coef_flu1_x__x_jt,\
                                                                                                euc_coef_flu1__tt_jrhorho,\
                                                                                                euc_coef_flu1__phiphi_jcstt,\
                                                                                                hyp_coef_flu2_x__x_jt,\
                                                                                                hyp_coef_flu2__tt_jrhorho,\
                                                                                                hyp_coef_flu2__phiphi_jcstt,\
                                                                                                euc_coef_flu2_x__x_jt,\
                                                                                                euc_coef_flu2__tt_jrhorho,\
                                                                                                euc_coef_flu2__phiphi_jcstt,\
                                                                                                hyp_coef_flu3_x__x_jt,\
                                                                                                hyp_coef_flu3__tt_jrhorho,\
                                                                                                hyp_coef_flu3__phiphi_jcstt,\
                                                                                                euc_coef_flu3_x__x_jt,\
                                                                                                euc_coef_flu3__tt_jrhorho,\
                                                                                                euc_coef_flu3__phiphi_jcstt,\
                )

                ################################################################
                if nb_of_fluids==3:
            
                    gamma___1__j_relation,gamma___2__j_relation,gamma___3__j_relation=three_fluids__gammas_and_gamma_relation___vfunc(g_n_t__t_jX,\
                                                                                                g_n__rho2_jX2,\
                                                                                                g__v_jn__phiphi,\
                                                                                                g_v_t__t_jX,\
                                                                                                g_v__rho2_jX2,\
                                                                                                meshed__cos_jsin__theta,\
                                                                                                sqrt_lambda__pl_jmin,\
                                                                                                G__t_t_jA,\
                                                                                                G__t__x_jrho,\
                                                                                                G__x__x_jrho,\
                                                                                                G_x__rho2_jphi2,\
                                                                                                hyp_coef_flu1_x__x_jt,\
                                                                                                hyp_coef_flu1__tt_jrhorho,\
                                                                                                hyp_coef_flu1__phiphi_jcstt,\
                                                                                                euc_coef_flu1_x__x_jt,\
                                                                                                euc_coef_flu1__tt_jrhorho,\
                                                                                                euc_coef_flu1__phiphi_jcstt,\
                                                                                                hyp_coef_flu2_x__x_jt,\
                                                                                                hyp_coef_flu2__tt_jrhorho,\
                                                                                                hyp_coef_flu2__phiphi_jcstt,\
                                                                                                euc_coef_flu2_x__x_jt,\
                                                                                                euc_coef_flu2__tt_jrhorho,\
                                                                                                euc_coef_flu2__phiphi_jcstt,\
                                                                                                hyp_coef_flu3_x__x_jt,\
                                                                                                hyp_coef_flu3__tt_jrhorho,\
                                                                                                hyp_coef_flu3__phiphi_jcstt,\
                                                                                                euc_coef_flu3_x__x_jt,\
                                                                                                euc_coef_flu3__tt_jrhorho,\
                                                                                                euc_coef_flu3__phiphi_jcstt,\
                    )
                    
                    ###############################################################

                    current_EOS,current_NEC__1j_current_min_rank=three_fluids__EOS_NEC_RANK___vfunc(g_n_t__t_jX,\
                                                                                                g_n__rho2_jX2,\
                                                                                                g__v_jn__phiphi,\
                                                                                                g_v_t__t_jX,\
                                                                                                g_v__rho2_jX2,\
                                                                                                meshed__cos_jsin__theta,\
                                                                                                sqrt_lambda__pl_jmin,\
                                                                                                G__t_t_jA,\
                                                                                                G__t__x_jrho,\
                                                                                                G__x__x_jrho,\
                                                                                                G_x__rho2_jphi2,\
                                                                                                hyp_coef_flu1_x__x_jt,\
                                                                                                hyp_coef_flu1__tt_jrhorho,\
                                                                                                hyp_coef_flu1__phiphi_jcstt,\
                                                                                                euc_coef_flu1_x__x_jt,\
                                                                                                euc_coef_flu1__tt_jrhorho,\
                                                                                                euc_coef_flu1__phiphi_jcstt,\
                                                                                                hyp_coef_flu2_x__x_jt,\
                                                                                                hyp_coef_flu2__tt_jrhorho,\
                                                                                                hyp_coef_flu2__phiphi_jcstt,\
                                                                                                euc_coef_flu2_x__x_jt,\
                                                                                                euc_coef_flu2__tt_jrhorho,\
                                                                                                euc_coef_flu2__phiphi_jcstt,\
                                                                                                hyp_coef_flu3_x__x_jt,\
                                                                                                hyp_coef_flu3__tt_jrhorho,\
                                                                                                hyp_coef_flu3__phiphi_jcstt,\
                                                                                                euc_coef_flu3_x__x_jt,\
                                                                                                euc_coef_flu3__tt_jrhorho,\
                                                                                                euc_coef_flu3__phiphi_jcstt,\
                                                                                                res_batch__1j_LAMBDA,\
                    )
                    #################################################################################################################

                    p_rho__p_phi_1,\
                        p_rho__p_phi_2,\
                        p_rho__p_phi_3=three_fluids__element_wise__compute_p_rho__p_phi_pressures__output_them___vfunc(g_n_t__t_jX,\
                                                                                                         g_n__rho2_jX2,\
                                                                                                         g__v_jn__phiphi,\
                                                                                                         g_v_t__t_jX,\
                                                                                                         g_v__rho2_jX2,\
                                                                                                         meshed__cos_jsin__theta,\
                                                                                                         sqrt_lambda__pl_jmin,\
                                                                                                         G__t_t_jA,\
                                                                                                         G__t__x_jrho,\
                                                                                                         G__x__x_jrho,\
                                                                                                         G_x__rho2_jphi2,\
                                                                                                         hyp_coef_flu1_x__x_jt,\
                                                                                                         hyp_coef_flu1__tt_jrhorho,\
                                                                                                         hyp_coef_flu1__phiphi_jcstt,\
                                                                                                         euc_coef_flu1_x__x_jt,\
                                                                                                         euc_coef_flu1__tt_jrhorho,\
                                                                                                         euc_coef_flu1__phiphi_jcstt,\
                                                                                                         hyp_coef_flu2_x__x_jt,\
                                                                                                         hyp_coef_flu2__tt_jrhorho,\
                                                                                                         hyp_coef_flu2__phiphi_jcstt,\
                                                                                                         euc_coef_flu2_x__x_jt,\
                                                                                                         euc_coef_flu2__tt_jrhorho,\
                                                                                                         euc_coef_flu2__phiphi_jcstt,\
                                                                                                         hyp_coef_flu3_x__x_jt,\
                                                                                                         hyp_coef_flu3__tt_jrhorho,\
                                                                                                         hyp_coef_flu3__phiphi_jcstt,\
                                                                                                         euc_coef_flu3_x__x_jt,\
                                                                                                         euc_coef_flu3__tt_jrhorho,\
                                                                                                         euc_coef_flu3__phiphi_jcstt,\
                    )




                    
                    rho__p_x_1,\
                        rho__p_x_2,\
                        rho__p_x_3=three_fluids__element_wise__compute__pressures_pX__densities__output_them___vfunc(g_n_t__t_jX,\
                                                                                                       g_n__rho2_jX2,\
                                                                                                        g__v_jn__phiphi,\
                                                                                                       g_v_t__t_jX,\
                                                                                                       g_v__rho2_jX2,\
                                                                                                       meshed__cos_jsin__theta,\
                                                                                                       sqrt_lambda__pl_jmin,\
                                                                                                       G__t_t_jA,\
                                                                                                       G__t__x_jrho,\
                                                                                                       G__x__x_jrho,\
                                                                                                       G_x__rho2_jphi2,\
                                                                                                       hyp_coef_flu1_x__x_jt,\
                                                                                                       hyp_coef_flu1__tt_jrhorho,\
                                                                                                       hyp_coef_flu1__phiphi_jcstt,\
                                                                                                       euc_coef_flu1_x__x_jt,\
                                                                                                       euc_coef_flu1__tt_jrhorho,\
                                                                                                       euc_coef_flu1__phiphi_jcstt,\
                                                                                                       hyp_coef_flu2_x__x_jt,\
                                                                                                       hyp_coef_flu2__tt_jrhorho,\
                                                                                                       hyp_coef_flu2__phiphi_jcstt,\
                                                                                                       euc_coef_flu2_x__x_jt,\
                                                                                                       euc_coef_flu2__tt_jrhorho,\
                                                                                                       euc_coef_flu2__phiphi_jcstt,\
                                                                                                       hyp_coef_flu3_x__x_jt,\
                                                                                                       hyp_coef_flu3__tt_jrhorho,\
                                                                                                       hyp_coef_flu3__phiphi_jcstt,\
                                                                                                       euc_coef_flu3_x__x_jt,\
                                                                                                       euc_coef_flu3__tt_jrhorho,\
                                                                                                       euc_coef_flu3__phiphi_jcstt,\
                    )





                    



                    norm2_rel__p1j__sing_val_mat_0,\
                        b__ranks__p1j__sing_val_mat_6,\
                        b__sing_val__p1j__sing_val_mat__min=three_fluids__element_wise__compute_relat_error__norm2__output_them___vfunc(g_n_t__t_jX,\
                                                                                                                                        g_n__rho2_jX2,\
                                                                                                             g__v_jn__phiphi,\
                                                                                                             g_v_t__t_jX,\
                                                                                                             g_v__rho2_jX2,\
                                                                                                             meshed__cos_jsin__theta,\
                                                                                                             sqrt_lambda__pl_jmin,\
                                                                                                             G__t_t_jA,\
                                                                                                             G__t__x_jrho,\
                                                                                                             G__x__x_jrho,\
                                                                                                             G_x__rho2_jphi2,\
                                                                                                             hyp_coef_flu1_x__x_jt,\
                                                                                                             hyp_coef_flu1__tt_jrhorho,\
                                                                                                             hyp_coef_flu1__phiphi_jcstt,\
                                                                                                             euc_coef_flu1_x__x_jt,\
                                                                                                             euc_coef_flu1__tt_jrhorho,\
                                                                                                             euc_coef_flu1__phiphi_jcstt,\
                                                                                                             hyp_coef_flu2_x__x_jt,\
                                                                                                             hyp_coef_flu2__tt_jrhorho,\
                                                                                                             hyp_coef_flu2__phiphi_jcstt,\
                                                                                                             euc_coef_flu2_x__x_jt,\
                                                                                                             euc_coef_flu2__tt_jrhorho,\
                                                                                                             euc_coef_flu2__phiphi_jcstt,\
                                                                                                             hyp_coef_flu3_x__x_jt,\
                                                                                                             hyp_coef_flu3__tt_jrhorho,\
                                                                                                             hyp_coef_flu3__phiphi_jcstt,\
                                                                                                             euc_coef_flu3_x__x_jt,\
                                                                                                             euc_coef_flu3__tt_jrhorho,\
                                                                                                             euc_coef_flu3__phiphi_jcstt,\
                    )


                    
                    diff_over_sum__p1j__sing_val_mat_0,\
                        ranks__p1j__sing_val_mat_6,\
                        sing_val__p1j__sing_val_mat__min=three_fluids__element_wise__compute_relat_error__dif_over_sum__output_them___vfunc(g_n_t__t_jX,\
                                                                                                             g_n__rho2_jX2,\
                                                                                                             g__v_jn__phiphi,\
                                                                                                             g_v_t__t_jX,\
                                                                                                             g_v__rho2_jX2,\
                                                                                                             meshed__cos_jsin__theta,\
                                                                                                             sqrt_lambda__pl_jmin,\
                                                                                                             G__t_t_jA,\
                                                                                                             G__t__x_jrho,\
                                                                                                             G__x__x_jrho,\
                                                                                                             G_x__rho2_jphi2,\
                                                                                                             hyp_coef_flu1_x__x_jt,\
                                                                                                             hyp_coef_flu1__tt_jrhorho,\
                                                                                                             hyp_coef_flu1__phiphi_jcstt,\
                                                                                                             euc_coef_flu1_x__x_jt,\
                                                                                                             euc_coef_flu1__tt_jrhorho,\
                                                                                                             euc_coef_flu1__phiphi_jcstt,\
                                                                                                             hyp_coef_flu2_x__x_jt,\
                                                                                                             hyp_coef_flu2__tt_jrhorho,\
                                                                                                             hyp_coef_flu2__phiphi_jcstt,\
                                                                                                             euc_coef_flu2_x__x_jt,\
                                                                                                             euc_coef_flu2__tt_jrhorho,\
                                                                                                             euc_coef_flu2__phiphi_jcstt,\
                                                                                                             hyp_coef_flu3_x__x_jt,\
                                                                                                             hyp_coef_flu3__tt_jrhorho,\
                                                                                                             hyp_coef_flu3__phiphi_jcstt,\
                                                                                                             euc_coef_flu3_x__x_jt,\
                                                                                                             euc_coef_flu3__tt_jrhorho,\
                                                                                                             euc_coef_flu3__phiphi_jcstt,\
                    )



                    sin_theta_errors,diff_over_sum__modeled,ranks2=three_fluids__element_wise__analyse__dif_over_sum__einst__tensor___vfunc(g_n_t__t_jX,\
                                                                                                             g_n__rho2_jX2,\
                                                                                                             g__v_jn__phiphi,\
                                                                                                             g_v_t__t_jX,\
                                                                                                             g_v__rho2_jX2,\
                                                                                                             meshed__cos_jsin__theta,\
                                                                                                             sqrt_lambda__pl_jmin,\
                                                                                                             G__t_t_jA,\
                                                                                                             G__t__x_jrho,\
                                                                                                             G__x__x_jrho,\
                                                                                                             G_x__rho2_jphi2,\
                                                                                                             hyp_coef_flu1_x__x_jt,\
                                                                                                             hyp_coef_flu1__tt_jrhorho,\
                                                                                                             hyp_coef_flu1__phiphi_jcstt,\
                                                                                                             euc_coef_flu1_x__x_jt,\
                                                                                                             euc_coef_flu1__tt_jrhorho,\
                                                                                                             euc_coef_flu1__phiphi_jcstt,\
                                                                                                             hyp_coef_flu2_x__x_jt,\
                                                                                                             hyp_coef_flu2__tt_jrhorho,\
                                                                                                             hyp_coef_flu2__phiphi_jcstt,\
                                                                                                             euc_coef_flu2_x__x_jt,\
                                                                                                             euc_coef_flu2__tt_jrhorho,\
                                                                                                             euc_coef_flu2__phiphi_jcstt,\
                                                                                                             hyp_coef_flu3_x__x_jt,\
                                                                                                             hyp_coef_flu3__tt_jrhorho,\
                                                                                                             hyp_coef_flu3__phiphi_jcstt,\
                                                                                                             euc_coef_flu3_x__x_jt,\
                                                                                                             euc_coef_flu3__tt_jrhorho,\
                                                                                                             euc_coef_flu3__phiphi_jcstt,\
                    )







                    max_abs_u_squared_plus_one,max_abs_of_velocities_components=three_fluids__extract_velocity_unitarity___vfunc(g_n_t__t_jX,\
                                                                                                  g_n__rho2_jX2,\
                                                                                                  g__v_jn__phiphi,\
                                                                                                  g_v_t__t_jX,\
                                                                                                  g_v__rho2_jX2,\
                                                                                                  meshed__cos_jsin__theta,\
                                                                                                  sqrt_lambda__pl_jmin,\
                                                                                                  G__t_t_jA,\
                                                                                                  G__t__x_jrho,\
                                                                                                  G__x__x_jrho,\
                                                                                                  G_x__rho2_jphi2,\
                                                                                                  hyp_coef_flu1_x__x_jt,\
                                                                                                  hyp_coef_flu1__tt_jrhorho,\
                                                                                                  hyp_coef_flu1__phiphi_jcstt,\
                                                                                                  euc_coef_flu1_x__x_jt,\
                                                                                                  euc_coef_flu1__tt_jrhorho,\
                                                                                                  euc_coef_flu1__phiphi_jcstt,\
                                                                                                  hyp_coef_flu2_x__x_jt,\
                                                                                                  hyp_coef_flu2__tt_jrhorho,\
                                                                                                  hyp_coef_flu2__phiphi_jcstt,\
                                                                                                  euc_coef_flu2_x__x_jt,\
                                                                                                  euc_coef_flu2__tt_jrhorho,\
                                                                                                  euc_coef_flu2__phiphi_jcstt,\
                                                                                                  hyp_coef_flu3_x__x_jt,\
                                                                                                  hyp_coef_flu3__tt_jrhorho,\
                                                                                                  hyp_coef_flu3__phiphi_jcstt,\
                                                                                                  euc_coef_flu3_x__x_jt,\
                                                                                                  euc_coef_flu3__tt_jrhorho,\
                                                                                                  euc_coef_flu3__phiphi_jcstt,\
                    )

                    diff_over_sum=diff_over_sum__p1j__sing_val_mat_0.real
                    ranks=ranks__p1j__sing_val_mat_6.real
                    sing_val=sing_val__p1j__sing_val_mat__min.real

                    sing_val_mat_0=diff_over_sum__p1j__sing_val_mat_0.imag
                    sing_val_mat_6=ranks__p1j__sing_val_mat_6.imag
                    sing_val_mat__min=sing_val__p1j__sing_val_mat__min.imag



                    
                    

                    output_dict["p_rho__p_phi_3"]=p_rho__p_phi_3
                    output_dict["rho__p_x_3"]=rho__p_x_3

                    output_dict["gamma___1__j_relation"]=gamma___1__j_relation
                    output_dict["gamma___2__j_relation"]=gamma___2__j_relation
                    output_dict["gamma___3__j_relation"]=gamma___3__j_relation
                    output_dict["diff_over_sum__modeled"]=diff_over_sum__modeled

                    ##################################################################################################################
                elif nb_of_fluids==2:


                    gamma___1__j_relation,gamma___2__j_relation=two_fluids__gammas_and_gamma_relation___vfunc(g_n_t__t_jX,\
                                                                                                g_n__rho2_jX2,\
                                                                                                g__v_jn__phiphi,\
                                                                                                g_v_t__t_jX,\
                                                                                                g_v__rho2_jX2,\
                                                                                                meshed__cos_jsin__theta,\
                                                                                                sqrt_lambda__pl_jmin,\
                                                                                                G__t_t_jA,\
                                                                                                G__t__x_jrho,\
                                                                                                G__x__x_jrho,\
                                                                                                G_x__rho2_jphi2,\
                                                                                                hyp_coef_flu1_x__x_jt,\
                                                                                                hyp_coef_flu1__tt_jrhorho,\
                                                                                                hyp_coef_flu1__phiphi_jcstt,\
                                                                                                euc_coef_flu1_x__x_jt,\
                                                                                                euc_coef_flu1__tt_jrhorho,\
                                                                                                euc_coef_flu1__phiphi_jcstt,\
                                                                                                hyp_coef_flu2_x__x_jt,\
                                                                                                hyp_coef_flu2__tt_jrhorho,\
                                                                                                hyp_coef_flu2__phiphi_jcstt,\
                                                                                                euc_coef_flu2_x__x_jt,\
                                                                                                euc_coef_flu2__tt_jrhorho,\
                                                                                                euc_coef_flu2__phiphi_jcstt,\
                                                                                                hyp_coef_flu3_x__x_jt,\
                                                                                                hyp_coef_flu3__tt_jrhorho,\
                                                                                                hyp_coef_flu3__phiphi_jcstt,\
                                                                                                euc_coef_flu3_x__x_jt,\
                                                                                                euc_coef_flu3__tt_jrhorho,\
                                                                                                euc_coef_flu3__phiphi_jcstt,\
                    )

                    #################################################################

                    current_EOS,current_NEC__1j_current_min_rank=two_fluids__EOS_NEC_RANK___vfunc(g_n_t__t_jX,\
                                                                                                    g_n__rho2_jX2,\
                                                                                                    g__v_jn__phiphi,\
                                                                                                    g_v_t__t_jX,\
                                                                                                    g_v__rho2_jX2,\
                                                                                                    meshed__cos_jsin__theta,\
                                                                                                    sqrt_lambda__pl_jmin,\
                                                                                                    G__t_t_jA,\
                                                                                                    G__t__x_jrho,\
                                                                                                    G__x__x_jrho,\
                                                                                                    G_x__rho2_jphi2,\
                                                                                                    hyp_coef_flu1_x__x_jt,\
                                                                                                    hyp_coef_flu1__tt_jrhorho,\
                                                                                                    hyp_coef_flu1__phiphi_jcstt,\
                                                                                                    euc_coef_flu1_x__x_jt,\
                                                                                                    euc_coef_flu1__tt_jrhorho,\
                                                                                                    euc_coef_flu1__phiphi_jcstt,\
                                                                                                    hyp_coef_flu2_x__x_jt,\
                                                                                                    hyp_coef_flu2__tt_jrhorho,\
                                                                                                    hyp_coef_flu2__phiphi_jcstt,\
                                                                                                    euc_coef_flu2_x__x_jt,\
                                                                                                    euc_coef_flu2__tt_jrhorho,\
                                                                                                    euc_coef_flu2__phiphi_jcstt,\
                                                                                                    hyp_coef_flu3_x__x_jt,\
                                                                                                    hyp_coef_flu3__tt_jrhorho,\
                                                                                                    hyp_coef_flu3__phiphi_jcstt,\
                                                                                                    euc_coef_flu3_x__x_jt,\
                                                                                                    euc_coef_flu3__tt_jrhorho,\
                                                                                                    euc_coef_flu3__phiphi_jcstt,\
                                                                                                    res_batch__1j_LAMBDA,\
                    )

                    p_rho__p_phi_1,\
                        p_rho__p_phi_2=two_fluids____element_wise__compute_p_rho__p_phi_pressures__output_them___vfunc(g_n_t__t_jX,\
                                                                                                    g_n__rho2_jX2,\
                                                                                                    g__v_jn__phiphi,\
                                                                                                    g_v_t__t_jX,\
                                                                                                    g_v__rho2_jX2,\
                                                                                                    meshed__cos_jsin__theta,\
                                                                                                    sqrt_lambda__pl_jmin,\
                                                                                                    G__t_t_jA,\
                                                                                                    G__t__x_jrho,\
                                                                                                    G__x__x_jrho,\
                                                                                                    G_x__rho2_jphi2,\
                                                                                                    hyp_coef_flu1_x__x_jt,\
                                                                                                    hyp_coef_flu1__tt_jrhorho,\
                                                                                                    hyp_coef_flu1__phiphi_jcstt,\
                                                                                                    euc_coef_flu1_x__x_jt,\
                                                                                                    euc_coef_flu1__tt_jrhorho,\
                                                                                                    euc_coef_flu1__phiphi_jcstt,\
                                                                                                    hyp_coef_flu2_x__x_jt,\
                                                                                                    hyp_coef_flu2__tt_jrhorho,\
                                                                                                    hyp_coef_flu2__phiphi_jcstt,\
                                                                                                    euc_coef_flu2_x__x_jt,\
                                                                                                    euc_coef_flu2__tt_jrhorho,\
                                                                                                    euc_coef_flu2__phiphi_jcstt,\
                                                                                                    hyp_coef_flu3_x__x_jt,\
                                                                                                    hyp_coef_flu3__tt_jrhorho,\
                                                                                                    hyp_coef_flu3__phiphi_jcstt,\
                                                                                                    euc_coef_flu3_x__x_jt,\
                                                                                                    euc_coef_flu3__tt_jrhorho,\
                                                                                                    euc_coef_flu3__phiphi_jcstt,\
                    )

                    rho__p_x_1,\
                        rho__p_x_2=two_fluids__element_wise__compute__pressures_pX__densities__output_them___vfunc(g_n_t__t_jX,\
                                                                                                    g_n__rho2_jX2,\
                                                                                                    g__v_jn__phiphi,\
                                                                                                    g_v_t__t_jX,\
                                                                                                    g_v__rho2_jX2,\
                                                                                                    meshed__cos_jsin__theta,\
                                                                                                    sqrt_lambda__pl_jmin,\
                                                                                                    G__t_t_jA,\
                                                                                                    G__t__x_jrho,\
                                                                                                    G__x__x_jrho,\
                                                                                                    G_x__rho2_jphi2,\
                                                                                                    hyp_coef_flu1_x__x_jt,\
                                                                                                    hyp_coef_flu1__tt_jrhorho,\
                                                                                                    hyp_coef_flu1__phiphi_jcstt,\
                                                                                                    euc_coef_flu1_x__x_jt,\
                                                                                                    euc_coef_flu1__tt_jrhorho,\
                                                                                                    euc_coef_flu1__phiphi_jcstt,\
                                                                                                    hyp_coef_flu2_x__x_jt,\
                                                                                                    hyp_coef_flu2__tt_jrhorho,\
                                                                                                    hyp_coef_flu2__phiphi_jcstt,\
                                                                                                    euc_coef_flu2_x__x_jt,\
                                                                                                    euc_coef_flu2__tt_jrhorho,\
                                                                                                    euc_coef_flu2__phiphi_jcstt,\
                                                                                                    hyp_coef_flu3_x__x_jt,\
                                                                                                    hyp_coef_flu3__tt_jrhorho,\
                                                                                                    hyp_coef_flu3__phiphi_jcstt,\
                                                                                                    euc_coef_flu3_x__x_jt,\
                                                                                                    euc_coef_flu3__tt_jrhorho,\
                                                                                                    euc_coef_flu3__phiphi_jcstt,\
                    )

                    norm2_rel__p1j__sing_val_mat_0,\
                        b_ranks__p1j__sing_val_mat_6,\
                        b_sing_val__p1j__sing_val_mat__min=two_fluids__element_wise__compute_relat_error__norm2__output_them___vfunc(g_n_t__t_jX,\
                                                                                                    g_n__rho2_jX2,\
                                                                                                    g__v_jn__phiphi,\
                                                                                                    g_v_t__t_jX,\
                                                                                                    g_v__rho2_jX2,\
                                                                                                    meshed__cos_jsin__theta,\
                                                                                                    sqrt_lambda__pl_jmin,\
                                                                                                    G__t_t_jA,\
                                                                                                    G__t__x_jrho,\
                                                                                                    G__x__x_jrho,\
                                                                                                    G_x__rho2_jphi2,\
                                                                                                    hyp_coef_flu1_x__x_jt,\
                                                                                                    hyp_coef_flu1__tt_jrhorho,\
                                                                                                    hyp_coef_flu1__phiphi_jcstt,\
                                                                                                    euc_coef_flu1_x__x_jt,\
                                                                                                    euc_coef_flu1__tt_jrhorho,\
                                                                                                    euc_coef_flu1__phiphi_jcstt,\
                                                                                                    hyp_coef_flu2_x__x_jt,\
                                                                                                    hyp_coef_flu2__tt_jrhorho,\
                                                                                                    hyp_coef_flu2__phiphi_jcstt,\
                                                                                                    euc_coef_flu2_x__x_jt,\
                                                                                                    euc_coef_flu2__tt_jrhorho,\
                                                                                                    euc_coef_flu2__phiphi_jcstt,\
                                                                                                    hyp_coef_flu3_x__x_jt,\
                                                                                                    hyp_coef_flu3__tt_jrhorho,\
                                                                                                    hyp_coef_flu3__phiphi_jcstt,\
                                                                                                    euc_coef_flu3_x__x_jt,\
                                                                                                    euc_coef_flu3__tt_jrhorho,\
                                                                                                    euc_coef_flu3__phiphi_jcstt,\
                    )



                    diff_over_sum__p1j__sing_val_mat_0,\
                        ranks__p1j__sing_val_mat_6,\
                        sing_val__p1j__sing_val_mat__min=two_fluids__element_wise__compute_relat_error__dif_over_sum__output_them___vfunc(g_n_t__t_jX,\
                                                                                                    g_n__rho2_jX2,\
                                                                                                    g__v_jn__phiphi,\
                                                                                                    g_v_t__t_jX,\
                                                                                                    g_v__rho2_jX2,\
                                                                                                    meshed__cos_jsin__theta,\
                                                                                                    sqrt_lambda__pl_jmin,\
                                                                                                    G__t_t_jA,\
                                                                                                    G__t__x_jrho,\
                                                                                                    G__x__x_jrho,\
                                                                                                    G_x__rho2_jphi2,\
                                                                                                    hyp_coef_flu1_x__x_jt,\
                                                                                                    hyp_coef_flu1__tt_jrhorho,\
                                                                                                    hyp_coef_flu1__phiphi_jcstt,\
                                                                                                    euc_coef_flu1_x__x_jt,\
                                                                                                    euc_coef_flu1__tt_jrhorho,\
                                                                                                    euc_coef_flu1__phiphi_jcstt,\
                                                                                                    hyp_coef_flu2_x__x_jt,\
                                                                                                    hyp_coef_flu2__tt_jrhorho,\
                                                                                                    hyp_coef_flu2__phiphi_jcstt,\
                                                                                                    euc_coef_flu2_x__x_jt,\
                                                                                                    euc_coef_flu2__tt_jrhorho,\
                                                                                                    euc_coef_flu2__phiphi_jcstt,\
                                                                                                    hyp_coef_flu3_x__x_jt,\
                                                                                                    hyp_coef_flu3__tt_jrhorho,\
                                                                                                    hyp_coef_flu3__phiphi_jcstt,\
                                                                                                    euc_coef_flu3_x__x_jt,\
                                                                                                    euc_coef_flu3__tt_jrhorho,\
                                                                                                    euc_coef_flu3__phiphi_jcstt,\
                    )
                    
                    



                    diff_over_sum=diff_over_sum__p1j__sing_val_mat_0.real
                    ranks=ranks__p1j__sing_val_mat_6.real
                    sing_val=sing_val__p1j__sing_val_mat__min.real

                    sing_val_mat_0=diff_over_sum__p1j__sing_val_mat_0.imag
                    sing_val_mat_6=ranks__p1j__sing_val_mat_6.imag
                    sing_val_mat__min=sing_val__p1j__sing_val_mat__min.imag




                    sin_theta_errors,diff_over_sum__modeled,ranks2=two_fluids__element_wise__analyse__dif_over_sum__einst__tensor___vfunc(g_n_t__t_jX,\
                                                                                                                                           g_n__rho2_jX2,\
                                                                                                                                           g__v_jn__phiphi,\
                                                                                                                                           g_v_t__t_jX,\
                                                                                                                                           g_v__rho2_jX2,\
                                                                                                                                           meshed__cos_jsin__theta,\
                                                                                                                                           sqrt_lambda__pl_jmin,\
                                                                                                                                           G__t_t_jA,\
                                                                                                                                           G__t__x_jrho,\
                                                                                                                                           G__x__x_jrho,\
                                                                                                                                           G_x__rho2_jphi2,\
                                                                                                                                           hyp_coef_flu1_x__x_jt,\
                                                                                                                                           hyp_coef_flu1__tt_jrhorho,\
                                                                                                                                           hyp_coef_flu1__phiphi_jcstt,\
                                                                                                                                           euc_coef_flu1_x__x_jt,\
                                                                                                                                           euc_coef_flu1__tt_jrhorho,\
                                                                                                                                           euc_coef_flu1__phiphi_jcstt,\
                                                                                                                                           hyp_coef_flu2_x__x_jt,\
                                                                                                                                           hyp_coef_flu2__tt_jrhorho,\
                                                                                                                                           hyp_coef_flu2__phiphi_jcstt,\
                                                                                                                                           euc_coef_flu2_x__x_jt,\
                                                                                                                                           euc_coef_flu2__tt_jrhorho,\
                                                                                                                                           euc_coef_flu2__phiphi_jcstt,\
                                                                                                                                           hyp_coef_flu3_x__x_jt,\
                                                                                                                                           hyp_coef_flu3__tt_jrhorho,\
                                                                                                                                           hyp_coef_flu3__phiphi_jcstt,\
                                                                                                                                           euc_coef_flu3_x__x_jt,\
                                                                                                                                           euc_coef_flu3__tt_jrhorho,\
                                                                                                                                           euc_coef_flu3__phiphi_jcstt,\
                    )


            

                    max_abs_u_squared_plus_one,max_abs_of_velocities_components=two_fluids__extract_velocity_unitarity___vfunc(g_n_t__t_jX,\
                                                                                                    g_n__rho2_jX2,\
                                                                                                    g__v_jn__phiphi,\
                                                                                                    g_v_t__t_jX,\
                                                                                                    g_v__rho2_jX2,\
                                                                                                    meshed__cos_jsin__theta,\
                                                                                                    sqrt_lambda__pl_jmin,\
                                                                                                    G__t_t_jA,\
                                                                                                    G__t__x_jrho,\
                                                                                                    G__x__x_jrho,\
                                                                                                    G_x__rho2_jphi2,\
                                                                                                    hyp_coef_flu1_x__x_jt,\
                                                                                                    hyp_coef_flu1__tt_jrhorho,\
                                                                                                    hyp_coef_flu1__phiphi_jcstt,\
                                                                                                    euc_coef_flu1_x__x_jt,\
                                                                                                    euc_coef_flu1__tt_jrhorho,\
                                                                                                    euc_coef_flu1__phiphi_jcstt,\
                                                                                                    hyp_coef_flu2_x__x_jt,\
                                                                                                    hyp_coef_flu2__tt_jrhorho,\
                                                                                                    hyp_coef_flu2__phiphi_jcstt,\
                                                                                                    euc_coef_flu2_x__x_jt,\
                                                                                                    euc_coef_flu2__tt_jrhorho,\
                                                                                                    euc_coef_flu2__phiphi_jcstt,\
                                                                                                    hyp_coef_flu3_x__x_jt,\
                                                                                                    hyp_coef_flu3__tt_jrhorho,\
                                                                                                    hyp_coef_flu3__phiphi_jcstt,\
                                                                                                    euc_coef_flu3_x__x_jt,\
                                                                                                    euc_coef_flu3__tt_jrhorho,\
                                                                                                    euc_coef_flu3__phiphi_jcstt,\
                    )




                ##############################################
                output_dict["r_mesh__one_theta_real"]=r_mesh__one_theta_real
                output_dict["theta_real_mesh"]=theta_real_mesh
    
                #################################################
                    

                output_dict["sing_val"]=sing_val


                output_dict["sing_val_mat_0"]=sing_val_mat_0
                output_dict["sing_val_mat_6"]=sing_val_mat_6
                output_dict["sing_val_mat__min"]=sing_val_mat__min

                
                    
                output_dict["gamma___1__j_relation"]=gamma___1__j_relation
                output_dict["gamma___2__j_relation"]=gamma___2__j_relation
                
                output_dict["p_rho__p_phi_1"]=p_rho__p_phi_1
                output_dict["p_rho__p_phi_2"]=p_rho__p_phi_2
                
                output_dict["rho__p_x_1"]=rho__p_x_1
                output_dict["rho__p_x_2"]=rho__p_x_2
                
                output_dict["diff_over_sum"]=diff_over_sum
                output_dict["ranks"]=ranks
                output_dict["max_abs_u_squared_plus_one"]=max_abs_u_squared_plus_one
                output_dict["max_abs_of_velocities_components"]=max_abs_of_velocities_components
                output_dict["nb_of_fluids"]=nb_of_fluids
                output_dict["sin_theta_errors"]=sin_theta_errors

                 
    output_dict["current_EOS_map"]=current_EOS.real
    output_dict["current_NEC_map"]=current_NEC__1j_current_min_rank.real
    output_dict["einst_mixed_largest_value__map"]=einst_mixed_largest_value


    output_dict["einstein_tens_largest__map"]=maximum_absolute_value_of_einstein_tensor_components___vfunc(G__t_t_jA.real,\
                                                                                                           G__t__x_jrho.real,\
                                                                                                           G__t__x_jrho.imag,\
                                                                                                           G__x__x_jrho.real,\
                                                                                                           G__x__x_jrho.imag,\
                                                                                                           G_x__rho2_jphi2.real,\
                                                                                                           G_x__rho2_jphi2.imag,\
    )

    
    
    
    
    current_EOS=np.max(current_EOS.real)
    current_NEC=np.min(current_NEC__1j_current_min_rank.real)
    current_min_rank=np.min(current_NEC__1j_current_min_rank.imag)


    output_dict["current_EOS"]=current_EOS
    output_dict["current_NEC"]=current_NEC
    output_dict["current_min_rank"]=current_min_rank
    output_dict['small_volumes']=small_volumes
    output_dict['very_small_volumes']=very_small_volumes

    output_dict['norm2_rel__p1j__sing_val_mat_0']=norm2_rel__p1j__sing_val_mat_0
    return output_dict

###########################################################################################################################
#######################################################################
        
####################################################################
if __name__ == "__main__":

    ###########################################################################################

    #parameters sets to be explored are concentrated in a file, we need to explore them now
    
    #current working directory (CWD)
    cwd = os.getcwd()
    
    timestamp_now_current=str(datetime.datetime.now())


    #the input comes from this below
    complete_path_filename=cwd+'/'+"OUTPUTS--of--analyze__drop_duplicates/"
    list_of_filenames=os.listdir(complete_path_filename)
    print(list_of_filenames)
    print("list_of_filenames \n line 7453 \n --------- \n ")
    
    
    list_of_filenames=[el for el in list_of_filenames if "gitignore" not in el.lower()]
    complete_path_filename+=list_of_filenames[0]

    print(complete_path_filename)
    print("complete_path_filename \n line 10302")
    
    with open(complete_path_filename, 'r') as fd:
        list_of_dicts_reloaded2=json.load(fd)

    print(len(list_of_dicts_reloaded2))
    print("len(list_of_dicts_reloaded2) \n line 11837")
    
    ####################################################################
    ####################################################################
    print('reloaded!! \n line 10576')
    
    #############################################
    # now below we check what is the best model
    #but before we need to make a choice between the number of fluid and acceleration values /// and VECTORIZE functions

    two_fluids__element_wise__analyse__dif_over_sum__einst__tensor___vfunc=np.vectorize(two_fluids__element_wise__analyse__dif_over_sum__einst__tensor___myfunc)


    any_nb_of_fluids__einst_mixed_largest_value___vfunc=np.vectorize(any_nb_of_fluids__einst_mixed_largest_value___myfunc)
    
    compute_inv_or_zero___vfunc=np.vectorize(compute_inv_or_zero___myfunc)
    compute_each_space_box_minimum_nec___vfunc=np.vectorize(compute_each_space_box_minimum_nec___myfunc)
    maximum_absolute_value_of_einstein_tensor_components___vfunc=np.vectorize(maximum_absolute_value_of_einstein_tensor_components___myfunc)

    two_fluids__gammas_and_gamma_relation___vfunc=np.vectorize(two_fluids__gammas_and_gamma_relation___myfunc)
    two_fluids__extract_velocity_unitarity___vfunc=np.vectorize(two_fluids__extract_velocity_unitarity___myfunc)

    two_fluids__element_wise__compute_relat_error__norm2__output_them___vfunc=np.vectorize(two_fluids__element_wise__compute_relat_error__norm2__output_them___myfunc)
    three_fluids__element_wise__compute_relat_error__norm2__output_them___vfunc=np.vectorize(three_fluids__element_wise__compute_relat_error__norm2__output_them___myfunc)
    
    two_fluids__element_wise__compute_relat_error__dif_over_sum__output_them___vfunc=np.vectorize(two_fluids__element_wise__compute_relat_error__dif_over_sum__output_them___myfunc)
    two_fluids__element_wise__compute__pressures_pX__densities__output_them___vfunc=np.vectorize(two_fluids__element_wise__compute__pressures_pX__densities__output_them___myfunc)
    two_fluids____element_wise__compute_p_rho__p_phi_pressures__output_them___vfunc=np.vectorize(two_fluids____element_wise__compute_p_rho__p_phi_pressures__output_them___myfunc)

    three_fluids__gammas_and_gamma_relation___vfunc=np.vectorize(three_fluids__gammas_and_gamma_relation___myfunc)
    three_fluids__extract_velocity_unitarity___vfunc=np.vectorize(three_fluids__extract_velocity_unitarity___myfunc)
    three_fluids__element_wise__compute_relat_error__dif_over_sum__output_them___vfunc=np.vectorize(three_fluids__element_wise__compute_relat_error__dif_over_sum__output_them___myfunc)
    three_fluids__element_wise__compute__pressures_pX__densities__output_them___vfunc=np.vectorize(three_fluids__element_wise__compute__pressures_pX__densities__output_them___myfunc)
    three_fluids__element_wise__compute_p_rho__p_phi_pressures__output_them___vfunc=np.vectorize(three_fluids__element_wise__compute_p_rho__p_phi_pressures__output_them___myfunc)


    three_fluids__element_wise__analyse__dif_over_sum__einst__tensor___vfunc=np.vectorize(three_fluids__element_wise__analyse__dif_over_sum__einst__tensor___myfunc)
    
    two_fluids__EOS_NEC_RANK___vfunc=np.vectorize(two_fluids__EOS_NEC_RANK___myfunc)
    three_fluids__EOS_NEC_RANK___vfunc=np.vectorize(three_fluids__EOS_NEC_RANK___myfunc)

    large_dict_of_best_dict={}
    complete_large_list_of_dicts=[]
    
    for abcd in range(len(list_of_dicts_reloaded2)):
        
        dict_reloaded=list_of_dicts_reloaded2[abcd]

        if 1:

            print('----------------------')
            print('############################')
            print('----------------------')
            
            print('####'+str(abcd)+'//'+str(len(list_of_dicts_reloaded2)-1)+'####')


            
            # each of these models should be tested , thoroughly

            v_t=dict_reloaded['warp_acceleration']
            print('-----------')
            print(v_t)
            print("v_t \n line 8583")
            print('-----------')
            v=dict_reloaded['warp_speed']

            Lambda_cosmological_constant=0
            #1.11e-52 # ceci est codé en dur en général pour le moment
            

            if "--2flu" in  dict_reloaded['metric-fun']:
                nb_of_fluids=2
            else:
                nb_of_fluids=3


            
            N__fluids_sol__output_dict=Nfluids_solution__check__EOS_NEC_RANK(dict_reloaded,v_t,v,Lambda_cosmological_constant,nb_of_fluids,\
                                                                                           two_fluids__EOS_NEC_RANK___vfunc,\
                                                                                           three_fluids__EOS_NEC_RANK___vfunc,\
            )
            current_EOS=N__fluids_sol__output_dict["current_EOS"]
            current_NEC=N__fluids_sol__output_dict["current_NEC"]
            current_min_rank=N__fluids_sol__output_dict['current_min_rank']
            print(' \n ----------- \n ')
            print(current_min_rank)
            print('current_min_rank \n line 7569 \n ----------- \n ')
    


            for key_retrieved in list(N__fluids_sol__output_dict.keys()):
                dict_reloaded[key_retrieved]=N__fluids_sol__output_dict[key_retrieved]

            for key in dict_reloaded.keys():
                if "current_EOS" in key or \
                   "current_NEC" in key or \
                   "current_min_rank" in key or\
                   "warp_acceleration" in key or \
                   "warp_speed" in key:
                    print(str(key)+':'+str(dict_reloaded[key]))

            print('line 7529')

            print(nb_of_fluids)
            print('nb_of fluids \n line 11419')
            print('----------------------')
            print(dict_reloaded["timestamp"])
            print('dict_reloaded["timestamp"] \n line 2965')
            print('----------------------')
            #for each of these dict we save the results to reload only the best results
            #create the key

            long_string_key='Lambda_cosmological_constant'+'___'+str(Lambda_cosmological_constant)
            long_string_key+='____'+'warp_speed'+'___'+str(v)
            long_string_key+='____'+'warp_acceleration'+'___'+str(v_t)
            long_string_key+='____'+'nb_fluids'+'___'+str(nb_of_fluids)
            print(long_string_key)
            print("long_string_key \n line 11431 \n ---------------- \n ")

            to_be_written=0
            dict_reloaded=finish_dict_reloaded__compute_meaningful_physical_quantities(dict_reloaded,N__fluids_sol__output_dict)
            if long_string_key not in list(large_dict_of_best_dict.keys()):
                
                large_dict_of_best_dict[long_string_key]=dict_reloaded
                to_be_written=1
            elif long_string_key in list(large_dict_of_best_dict.keys()):
                if large_dict_of_best_dict[long_string_key]['current_min_rank']<=current_min_rank:
                    # past rank <= present rank 

                    if large_dict_of_best_dict[long_string_key]["current_EOS"]> current_EOS and large_dict_of_best_dict[long_string_key]['current_NEC']< current_NEC :
                        # passé EOS> EOS PReSENT, NEC present>NEC PASSE
                        
                        
                        
                        to_be_written=1
                        large_dict_of_best_dict[long_string_key]=dict_reloaded


            complete_large_list_of_dicts.append(dict_reloaded)


            if to_be_written:
                timestamp_now_current=str(datetime.datetime.now())

                filename="EKOBAI___best__param_warpdrive_1.json"
                

                print('-------------------------')
                print(filename+' SAVED! \n line 11444')
                print('-------------------------')



                complete_path_filename=cwd+'/'+"OUTPUTS--of--analyze__explore_keep_only_best__all_indicators/"+filename

                with open(complete_path_filename, 'w') as fd:
                    fd.write(json.dumps(large_dict_of_best_dict,sort_keys=True, indent=4))    #  dicts in a list here


                with open(complete_path_filename, 'r') as fd:
                    small_dict_of_dicts2=json.load(fd)
                print(len(small_dict_of_dicts2))
                print("len(small_dict_of_dicts2) \n line 7647")
        
                
                #########
            filename2="EKOBAI__list_of_dicts___best__param_warpdrive_1.json"
            complete_path_filename2=cwd+'/'+"OUTPUTS--of--analyze__explore_keep_only_best__all_indicators__list_of_dicts/"+filename2

            with open(complete_path_filename2, 'w') as fd:
                fd.write(json.dumps(complete_large_list_of_dicts,sort_keys=True, indent=4))    #  dicts in a list here


            with open(complete_path_filename2, 'r') as fd:
                complete_large_list_of_dicts2=json.load(fd)

            print(len(complete_large_list_of_dicts2))
            print("len(complete_large_list_of_dicts2) \n line 7662")
    
            print('----------------------')
            print('################################')
            
        
                
            print(' \n line 7635 \n ----------- \n ')
            
            print(large_dict_of_best_dict[long_string_key]['timestamp'])
            print("large_dict_of_best_dict[long_string_key]['timestamp'] \n line 7640 \n ------------ \n ")

            print(large_dict_of_best_dict[long_string_key]['current_min_rank'])
            print("large_dict_of_best_dict[long_string_key]['current_min_rank'] \n line 7643 \n ------------ \n ")

    
            print('--------------------------------------------------------------------------------')
            print('################################################################################ \n line 7578')

#!/usr/bin/python
# -*- coding: utf-8 -*-


import pandas as pd

from sklearn import datasets, linear_model
from sklearn.linear_model import LinearRegression
import statsmodels.api as sm
from scipy import stats

import datetime
import matplotlib.pyplot as plt

plt.gca()

import copy

import matplotlib as mpl
import os, sys

import time, sys
import numpy as np
import matplotlib
matplotlib.use('QT4Agg')

from matplotlib.font_manager import FontProperties
import cmath as mth
import math
import time
import _pickle as cPickle
import pickle as Pickle

import matplotlib.mlab as mlab
import scipy
import scipy.fftpack
import scipy.stats.stats
import scipy.special
import scipy.linalg
import scipy.signal

import ctypes
import random

import locale
import operator
from matplotlib.ticker import ScalarFormatter
import re
import decimal
locale.setlocale(locale.LC_NUMERIC, "")
import urllib
import string
import pandas as pd
from scipy import stats

import os

from matplotlib import rcParams
import datetime


from cmath import *

from collections import Counter

import copy

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.colors as colors


import numpy as np
from scipy import ndimage


from scipy.optimize import minimize

from numpy import linalg as LA

import scipy as scipy
import numpy
print(numpy.version.version)
print("numpy.version.version")


import json

from os import listdir
from os.path import isfile, join
print('--------------')
print(scipy.__version__)
print("scipy.__version__")
print('--------------')
import datetime      
from matplotlib import rc
from matplotlib import rcdefaults

from matplotlib.ticker import NullFormatter  
from matplotlib.ticker import MaxNLocator
from matplotlib.offsetbox import AnchoredText
import matplotlib as mpl
import matplotlib.pyplot as plt 
import matplotlib.ticker

import sys
from produce_plots__NEC_RANK_EOS_simple_less_than_7_rank__LATEX__x_y_labels import retreat_output_list_of_dict,create_counterpart_everywhere,create_some_new_features,create_new_list_of_features,\
    dataframe__from_list_of_dict__and_list_cols
from sklearn.linear_model import LinearRegression

from matplotlib.offsetbox import AnchoredText
from numpy.linalg import inv


##########################################################################################
################################################################################


def plot_one_label__all_cases__loglog(label,dict_of_dfs__accel_decel__two_three__each_case_label,addendum='',plt='',sub_directory=""):
    add_to_title_negativity=""
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    dict_colors={}
    it_colors=0
    colors_set=['r','g','b','k']
    markers_set=['+','x','.','*']
    shift_set=range(len(colors_set))
    dict_shift={}
    dict_markers={}

    all_xs_coll=[]
    all_ys_coll=[]
    for key in list(dict_of_dfs__accel_decel__two_three__each_case_label.keys()):
        dict_markers[key]=markers_set[it_colors]
        dict_colors[key]=colors_set[it_colors]
        dict_shift[key]=shift_set[it_colors]
        it_colors+=1

    non_all_same_sign=["density_meanplotting"]


    concatenate_all_ys=[]
    

    ############
    #compute correct x ticks
    complete_x_list=[]
    for key in list(dict_of_dfs__accel_decel__two_three__each_case_label.keys()):
        ylabel=label

        latex_use=False
        df=dict_of_dfs__accel_decel__two_three__each_case_label[key]
        df=df.sort_values("warp_speed")
        x=list(df["warp_speed"])
        complete_x_list.extend(x)

    complete_x_list=list(set(complete_x_list))
    complete_x_list.sort()


    dict_from_x_str_to_xticks={r'$'+('%.3e' % complete_x_list[el_ind])+r'$':el_ind  for el_ind in range(len(complete_x_list))}

    dict_from_x_str_to_xticks___values={complete_x_list[el_ind]:el_ind  for el_ind in range(len(complete_x_list))}

    
    x_str_sticks=[r'$'+('%.3e' % el)+r'$'  for el in x]
    
    #########################
    for key in list(dict_of_dfs__accel_decel__two_three__each_case_label.keys()):


        #####################################
        ylabel=label

        latex_use=False
        df=dict_of_dfs__accel_decel__two_three__each_case_label[key]
        df=df.sort_values("warp_speed")

        if ylabel=='map--mean-rank':
            ylabel=r'map average of matrix rank'
            xlabel=r'warp speeds (v/c)'

            key_label=key

            y_ticks=[2,3,4,5,6,7,8,9,10,11]
            y_ticks=np.array(y_ticks)/10.0+6
            plt.yticks(y_ticks)
            ax=plt.gca()
            ax.set_ylim(np.min(y_ticks),np.max(y_ticks))
            
            y=list(df[label])
        elif ylabel=='np.max(diff_sur_som)':

            ylabel=r'map max of RD'
            xlabel=r'warp speeds (v/c)'

            key_label=key
        
            y=list(df[label])
        elif ylabel.lower()=='current_eos'  :

            ylabel=r'MAEOS'
            xlabel=r'warp speeds (v/c)'

            key_label=key
        
            y=list(df[label]/3.)
        elif ylabel.lower()=='max-gamma'  :



            ylabel=r'max |y_h|'
            xlabel=r'warp speeds (v/c)'

            key_label=key
        
            y=list(df[label])
        elif ylabel.lower()=='hyperbolic-relation--max-deviation'  :



            ylabel=r'max Delta_h(y_{h,m})'
            xlabel=r'warp speeds (v/c)'

            key_label=key
        
            y=list(df[label])
        elif ylabel.lower()=='np.max(condition_schwarzschild)'  :
            # schwarzschld condition quantity


            ylabel=r'SCQ'
            xlabel=r'warp speeds (v/c)'

            key_label=key
        
            y=list(df[label])


            
        elif ylabel.lower()=='vol_dens_neg_sum__TOT__sun_mass'.lower()  :

            ylabel=r'total negative mass (in solar mass)'
            xlabel=r'warp speeds (v/c)'
            key_label=key
            y=list(df[label])
        elif ylabel.lower()=='vol_dens_pos_sum__TOT__sun_mass'.lower()  :


            ylabel=r'total positive mass (in solar mass)'
            xlabel=r'warp speeds (v/c)'
            key_label=key
            y=list(df[label])
        elif ylabel.lower()=='current_NEC__density__kg_m_minus_3'.lower()  :


            ylabel=r'NEC (kg/m^3)'
            xlabel=r'warp speeds (v/c)'
            key_label=key
            y=list(df[label])

        elif ylabel.lower()=='max_abs__max_abs_u_squared_plus_one'.lower()  :


            ylabel=r'max|u^2+1|'
            xlabel=r'warp speeds (v/c)'
            key_label=key
            y=list(df[label])



        else:
            key_label=key
            xlabel='warp speeds (v/c)'
        
            y=list(df[label])
        #####################################
        if y==['' for el in y]:
            if 0:
                pass
            else:
                pass
                            
        ##################################################################################
        else:    


        
        

            concatenate_all_ys.extend(y)
            if label not in non_all_same_sign:
                try:
                    
                    if y[0]<0:
                        add_to_title_negativity="all_negative"
                        y=[el*(-1.0) for el in y]
                except:
                    
                    print(label)
                    print("label \n line 11029 \n ---------- \n ")
                    print(y)
                    print("y \n line 11098 \n ------- \n ")

                    raise

            means=np.array(list(df[label]))

            x=list(df["warp_speed"])

            if df["warp_acceleration"].unique().shape[0]>1:
                print(df["warp_acceleration"].unique().shape[0]>1)
                print('df["warp_acceleration"].unique().shape[0]>1')

                print(df["warp_acceleration"].unique().shape[0])
                print('df["warp_acceleration"].unique().shape[0]')
                print('line 10536')
                raise
            warp_accel_extracted=df["warp_acceleration"].unique()[0]





            ##############



            if "--2flu" in df["metric-fun"].unique()[0]:
                nFluid_txt="2 fluids"

            else:
                nFluid_txt="3 fluids"
            nFluid__filename_txt=nFluid_txt.replace(' ','_') #"2fluids"
            if warp_accel_extracted>0:
                accel_txt="accel"

            else:
                accel_txt="decel"
            if "--2flu" not in df["metric-fun"].unique()[0]:
                all_xs_coll+=x
                all_ys_coll+=y


            
            plt.loglog(x, y, ls='',c=dict_colors[key],marker=dict_markers[key],label=key_label)

    # below are a suppression of labels for which no trend is necessary 
    if "np.mean(condition_schwarzschild)".lower() not in ylabel.lower() and ("current" not in ylabel.lower() and "EOS".lower() not in ylabel.lower()) and\
       "sing_val_mat_6" not in ylabel and            "einst_mixed_largest_value__smallest_sv7" not  in label  and  "min_singular_value__smallest_sv7" not in label and \
       "max_abs_rho__min_sing_val" not in ylabel :
        log_all_xs_coll=np.array(np.log(all_xs_coll)).reshape(-1, 1)
        log_all_ys_coll=np.array(np.log(all_ys_coll)).reshape(-1, 1)
        

        
        model=LinearRegression(fit_intercept=True, normalize=False, copy_X=True, n_jobs=None).fit(log_all_xs_coll,log_all_ys_coll)
        predictions=model.predict(log_all_xs_coll)
        
        plt.plot(np.exp(log_all_xs_coll),np.exp(predictions),c='gray',linestyle=':')

        

        #...
        ################


        X2 = sm.add_constant(log_all_xs_coll)

        est = sm.OLS(log_all_ys_coll, X2)
        est2 = est.fit()
        

        data_str_array=est2.summary().tables[1].data

        index_cstt=[ idx for idx in range(len(data_str_array)) if "const" in data_str_array[idx]]
        index_x1=[ idx for idx in range(len(data_str_array)) if "x1" in data_str_array[idx] and "coef" not in data_str_array[idx] ]
        index_colnames=[ idx for idx in range(len(data_str_array)) if "std err" in data_str_array[idx] and "coef"  in data_str_array[idx] ]

        index_2nd_dim__coef=[idx for idx in range(len(data_str_array[index_colnames[0]])) if data_str_array[index_colnames[0]][idx]=='coef']
        index_2nd_dim__std_err=[idx for idx in range(len(data_str_array[index_colnames[0]])) if data_str_array[index_colnames[0]][idx]=='std err']

        box_text=r"Y= e^( " + str(data_str_array[index_cstt[0]][index_2nd_dim__coef[0]]).strip()+r' $ \pm $ ' + \
                      str(data_str_array[index_cstt[0]][index_2nd_dim__std_err[0]]).strip()+\
                      r' ) X^( '+str(data_str_array[index_x1[0]][index_2nd_dim__coef[0]]).strip()+\
                      r' $ \pm $ '+str(data_str_array[index_x1[0]][index_2nd_dim__std_err[0]]).strip()+\
                      ' )'
        
        
        text_box = AnchoredText(box_text, frameon=True, loc='lower right', pad=0.5)
        plt.setp(text_box.patch, facecolor='white', alpha=0.5)
        plt.gca().add_artist(text_box)

    
    plt.gca().yaxis.set_minor_formatter(NullFormatter())
    plt.grid(True)
    

    mpl.rcParams['xtick.major.pad'] = 8
    mpl.rcParams['ytick.major.pad'] = 8
    
    nan_max_y=np.nanmax(concatenate_all_ys)
    nan_min_y=np.nanmin(concatenate_all_ys)

    
    plt.xlabel(xlabel)    
    plt.ylabel(ylabel)
    plt.tight_layout()



    if 'np' in label and \
       'max' in label and \
       "condition" in label and \
       "schwarzschild" in label:
        plt.hlines(1, 0, len(x),color='darksalmon')
    

    if "rank" in label.lower()  :
        legend = ax.legend(loc='lower left', shadow=True, fontsize='x-large')
        legend.get_frame().set_facecolor('C0')

    elif "diff_sur_som" in label.lower():

        legend = ax.legend(loc='best', shadow=True, fontsize='x-large')
        legend.get_frame().set_facecolor('C0')
    elif "max-gamma" in label.lower() : 
                                          
        pass
    else:
        legend = ax.legend(loc='best', shadow=True, fontsize='x-large')
        legend.get_frame().set_facecolor('C0')
    ##########################"
    if len(addendum)!=0:
        plt.savefig(sub_directory+label.replace('(','_').replace(')','_').replace('.','_').replace('<','_').replace('/','_')+'___'+addendum+'_'+add_to_title_negativity+'.eps')
    else:
        plt.savefig(sub_directory+label.replace('(','_').replace(')','_').replace('.','_').replace('<','_').replace('/','_')+'__all_accel__allfluidmixes'+'_'+add_to_title_negativity+'.eps')
    plt.cla()
    plt.clf()
    plt.close('all')
    if latex_use:
        rcdefaults()
    return
####

###########################################################################

################################################################################


def plot_one_label__all_cases__loglog_latex(label,dict_of_dfs__accel_decel__two_three__each_case_label,addendum='',plt='',sub_directory=""):


    rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
    rc('text', usetex=True)

    key_label_dict__fluid_nb={"A_2F":"A-2FM",\
                              "D_2F":"D-2FM",\
                              "A_3F":"A-3FM",\
                              "D_3F":"D-3FM",\
    }

    add_to_title_negativity=""
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    dict_colors={}
    it_colors=0
    colors_set=['r','g','b','k']
    markers_set=['+','x','.','*']
    shift_set=range(len(colors_set))
    dict_shift={}
    dict_markers={}

    all_xs_coll=[]
    all_ys_coll=[]
    for key in list(dict_of_dfs__accel_decel__two_three__each_case_label.keys()):
        dict_markers[key]=markers_set[it_colors]
        dict_colors[key]=colors_set[it_colors]
        dict_shift[key]=shift_set[it_colors]
        it_colors+=1

    non_all_same_sign=["density_meanplotting"]


    concatenate_all_ys=[]
    

    ############
    #compute correct x ticks
    complete_x_list=[]
    for key in list(dict_of_dfs__accel_decel__two_three__each_case_label.keys()):
        ylabel=label

        latex_use=False
        df=dict_of_dfs__accel_decel__two_three__each_case_label[key]
        df=df.sort_values("warp_speed")
        x=list(df["warp_speed"])
        complete_x_list.extend(x)

    complete_x_list=list(set(complete_x_list))
    complete_x_list.sort()


    dict_from_x_str_to_xticks={r'$'+('%.3e' % complete_x_list[el_ind])+r'$':el_ind  for el_ind in range(len(complete_x_list))}

    dict_from_x_str_to_xticks___values={complete_x_list[el_ind]:el_ind  for el_ind in range(len(complete_x_list))}

    
    x_str_sticks=[r'$'+('%.3e' % el)+r'$'  for el in x]

    if label=="current_NEC__density__kg_m_minus_3":
        print('label=="current_NEC__density__kg_m_minus_3": line 515 \n ----- \n ')
        #input()
    #########################
    for key in list(dict_of_dfs__accel_decel__two_three__each_case_label.keys()):


        #####################################
        ylabel=label

        latex_use=False
        df=dict_of_dfs__accel_decel__two_three__each_case_label[key]
        df=df.sort_values("warp_speed")

        if ylabel=='map--mean-rank':
            ylabel=r'map average of matrix rank'
            xlabel=r'warp speeds (v/c)'

            key_label=key

            y_ticks=[2,3,4,5,6,7,8,9,10,11]
            y_ticks=np.array(y_ticks)/10.0+6
            plt.yticks(y_ticks)
            ax=plt.gca()
            ax.set_ylim(np.min(y_ticks),np.max(y_ticks))
            
            y=list(df[label])
        elif ylabel=='np.max(diff_sur_som)':

            ylabel=r'map max of RD'
            xlabel=r'warp speeds (v/c)'

            key_label=key
        
            y=list(df[label])
        elif ylabel.lower()=='current_eos'  :

            ylabel=r'MAEOS'
            xlabel=r'warp speeds (v/c)'

            key_label=key
        
            y=list(df[label]/3.)
        elif ylabel.lower()=='max-gamma'  :



            ylabel=r'max |y_h|'
            xlabel=r'warp speeds (v/c)'

            key_label=key
        
            y=list(df[label])
        elif ylabel.lower()=='hyperbolic-relation--max-deviation'  :



            ylabel=r'max Delta_h(y_{h,m})'
            xlabel=r'warp speeds (v/c)'

            key_label=key
        
            y=list(df[label])
        elif ylabel.lower()=='np.max(condition_schwarzschild)'  :
            # schwarzschld condition quantity


            ylabel=r'SCQ'
            xlabel=r'warp speeds (v/c)'

            key_label=key
        
            y=list(df[label])


            
        elif ylabel.lower()=='vol_dens_neg_sum__TOT__sun_mass'.lower()  :

            ylabel=r'total negative mass (in solar mass)'
            xlabel=r'warp speeds (v/c)'
            key_label=key
            y=list(df[label])
        elif ylabel.lower()=='vol_dens_pos_sum__TOT__sun_mass'.lower()  :


            ylabel=r'total positive mass (in solar mass)'
            xlabel=r'warp speeds (v/c)'
            key_label=key
            y=list(df[label])
        elif ylabel.lower()=='current_NEC__density__kg_m_minus_3'.lower()  :
            print("current_NEC__density__kg_m_minus_3 \n line 601 \n ----- \n ")
            #input()
            ylabel=r'NECV in $\left[kg\,m^{-3}\right]$'
            xlabel=r'warp speeds (v/c)'
            key_label=key
            y=list(df[label])

        elif ylabel.lower()=='max_abs__max_abs_u_squared_plus_one'.lower()  :


            ylabel=r'max|u^2+1|'
            xlabel=r'warp speeds (v/c)'
            key_label=key
            y=list(df[label])

        elif "max_abs_density"==ylabel.lower():
            ylabel=r'max absolute value of density'
            xlabel=r'warp speeds (v/c)'
            key_label=key
            y=list(df[label])
            
        elif "pressure_max__N_m_minus_2".lower()==ylabel.lower():
            ylabel=r'max pressure in $\left[N\,m^{-2}\right]$'
            xlabel=r'warp speeds (v/c)'
            key_label=key
            y=list(df[label])
        elif "pressure_min__N_m_minus_2".lower()==ylabel.lower():
            ylabel=r'opposite of min pressure in $\left[N\,m^{-2}\right]$'
            xlabel=r'warp speeds (v/c)'
            key_label=key
            y=list(df[label])

        elif "density_max__kg_m_minus_3"==ylabel.lower():
            ylabel=r'max density in $\left[kg\,m^{-3}\right]$'
            xlabel=r'warp speeds (v/c)'
            key_label=key
            y=list(df[label])
        elif "density_min__kg_m_minus_3"==ylabel.lower():
            ylabel=r'opposite of min density in $\left[kg\,m^{-3}\right]$'
            xlabel=r'warp speeds (v/c)'
            key_label=key
            y=list(df[label])

            #upper_bound___for_max_abs_density
        elif "upper_bound___for_max_abs_density"==ylabel.lower():
            ylabel=r'upper bound in cell of max absolute density'
            xlabel=r'warp speeds (v/c)'
            key_label=key
            y=list(df[label])
        elif "eins_mixed_largest___for_max_abs_density" in ylabel:
            ylabel=r' $\max |\mathbf{\tilde{G}}|$  in cell of max abs. density'
            xlabel=r'warp speeds (v/c)'
            key_label=key
            y=list(df[label])
        elif "sing_val_mat_6___for_max_abs_density" in ylabel:
            ylabel=r' MSV in cell of max absolute density'
            xlabel=r'warp speeds (v/c)'
            key_label=key
            y=list(df[label])

        elif "RATIO_upper_bound__over__max_abs_density" in ylabel:
            ylabel=r' (Upper bound)/(max abs density)'
            xlabel=r'warp speeds (v/c)'
            key_label=key
            y=list(df[label])


        else:
            key_label=key
            print(ylabel)
            print('ylabel in line 377')
            input()
            xlabel='warp speeds (v/c)'
        
            y=list(df[label])
        #####################################
        if y==['' for el in y]:
            if 0:
                
                pass
            else:
                pass
                            
        ##################################################################################
        else:    


        
        

            concatenate_all_ys.extend(y)
            if label not in non_all_same_sign:
                try:
                    
                    if y[0]<0:
                        add_to_title_negativity="all_negative"
                        y=[el*(-1.0) for el in y]
                except:
                    
                    print(label)
                    print("label \n line 11029 \n ---------- \n ")
                    print(y)
                    print("y \n line 11098 \n ------- \n ")

                    raise

            means=np.array(list(df[label]))

            x=list(df["warp_speed"])

            if df["warp_acceleration"].unique().shape[0]>1:
                print(df["warp_acceleration"].unique().shape[0]>1)
                print('df["warp_acceleration"].unique().shape[0]>1')

                print(df["warp_acceleration"].unique().shape[0])
                print('df["warp_acceleration"].unique().shape[0]')
                print('line 10536')
                raise
            warp_accel_extracted=df["warp_acceleration"].unique()[0]





            ##############



            if "--2flu" in df["metric-fun"].unique()[0]:
                nFluid_txt="2 fluids"

            else:
                nFluid_txt="3 fluids"
            nFluid__filename_txt=nFluid_txt.replace(' ','_') #"2fluids"
            if warp_accel_extracted>0:
                accel_txt="accel"

            else:
                accel_txt="decel"
            if "--2flu" not in df["metric-fun"].unique()[0]:
                all_xs_coll+=x
                all_ys_coll+=y


            


            plt.loglog(x, y, ls='',c=dict_colors[key],marker=dict_markers[key],label=key_label_dict__fluid_nb[key_label])

    # below are a suppression of labels for which no trend is necessary 
    if "np.mean(condition_schwarzschild)".lower() not in ylabel.lower() and ("current" not in ylabel.lower() and "EOS".lower() not in ylabel.lower()) and\
       "sing_val_mat_6" not in ylabel and            "einst_mixed_largest_value__smallest_sv7" not  in label  and  "min_singular_value__smallest_sv7" not in label and \
       "max_abs_rho__min_sing_val" not in ylabel :
        log_all_xs_coll=np.array(np.log(all_xs_coll)).reshape(-1, 1)
        log_all_ys_coll=np.array(np.log(all_ys_coll)).reshape(-1, 1)
        

        
        model=LinearRegression(fit_intercept=True, normalize=False, copy_X=True, n_jobs=None).fit(log_all_xs_coll,log_all_ys_coll)
        predictions=model.predict(log_all_xs_coll)
        
        plt.plot(np.exp(log_all_xs_coll),np.exp(predictions),c='gray',linestyle=':')

        

        #...
        ################


        X2 = sm.add_constant(log_all_xs_coll)

        est = sm.OLS(log_all_ys_coll, X2)
        est2 = est.fit()
        

        data_str_array=est2.summary().tables[1].data

        index_cstt=[ idx for idx in range(len(data_str_array)) if "const" in data_str_array[idx]]
        index_x1=[ idx for idx in range(len(data_str_array)) if "x1" in data_str_array[idx] and "coef" not in data_str_array[idx] ]
        index_colnames=[ idx for idx in range(len(data_str_array)) if "std err" in data_str_array[idx] and "coef"  in data_str_array[idx] ]

        index_2nd_dim__coef=[idx for idx in range(len(data_str_array[index_colnames[0]])) if data_str_array[index_colnames[0]][idx]=='coef']
        index_2nd_dim__std_err=[idx for idx in range(len(data_str_array[index_colnames[0]])) if data_str_array[index_colnames[0]][idx]=='std err']

        box_text=r"$Y = \exp( " + str(data_str_array[index_cstt[0]][index_2nd_dim__coef[0]]).strip()+r'  \pm  ' + \
                      str(data_str_array[index_cstt[0]][index_2nd_dim__std_err[0]]).strip()+\
                      r' ) X^{ '+str(data_str_array[index_x1[0]][index_2nd_dim__coef[0]]).strip()+\
                      r'  \pm  '+str(data_str_array[index_x1[0]][index_2nd_dim__std_err[0]]).strip()+\
                      ' }$'
        
        
        
        text_box = AnchoredText(box_text, frameon=True, loc='lower right', pad=0.5)
        plt.setp(text_box.patch, facecolor='white', alpha=0.5)
        plt.gca().add_artist(text_box)

    
    plt.gca().yaxis.set_minor_formatter(NullFormatter())
    plt.grid(True)
    

    mpl.rcParams['xtick.major.pad'] = 8
    mpl.rcParams['ytick.major.pad'] = 8
    
    nan_max_y=np.nanmax(concatenate_all_ys)
    nan_min_y=np.nanmin(concatenate_all_ys)

    
    plt.xlabel(xlabel)    
    plt.ylabel(ylabel)
    plt.tight_layout()



    if 'np' in label and \
       'max' in label and \
       "condition" in label and \
       "schwarzschild" in label:
        plt.hlines(1, 0, len(x),color='darksalmon')
    if "current_EOS" in label:
        print('label=="current_NEC__density__kg_m_minus_3": line 515 \n ----- \n ')
        #input()
        print("draw hline current_NEC__density__kg_m_minus_3 \n line 826")
        #input()
        xlims=ax.get_xlim()
        ylims=ax.get_ylim()
        print(xlims)
        print("xlims \n line 830 \n ----- \n ")
        print(ylims)
        print("ylims \n line 833 \n ----- \n ")

        print(1.0/3.)
        print("1.0/3. \n line 832 \n --- \n ")
        
        plt.hlines(1.0/3., xlims[0], xlims[1],color='green',alpha=0.5)
        # ax.hlines(1.0/3., xlims[0], xlims[1],color='darksalmon',alpha=0.5)
        # ax.hlines(0.333, xlims[0]/100.0, xlims[1]*100.,color='green')#,alpha=0.5)

    if "rank" in label.lower()  :
        legend = ax.legend(loc='lower left', shadow=True, fontsize='x-large')
        legend.get_frame().set_facecolor('C0')

    elif "diff_sur_som" in label.lower():

        legend = ax.legend(loc='best', shadow=True, fontsize='x-large')
        legend.get_frame().set_facecolor('C0')
    elif "max-gamma" in label.lower() : 
                                          
        pass
    elif "sing_val_mat_6___for_max_abs_density" in ylabel or "MSV in cell of max absolute density" in ylabel:
        legend = ax.legend(loc='center right', shadow=True, fontsize='x-large')
        legend.get_frame().set_facecolor('C0')
    else:
        legend = ax.legend(loc='best', shadow=True, fontsize='x-large')
        legend.get_frame().set_facecolor('C0')
    ##########################"
    if len(addendum)!=0:
        plt.savefig(sub_directory+label.replace('(','_').replace(')','_').replace('.','_').replace('<','_').replace('/','_')+'___'+addendum+'_'+add_to_title_negativity+'.eps')
    else:
        plt.savefig(sub_directory+label.replace('(','_').replace(')','_').replace('.','_').replace('<','_').replace('/','_')+'__all_accel__allfluidmixes'+'_'+add_to_title_negativity+'.eps')
    plt.cla()
    plt.clf()
    plt.close('all')
    if latex_use:
        rcdefaults()

    mpl.rcParams.update(mpl.rcParamsDefault)

    return
####

###########################################################################

####################################################################
if __name__ == "__main__":


    saving_directory_for_saving_files_produced_here="2019-11-24--OUTPUTS--plots--regular--loglog"
    saving_directory_for_saving_files_produced_here_latex="2020-02-12--OUTPUTS--plots--regular--loglog-latex"
    cwd=os.getcwd()

    complete_path_OUTPUTS__of__this_script=cwd+'/'+saving_directory_for_saving_files_produced_here+'/'
    complete_path_OUTPUTS__of__this_script_latex=cwd+'/'+saving_directory_for_saving_files_produced_here_latex+'/'
    ######################


    #####################

    complete_path_OUTPUTS__of__dict_of_dict__2list_of_dicts=cwd+'/'+"OUTPUTS-OF--analyze_kip_threshold_date_2flu/"
    list_files=os.listdir(complete_path_OUTPUTS__of__dict_of_dict__2list_of_dicts)
    if "extracted_list_of_dicts__param_warpdrive_1.json" in list_files:
        new_filename_complete_filename=complete_path_OUTPUTS__of__dict_of_dict__2list_of_dicts+"extracted_list_of_dicts__param_warpdrive_1.json"

        

    
    with open(new_filename_complete_filename, 'r') as fd:
        list_instead_of_dict2=json.load(fd)

    ##################
    timestamps= [list_instead_of_dict2[a]["timestamp"] for a in range(len(list_instead_of_dict2))]
    timestamps.sort()
    for el in timestamps:
        print(el)

    print('line 10988, waiting for input...')
    print('-------------------------------------------------- \n line 805 \n -------- \n hello')





        
    print('------------------------------------' )#str(key)+'  :  '+str(list_instead_of_dict2[0][key]))
    for key in list(list_instead_of_dict2[0].keys()):
        print(key)
    print(' key in list(list_instead_of_dict2[0].keys()): \n line 10778 \n -------- \n ')
    #########################################################################
    #we complete method optim for cases where there is no such key
    for a in range(len(list_instead_of_dict2)):
        if "method-optim" not in list(list_instead_of_dict2[a].keys()):
            list_instead_of_dict2[a]["method-optim"]=''

    #########################################################################
    
    
    # below we add a lot of characteristics
    list_instead_of_dict2=retreat_output_list_of_dict(list_instead_of_dict2)



    
    ###########################################################################
    ######################################

            
    #################################
    
    ###############################################################################

        

    for key in list_instead_of_dict2[0].keys():
        print(str(key))

    #################

    list_instead_of_dict2=create_counterpart_everywhere(list_instead_of_dict2)
    


    ###########################################################################
    ######################################
    #################################


    
    
    compute_max_std_min_mean_for_all_these_keys=['diff_over_sum__modeled','sing_val_mat_6','sing_val_mat_0']
    list_instead_of_dict2=create_some_new_features(list_instead_of_dict2,compute_max_std_min_mean_for_all_these_keys)
    list_b=create_new_list_of_features(list_instead_of_dict2)
    ##########################



    
    df=dataframe__from_list_of_dict__and_list_cols(list_b,list_instead_of_dict2)
    print(df)
    print("df line 10558 \n ----------- \n ")
    ##########################################################
    c__speed_of_light=2.99792458e8

    G__gravitational_cstt=6.67408e-11


    df['pressure_max__N_m_minus_2']=df[['pressure_max']].sum(axis=1)*c__speed_of_light**4/G__gravitational_cstt
    df['pressure_min__N_m_minus_2']=df[['pressure_min']].sum(axis=1)*c__speed_of_light**4/G__gravitational_cstt
    
    df['proxy_pressure_max__N_m_minus_2']=df[["p_X_max","p_rho_max","p_phi_max"]].sum(axis=1)*c__speed_of_light**4/G__gravitational_cstt
    df['proxy_pressure_min__N_m_minus_2']=df[["p_X_min","p_rho_min","p_phi_min"]].sum(axis=1)*c__speed_of_light**4/G__gravitational_cstt


    df['vol_dens_pos_sum__TOT__kg']=df[["vol_dens_pos_sum__TOT"]].sum(axis=1)*c__speed_of_light**2/G__gravitational_cstt
    df['vol_dens_neg_sum__TOT__kg']=df[["vol_dens_neg_sum__TOT"]].sum(axis=1)*c__speed_of_light**2/G__gravitational_cstt

    df['vol_dens_pos_sum__TOT__sun_mass']=df[["vol_dens_pos_sum__TOT"]].sum(axis=1)*c__speed_of_light**2/G__gravitational_cstt*1./(1.9884e30)
    df['vol_dens_neg_sum__TOT__sun_mass']=df[["vol_dens_neg_sum__TOT"]].sum(axis=1)*c__speed_of_light**2/G__gravitational_cstt*1./(1.9884e30)
    
    
    df['density_max__kg_m_minus_3']=df['density_max']*c__speed_of_light**2/G__gravitational_cstt
    df['density_min__kg_m_minus_3']=df['density_min']*c__speed_of_light**2/G__gravitational_cstt


    df['current_NEC__density__kg_m_minus_3']=df['current_NEC']*c__speed_of_light**2/G__gravitational_cstt


    list_b=[el for el in list(df.columns) ]
    print('sortir les features above in the plot \n line 10990')


    
    list_c=['current_EOS', 'current_NEC', 'current_min_rank', 'metric-fun', 'nfev',  'rank',  'timestamp',\
            'warp_acceleration', 'warp_speed']
    list_d=[ 'current_min_rank',"map--mean-rank", "method-optim",  'timestamp',\
            'metric-fun', 'warp_speed']


    
    two_fluids__df=df[df["metric-fun"].str.contains('--2flu')]
    three_fluids__df=df[~df["metric-fun"].str.contains('--2flu')]

    accel_two_fluids__df=two_fluids__df[two_fluids__df["warp_acceleration"]>=0]
    decel_two_fluids__df=two_fluids__df[two_fluids__df["warp_acceleration"]<=0]

    
    
    
    
    accel_three_fluids__df=three_fluids__df[three_fluids__df["warp_acceleration"]>=0]
    decel_three_fluids__df=three_fluids__df[three_fluids__df["warp_acceleration"]<=0]


    if 1:
        list_ws_d3=list(decel_three_fluids__df["warp_speed"].unique())
        list_ws_d3.sort()

        list_ws_a3=list(accel_three_fluids__df["warp_speed"].unique())
        list_ws_a3.sort()

        list_ws_a2=list(accel_two_fluids__df["warp_speed"].unique())
        list_ws_a2.sort()

        list_ws_d2=list(decel_two_fluids__df["warp_speed"].unique())
        list_ws_d2.sort()
        
    
    ###########################
    # we make plots separated per case

    list_of_dfs__accel_decel__two_three=[accel_two_fluids__df,decel_two_fluids__df,accel_three_fluids__df,decel_three_fluids__df]
    dict_of_dfs__accel_decel__two_three__each_case_label={"A_2F":accel_two_fluids__df,\
                                                          "D_2F":decel_two_fluids__df,\
                                                          "A_3F":accel_three_fluids__df,\
                                                          "D_3F":decel_three_fluids__df}
    dict_of_dfs__accel_decel__ONLYthree__each_case_label={"A_3F":accel_three_fluids__df,\
                                                          "D_3F":decel_three_fluids__df}


    
    accel_two_fluids__df_nrd=accel_two_fluids__df[(accel_two_fluids__df["map--mean-rank"]==7)]
    decel_two_fluids__df_nrd=decel_two_fluids__df[(decel_two_fluids__df["map--mean-rank"]==7)]
    accel_three_fluids__df_nrd=accel_three_fluids__df[(accel_three_fluids__df["map--mean-rank"]==7)]
    decel_three_fluids__df_nrd=decel_three_fluids__df[(decel_three_fluids__df["map--mean-rank"]==7)]


    dict_of_dfs__accel_decel__ONLYnrd__each_case_label={"A_2F":accel_two_fluids__df_nrd,\
                                                        "D_2F":decel_two_fluids__df_nrd,\
                                                        "A_3F":accel_three_fluids__df_nrd,\
                                                        "D_3F":decel_three_fluids__df_nrd}

    #non rank defects NRD nrd


    for col in list(dict_of_dfs__accel_decel__ONLYnrd__each_case_label["A_2F"].columns):
        print(col)
    print('---- \n line 1049 \n ------ \n ')
    #input()
    ################## ####################################
    #below interesting but time consuming
    plt.gca()

    #########################################
    labels_2=[el for el in list_b if el!='warp_speed' and \
              el!='warp_acceleration' and \
              el!='metric-fun' and   \
              el!='direc' and \
              el!='min-ranks' \
              and el not in ['message', 'metric-fun', 'min-ranks', "timestamp",'x','method-optim','nfev','status','success','rank']
    ]

    labels_2=['pressure_max__N_m_minus_2',\
              'pressure_min__N_m_minus_2',\
              'density_max__kg_m_minus_3',\
              'density_min__kg_m_minus_3',\
              'current_NEC__density__kg_m_minus_3',\
    ]+labels_2

    for label in labels_2 :
        print(label)
    print('----------- \n ----------------------------- \n line 11501 \n ---------- \n ')
    ########################################

    for label in labels_2 :

        if "pressure_min" in label or \
           "pressure_max" in label or \
           "density_max" in label or \
           "density_min" in label or \
           "density_min" in label or \
           "NEC" in label or \
           "gamma" in label or \
           "current_eos" in label.lower() or \
           "hyperbolic-relation--max-deviation" in label.lower() or \
           "max_abs__max_abs_u_squared_plus_one" in label.lower() or \
           "max_abs__max_abs_of_velocities_components" in label.lower() or \
           "vol_dens_neg_sum__TOT" in label.lower() or \
           "vol_dens_pos_sum__TOT" in label.lower() or \
           "hyperbolic-relation--max-deviation" in label.lower() or \
           "max-gamma" in label.lower() or \
           "pressure_std__N_m_minus_2".lower() in label.lower() or \
           "pressure_mean__N_m_minus_2".lower() in label.lower() or \
           "pressure_max__N_m_minus_2".lower() in label.lower() or \
           "pressure_min__N_m_minus_2".lower() in label.lower() or \
           "proxy_pressure_max__N_m_minus_2".lower() in label.lower() or \
           "proxy_pressure_min__N_m_minus_2".lower() in label.lower() or \
           "proxy_pressure_std__N_m_minus_2".lower() in label.lower() or \
           "proxy_pressure_mean__N_m_minus_2".lower() in label.lower() or \
           "vol_dens_pos_sum__TOT__sun_mass".lower() in label.lower() or \
           "vol_dens_neg_sum__TOT__sun_mass".lower() in label.lower() or \
           "density_max__kg_m_minus_3".lower() in label.lower() or \
           "density_min__kg_m_minus_3".lower() in label.lower() or \
           "density_std__kg_m_minus_3".lower() in label.lower() or \
           "density_mean__kg_m_minus_3".lower() in label.lower() or \
           ("sing_val__biggest_RD_EinstTens").lower() in label.lower() or \
           ("schwarzschild").lower() in label.lower() or \
           ("current_nec").lower() in label.lower() or \
           label.lower()=='vol_dens_neg_sum__TOT__sun_mass'.lower()  or \
           label.lower()=='vol_dens_pos_sum__TOT__sun_mass'.lower()  or \
           label.lower()=='current_NEC__density__kg_m_minus_3'.lower() or \
           "plus_one".lower() in label.lower() or \
           "sing_val_mat_6" in label or \
           "sing_val_mat_0" in label or \
           "einst_tens_largest__smallest_sv7" in label or \
           "einst_mixed_largest_value__smallest_sv7" in label or \
           "eins_mixed_largest___for_max_density" in label or \
           "sing_val_mat_6___for_max_density" in label or \
           "eins_mixed_largest___for_min_density" in label or \
           "sing_val_mat_6___for_min_density" in label or \
           "density___for_min_density" in label or \
           "eins_mixed_largest___for_max_abs_density" in label or \
           "sing_val_mat_6___for_max_abs_density" in label or \
           "max_abs_density" in label or \
           "upper_bound___for_max_abs_density" in label or \
           "max_abs_rho__min_sing_val" in label or \
           "einst_mixed_largest_value__smallest_sv7"  in label or \
           "min_singular_value__smallest_sv7"  in label  :
            



            print(label +'  plotting ONLY no rank defects ...')
            plot_one_label__all_cases__loglog(label,dict_of_dfs__accel_decel__ONLYnrd__each_case_label,addendum="_noRankDefects",plt=plt,sub_directory=complete_path_OUTPUTS__of__this_script)



        if "pressure_max__N_m_minus_2" in label or\
           "pressure_min__N_m_minus_2" in label or\
           "current_eos" in label.lower() or \
           "vol_dens_pos_sum__TOT__sun_mass".lower() in label.lower() or \
           "vol_dens_neg_sum__TOT__sun_mass".lower() in label.lower() or \
           "density_max__kg_m_minus_3".lower() in label.lower() or \
           "density_min__kg_m_minus_3".lower() in label.lower() or \
           ("schwarzschild").lower() in label.lower() or \
           label.lower()=='vol_dens_neg_sum__TOT__sun_mass'.lower()  or \
           label.lower()=='vol_dens_pos_sum__TOT__sun_mass'.lower()  or \
           label.lower()=='current_NEC__density__kg_m_minus_3'.lower() or \
           "eins_mixed_largest___for_max_abs_density" in label or \
           "sing_val_mat_6___for_max_abs_density" in label or \
           "max_abs_density" in label or \
           "upper_bound___for_max_abs_density" in label or \
           "RATIO_upper_bound__over__max_abs_density" in label or\
           "current_NEC__density__kg_m_minus_3" in label or\
           'current_NEC__density__kg_m_minus_3'.lower() in label.lower() :
            
           
            if "current_NEC__density__kg_m_minus_3".lower() in label.lower() or ( "np.max(absolute_minimum_NEC)" not in label and "pressure_min"!=label.lower() and "pressure_max"!=label.lower() and "np.mean(absolute_minimum_NEC)"!=label \
               and "min__sing_val_mat_6"!=label and "np.min(condition_schwarzschild)"!=label and "np.std(condition_schwarzschild)"!=label and "mean(condition_schwarzschild)" not in label and "current_NEC" not in label and "proxy_pre" not in label):
                plot_one_label__all_cases__loglog_latex(label,dict_of_dfs__accel_decel__ONLYnrd__each_case_label,addendum="_noRankDefects",plt=plt,sub_directory=complete_path_OUTPUTS__of__this_script_latex)

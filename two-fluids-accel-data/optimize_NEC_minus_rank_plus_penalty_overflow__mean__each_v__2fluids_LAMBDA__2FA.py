#!/usr/bin/python
# -*- coding: utf-8 -*-

import datetime
import matplotlib.pyplot as plt


import copy

import matplotlib as mpl
import os, sys

import time, sys
import numpy as np
import matplotlib
matplotlib.use('QT4Agg')

from matplotlib.font_manager import FontProperties
import cmath as mth
import math
import time
import _pickle as cPickle
import pickle as Pickle

import matplotlib.mlab as mlab
import scipy
import scipy.fftpack
import scipy.stats.stats
import scipy.special
import scipy.linalg
import scipy.signal

import ctypes
import random

import locale
import operator
from matplotlib.ticker import ScalarFormatter
import re
import decimal
locale.setlocale(locale.LC_NUMERIC, "")
import urllib
import string

from scipy import stats

import os

from matplotlib import rcParams
import datetime


from cmath import *

from collections import Counter

import copy

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.colors as colors


import numpy as np
from scipy import ndimage


from scipy.optimize import minimize

from numpy import linalg as LA

import scipy as scipy
import numpy
print(numpy.version.version)
print("numpy.version.version")


import json

from os import listdir
from os.path import isfile, join
print('--------------')
print(scipy.__version__)
print("scipy.__version__")
print('--------------')
import datetime      



import sys
mypath = os.getcwd()
directory_common_functions="/".join(mypath.split("/")[:-1])+"/2020-03-08--common-functions-2fluids"
sys.path.insert(0, directory_common_functions)
from common_functions_2FA import choose_filename_saving,extract_last_filename_for_loading,\
    compute_things_with_r_and_thetas,\
    compute_f_and_derivatives,\
    mix_derivatives_of_f__and_theta,\
    extract_right_dict,\
    compute_composites_warp_v__meshed_f__and_theta,\
    e_prime_computation,\
    compute_einstein_tensor_components_doublyCov,\
    compute_doublycov_doublycontra_metrics_comp,\
    minus_min_of_density_plus_pressure


#######################################################################################################################################################################################


####################################################################
if __name__ == "__main__":

    
        Lambda_cosmological_constant=0 # cosmological constant not taken into account , the right way of taking it
        # into account is not implemented
        


        loading_filename=extract_last_filename_for_loading(mypath)

        print('---- \n')
        print(loading_filename)
        print('loading_filename \n line 1207 \n --------- \n line 1424 \n  ')
        
        with open(loading_filename, 'r') as fd:
            list_of_old_dict_res=json.load(fd)
        print(len(list_of_old_dict_res))
        print("len(list_of _old_dict_res) \n line 1279")
        print(list_of_old_dict_res[0].keys())
        print("list_of_old_dict_res[0].keys() \n ")
        print(datetime.datetime.now())
        print("datetime.datetime.now() \n ")
        last_warp_speed_saved=list_of_old_dict_res[0]['warp_speed']


        #########################

        list_to_save_b=[{'machin':str(datetime.datetime.now())}]
        with open('test_bidule', 'w') as fp:
            fp.write(json.dumps(list_to_save_b))
            
        #json serialized
        print("json serialized TEST worked")
        
        
        #####################################
        # saving filename below

        saving_filename=choose_filename_saving(mypath=mypath,now=datetime.datetime.now())
        
        print('\n')
        print(saving_filename)
        print("saving_filename \n line 1307 \n --------- \n ")
        
        ######################################################
        ########################################################

        
        
        
        np.random.seed(seed=42)

        
        
        the_world_is_flat = 0

        if not the_world_is_flat:
                print ("Flight preparation and take off ... --- \'\'\'   ")
        
                print ('')


        
        

        v_t=1.1e-16 # warp acceleration ####  9.8/(299792458)^2
        #9.8m/s-2 en m-1


        
        

        ############
        DELTA_theta=np.pi/100.
        theta_real=np.arange(0.0,np.pi,DELTA_theta)

        theta=theta_real/4. # any continuous function of theta could be used for optim / but not when we are analyzing results !! 
        # this could be explored in optim

        #this is the radial range that is explored
        DELTA_r=0.05
        r_range=np.arange(0.01,2.7,DELTA_r) 
        
        #############

        
        
        sin_theta,cos_theta,ones_theta,r_mesh,cos_theta_squared,sin_theta_squared,sin_half_theta,sin_half_theta_squared,ones_theta_and_r,\
            meshed_sin_half_theta_squared,meshed_sin_theta,meshed_cos_theta,theta_mesh=compute_things_with_r_and_thetas(theta,r_range)



        sin_theta_real,cos_theta_real,ones_theta_real,r_mesh__one_theta_real,cos_theta_real_squared,sin_theta_real_squared,sin_half_theta_real,sin_half_theta_real_squared,ones_theta_real_and_r,\
            meshed_sin_half_theta_real_squared,meshed_sin_theta_real,meshed_cos_theta_real,theta_real_mesh=compute_things_with_r_and_thetas(theta_real,r_range)



        v_range=np.array([1e-5,5e-5,1e-4,4e-4,7e-4,1e-3,4e-3,7e-3,1e-2,4e-2,7e-2,1e-1,2.5e-1,5e-1,7.5e-1,0.9,0.99,0.999,0.9999,1.,1.1,1.5,2,2.5])

        #voyager speed in units of c : 1e-5 minimum actually more around 7.0e-5
        v_max=np.max(v_range)
        v_min=np.min(v_range)

        #comment below if possible
        v_range=np.flip(v_range)
        
        print('\n ')
        print(v_range)
        print("v_range \n line 201 \n ")
        
        print (r_range)
        print ("r_range \n line 204 \n ")



        

        r=r_range


        sigma=8

        R=1

        f,f_r,f_rr,f_r_over_r=compute_f_and_derivatives(r,R,sigma)

        ######################################################################
        #necessary requirements for einstein tensor computation
        meshed_cos_theta_real_times_meshed_sin_theta_real=np.multiply(meshed_cos_theta_real,meshed_sin_theta_real)
        meshed_cos_squared_theta_real=np.multiply(meshed_cos_theta_real,meshed_cos_theta_real)
        meshed_sin_squared_theta_real=np.multiply(meshed_sin_theta_real,meshed_sin_theta_real)
        f_r__squared=np.multiply(f_r,f_r)
        f_rr_minus__f_r_over_r=f_rr-f_r_over_r


        
        #############################################################
        
        print('-----------------------------')
        print('---------------')
        print(f_r.shape)
        print("f_r.shape")
        print(sin_theta.T.shape)
        print("sin_theta.T.shape")
        
    


    
        meshed_f,cos_theta_squared_dot__f_rr_minus_f_r_over_r,\
            sin_theta_squared_dot__f_rr_minus_f_r_over_r,meshed_f_r,meshed_f_r_over_r,meshed_f_rr=mix_derivatives_of_f__and_theta(f,f_r,f_rr,f_r_over_r,sin_theta,cos_theta,ones_theta,\
                                                                                                                                  cos_theta_squared,sin_theta_squared)

        meshed_f__ones_theta_real,cos_theta_real_squared_dot__f_rr_minus_f_r_over_r,\
            sin_theta_real_squared_dot__f_rr_minus_f_r_over_r,\
            meshed_f_r__ones_theta_real,\
            meshed_f_r_over_r__ones_theta_real,\
            meshed_f_rr__ones_theta_real=mix_derivatives_of_f__and_theta(f,f_r,f_rr,f_r_over_r,sin_theta_real,cos_theta_real,ones_theta_real,cos_theta_real_squared,sin_theta_real_squared)


        limit_v_range_to_what_remains_to_be_done=True
        if limit_v_range_to_what_remains_to_be_done:
            v_range_ini=v_range
            print(v_range)
            print('OLD V_range')
            
            

            
            v_range_the_rest=[v_value for v_value in list(v_range) if v_value<=last_warp_speed_saved ]
            v_range_continuous=v_range_the_rest+list(v_range)*1000
            v_range=v_range_continuous

            

        ############################################################
        # filtrage des vitesses ci dessous
        filtre=False
        if filtre:
            v_range=[vv for vv in v_range if (vv>=0.9999999999 or vv==0.04 or vv<7.5e-3) and vv!=1.1]
        ############################################################
        for v in v_range:
            ###############################
            #below we actually take the three starting blocks before middle and after the velocity to change initial conditions
            maximum_of_three_velocity_in_theneighbourhood=[]
            maximum_of_three_velocity_in_theneighbourhood.append(v)
            #########################################
            the_smaller_v__abs_v_diff=[np.abs(v_b-v) for v_b in v_range_ini if v_b<v]
            if len(the_smaller_v__abs_v_diff)!=0:
                smaller_vel_closest=[v_b for v_b in v_range_ini if v_b<v and np.abs(v_b-v)==np.min(the_smaller_v__abs_v_diff)]
                maximum_of_three_velocity_in_theneighbourhood.append(smaller_vel_closest[0])
            else:
                pass
                #no v smaller


            #larger velocities
            the_larger_v__abs_v_diff=[np.abs(v_b-v) for v_b in v_range_ini if v_b>v]
            if len(the_larger_v__abs_v_diff)!=0:
                larger_vel_closest=[v_b for v_b in v_range_ini if v_b>v and np.abs(v_b-v)==np.min(the_larger_v__abs_v_diff)]
                maximum_of_three_velocity_in_theneighbourhood.append(larger_vel_closest[0])
            else:
                pass
                #no v larger
            print('----------------------------------------------------')
            print(maximum_of_three_velocity_in_theneighbourhood)
            print("maximum_of_three_velocities_in_the___neighbourhood \n line 1752--1827")
            print('----------------------------------------------------')
            
            for v_spec in maximum_of_three_velocity_in_theneighbourhood:
                right_dict=extract_right_dict(list_of_old_dict_res,v_spec,v_t,sigma=sigma)

                # ####################################

                print(' ------ ')
                print (' CURRENT warp speed : '+str(v))
                print(' -------------- \n')


                A, lambda_plus,sqrt_lambda_plus,sqrt_lambda_minus,N_plus_squared,N_plus,sin_half_theta_squared_over_N_plus_squared,\
                    sin_theta_over_N_plus=compute_composites_warp_v__meshed_f__and_theta(v,meshed_f,meshed_sin_half_theta_squared,meshed_sin_theta)

                A__ones_theta_real, lambda_plus__ones_theta_real,sqrt_lambda_plus__ones_theta_real,sqrt_lambda_minus__ones_theta_real,N_plus_squared__ones_theta_real,N_plus__ones_theta_real,\
                    sin_half_theta_real_squared_over_N_plus_squared,\
                    sin_theta_real_over_N_plus=compute_composites_warp_v__meshed_f__and_theta(v,meshed_f__ones_theta_real,meshed_sin_half_theta_real_squared,meshed_sin_theta_real)

                e_p_t_t,\
                    e_p_t_X,\
                    e_p_t_rho,\
                    e_p_t_phi,\
                    e_p_X_t,\
                    e_p_X_X,\
                    e_p_X_rho,\
                    e_p_X_phi,\
                    e_p_rho_t,\
                    e_p_rho_X,\
                    e_p_rho_rho,\
                    e_p_rho_phi,\
                    e_p_phi_t,\
                    e_p_phi_X,\
                    e_p_phi_rho,\
                    e_p_phi_phi,\
                    real_rho_values=e_prime_computation(sin_half_theta_squared_over_N_plus_squared,\
                                                        lambda_plus,sqrt_lambda_plus,A,v,meshed_f,sin_theta_over_N_plus,meshed_f_r,meshed_cos_theta,theta_real_mesh,r_mesh)



                G__t_t,\
                    G__t_x,\
                    G__x_x,\
                    G__t_rho,\
                    G__x_rho,\
                    G__rho_rho,\
                    G__phi_phi=compute_einstein_tensor_components_doublyCov(meshed_f_r_over_r__ones_theta_real,\
                                                                            sin_theta_real_squared_dot__f_rr_minus_f_r_over_r,\
                                                                            meshed_sin_squared_theta_real,\
                                                                            meshed_f_r__ones_theta_real,\
                                                                            v,\
                                                                            meshed_f__ones_theta_real,\
                                                                            meshed_cos_theta_real_times_meshed_sin_theta_real,\
                                                                            meshed_f_rr__ones_theta_real,\
                                                                            v_t,\
                                                                            meshed_sin_theta_real,\
                                                                            meshed_cos_theta_real,\
                                                                            cos_theta_real_squared_dot__f_rr_minus_f_r_over_r,\
                                                                            real_rho_values,\
                )

                
                

                ################"

                g_n_t_t,\
                    g_n_t_X,\
                    g_n_X_X,\
                    g_n_rho_rho,\
                    g_n_phi_phi,\
                    g_v_t_t,\
                    g_v_t_X,\
                    g_v_X_X,\
                    g_v_rho_rho,\
                    g_v_phi_phi=compute_doublycov_doublycontra_metrics_comp(ones_theta_and_r,v,meshed_f,A,real_rho_values)

                
                ##########################################################
                
                ##############################################################################################################################################
                #############################################################################################################
                ############################################################################################################
                ##############################################################################################################################################
                            

                    
                #############################################################################################################

                

                
                

                
                full_outputs=[]

                for batch_nb in range(1):

                    
                    print('computing for res_batch  '+batch_nb.__str__()+' ... ')
                    
                    res_batch=batch_nb*np.ones(meshed_f_rr__ones_theta_real.shape)


                    #1
                    g_n_t__t_jX=1j*g_n_t_X
                    g_n_t__t_jX+=g_n_t_t

                    #2
                    g_n__rho2_jX2=1j*g_n_X_X
                    g_n__rho2_jX2+=g_n_rho_rho


                    
                    g__v_jn__phiphi=1j*g_n_phi_phi
                    g__v_jn__phiphi+=g_v_phi_phi

                          

                    #4
                    g_v_t__t_jX=1j*g_v_t_X
                    g_v_t__t_jX+=g_v_t_t

                    #5
                    g_v__rho2_jX2=1j*g_v_X_X
                    g_v__rho2_jX2+=g_v_rho_rho

                    #5
                    meshed__cos_jsin__theta=1j*meshed_sin_theta
                    meshed__cos_jsin__theta+=meshed_cos_theta

                    #6
                    sqrt_lambda__pl_jmin=1j*sqrt_lambda_minus
                    sqrt_lambda__pl_jmin+=sqrt_lambda_plus

                    #7
                    G__t_t_jA=1j*A
                    G__t_t_jA+=G__t_t

                    #8
                    G__t__x_jrho=1j*G__t_rho
                    G__t__x_jrho+=G__t_x

                    #9
                    G__x__x_jrho=1j*G__x_rho
                    G__x__x_jrho+=G__x_x

                    #10
                    G_x__rho2_jphi2=1j*G__phi_phi
                    G_x__rho2_jphi2+=G__rho_rho



                    if len(right_dict)!=0 :

                        [hyp_flu1_x_t,\
                         hyp_flu1_x_x,\
                         hyp_flu1_rho_rho,\
                         hyp_flu1_t_t,\
                         hyp_flu1_cstt,\
                         hyp_flu1_phiphi,\
                         euc_flu1_x_t,\
                         euc_flu1_x_x,\
                         euc_flu1_rho_rho,\
                         euc_flu1_t_t,\
                         euc_flu1_cstt,\
                         euc_flu1_phiphi,\
                         hyp_flu2_x_t,\
                         hyp_flu2_x_x,\
                         hyp_flu2_rho_rho,\
                         hyp_flu2_t_t,\
                         hyp_flu2_cstt,\
                         hyp_flu2_phiphi,\
                         euc_flu2_x_t,\
                         euc_flu2_x_x,\
                         euc_flu2_rho_rho,\
                         euc_flu2_t_t,\
                         euc_flu2_cstt,\
                         euc_flu2_phiphi,\
                         hyp_flu3_x_t,\
                         hyp_flu3_x_x,\
                         hyp_flu3_rho_rho,\
                         hyp_flu3_t_t,\
                         hyp_flu3_cstt,\
                         hyp_flu3_phiphi,\
                         euc_flu3_x_t,\
                         euc_flu3_x_x,\
                         euc_flu3_rho_rho,\
                         euc_flu3_t_t,\
                         euc_flu3_cstt,\
                         euc_flu3_phiphi,\
                        ]=right_dict[0]['x']
                    else:
                        #initialisation of parameters
                        
                        #this below has to be done only once

                        hyp_flu1_x_t,\
                        hyp_flu1_x_x,\
                        hyp_flu1_rho_rho,\
                        hyp_flu1_t_t,\
                        hyp_flu1_cstt,\
                        hyp_flu1_phiphi,\
                        euc_flu1_x_t,\
                        euc_flu1_x_x,\
                        euc_flu1_rho_rho,\
                        euc_flu1_t_t,\
                        euc_flu1_cstt,\
                        euc_flu1_phiphi=list(np.random.rand(12)*2-1)

                        hyp_flu2_x_t,\
                        hyp_flu2_x_x,\
                        hyp_flu2_rho_rho,\
                        hyp_flu2_t_t,\
                        hyp_flu2_cstt,\
                        hyp_flu2_phiphi,\
                        euc_flu2_x_t,\
                        euc_flu2_x_x,\
                        euc_flu2_rho_rho,\
                        euc_flu2_t_t,\
                        euc_flu2_cstt,\
                        euc_flu2_phiphi=list(np.random.rand(12)*2-1)

                        hyp_flu3_x_t,\
                        hyp_flu3_x_x,\
                        hyp_flu3_rho_rho,\
                        hyp_flu3_t_t,\
                        hyp_flu3_cstt,\
                        hyp_flu3_phiphi,\
                        euc_flu3_x_t,\
                        euc_flu3_x_x,\
                        euc_flu3_rho_rho,\
                        euc_flu3_t_t,\
                        euc_flu3_cstt,\
                        euc_flu3_phiphi=list(np.random.rand(12)*2-1)

                    x0=np.array([hyp_flu1_x_t,\
                                 hyp_flu1_x_x,\
                                 hyp_flu1_rho_rho,\
                                 hyp_flu1_t_t,\
                                 hyp_flu1_cstt,\
                                 hyp_flu1_phiphi,\
                                 euc_flu1_x_t,\
                                 euc_flu1_x_x,\
                                 euc_flu1_rho_rho,\
                                 euc_flu1_t_t,\
                                 euc_flu1_cstt,\
                                 euc_flu1_phiphi,\
                                 hyp_flu2_x_t,\
                                 hyp_flu2_x_x,\
                                 hyp_flu2_rho_rho,\
                                 hyp_flu2_t_t,\
                                 hyp_flu2_cstt,\
                                 hyp_flu2_phiphi,\
                                 euc_flu2_x_t,\
                                 euc_flu2_x_x,\
                                 euc_flu2_rho_rho,\
                                 euc_flu2_t_t,\
                                 euc_flu2_cstt,\
                                 euc_flu2_phiphi,\
                                 hyp_flu3_x_t,\
                                 hyp_flu3_x_x,\
                                 hyp_flu3_rho_rho,\
                                 hyp_flu3_t_t,\
                                 hyp_flu3_cstt,\
                                 hyp_flu3_phiphi,\
                                 euc_flu3_x_t,\
                                 euc_flu3_x_x,\
                                 euc_flu3_rho_rho,\
                                 euc_flu3_t_t,\
                                 euc_flu3_cstt,\
                                 euc_flu3_phiphi,\
                                 ])

                    args=(g_n_t__t_jX,\
                          g_n__rho2_jX2,\
                          g__v_jn__phiphi,\
                          g_v_t__t_jX,\
                          g_v__rho2_jX2,\
                          meshed__cos_jsin__theta,\
                          sqrt_lambda__pl_jmin,\
                          G__t_t_jA,\
                          G__t__x_jrho,\
                          G__x__x_jrho,\
                          G_x__rho2_jphi2)

                    


                    #Iterative Methods for Optimization
                    #C.T. Kelley
                    #Frontiers in Applied Mathematics 18
                    ########################################################
                    # OPTIMIZATION METHODS
                    #######################################################
                    # res = scipy.optimize.minimize(minus_min_of_density_plus_pressure, x0, method='nelder-mead',\
                    #                               options={ 'disp': True,\
                    #                                         'maxfev': maxfev,\
                    #                                         'maxiter': maxfev,\
                    #                                         'adaptive':True\
                    #                               },\
                    #                               args=args,\
                    # )
                    #######################################################
                    # res = scipy.optimize.minimize(minus_min_of_density_plus_pressure, x0, method='Powell',\
                    #                               options={ 'disp': True,\
                    #                                         'maxfev': maxfev,\
                    #                                         'maxiter': maxfev,\
                    #                               },\
                    #                               args=args,\
                    # )
                    #######################################################
                    # print('\n ---------- \n METHOD BFGS \n ---------- \n ')
                    # res = scipy.optimize.minimize(minus_min_of_density_plus_pressure, x0, method='BFGS',\
                    #                               options={ 'disp': True,\
                    #                                         'maxfev': maxfev,\
                    #                                         'maxiter': maxfev,\
                    #                               },\
                    #                               args=args,\
                    # )
                    ######################################################
                    # does no work (jacobian needed)
                    # print('\n ---------- \n METHOD trust krylov \n ---------- \n ')
                    # res = scipy.optimize.minimize(minus_min_of_density_plus_pressure, x0, method='trust-krylov',\
                    #                               options={ 'disp': True,\
                    #                                         'maxfev': maxfev,\
                    #                                         'maxiter': maxfev,\
                    #                                         'inexact': True,\
                    #                               },\
                    #                               args=args,\
                    # )
                    ########################################################
                    # does no work (jacobian needed)
                    # print('\n ---------- \n METHOD Newton-CG \n ---------- \n ')
                    # res = scipy.optimize.minimize(minus_min_of_density_plus_pressure, x0, method='Newton-CG',\
                    #                               options={ 'disp': True,\
                    #                                         'maxfev': maxfev,\
                    #                                         'maxiter': maxfev,\
                    #                               },\
                    #                               args=args,\
                    # )
                    ########################################################
                    # does no work (jacobian needed)
                    # print('\n ---------- \n METHOD Newton-CG \n ---------- \n ')
                    # res = scipy.optimize.minimize(minus_min_of_density_plus_pressure, x0, method='trust-exact',\
                    #                               options={ 'disp': True,\
                    #                                         'maxfev': maxfev,\
                    #                                         'maxiter': maxfev,\
                    #                               },\
                    #                               args=args,\
                    # )
                    ########################################################
                    ##does work (jacobian NOT needed)
                    # print('\n ---------- \n METHOD SLSQP \n ---------- \n ')
                    # res = scipy.optimize.minimize(minus_min_of_density_plus_pressure, x0, method='SLSQP',\
                    #                               options={ 'disp': True,\
                    #                                         'maxfev': maxfev,\
                    #                                         'maxiter': maxfev,\
                    #                               },\
                    #                               args=args,\
                    # )

                    
                    # #######################################################

                    
                    # np.random.seed()
                    # coins=np.random.rand()
                    # if coins<1./3.:
                    #     np.random.seed(seed=42)
                    #     ######################################################
                    #     print('\n ------ \n scipy.opt.fmin___l bfgs b \n ------ \n ')

                    #     maxfev=70
                    #     maxls=120
                    #     print('maxfev'+str(maxfev)+',maxls='+str(maxls))

                    #     res=scipy.optimize.fmin_l_bfgs_b(minus_min_of_density_plus_pressure, x0,\
                    #                                      maxfun=maxfev,\
                    #                                      maxiter=maxfev,\
                    #                                      approx_grad=1,\
                    #                                      disp=0,\
                    #                                      args=args,\
                    #                                      maxls=maxls,\
                    #     )

                    #     print(res)
                    #     print("res \n line 2895")
                    #     print(type(res))
                    #     print("TYPE(res) \n line 2897")
                    #     new_dict_res={}
                    #     new_dict_res["x"]=res[0]
                    #     new_dict_res["f"]=res[1]
                    #     new_dict_res["d"]=res[2]

                    #     for key in list(new_dict_res.keys()):
                    #         if isinstance(new_dict_res[key],(np.ndarray, np.generic)):
                    #             new_dict_res[key]=new_dict_res[key].tolist()

                    #         if isinstance(new_dict_res[key],dict):
                    #             for subk in list(new_dict_res[key].keys()):
                    #                 if isinstance(new_dict_res[key][subk],(np.ndarray, np.generic)):
                    #                     new_dict_res[key][subk]=new_dict_res[key][subk].tolist()
                    #                 else:
                    #                     try:
                    #                         new_dict_res[key][subk] = new_dict_res[key][subk].decode()
                    #                     except :
                    #                         pass

                    #     res=new_dict_res
                    #     res["method-optim"]='scipy.optimize.fmin_l_bfgs_b(minus_min_of_density_plus_pressure, x0, maxfun=maxfev,maxiter=maxfev,approx_grad=1,disp=0,maxfev'+str(maxfev)+',maxls='+str(maxls)
                    #     res["fun"]=new_dict_res["f"]

                    #     ######################################################

                    #elif coins<2./3.  :
                    np.random.seed(seed=42)
                    ####################################################### below before samedi 27 avril 2019
                    maxfev=50000
                    print('\n ---------- \n METHOD POWELL+docu \n ---------- \n maxfev'+str(maxfev))

                    res = scipy.optimize.minimize(minus_min_of_density_plus_pressure, x0, method='Powell',\
                                                  options={ 'disp': True,\
                                                            'maxfev': maxfev,\
                                                            'maxiter': maxfev,\
                                                  },\
                                                  args=args,\
                    )

                    res["method-optim"]='scipy.optimize.minimize(minus_min_of_density_plus_pressure, x0, method=Powell,maxfev='+str(maxfev)

                    # else:
                    #     maxfev=7000
                    #     np.random.seed(seed=42)
                    #     print('\n ---------- \n METHOD least-squares +docu \n ---------- \n maxfev'+str(maxfev))
                    #     res = scipy.optimize.least_squares(minus_min_of_density_plus_pressure, x0,jac='3-point',\
                    #                                        max_nfev=maxfev,\
                    #                                        args=args,\
                    #                                        verbose=2,\
                    #     )

                    #     res["method-optim"]='scipy.optimize.least_squares(minus_min_of_density_plus_pressure, x0,verb2,jac=3-point,maxfev='+str(maxfev)

                    
                    


                    ########################################################
                    
                    # ########################################################

                    # print('\n ------- \n nelder \n ------- \n ')
                    # res = scipy.optimize.minimize(minus_min_of_density_plus_pressure, x0, method='nelder-mead',\
                    #                               options={ 'disp': True,\
                    #                                         'maxfev': maxfev,\
                    #                                         'maxiter': maxfev,\
                    #                                         'adaptive':True\
                    #                               },\
                    #                               args=args,\
                    # )
                    # res["method-optim"]='scipy.optimize.minimize(minus_min_of_density_plus_pressure, x0, adapative true,method=nelder-mead,maxfev='+str(maxfev)
                    

                    # ######################################################
                    # print("\n ------ \n scipy.optimize.fmin_tnc \n ------ \n ")
                    # maxfev=10
                    # res=scipy.optimize.fmin_tnc(minus_min_of_density_plus_pressure, x0,\
                    #                             maxfun=maxfev,\
                    #                             approx_grad=1,\
                    #                             disp=5,\
                    #                             args=args,\
                    # )


                    # new_dict_res={}
                    # new_dict_res["x"]=res[0]
                    # new_dict_res["nfeval"]=res[1]
                    # new_dict_res["rc"]=res[2]

                    
                    # res=new_dict_res
                    # res["method-optim"]='scipy.optimize.fmin_tnc(minus_min_of_density_plus_pressure, x0,maxfun='+str(maxfev)
                    # res["fun"]=minus_min_of_density_plus_pressure(new_dict_res["x"],*args)
                    ######################################################


                    
                    ################################
                    ################################


                    
                    for key in res.keys():
                        if 'fun' in key:
                            print(str(key)+'   :   '+str(res[key]))
                            res['fun']=np.float(res['fun'])
                            
                        if isinstance(res[key], np.ndarray):
                            res[key]=res[key].tolist()
                        if key=='final_simplex':
                            res[key]=res[key][0].tolist()
                    
                    print('-------------------------------')
                    print('\n')
                    res['warp_speed']=v
                    res['warp_acceleration']=v_t
                    res["LAMBDA_into_account"]=Lambda_cosmological_constant
                    res['timestamp']=str(datetime.datetime.now())
                    res['sigma']=sigma
                    res['rank']=True
                    

                    res['metric-fun']='minimize--Mean5-reborn----EOS-1thres----FANCY2--4overflow-minus-16rank-minus-NEC--2flu--2boosts--accel--max--newRealAccel'
                    

                    #########################################################""
                    #######################################################

                    
                    
                    list_to_save=[res]+list_of_old_dict_res
                    
                    with open(saving_filename, 'w') as fp:
                        fp.write(json.dumps(list_to_save))


                    
                    with open(saving_filename, 'r') as fd:
                        list_of_old_dict_res2=json.load(fd)

                    print(len(list_of_old_dict_res2))
                    print("len(list_of_old_dict_res2) \n line 3026 \n --------- \n ")

                    list_of_old_dict_res=list_of_old_dict_res2


                    print(saving_filename)
                    print('saving_filename \n line 3027 \n ')
                    print('\n ------------------------- \n ')
                    print('\n ------------------------- \n ')
                    
                    print('json is dumped! \n line 3032')
                    



                    

                

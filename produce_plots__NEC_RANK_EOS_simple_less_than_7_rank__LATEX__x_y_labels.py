#!/usr/bin/python
# -*- coding: utf-8 -*-

import datetime
import matplotlib.pyplot as plt

plt.gca()

import copy

import matplotlib as mpl
import os, sys

import time, sys
import numpy as np
import matplotlib
matplotlib.use('QT4Agg')

from matplotlib.font_manager import FontProperties
import cmath as mth
import math
import time
import _pickle as cPickle
import pickle as Pickle

import matplotlib.mlab as mlab
import scipy
import scipy.fftpack
import scipy.stats.stats
import scipy.special
import scipy.linalg
import scipy.signal

import ctypes
import random

import locale
import operator
from matplotlib.ticker import ScalarFormatter
import re
import decimal
locale.setlocale(locale.LC_NUMERIC, "")
import urllib
import string
import pandas as pd
from scipy import stats

import os

from matplotlib import rcParams
import datetime


from cmath import *

from collections import Counter

import copy

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.colors as colors


import numpy as np
from scipy import ndimage


from scipy.optimize import minimize

from numpy import linalg as LA

import scipy as scipy
import numpy
print(numpy.version.version)
print("numpy.version.version")


import json

from os import listdir
from os.path import isfile, join
print('--------------')
print(scipy.__version__)
print("scipy.__version__")
print('--------------')
import datetime      
from matplotlib import rc
from matplotlib import rcdefaults

from matplotlib.ticker import NullFormatter  # useful for `logit` scale
from matplotlib.ticker import MaxNLocator
from matplotlib.offsetbox import AnchoredText
import matplotlib as mpl
import matplotlib.pyplot as plt 
import matplotlib.ticker

from matplotlib import rc
import sys

import matplotlib as mpl

####################################################################################

def create_counterpart_everywhere(list_instead_of_dict2):


    all_keys=[]
    for ind_ in range(len(list_instead_of_dict2)):
        all_keys.extend(list(list_instead_of_dict2[ind_].keys()))
    all_keys=list(set(all_keys))
    list_of_index_where_somethings_amiss=[]
    for a in range(len(list_instead_of_dict2)): # we extract each dict
        the_dict=list_instead_of_dict2[a]
        
        if np.any([el not in list(the_dict.keys()) for el in all_keys]):
            el_list_missing=[el for el in all_keys if el not in list(the_dict.keys())]

            # en fait on complete quand on ne trouve pas la bonne clef, par une donnée (explicitement une donnée labelisée comme manquante)
            for el in el_list_missing:
                the_dict[el]=''
        list_instead_of_dict2[a]=the_dict
    #counterparts have been created for each key of list_b
    return list_instead_of_dict2
##########################################################################################################################


##########################################################################################################################
def create_some_new_features(list_instead_of_dict2,compute_max_std_min_mean_for_all_these_keys):
    
    for dict_b__ind in range(len(list_instead_of_dict2)):
        dict_b=list_instead_of_dict2[dict_b__ind]

        
        ###############
        #below we seek to
        #extract radius and theta of  [pressure max, pressure min], [density max, density min], NECV biggest violation,

            
        DIFF8OVER8SUM=np.array(dict_b["diff_over_sum"])

        DIFF8OVER8SUM8MODELED=np.array(dict_b['diff_over_sum__modeled'])

        einstein_tens_largest__map=np.array(dict_b["einstein_tens_largest__map"])

        einst_mixed_largest_value__map=np.array(dict_b["einst_mixed_largest_value__map"])

        sing_val_mat_6__map=np.array(dict_b["sing_val_mat_6"])
        



        ind__argmin_sing_val_6 = np.unravel_index(np.argmin(sing_val_mat_6__map, axis=None), sing_val_mat_6__map.shape)

        min_singular_value = np.min(np.array(dict_b["sing_val_mat_6"]))
        dict_b["min_singular_value__smallest_sv7"]=min_singular_value
        dict_b["einst_tens_largest__smallest_sv7"]=einstein_tens_largest__map[ind__argmin_sing_val_6]
        dict_b["einst_mixed_largest_value__smallest_sv7"]=einst_mixed_largest_value__map[ind__argmin_sing_val_6]
        ##################
        
        if '--2flu' not in dict_b["metric-fun"]:
            
            max_density= np.max([np.abs(np.array(dict_b["rho__p_x_1___real"])),\
                                 np.abs(np.array(dict_b["rho__p_x_2___real"])),\
                                 np.abs(np.array(dict_b["rho__p_x_3___real"]))])
            
            mat0=np.abs(np.array(dict_b["rho__p_x_1___real"]))
            ind__argmax_density__1 = np.unravel_index(np.argmax(mat0, axis=None), mat0.shape)
            max_1=mat0[ind__argmax_density__1]

            mat0=np.abs(np.array(dict_b["rho__p_x_2___real"]))
            ind__argmax_density__2 = np.unravel_index(np.argmax(mat0, axis=None), mat0.shape)
            max_2=mat0[ind__argmax_density__2]

            mat0=np.abs(np.array(dict_b["rho__p_x_3___real"]))
            ind__argmax_density__3 = np.unravel_index(np.argmax(mat0, axis=None), mat0.shape)
            max_3=mat0[ind__argmax_density__3]

            if max_density==max_3:
                ind__argmax_density=ind__argmax_density__3
            elif max_density==max_2:
                ind__argmax_density=ind__argmax_density__2
            elif max_density==max_1:
                ind__argmax_density=ind__argmax_density__1
            else:
                print('max_density==max_ NTH, not found !!! line 12849 \n ------ \n  "latex x y labels" 3 fluids')

            ################
            abs_rho1=np.abs(np.array(dict_b["rho__p_x_1___real"]))
            abs_rho2=np.abs(np.array(dict_b["rho__p_x_2___real"]))
            abs_rho3=np.abs(np.array(dict_b["rho__p_x_3___real"]))

            abs_rho1__min_sing_val=abs_rho1[ind__argmin_sing_val_6]#ind__argmin_sing_value]
            abs_rho2__min_sing_val=abs_rho2[ind__argmin_sing_val_6]#ind__argmin_sing_value]
            abs_rho3__min_sing_val=abs_rho3[ind__argmin_sing_val_6]#ind__argmin_sing_value]
            
            max_abs_rho__min_sing_val=np.max([abs_rho1__min_sing_val,\
                                              abs_rho2__min_sing_val,\
                                              abs_rho3__min_sing_val,\
            ])

            
            
                                              
                
        else:
            max_density= np.max([np.abs(np.array(dict_b["rho__p_x_1___real"])),\
                                 np.abs(np.array(dict_b["rho__p_x_2___real"]))])
            
            mat0=np.abs(np.array(dict_b["rho__p_x_1___real"]))
            ind__argmax_density__1 = np.unravel_index(np.argmax(mat0, axis=None), mat0.shape)
            max_1=mat0[ind__argmax_density__1]

            mat0=np.abs(np.array(dict_b["rho__p_x_2___real"]))
            ind__argmax_density__2 = np.unravel_index(np.argmax(mat0, axis=None), mat0.shape)
            max_2=mat0[ind__argmax_density__2]

            if max_density==max_2:
                ind__argmax_density=ind__argmax_density__2
            elif max_density==max_1:
                ind__argmax_density=ind__argmax_density__1
            else:
                print('max_density==max_ NTH, not found !!! line 12849 \n ------ \n  "latex x y labels" 2 fluids!!')
            
            ########
            # do the same for 2 fluids / vs min of 6th sing val 
            abs_rho1=np.abs(np.array(dict_b["rho__p_x_1___real"]))
            abs_rho2=np.abs(np.array(dict_b["rho__p_x_2___real"]))
            

            abs_rho1__min_sing_val=abs_rho1[ind__argmin_sing_val_6]#ind__argmin_sing_value]
            abs_rho2__min_sing_val=abs_rho2[ind__argmin_sing_val_6]#ind__argmin_sing_value]
            
            
            max_abs_rho__min_sing_val=np.max([abs_rho1__min_sing_val,\
                                              abs_rho2__min_sing_val,\
            ])
            



        dict_b["max_abs_rho__min_sing_val"]=max_abs_rho__min_sing_val
        
        dict_b["eins_mixed_largest___for_max_abs_density"]=einst_mixed_largest_value__map[ind__argmax_density]
        dict_b["sing_val_mat_6___for_max_abs_density"]=sing_val_mat_6__map[ind__argmax_density]

        dict_b["max_abs_density"]=max_density #density[ind__argmin_density__]


        dict_b["upper_bound___for_max_abs_density"]=7*dict_b["eins_mixed_largest___for_max_abs_density"]*(dict_b["sing_val_mat_6___for_max_abs_density"])**(-1)


        dict_b["RATIO_upper_bound__over__max_abs_density"]=(7.0*dict_b["eins_mixed_largest___for_max_abs_density"]*(dict_b["sing_val_mat_6___for_max_abs_density"])**(-1))/max_density


        dict_b["einst_mixed_largest_value__map__absolute_max"]=np.nanmax(einst_mixed_largest_value__map)

        ####################
        try:
            dict_b["diff_ov_sum_modeled__located_a_max_diff_ov_sum"]=DIFF8OVER8SUM8MODELED[ind]


        except:
            dict_b["diff_ov_sum_modeled__located_a_max_diff_ov_sum"]=""


        #################
        


        DIFF8OVER8SUM=np.array(dict_b["diff_over_sum"])

        DIFF8OVER8SUM8MODELED=np.array(dict_b['diff_over_sum__modeled'])



        

        ind = np.unravel_index(np.argmax(DIFF8OVER8SUM, axis=None), DIFF8OVER8SUM.shape)
        ####################
        try:
            dict_b["diff_ov_sum_modeled__located_a_max_diff_ov_sum"]=DIFF8OVER8SUM8MODELED[ind]


        except:
            dict_b["diff_ov_sum_modeled__located_a_max_diff_ov_sum"]=""

            


        ###############
        try:
            #########
            table=np.array(dict_b["sin_theta_errors"])
            dict_b["sin_theta_errors__biggest_RD_EinstTens"]=table[ind]
            sin_theta_err_brd_e=table[ind]
            
            table=np.array(dict_b["sing_val"])
            dict_b["sing_val__biggest_RD_EinstTens"]=table[ind]
            sing_val_brd_e=table[ind]


            
            #>>> sys.float_info.epsilon
            upper_boundary=sys.float_info.epsilon*2*sing_val_brd_e/np.sqrt(1-sin_theta_err_brd_e**2)+sin_theta_err_brd_e/np.sqrt(1-sin_theta_err_brd_e**2)*sing_val_brd_e**2
            dict_b["up_bo__biggestRDEinTens"]=upper_boundary
        except:
            dict_b["sin_theta_errors__biggest_RD_EinstTens"]=""
            dict_b["sing_val__biggest_RD_EinstTens"]=""
            dict_b["up_bo__biggestRDEinTens"]=""
            
            print(type(dict_b["sin_theta_errors"]))
            print("TYPE np.array(dict_b['sin_theta_errors']) \n line 12748 \n ---------- \n ")
            
            print(ind)
            print('ind \n line 12750 \n ----------- \n ')

            print(DIFF8OVER8SUM)
            print("DIFF8OVER8SUM \n line 12752 \n ----------- \n ")

            input()


        ###################
        for key_b in compute_max_std_min_mean_for_all_these_keys:
            
            if isinstance(dict_b[key_b],type(np.array([]))) or \
               isinstance(dict_b[key_b],type(list([]))):
                dict_b["np.nanmax("+key_b+")"]=np.nanmax(dict_b[key_b])
                dict_b["np.nanmin("+key_b+")"]=np.nanmin(dict_b[key_b])
                dict_b["np.nanstd("+key_b+")"]=np.nanstd(dict_b[key_b])
                dict_b["np.nanmean("+key_b+")"]=np.nanmean(dict_b[key_b])
            else:
                dict_b["np.nanmax("+key_b+")"]=''
                dict_b["np.nanmin("+key_b+")"]=''
                dict_b["np.nanstd("+key_b+")"]=''
                dict_b["np.nanmean("+key_b+")"]=''
        list_instead_of_dict2[dict_b__ind]=dict_b
    return list_instead_of_dict2
##########################################################################################################################

#############################################################
def retreat_output_list_of_dict(list_of_dict):


    # we want to extract the sum of positive mass
    # the sum of negative mass, and all sorts of quantities
    ########################################"""
    ########################################"""
    for dict_b in list_of_dict:
        dict_b['max_abs__max_abs_u_squared_plus_one']=np.max(np.abs(dict_b['max_abs_u_squared_plus_one']))
        dict_b['max_abs__max_abs_of_velocities_components']=np.max(np.abs(dict_b['max_abs_of_velocities_components']))

        dict_b['max__sing_val_mat_6']=np.max(dict_b['sing_val_mat_6'])
        dict_b['min__sing_val_mat_6']=np.min(dict_b['sing_val_mat_6'])
        dict_b['mean__sing_val_mat_6']=np.mean(dict_b['sing_val_mat_6'])
        dict_b['std__sing_val_mat_6']=np.std(dict_b['sing_val_mat_6'])

        
        if "--2flu" in dict_b["metric-fun"]:
            # 2 fluids
            dict_b['vol_dens_neg_sum__TOT']=dict_b['vol_dens_2_neg_sum']+dict_b['vol_dens_1_neg_sum']
            dict_b['vol_dens_pos_sum__TOT']=dict_b['vol_dens_2_pos_sum']+dict_b['vol_dens_1_pos_sum']

            dict_b['hyperbolic-relation--max-deviation']=np.max([np.abs(dict_b['gamma___2__j_relation___imag']),\
                                                                 np.abs(dict_b['gamma___1__j_relation___imag']),\
                                                                 ])
            dict_b['max-gamma']=np.max([np.abs(dict_b['gamma___2__j_relation___real']),\
                                        np.abs(dict_b['gamma___1__j_relation___real']),\
            ])

        else:
                    
            # 3 fluids
            dict_b['vol_dens_neg_sum__TOT']=dict_b['vol_dens_2_neg_sum']+dict_b['vol_dens_1_neg_sum']+dict_b['vol_dens_3_neg_sum']
            dict_b['vol_dens_pos_sum__TOT']=dict_b['vol_dens_2_pos_sum']+dict_b['vol_dens_1_pos_sum']+dict_b['vol_dens_3_pos_sum']

            dict_b['hyperbolic-relation--max-deviation']=np.max([np.abs(dict_b['gamma___3__j_relation___imag']),\
                                                                 np.abs(dict_b['gamma___2__j_relation___imag']),\
                                                                 np.abs(dict_b['gamma___1__j_relation___imag']),\
                                                                 ])
            dict_b['max-gamma']=np.max([np.abs(dict_b['gamma___3__j_relation___real']),\
                                        np.abs(dict_b['gamma___2__j_relation___real']),\
                                        np.abs(dict_b['gamma___1__j_relation___real']),\
            ])

            


    
    return list_of_dict
###########################################################################
###########################################################################
def create_new_list_of_features(list_fo_dict):
    
    
    all_keys=[]
    for ind_ in range(len(list_fo_dict)):
        
        all_keys.extend(list(list_fo_dict[ind_].keys()))
    all_keys=list(set(all_keys))
        
    return all_keys

################################################################################
################################################################################
def plot_one_label__all_cases_latex(label,dict_of_dfs__accel_decel__two_three__each_case_label,addendum='',plt='',sub_directory="",\
                                    dico_results_numeric_article={},\
                                    save_res_art=False,\
):



    rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
    rc('text', usetex=True)

    fig = plt.figure()

    ax = fig.add_subplot(1, 1, 1)

    dict_colors={}
    it_colors=0
    colors_set=['r','g','b','k']
    shift_set=range(len(colors_set))
    dict_shift={}
    

    for key in list(dict_of_dfs__accel_decel__two_three__each_case_label.keys()):
        dict_colors[key]=colors_set[it_colors]
        dict_shift[key]=shift_set[it_colors]
        it_colors+=1

    non_all_same_sign=["density_meanplotting"]


    concatenate_all_ys=[]
    

    ############
    #compute correct x ticks
    complete_x_list=[]
    for key in list(dict_of_dfs__accel_decel__two_three__each_case_label.keys()):
        ylabel=label

        latex_use=False
        df=dict_of_dfs__accel_decel__two_three__each_case_label[key]
        df=df.sort_values("warp_speed")
        x=list(df["warp_speed"])#_s)
        complete_x_list.extend(x)

    complete_x_list=list(set(complete_x_list))
    complete_x_list.sort()


    dict_from_x_str_to_xticks={r'$'+('%.3e' % complete_x_list[el_ind])+r'$':el_ind  for el_ind in range(len(complete_x_list))}

    dict_from_x_str_to_xticks___values={complete_x_list[el_ind]:el_ind  for el_ind in range(len(complete_x_list))}

    
    x_str_sticks=[r'$'+('%.3e' % el)+r'$'  for el in x]
    plt.xticks([ dict_from_x_str_to_xticks[el_b] for el_b in x_str_sticks],tuple(x_str_sticks),rotation=90)
        

    if save_res_art:
        dico_results_numeric_article[label]={"dict_from_x_str_to_xticks":dict_from_x_str_to_xticks}
    #########################
    for key in list(dict_of_dfs__accel_decel__two_three__each_case_label.keys()):

        

        #####################################
        ylabel=label

        latex_use=False
        df=dict_of_dfs__accel_decel__two_three__each_case_label[key]
        df=df.sort_values("warp_speed")

           
        
        if ylabel=='map--mean-rank':
            ylabel=r'map average of $\mathbf{[e \otimes e]}$ rank'
            xlabel=r'warp speeds (v/c)'

            key_label=key

            y_ticks=[2,3,4,5,6,7,8,9,10,11]
            y_ticks=np.array(y_ticks)/10.0+6
            plt.yticks(y_ticks)
            ax=plt.gca()
            ax.set_ylim(np.min(y_ticks),np.max(y_ticks))
            
            y=list(df[label])
            
        elif ylabel=='np.max(diff_sur_som)':

            ylabel=r'map max of RD'
            xlabel=r'warp speeds (v/c)'

            key_label=key
        
            y=list(df[label])
        elif ylabel.lower()=='current_eos'  :

            ylabel=r'MAEOS'
            xlabel=r'warp speeds (v/c)'

            key_label=key
        
            y=list(df[label]/3.)
        elif ylabel.lower()=='max-gamma'  :


            ylabel=r'$max(|y_{h}|)$'
            xlabel=r'warp speeds (v/c)'

            key_label=key
        
            y=list(df[label])
        elif ylabel.lower()=='hyperbolic-relation--max-deviation'  :


            ylabel=r'$\max( \Delta_{ h }( y_{h,m}) ) $'
            xlabel=r'warp speeds (v/c)'

            key_label=key
        
            y=list(df[label])
        elif ylabel.lower()=='\max(SCQ)'.lower() or ('SCQ'.lower() in ylabel.lower() and 'max' in ylabel.lower()) or ('np_max_condition_schwarzschild' in label)   or ('max' in label.lower() and 'condition' in label.lower() and 'schwarzschild'  in ylabel.lower()):
            # schwarzschld condition quantity

            ylabel=r'max(SCQ)'
            xlabel=r'warp speeds (v/c)'

            key_label=key
        
            y=list(df[label])


            
        elif ylabel.lower()=='vol_dens_neg_sum__TOT__sun_mass'.lower()  :

            ylabel=r'opposite of total negative mass (in solar mass)'

            xlabel=r'warp speeds (v/c)'
            key_label=key
            y=list(df[label])
        elif ylabel.lower()=='vol_dens_pos_sum__TOT__sun_mass'.lower()  :


            ylabel=r'total positive mass (in solar mass)'
            xlabel=r'warp speeds (v/c)'
            key_label=key
            y=list(df[label])
        elif ylabel.lower()=='current_NEC__density__kg_m_minus_3'.lower()  :


            ylabel=r'NECV'
            #\left[ kg m^{-3} \right]'

            xlabel=r'warp speeds (v/c)'
            key_label=key
            y=list(df[label])

        elif ylabel.lower()=='max_abs__max_abs_u_squared_plus_one'.lower()  :


            ylabel=r'$\max |u^2+1| $'
            xlabel=r'warp speeds (v/c)'
            key_label=key
            y=list(df[label])
        elif "RATIO_upper_bound__over__max_abs_density".lower() in ylabel.lower() : 
            ylabel=r'(upper bound)/(max abs. density)'
            xlabel=r'warp speeds (v/c)'
            key_label=key
            y=list(df[label])

        elif "density" in ylabel.lower() or "pressure" in ylabel.lower() or "einst" in ylabel.lower():
            ylabel=ylabel.replace('-',' ').replace('_',' ')
            xlabel=r'warp speeds (v/c)'
            key_label=key
            y=list(df[label])

            


        else:
            key_label=key
            xlabel='warp speeds (v/c)'
        
            y=list(df[label])
        #####################################
        if y==['' for el in y]:
            pass
                            
        ##################################################################################
        else:    
            

        
        

            concatenate_all_ys.extend(y)
            if label not in non_all_same_sign:
                try:
                    if y[0]<0:
                        y=[el*(-1.0) for el in y]
                except:
                    print(label)
                    print("label \n line 11029 \n ---------- \n ")
                    print(y)
                    print("y \n line 11098 \n ------- \n ")

                    raise

            means=np.array(list(df[label]))

            x=list(df["warp_speed"])#_s)
            
            if df["warp_acceleration"].unique().shape[0]>1:
                print(df["warp_acceleration"].unique().shape[0]>1)
                print('df["warp_acceleration"].unique().shape[0]>1')

                print(df["warp_acceleration"].unique().shape[0])
                print('df["warp_acceleration"].unique().shape[0]')
                print('line 10536')
                raise
            warp_accel_extracted=df["warp_acceleration"].unique()[0]





            ##############



            # loc works the same as it does with figures (though best doesn't work)
            # pad=5 will increase the size of padding between the border and text
            # borderpad=5 will increase the distance between the border and the axes
            # frameon=False will remove the box around the text

            if "--2flu" in df["metric-fun"].unique()[0]:
                nFluid_txt="2 fluids"

            else:
                nFluid_txt="3 fluids"
            nFluid__filename_txt=nFluid_txt.replace(' ','_') #"2fluids"
            if warp_accel_extracted>0:
                accel_txt="accel"

            else:
                accel_txt="decel"
            
            

            key_label_dict__fluid_nb={"A_2F":"A-2FM",\
                                      "D_2F":"D-2FM",\
                                      "A_3F":"A-3FM",\
                                      "D_3F":"D-3FM",\
            }
            dict_of_dfs__accel_decel__ONLYnrd__each_case_label={"A_2F":accel_two_fluids__df_nrd,\
                                                        "D_2F":decel_two_fluids__df_nrd,\
                                                        "A_3F":accel_three_fluids__df_nrd,\
                                                        "D_3F":decel_three_fluids__df_nrd}

            
            x_values_for_plot=np.array([dict_from_x_str_to_xticks___values[el] for el in x])

            if  label in ['nit',"nfev", "current_min_rank"]+non_all_same_sign or "map" in label.lower():
                ax.plot(x_values_for_plot+dict_shift[key]*0.1, y, ls='',marker='.',c=dict_colors[key],label=key_label_dict__fluid_nb[key_label])
            else:
                ax.semilogy(x_values_for_plot+dict_shift[key]*0.1, y, ls='',marker='.',c=dict_colors[key],label=key_label_dict__fluid_nb[key_label])
            
            for b_ind in range(len(x_values_for_plot)):

                ax.plot((x_values_for_plot[b_ind]+dict_shift[key]*0.1, x_values_for_plot[b_ind]+dict_shift[key]*0.1), (0, y[b_ind]), c=dict_colors[key])
        if save_res_art:
            y_save=[float(el) for el in y]
            x_save=[float(el) for el in x_values_for_plot]
                
            dico_results_numeric_article[label][key]={"xlabel":xlabel,\
                                                      "ylabel":ylabel,\
                                                      "y_values":list(y),\
                                                      "x_values":list(x_save),\
            }

    plt.gca().yaxis.set_minor_formatter(NullFormatter())
    mpl.rcParams['xtick.major.pad'] = 8
    mpl.rcParams['ytick.major.pad'] = 8
    
    if "RATIO_upper_bound__over__max_abs_density".lower() in ylabel.lower() or r'(upper bound)/(max abs. density)' in ylabel: 
        locmaj = matplotlib.ticker.LogLocator(base=10,numticks=4) 
        ax.yaxis.set_major_locator(locmaj) 
        ax.tick_params(axis='y', which='minor', bottom=False)

        max_y=np.max(y)
        min_y=np.min(y)

        max_int=int(np.ceil(np.log(max_y)/np.log(10)))
        min_int=int(np.floor(np.log(min_y)/np.log(10)))

        int_list_b=np.arange(min_int,max_int+1)
        ax.set_yticks([10**el for el in int_list_b])
        
    nan_max_y=np.nanmax(concatenate_all_ys)
    nan_min_y=np.nanmin(concatenate_all_ys)

    
    plt.xlabel(xlabel)    
    plt.ylabel(ylabel)
    plt.tight_layout()
    plt.grid(axis='y')


    if 'np' in label and \
       'max' in label and \
       "condition" in label and \
       "schwarzschild" in label:
        plt.hlines(1, 0, len(x),color='darksalmon')
    

    if "rank" in label.lower()  :
        legend = ax.legend(loc='lower left', shadow=True, fontsize='x-large')
        legend.get_frame().set_facecolor('C0')

    elif "diff_sur_som" in label.lower():

        legend = ax.legend(loc='best', shadow=True, fontsize='x-large')
        legend.get_frame().set_facecolor('C0')
    elif "max-gamma" in label.lower():
        pass
    else:
        legend = ax.legend(loc='best', shadow=True, fontsize='x-large')
        legend.get_frame().set_facecolor('C0')

    
    ###########################"
    ##########################"
    if len(addendum)!=0:
        plt.savefig(sub_directory+label.replace('(','_').replace(')','_').replace('.','_').replace('<','_').replace('/','_')+'___'+addendum+'.eps')
    else:
        plt.savefig(sub_directory+label.replace('(','_').replace(')','_').replace('.','_').replace('<','_').replace('/','_')+'__all_accel__allfluidmixes.eps')
    plt.cla()
    plt.clf()
    plt.close('all')
    mpl.rcParams.update(mpl.rcParamsDefault)
    if save_res_art:
        return dico_results_numeric_article
    else:
        return
####

###########################################################################

#############################################################################
##########################################################################################################################################
################################################################################
def plot_one_label__all_cases(label,dict_of_dfs__accel_decel__two_three__each_case_label,addendum='',plt='',sub_directory=""):


 
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    dict_colors={}
    it_colors=0
    colors_set=['r','g','b','k']
    shift_set=range(len(colors_set))
    dict_shift={}
    
 
    for key in list(dict_of_dfs__accel_decel__two_three__each_case_label.keys()):
        dict_colors[key]=colors_set[it_colors]
        dict_shift[key]=shift_set[it_colors]
        it_colors+=1

    non_all_same_sign=["density_meanplotting"]


    concatenate_all_ys=[]


    ###################################
    # we seek how many cases are found for each warp velocity
    for key in list(dict_of_dfs__accel_decel__two_three__each_case_label.keys()):
        print(key)
    print('line 788')
    print(dict_of_dfs__accel_decel__two_three__each_case_label[key])
    print('key   :  '+key)
    print('line 791  \n line --')
    print(dict_of_dfs__accel_decel__two_three__each_case_label[key].columns)
    print("dict_of_dfs__accel_decel__two_three__each_case_label[key].columns \n line 793 \n ---- \n ")

    print(list(dict_of_dfs__accel_decel__two_three__each_case_label[key].columns))
    print("list(dict_of_dfs__accel_decel__two_three__each_case_label[key].columns) \n line 796 \n --- \n ")
    for col_bidule in list(dict_of_dfs__accel_decel__two_three__each_case_label[key].columns):
        print(col_bidule)
    print('line 799 \n ------- \n ')

    print(np.unique(dict_of_dfs__accel_decel__two_three__each_case_label[key]["warp_speed"]))
    print('np.unique(dict_of_dfs__accel_decel__two_three__each_case_label[key]["warp_speed"]) \n line 802 \n ----- \ ')
    print(len(np.unique(dict_of_dfs__accel_decel__two_three__each_case_label[key]["warp_speed"])))
    print('len(np.unique(dict_of_dfs__accel_decel__two_three__each_case_label[key]["warp_speed"])) \n line 804 \n ---- \n ')
    print(len(dict_of_dfs__accel_decel__two_three__each_case_label[key]["warp_speed"]))
    print('len( NON UNIK !! (dict_of_dfs__accel_decel__two_three__each_case_label[key]["warp_speed"])) \n line 804 \n ---- \n ')
    
    
    
    #input()
    

    ############
    #compute correct x ticks
    complete_x_list=[]
    for key in list(dict_of_dfs__accel_decel__two_three__each_case_label.keys()):
        ylabel=label

        latex_use=False
        df=dict_of_dfs__accel_decel__two_three__each_case_label[key]
        df=df.sort_values("warp_speed")
        x=list(df["warp_speed"])
        complete_x_list.extend(x)

    complete_x_list=list(set(complete_x_list))
    complete_x_list.sort()
    dict_from_x_str_to_xticks={r'$'+('%.3e' % complete_x_list[el_ind])+r'$':el_ind  for el_ind in range(len(complete_x_list))}

    dict_from_x_str_to_xticks___values={complete_x_list[el_ind]:el_ind  for el_ind in range(len(complete_x_list))}

    
    x_str_sticks=[r'$'+('%.3e' % el)+r'$'  for el in x]
    plt.xticks([ dict_from_x_str_to_xticks[el_b] for el_b in x_str_sticks],tuple(x_str_sticks),rotation=90)
        

    
    #########################
    for key in list(dict_of_dfs__accel_decel__two_three__each_case_label.keys()):


        #####################################
        ylabel=label

        latex_use=False
        df=dict_of_dfs__accel_decel__two_three__each_case_label[key]
        df=df.sort_values("warp_speed")

        if ylabel=='map--mean-rank':
            ylabel=r'map average of matrix rank'


            xlabel=r'warp speeds (v/c)'

            key_label=key

            y_ticks=[2,3,4,5,6,7,8,9,10,11]
            y_ticks=np.array(y_ticks)/10.0+6
            plt.yticks(y_ticks)
            ax=plt.gca()
            ax.set_ylim(np.min(y_ticks),np.max(y_ticks))
            
            y=list(df[label])
        elif ylabel=='np.max(diff_sur_som)':

            ylabel=r'map max of RD'
            xlabel=r'warp speeds (v/c)'

            key_label=key
        
            y=list(df[label])
        elif ylabel.lower()=='current_eos'  :

            ylabel=r'MAEOS'
            xlabel=r'warp speeds (v/c)'

            key_label=key
        
            y=list(df[label]/3.)
        elif ylabel.lower()=='max-gamma'  :


            ylabel=r'max |y_h|'
            xlabel=r'warp speeds (v/c)'

            key_label=key
        
            y=list(df[label])
        elif ylabel.lower()=='hyperbolic-relation--max-deviation'  :

            ylabel=r'max Delta_h(y_{h,m})'
            xlabel=r'warp speeds (v/c)'

            key_label=key
        
            y=list(df[label])
        elif ylabel.lower()=='np.max(condition_schwarzschild)'  :
            # schwarzschld condition quantity
            ylabel=r'SCQ'
            xlabel=r'warp speeds (v/c)'

            key_label=key
        
            y=list(df[label])


            
        elif ylabel.lower()=='vol_dens_neg_sum__TOT__sun_mass'.lower()  :
            ylabel=r'opposite of total negative mass (in solar mass)'
            xlabel=r'warp speeds (v/c)'
            key_label=key
            y=list(df[label])
        elif ylabel.lower()=='vol_dens_pos_sum__TOT__sun_mass'.lower()  :

            ylabel=r'total positive mass (in solar mass)'
            xlabel=r'warp speeds (v/c)'
            key_label=key
            y=list(df[label])
        elif ylabel.lower()=='current_NEC__density__kg_m_minus_3'.lower()  :

            ylabel=r'NEC (kg/m^3)'
            xlabel=r'warp speeds (v/c)'
            key_label=key
            y=list(df[label])

        elif ylabel.lower()=='max_abs__max_abs_u_squared_plus_one'.lower()  :

            ylabel=r'max|u^2+1|'
            xlabel=r'warp speeds (v/c)'
            key_label=key
            y=list(df[label])



        else:
            key_label=key
            xlabel='warp speeds (v/c)'
        
            y=list(df[label])
        #####################################
        if y==['' for el in y]:
            if 0:
                print('-------------------------------')
            else:
                pass
                            
        ##################################################################################
        else:    


        
        

            concatenate_all_ys.extend(y)
            if label not in non_all_same_sign:
                try:
                    if y[0]<0:
                        y=[el*(-1.0) for el in y]
                except:
                    print(label)
                    print("label \n line 11029 \n ---------- \n ")
                    print(y)
                    print("y \n line 11098 \n ------- \n ")

                    raise

            means=np.array(list(df[label]))

            x=list(df["warp_speed"])#_s)

            if df["warp_acceleration"].unique().shape[0]>1:
                print(df["warp_acceleration"].unique().shape[0]>1)
                print('df["warp_acceleration"].unique().shape[0]>1')

                print(df["warp_acceleration"].unique().shape[0])
                print('df["warp_acceleration"].unique().shape[0]')
                print('line 10536')
                raise
            warp_accel_extracted=df["warp_acceleration"].unique()[0]





            ##############




            if "--2flu" in df["metric-fun"].unique()[0]:
                nFluid_txt="2 fluids"

            else:
                nFluid_txt="3 fluids"
            nFluid__filename_txt=nFluid_txt.replace(' ','_') 
            if warp_accel_extracted>0:
                accel_txt="accel"

            else:
                accel_txt="decel"


            x_values_for_plot=np.array([dict_from_x_str_to_xticks___values[el] for el in x])

            if  label in ['nit',"nfev", "current_min_rank"]+non_all_same_sign or "map" in label.lower():
                plt.plot(x_values_for_plot+dict_shift[key]*0.1, y, ls='',marker='.',c=dict_colors[key],label=key_label)


            else:

                plt.semilogy(x_values_for_plot+dict_shift[key]*0.1, y, ls='',marker='.',c=dict_colors[key],label=key_label)

            for b_ind in range(len(x_values_for_plot)):


                plt.plot((x_values_for_plot[b_ind]+dict_shift[key]*0.1, x_values_for_plot[b_ind]+dict_shift[key]*0.1), (0, y[b_ind]), c=dict_colors[key])


    
    plt.gca().yaxis.set_minor_formatter(NullFormatter())

    

    

    mpl.rcParams['xtick.major.pad'] = 8
    mpl.rcParams['ytick.major.pad'] = 8
    
    nan_max_y=np.nanmax(concatenate_all_ys)
    nan_min_y=np.nanmin(concatenate_all_ys)

    
    plt.xlabel(xlabel)    
    plt.ylabel(ylabel)
    plt.tight_layout()
    plt.grid(axis='y')


    if 'np' in label and \
       'max' in label and \
       "condition" in label and \
       "schwarzschild" in label:
        plt.hlines(1, 0, len(x),color='darksalmon')
    

    if "rank" in label.lower()  :
        legend = ax.legend(loc='lower left', shadow=True, fontsize='x-large')
        legend.get_frame().set_facecolor('C0')

    elif "diff_sur_som" in label.lower():

        legend = ax.legend(loc='best', shadow=True, fontsize='x-large')
        legend.get_frame().set_facecolor('C0')
    elif "max-gamma" in label.lower():
        pass
    else:
        legend = ax.legend(loc='best', shadow=True, fontsize='x-large')
        legend.get_frame().set_facecolor('C0')

    
    ###########################"
    ##########################"
    if len(addendum)!=0:
        plt.savefig(sub_directory+label.replace('(','_').replace(')','_').replace('.','_').replace('<','_').replace('/','_')+'___'+addendum+'.eps')
    else:
        plt.savefig(sub_directory+label.replace('(','_').replace(')','_').replace('.','_').replace('<','_').replace('/','_')+'__all_accel__allfluidmixes.eps')
    plt.cla()
    plt.clf()
    plt.close('all')
    if latex_use:
        rcdefaults()
    return
####

###########################################################################

################################################################################
def dataframe__from_list_of_dict__and_list_cols(list_b,list_instead_of_dict2):


    #check_all_keys / purge keys where the format is not the ight one
    
    dict_key_good={}
    ###################
    ##################
    for key in list_b:

        if 1:
            print(key)
            print("key \n line 10528 \n --------- \n ")
            
            for dict_b in list_instead_of_dict2: # we check all dicts -- each cases
                if key in list(dict_b.keys()):
                    dict_key_good[key]='ini'

                    if str(type(dict_b[key]))=="<class 'float'>":
                        dict_key_good[key]="True"
                        #pass
                    elif str(type(dict_b[key]))=="<class 'int'>":
                        dict_key_good[key]="True"
                    elif str(type(dict_b[key]))=="<class 'bool'>":
                        dict_key_good[key]="True"
                    elif str(type(dict_b[key]))=="<class 'numpy.float64'>":
                        dict_key_good[key]="True"
                    elif str(type(dict_b[key]))=="<class 'list'>":
                        dict_key_good[key]="False"




                        break
                    elif str(type(dict_b[key]))=="<class 'str'>":
                        #dict_key_good[key]=False
                        dict_key_good[key]="True"
                    else:

                        print('key  :   '+str(key))
                        print(type(dict_b[key]))
                        print("type(dict_b[key]) \n line 10503 \n ---------- \n ")
                        if len(dict_b[key])>0:
                            print(len(dict_b[key]))
                            print("len(dict_b[key]) \n line 10513 \n ------- \n ")
                        print(dict_b[key])
                        print("dict_b[key] \n line 10515 \n ----------- \n ")
                        print("BIZZARE ! bizarre")
                        input()
    ##################"""

    ##################"""
    list_b=[el for el in list_b if dict_key_good[el]!="False"]


    #################################################

    ###################################
    #create a function for this
    list_of_list=[]
    for colname in list_b:
        list_of_list.append([])
        # one list for each element in list_b
    ###################################    
    for dict_b in list_instead_of_dict2:
        for ind_list in range(len(list_b)):
            list_of_list[ind_list].append(dict_b[list_b[ind_list]])
    dict_to_make_df={}
    for colname_ind in range(len(list_b)):
        dict_to_make_df[list_b[colname_ind]]=list_of_list[colname_ind]

    df=pd.DataFrame(dict_to_make_df)
    return df

##########################################################################

####################################################################
if __name__ == "__main__":
    # the objective here is to produce graphs

    saving_directory_for_saving_files_produced_here="OUTPUTS--plots--regular--each-case"
    cwd=os.getcwd()

    complete_path_OUTPUTS__of__this_script=cwd+'/'+saving_directory_for_saving_files_produced_here+'/'

    complete_path_OUTPUTS__of__this_script_latex=cwd+'/'+saving_directory_for_saving_files_produced_here+'_latex'+'/'
    complete_path_OUTPUTS__of__this_script_latex_nrd=cwd+'/'+saving_directory_for_saving_files_produced_here+'_latex_nonrankdeficient'+'/'

    ######################

    #####################

    complete_path_OUTPUTS__of__dict_of_dict__2list_of_dicts=cwd+'/'+"OUTPUTS-OF--analyze_kip_threshold_date_2flu/"
    list_files=os.listdir(complete_path_OUTPUTS__of__dict_of_dict__2list_of_dicts)
    if "extracted_list_of_dicts__param_warpdrive_1.json" in list_files:
        new_filename_complete_filename=complete_path_OUTPUTS__of__dict_of_dict__2list_of_dicts+"extracted_list_of_dicts__param_warpdrive_1.json"

        

    
    with open(new_filename_complete_filename, 'r') as fd:
        list_instead_of_dict2=json.load(fd)

    ##################
    print(len(list_instead_of_dict2))
    print("len(list_instead_of_dict2) \n line 10985")
    timestamps= [list_instead_of_dict2[a]["timestamp"] for a in range(len(list_instead_of_dict2))]
    timestamps.sort()
    for el in timestamps:
        print(el)

    print('line 10988, waiting for input...')
    print('--------------------------------------------------')





        
    print(list_instead_of_dict2[0].keys())
    print("list_instead_of_dict2[0].keys() \n line 10619")
    print('------------------------------------' )#str(key)+'  :  '+str(list_instead_of_dict2[0][key]))
    for key in list(list_instead_of_dict2[0].keys()):
        print(key)
    print(' key in list(list_instead_of_dict2[0].keys()): \n line 10778 \n -------- \n ')

    print('--------------------------------------')
    
    #########################################################################
    #we complete method optim for cases where there is no such key
    for a in range(len(list_instead_of_dict2)):
        if "method-optim" not in list(list_instead_of_dict2[a].keys()):
            list_instead_of_dict2[a]["method-optim"]=''

    #########################################################################
    
    
    # below we add a lot of characteristics

    list_instead_of_dict2=retreat_output_list_of_dict(list_instead_of_dict2)

    ###########################################################################
    ######################################

            
    #################################
    
    ###############################################################################

        

    for key in list_instead_of_dict2[0].keys():
        if 'rank'!=key and 'status'!=key and 'direc'!=key and 'x'!=key and "min-ranks"!=key and 'MAX-w--EOS'!=key and 'MIN-p-plus-rho'!=key and (not \
           ("gamma___" in key and ("__j_relation___imag" in key or "__j_relation___real" in key))) and key not in ['max_abs_u_squared_plus_one','max_abs_of_velocities_components'] :
            print(str(key)+'  :  '+str(list_instead_of_dict2[0][key]))
            print('------------------------------------' )
        if "min-ranks"==key:
            print("MIN--->"+str(key)+'  :  '+str(np.min(np.array(list_instead_of_dict2[0][key]))))
            print('------------------------------------' )

    #######################
    # the next  functions used should be used in this order
    # first we make a counterpart for each key in all dicts -- this should be done only for keys that does not depend on the number of fluids
    # but actually we do not want to segregate these features right here (so we'll deal with them later)

    list_instead_of_dict2=create_counterpart_everywhere(list_instead_of_dict2)
    


    ###########################################################################
    ######################################
    #################################



    
    compute_max_std_min_mean_for_all_these_keys=['diff_over_sum__modeled']
    list_instead_of_dict2=create_some_new_features(list_instead_of_dict2,compute_max_std_min_mean_for_all_these_keys)

    
    list_b=create_new_list_of_features(list_instead_of_dict2)
    ##########################


    
    df=dataframe__from_list_of_dict__and_list_cols(list_b,list_instead_of_dict2)
    print(df)
    print("df line 10558 \n ----------- \n ")
    ##########################################################
    ##########################################################
    c__speed_of_light=2.99792458e8

    G__gravitational_cstt=6.67408e-11
    #add density in the right unit


    df['pressure_max__N_m_minus_2']=df[['pressure_max']].sum(axis=1)*c__speed_of_light**4/G__gravitational_cstt
    df['pressure_min__N_m_minus_2']=df[['pressure_min']].sum(axis=1)*c__speed_of_light**4/G__gravitational_cstt
    
    df['proxy_pressure_max__N_m_minus_2']=df[["p_X_max","p_rho_max","p_phi_max"]].sum(axis=1)*c__speed_of_light**4/G__gravitational_cstt
    df['proxy_pressure_min__N_m_minus_2']=df[["p_X_min","p_rho_min","p_phi_min"]].sum(axis=1)*c__speed_of_light**4/G__gravitational_cstt


    df['vol_dens_pos_sum__TOT__kg']=df[["vol_dens_pos_sum__TOT"]].sum(axis=1)*c__speed_of_light**2/G__gravitational_cstt
    df['vol_dens_neg_sum__TOT__kg']=df[["vol_dens_neg_sum__TOT"]].sum(axis=1)*c__speed_of_light**2/G__gravitational_cstt

    df['vol_dens_pos_sum__TOT__sun_mass']=df[["vol_dens_pos_sum__TOT"]].sum(axis=1)*c__speed_of_light**2/G__gravitational_cstt*1./(1.9884e30)
    df['vol_dens_neg_sum__TOT__sun_mass']=df[["vol_dens_neg_sum__TOT"]].sum(axis=1)*c__speed_of_light**2/G__gravitational_cstt*1./(1.9884e30)
    
    
    df['density_max__kg_m_minus_3']=df['density_max']*c__speed_of_light**2/G__gravitational_cstt
    df['density_min__kg_m_minus_3']=df['density_min']*c__speed_of_light**2/G__gravitational_cstt


    df['current_NEC__density__kg_m_minus_3']=df['current_NEC']*c__speed_of_light**2/G__gravitational_cstt


    list_b=[el for el in list(df.columns) ]
    print('extract features above in the plot \n line 10990')


    
    list_c=['current_EOS', 'current_NEC', 'current_min_rank', 'metric-fun', 'nfev',  'rank',  'timestamp',\
            'warp_acceleration', 'warp_speed']
    list_d=[ 'current_min_rank',"map--mean-rank", "method-optim",  'timestamp',\
            'metric-fun', 'warp_speed']


    
    two_fluids__df=df[df["metric-fun"].str.contains('--2flu')]
    three_fluids__df=df[~df["metric-fun"].str.contains('--2flu')]

    accel_two_fluids__df=two_fluids__df[two_fluids__df["warp_acceleration"]>=0]
    decel_two_fluids__df=two_fluids__df[two_fluids__df["warp_acceleration"]<=0]
    
    
    
    
    accel_three_fluids__df=three_fluids__df[three_fluids__df["warp_acceleration"]>=0]
    decel_three_fluids__df=three_fluids__df[three_fluids__df["warp_acceleration"]<=0]



    if 1:
        list_ws_d3=list(decel_three_fluids__df["warp_speed"].unique())
        list_ws_d3.sort()

        list_ws_a3=list(accel_three_fluids__df["warp_speed"].unique())
        list_ws_a3.sort()

        list_ws_a2=list(accel_two_fluids__df["warp_speed"].unique())
        list_ws_a2.sort()

        list_ws_d2=list(decel_two_fluids__df["warp_speed"].unique())
        list_ws_d2.sort()
        
    
    ###########################
    # we make plots separated per case

    list_of_dfs__accel_decel__two_three=[accel_two_fluids__df,decel_two_fluids__df,accel_three_fluids__df,decel_three_fluids__df]
    dict_of_dfs__accel_decel__two_three__each_case_label={"A_2F":accel_two_fluids__df,\
                                                          "D_2F":decel_two_fluids__df,\
                                                          "A_3F":accel_three_fluids__df,\
                                                          "D_3F":decel_three_fluids__df}
    dict_of_dfs__accel_decel__ONLYthree__each_case_label={"A_3F":accel_three_fluids__df,\
                                                          "D_3F":decel_three_fluids__df}

    
    accel_two_fluids__df_nrd=accel_two_fluids__df[(accel_two_fluids__df["map--mean-rank"]==7)]
    decel_two_fluids__df_nrd=decel_two_fluids__df[(decel_two_fluids__df["map--mean-rank"]==7)]
    accel_three_fluids__df_nrd=accel_three_fluids__df[(accel_three_fluids__df["map--mean-rank"]==7)]
    decel_three_fluids__df_nrd=decel_three_fluids__df[(decel_three_fluids__df["map--mean-rank"]==7)]


    dict_of_dfs__accel_decel__ONLYnrd__each_case_label={"A_2F":accel_two_fluids__df_nrd,\
                                                        "D_2F":decel_two_fluids__df_nrd,\
                                                        "A_3F":accel_three_fluids__df_nrd,\
                                                        "D_3F":decel_three_fluids__df_nrd}

    #non rank defects: NRD nrd

    
    ################## ####################################
    #below interesting but time consuming
    plt.gca()

    #plot__nec_eos_ranks(label,el)
    #########################################
    labels_2=[el for el in list_b if el!='warp_speed' and \
              el!='warp_acceleration' and \
              el!='metric-fun' and   \
              el!='direc' and \
              el!='min-ranks' \
              and el not in ['message', 'metric-fun', 'min-ranks', "timestamp",'x','method-optim','nfev','status','success','rank']
    ]

    labels_2=['pressure_max__N_m_minus_2',\
               'pressure_min__N_m_minus_2',\
               'density_max__kg_m_minus_3',\
               'density_min__kg_m_minus_3',\
               'current_NEC__density__kg_m_minus_3',\
    ]+labels_2

    for label in labels_2 :
        if 'map--mean-rank'.lower() in label.lower() or \
           'np.max(diff_sur_som)'.lower() in label.lower() or \
           'max-gamma'.lower() in label.lower() or \
           'hyperbolic-relation--max-deviation'.lower() in label.lower() or \
           'max_abs__max_abs_u_squared_plus_one'.lower() in label.lower() or \
           "current_NEC__density__kg_m_minus_3".lower() in label.lower() or \
           'max(SCQ)'.lower() in label.lower() or \
           'current_eos'.lower() in label.lower() or \
           'vol_dens_pos_sum__TOT__sun_mass'.lower() in label.lower() or \
           'vol_dens_neg_sum__TOT__sun_mass'.lower() in label.lower(): 
           

    
            plot_one_label__all_cases_latex(label,dict_of_dfs__accel_decel__two_three__each_case_label,plt=plt,sub_directory=complete_path_OUTPUTS__of__this_script_latex)


        if "map" in label or \
           "rank" in label or \
           "diff" in label or \
           "max-w--eos" in label or \
           "current_EOS" in label or \
           "sing_val__biggest_RD_EinstTens" in label:
            
            print(label +'  plotting ...')


            
            
            plot_one_label__all_cases(label,dict_of_dfs__accel_decel__two_three__each_case_label,plt=plt,sub_directory=complete_path_OUTPUTS__of__this_script)

    for label in labels_2 :

        if "map" in label or \
           "diff_over_sum" in label:
            print(label +'  plotting ONLY 3F ...')
            plot_one_label__all_cases(label,dict_of_dfs__accel_decel__ONLYthree__each_case_label,addendum="3FOnly",plt=plt,sub_directory=complete_path_OUTPUTS__of__this_script)




    dico_results_numeric_article={}
    for label in labels_2 :

        if "gamma" in label or \
           "current_eos" in label.lower() or \
           "hyperbolic-relation--max-deviation" in label.lower() or \
           "max_abs__max_abs_u_squared_plus_one" in label.lower() or \
           "max_abs__max_abs_of_velocities_components" in label.lower() or \
           "vol_dens_neg_sum__TOT" in label.lower() or \
           "vol_dens_pos_sum__TOT" in label.lower() or \
           "hyperbolic-relation--max-deviation" in label.lower() or \
           "max-gamma" in label.lower() or \
           "pressure_max__N_m_minus_2" in label.lower() or \
           "pressure_min__N_m_minus_2" in label.lower() or \
           "proxy_pressure_max__N_m_minus_2" in label.lower() or \
           "proxy_pressure_min__N_m_minus_2" in label.lower() or \
           "vol_dens_pos_sum__TOT__sun_mass" in label.lower() or \
           "vol_dens_neg_sum__TOT__sun_mass" in label.lower() or \
           "density_max__kg_m_minus_3" in label.lower() or \
           ("sing_val__biggest_RD_EinstTens").lower() in label.lower() or \
           ("schwarzschild").lower() in label.lower() or \
           ("current_nec").lower() in label.lower() or \
           label.lower()=='vol_dens_neg_sum__TOT__sun_mass'.lower()  or \
           label.lower()=='vol_dens_pos_sum__TOT__sun_mass'.lower()  or \
           label.lower()=='current_NEC__density__kg_m_minus_3'.lower() or \
           "plus_one".lower() in label.lower() :

            print(label +'  plotting ONLY no rank defects ...')
            plot_one_label__all_cases(label,dict_of_dfs__accel_decel__ONLYnrd__each_case_label,addendum="_noRankDefects",plt=plt,sub_directory=complete_path_OUTPUTS__of__this_script)

        if 'map--mean-rank'.lower() in label.lower() or \
           'np.max(diff_sur_som)'.lower() in label.lower() or \
           'max-gamma'.lower() in label.lower() or \
           'hyperbolic-relation--max-deviation'.lower() in label.lower() or \
           'max_abs__max_abs_u_squared_plus_one'.lower() in label.lower() or \
           "current_NEC__density__kg_m_minus_3".lower() in label.lower() or \
           'max(SCQ)'.lower() in label.lower() or \
           'current_eos'.lower() in label.lower() or \
           'vol_dens_pos_sum__TOT__sun_mass'.lower() in label.lower() or \
           'vol_dens_neg_sum__TOT__sun_mass'.lower() in label.lower() or \
           ('schwarzschild'.lower() in label.lower() and "max" in label.lower()) or \
           "pressure_max__N_m_minus_2".lower() in label.lower() or \
           "pressure_min__N_m_minus_2".lower() in label.lower() or \
           "density_max__kg_m_minus_3".lower() in label.lower() or \
           "density_min__kg_m_minus_3".lower() in label.lower() or \
           "einst".lower() in label.lower() or \
           "einst_mixed_largest_value__map__absolute_max" in label.lower() or \
           "RATIO_upper_bound__over__max_abs_density".lower() in label.lower(): 
        

    
            dico_results_numeric_article=plot_one_label__all_cases_latex(label,dict_of_dfs__accel_decel__ONLYnrd__each_case_label,addendum="_noRankDefects",plt=plt,\
                                                                         sub_directory=complete_path_OUTPUTS__of__this_script_latex_nrd,dico_results_numeric_article=dico_results_numeric_article,\
                                                                         save_res_art=True)
    saving_directory_for_saving_files_produced_here__res_art="2020-02-22---res_article"
    complete_path_OUTPUTS__of__this_script__res_art=cwd+'/'+saving_directory_for_saving_files_produced_here__res_art+'/'+'res_art.json'
    
    with open(complete_path_OUTPUTS__of__this_script__res_art, 'w') as fd:
        fd.write(json.dumps(dico_results_numeric_article,sort_keys=True, indent=4))    #  dicts in a list here
        

        
    with open(complete_path_OUTPUTS__of__this_script__res_art, 'r') as fd:
        bid465=json.load(fd)


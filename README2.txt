First install the libraries corresponding to this project

either globally or in a virtual environment

pip install -r requirements.txt

or 

pip3 install -r requirements.txt

########################



2020-03-01 --- 

PRINTING /DISPLAYING ---> objective : creating figures

we enter a terminal opened in this directory, and then enter

python3 analyze__drop_duplicates.py

it puts stuff in OUTPUTS--of--analyze__drop_duplicates

-------------
then we use directly

python3 analyze__explore_keep_only_best__all_indicators.py

it puts stuff in directory
OUTPUTS--of--analyze__explore_keep_only_best__all_indicators

and also in directory
OUTPUTS--of--analyze__explore_keep_only_best__all_indicators__list_of_dicts

This takes a few hours ... on a computer bought in 2017 : ideapad 700 lenovo.
--------
we then take the files from directory below to do some other stuff
OUTPUTS--of--analyze__explore_keep_only_best__all_indicators__list_of_dicts
----------------
i.e. we do the following
python3 analyze__keep_min_rank_optimum__threshold_date_2flu.py
it puts things in
OUTPUTS-OF--analyze_kip_threshold_date_2flu
---> this is a "small list" ! ###### /// it also prints in display screen  the mean ranks ---> for legacy purposes / record purposes

This small list can directly be used as an initial point for optimization / and can also be used as an initial point for drawing plots etc. 
----------------------

we can for instance do:
python3 produce_plots__NEC_RANK_EOS_simple_less_than_7_rank__LATEX__x_y_labels.py

----------------
results from 
OUTPUTS-OF--analyze_kip_threshold_date_2flu
are useful for the following processing:
python3 produce_plots__loglog.py
(2020-01-04)

----------------------

EXPLORE___produce_plots__NEC_RANK_EOS_simple_less_than_7_rank__LATEX__x_y_labels.py
enables to produce map of the density and EOS for each fluid of the 3FM in a (X,rho) plane (invariant by rotation around the [OX)) --> for v=(1e-5 * c) and v=1c, 
(other studied warp velocities are accessible via minor corrections in the code)

--------------------------------------------

OPTIMIZATION PROCESS ---> objective : increase the quality of the solution

it is possible to launch an optimization run for three-fluids-decel

going to the directory
three-fluids-decel-data

opening a terminal in this directory and launching:
python3 optimize_NEC_minus_rank_plus_penalty_overflow__mean__each_v__3fluids_LAMBDA.py

-------------
similarly:

three-fluids-accel-data
python3 optimize_NEC_minus_rank_plus_penalty_overflow__mean__each_v__3fluids_LAMBDA.py

two-fluids-decel-data
python3 optimize_NEC_minus_rank_plus_penalty_overflow__mean__each_v__2fluids_LAMBDA__2FD.py

two-fluids-accel-data
python3 optimize_NEC_minus_rank_plus_penalty_overflow__mean__each_v__2fluids_LAMBDA__2FA.py

NOTA BENE: there is NO automatic retrieval of parameter files inside the optimization directories, if you want to start from a new initial point in optimization you have to 
put the new parameter file in the right directory yourself, this right directory could be the following: three-fluids-decel-data, please leave only one parameter file at
 a time when starting from a fresh start
this new param file should be called 
param_warpdrive_1.json

---------------------------

python3 empty_param_file_except_necessary_requirement.py
attempts a deletion of all large files including images/figures
 

--------------------------------
-------------------------------------------------------------------


-----------------------------------------------
------------------------------------------------
-------------------------------------------------





#!/usr/bin/python
# -*- coding: utf-8 -*-

import datetime
import matplotlib.pyplot as plt


import copy

import matplotlib as mpl
import os, sys

import time, sys
import numpy as np
import matplotlib
matplotlib.use('QT4Agg')

from matplotlib.font_manager import FontProperties
import cmath as mth
import math
import time
import _pickle as cPickle
import pickle as Pickle

import matplotlib.mlab as mlab
import scipy
import scipy.fftpack
import scipy.stats.stats
import scipy.special
import scipy.linalg
import scipy.signal

import ctypes
import random

import locale
import operator
from matplotlib.ticker import ScalarFormatter
import re
import decimal
locale.setlocale(locale.LC_NUMERIC, "")
import urllib
import string
import pandas as pd
from scipy import stats

import os

from matplotlib import rcParams
import datetime


from cmath import *

from collections import Counter

import copy

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.colors as colors


import numpy as np
from scipy import ndimage


from scipy.optimize import minimize

from numpy import linalg as LA

import scipy as scipy
import numpy
print(numpy.version.version)
print("numpy.version.version")


import json

from os import listdir
from os.path import isfile, join
print('--------------')
print(scipy.__version__)
print("scipy.__version__")
print('--------------')
import datetime      

try:
    import cPickle as pickle
except ImportError:  # python 3.x
    import pickle




        
####################################################################
if __name__ == "__main__":

    ###########################################################################################
    # aim: extract min_rank==7

    cwd=os.getcwd()
    filename2="EKOBAI__list_of_dicts___best__param_warpdrive_1.json"
    complete_path_filename2=cwd+'/'+"OUTPUTS--of--analyze__explore_keep_only_best__all_indicators__list_of_dicts/"+filename2

    print('reading   '+complete_path_filename2+'... \n line 7443')
    
    
    with open(complete_path_filename2, 'r') as fd:
        complete_large_list_of_dicts2=json.load(fd)

    print(len(complete_large_list_of_dicts2))
    print("len(complete_large_list_of_dicts2) \n line 7662 \n ----------- \n ")
    print('---------')
    
    for k in complete_large_list_of_dicts2[0].keys():
        print(k)
    print('---------')
    print("complete_large_list_of_dicts2[0] KEYS  \n -------------- \n line 7461 \n ---- \n  THESE ARE THE FEATURES... ")
    #input()

    # below we extract parameters giving full rank over the full extension of the simuated warp bubble
    higher_ranks=[complete_large_list_of_dicts2[a] for a in range(len(complete_large_list_of_dicts2)) if np.min(complete_large_list_of_dicts2[a]['current_min_rank'])>=7]

    powells=[complete_large_list_of_dicts2[a]["timestamp"] for a in range(len(complete_large_list_of_dicts2)) if "method-optim" in list(complete_large_list_of_dicts2[a].keys()) and \
                  'powell' in complete_large_list_of_dicts2[a]['method-optim'].lower()]
    powells.sort()
    # above we extract timestamps for use of the powell method of optimization
    
    #on cherche le max des 3 fluides: et tous les 2 fluides faits après sont bons
    

    two_flu_timestamps=[complete_large_list_of_dicts2[a]["timestamp"] for a in range(len(complete_large_list_of_dicts2)) if \
                     "2flu" in complete_large_list_of_dicts2[a]["metric-fun"]]

    two_flu_timestamps__after_thres=[complete_large_list_of_dicts2[a]["timestamp"] \
                                     for a in range(len(complete_large_list_of_dicts2)) if \
                                     "2flu" in complete_large_list_of_dicts2[a]["metric-fun"] \
                                     and complete_large_list_of_dicts2[a]["timestamp"]>'2019-04-25 16:58:55.964530']

    two_flu__minranks___after_thres=[np.min(complete_large_list_of_dicts2[a]["min-ranks"]) \
                                     for a in range(len(complete_large_list_of_dicts2)) if \
                                     "2flu" in complete_large_list_of_dicts2[a]["metric-fun"] \
                                     and complete_large_list_of_dicts2[a]["timestamp"]>'2019-04-25 16:58:55.964530']

    two_flu__minranks__max___after_thres=[np.max(complete_large_list_of_dicts2[a]["min-ranks"]) \
                                          for a in range(len(complete_large_list_of_dicts2)) if \
                                          "2flu" in complete_large_list_of_dicts2[a]["metric-fun"] \
                                          and complete_large_list_of_dicts2[a]["timestamp"]>'2019-04-25 16:58:55.964530']


    two_flu_timestamps__after_thres.sort()

    for el in two_flu_timestamps__after_thres:
        print(el)
    print('------- \n line 7541')

    #le but est d'extraire tous les dico intéressants depuis higher rank

    
    # on recree le large_dict_of_best dict
    large_dict_of_best_dict={}



    #####################################################################
    # we do a first run for the largest rank (i.e. all the 3 fluids mixes)
    for a in range(len(higher_ranks)):
        dict_reloaded=higher_ranks[a]
        v=higher_ranks[a]['warp_speed']
        v_t=higher_ranks[a]['warp_acceleration']

        Lambda_cosmological_constant=higher_ranks[a]['LAMBDA_into_account']
        # print(Lambda_cosmological_constant)
        # print("Lambda_cosmological_constant \n line 177 \n ----- \n ")
        # print(Lambda_cosmological_constant==0)
        # print("Lambda_cosmological_constant==0 \n line 46")
        #######################"
        # whetever 
        # if not Lambda_cosmological_constant==0:
        #     print(Lambda_cosmological_constant==0)
        #     print('Lambda_cosmological_constant==0 \n line 182 \n ----\n')
        #     raise
        #input()
        ###########################
        if "2flu" in higher_ranks[a]['metric-fun']:
            nb_of_fluids=2
            if "2flu" not in  higher_ranks[a]['metric-fun']:
                print('this condition cannot be verified like...ever, if it is it should be raised! line 182 improbable ;)')
                raise
                dict_reloaded["metric-fun"]+="-2flu-"
            
            
        else:
            nb_of_fluids=3
            if "3flu" not in  higher_ranks[a]['metric-fun']:
                dict_reloaded["metric-fun"]+="-3flu-"

        
        #long_string_key='Lambda_cosmological_constant'+'___'+str(Lambda_cosmological_constant)
        long_string_key='Lambda_cosmological_constant'+'___'
        long_string_key+='____'+'nb_fluids'+'___'+str(nb_of_fluids)
        long_string_key+='____'+'warp_acceleration'+'___'+str(v_t)
        long_string_key+='____'+'warp_speed'+'___'+str(v)
        
    
        if v_t>0 and "accel" not in dict_reloaded["metric-fun"]:
            dict_reloaded["metric-fun"]+="-accel-"
        if v_t<0 and "decel" not in dict_reloaded["metric-fun"]:
            dict_reloaded["metric-fun"]+="-decel-"
        
    
        if long_string_key not in list(large_dict_of_best_dict.keys()):
            
            large_dict_of_best_dict[long_string_key]=dict_reloaded
            
        elif long_string_key in list(large_dict_of_best_dict.keys()):
            print(dict_reloaded['current_min_rank'])
            print("dict_reloaded['current_min_rank'] \n line 217 \n ---- \n ")
            #input()
            if large_dict_of_best_dict[long_string_key]['current_min_rank']<=dict_reloaded['current_min_rank']:
                # passé rank <= rank présent
                
                if large_dict_of_best_dict[long_string_key]["current_EOS"]> dict_reloaded['current_EOS'] and large_dict_of_best_dict[long_string_key]['current_NEC']< dict_reloaded['current_NEC'] :
                    # passé EOS> EOS PReSENT, NEC present>NEC PASSE

    
                    large_dict_of_best_dict[long_string_key]=dict_reloaded

    list__large_dict_of_best_dict__keys=list(large_dict_of_best_dict.keys())
    list__large_dict_of_best_dict__keys.sort()
    
    for eky in list__large_dict_of_best_dict__keys:
        print(eky)
    print('-------------------------------------- \n line 236 \n ---- \n ')
    print(len(large_dict_of_best_dict))
    print("len(large_dict_of_best_dict) \n line 7517 \n ------ \n ")
    #input()
    ########################
    # then we do a second run for the losers (rank deficiency)
    for a in range(len(complete_large_list_of_dicts2)):
        dict_reloaded=complete_large_list_of_dicts2[a]
        v=dict_reloaded['warp_speed']
        v_t=dict_reloaded['warp_acceleration']

        Lambda_cosmological_constant=dict_reloaded['LAMBDA_into_account']
        if "2flu" in dict_reloaded['metric-fun']:
            nb_of_fluids=2
            if "2flu" not in  dict_reloaded['metric-fun']:
                dict_reloaded["metric-fun"]+="-2flu-"
            
            
        else:
            nb_of_fluids=3
            if "3flu" not in  dict_reloaded['metric-fun']:
                dict_reloaded["metric-fun"]+="-3flu-"

        
        long_string_key='Lambda_cosmological_constant'+'___'
        long_string_key+='____'+'nb_fluids'+'___'+str(nb_of_fluids)
        long_string_key+='____'+'warp_acceleration'+'___'+str(v_t)
        long_string_key+='____'+'warp_speed'+'___'+str(v)
        
        if v_t>0 and "accel" not in dict_reloaded["metric-fun"]:
            dict_reloaded["metric-fun"]+="-accel-"
        if v_t<0 and "decel" not in dict_reloaded["metric-fun"]:
            dict_reloaded["metric-fun"]+="-decel-"
        
        if long_string_key not in list(large_dict_of_best_dict.keys()):
            
            large_dict_of_best_dict[long_string_key]=dict_reloaded
            
        elif long_string_key in list(large_dict_of_best_dict.keys()):
            if np.mean(dict_reloaded["min-ranks"])>=    np.mean(large_dict_of_best_dict[long_string_key]['min-ranks']):
                # passé rank < rank présent
                if 1:
                    # passé EOS> EOS PReSENT, NEC present>NEC PASSE


                    large_dict_of_best_dict[long_string_key]=dict_reloaded

    
    print(len(large_dict_of_best_dict))
    print("len(large_dict_of_best_dict) \n line 7517 \n ------ \n ")

    ######################################################
    # second run for losers above
    ##############################################################################
    


    
    keys_l=list(large_dict_of_best_dict.keys())
    keys_l.sort()
    for k in keys_l:
        
        print(k+': -------->>>>>>>>>        '+large_dict_of_best_dict[k]['timestamp']+'        mean rank='+str(np.mean(large_dict_of_best_dict[k]['min-ranks'])))
        large_dict_of_best_dict[k]['map--mean-rank']=np.mean(large_dict_of_best_dict[k]['min-ranks'])
    print('-----------------------------')



    #et maintenant sortir le bon format
    #ci dessous on fait une simple liste des dict

    small_dict_of_dicts2=large_dict_of_best_dict

    ################
    # ci dessous ça vient de ## dict_of_dict__2list_of_dicts.py

    
    list_instead_of_dict=[]
    print('-------------------------')
    print('-------------------------')

    for key  in list(small_dict_of_dicts2.keys()):

        list_instead_of_dict.append(small_dict_of_dicts2[key])
    print('-------------------------')
    print('-------------------------')




    complete_path_OUTPUTS__of__dict_of_dict__2list_of_dicts=cwd+'/'+"OUTPUTS-OF--analyze_kip_threshold_date_2flu/"
    
    new_filename_complete_filename=complete_path_OUTPUTS__of__dict_of_dict__2list_of_dicts+'extracted_list_of_dicts__param_warpdrive_1.json'
    with open(new_filename_complete_filename, 'w') as fd:
        fd.write(json.dumps(list_instead_of_dict))    #  dicts in a list here

    with open(new_filename_complete_filename, 'r') as fd:
        list_instead_of_dict2=json.load(fd)

    


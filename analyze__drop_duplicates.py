#!/usr/bin/python
# -*- coding: utf-8 -*-

import datetime
import matplotlib.pyplot as plt


import copy

import matplotlib as mpl
import os, sys

import time, sys
import numpy as np
import matplotlib
matplotlib.use('QT4Agg')

from matplotlib.font_manager import FontProperties
import cmath as mth
import math
import time
import _pickle as cPickle
import pickle as Pickle

import matplotlib.mlab as mlab
import scipy
import scipy.fftpack
import scipy.stats.stats
import scipy.special
import scipy.linalg
import scipy.signal

import ctypes
import random

import locale
import operator
from matplotlib.ticker import ScalarFormatter
import re
import decimal
locale.setlocale(locale.LC_NUMERIC, "")
import urllib
import string
import pandas as pd
from scipy import stats

import os

from matplotlib import rcParams
import datetime


from cmath import *

from collections import Counter

import copy

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

import matplotlib.colors as colors


import numpy as np
from scipy import ndimage


from scipy.optimize import minimize

from numpy import linalg as LA

import scipy as scipy
import numpy
print(numpy.version.version)
print("numpy.version.version")

import json

from os import listdir
from os.path import isfile, join
print('--------------')
print(scipy.__version__)
print("scipy.__version__")
print('--------------')
import datetime      

try:
    import cPickle as pickle
except ImportError:  # python 3.x
    import pickle




def maximum_absolute_value_of_einstein_tensor_components___myfunc(G__t_t,\
                                                                  G__t_rho,\
                                                                  G__t_x,\
                                                                  G__x_rho,\
                                                                  G__x_x,\
                                                                  G__phi_phi,\
                                                                  G__rho_rho,\
):
    return np.max(np.abs([G__t_t,\
                          G__t_rho,\
                          G__t_x,\
                          G__x_rho,\
                          G__x_x,\
                          G__phi_phi,\
                          G__rho_rho]))

#########################################################################################################################
def compute_each_space_box_minimum_nec___myfunc(epsilon_plus__p_x,epsilon_plus__p_rho,epsilon_plus__p_phi):
    return np.min([0,epsilon_plus__p_x,epsilon_plus__p_rho,epsilon_plus__p_phi])

#########################################################################################################################

######################################################################################################################################################################################
##############################################################################################################################################

def element_wise__compute__bidule___myfunc(g_n_t__t_jX,\
                                                       g_n__rho2_jX2,\
                                                       g__v_jn__phiphi,\
                                                       g_v_t__t_jX,\
                                                       g_v__rho2_jX2,\
                                                       meshed__cos_jsin__theta,\
                                                       sqrt_lambda__pl_jmin,\
                                                       G__t_t_jA,\
                                                       G__t__x_jrho,\
                                                       G__x__x_jrho,\
                                                       G_x__rho2_jphi2,\
                                                       hyp_coef_flu1_x__x_jt,\
                                                       hyp_coef_flu1__tt_jrhorho,\
                                                       hyp_coef_flu1__phiphi_jcstt,\
                                                       euc_coef_flu1_x__x_jt,\
                                                       euc_coef_flu1__tt_jrhorho,\
                                                       euc_coef_flu1__phiphi_jcstt,\
                                                       hyp_coef_flu2_x__x_jt,\
                                                       hyp_coef_flu2__tt_jrhorho,\
                                                       hyp_coef_flu2__phiphi_jcstt,\
                                                       euc_coef_flu2_x__x_jt,\
                                                       euc_coef_flu2__tt_jrhorho,\
                                                       euc_coef_flu2__phiphi_jcstt,\
                                                       hyp_coef_flu3_x__x_jt,\
                                                       hyp_coef_flu3__tt_jrhorho,\
                                                       hyp_coef_flu3__phiphi_jcstt,\
                                                       euc_coef_flu3_x__x_jt,\
                                                       euc_coef_flu3__tt_jrhorho,\
                                                       euc_coef_flu3__phiphi_jcstt,\
                                                       res_batch__1j_LAMBDA,\
):


    do_plot=False

    #1
    g_n_t_X=g_n_t__t_jX.imag
    g_n_t_t=g_n_t__t_jX.real

    #2
    g_n_X_X=g_n__rho2_jX2.imag
    g_n_rho_rho=g_n__rho2_jX2.real


    #3
    g_n_phi_phi=g__v_jn__phiphi.imag
    g_v_phi_phi=g__v_jn__phiphi.real



    #4
    g_v_t_X=g_v_t__t_jX.imag
    g_v_t_t=g_v_t__t_jX.real

    #5
    g_v_X_X=g_v__rho2_jX2.imag
    g_v_rho_rho=g_v__rho2_jX2.real

    #5
    meshed_sin_theta=meshed__cos_jsin__theta.imag
    meshed_cos_theta=meshed__cos_jsin__theta.real

    #6
    sqrt_lambda_minus=sqrt_lambda__pl_jmin.imag
    sqrt_lambda_plus=sqrt_lambda__pl_jmin.real

    #7
    A=G__t_t_jA.imag
    G__t_t=G__t_t_jA.real

    #8
    G__t_rho=G__t__x_jrho.imag
    G__t_x=G__t__x_jrho.real

    #9
    G__x_rho=G__x__x_jrho.imag
    G__x_x=G__x__x_jrho.real

    #10
    G__phi_phi=G_x__rho2_jphi2.imag
    G__rho_rho=G_x__rho2_jphi2.real

    # there are seven elements {tt tx trho , xx xrho, rhorho, phiphi}
    #11


    hyp_flu1_x_t=hyp_coef_flu1_x__x_jt.imag
    hyp_flu1_x_x=hyp_coef_flu1_x__x_jt.real

    #12
    hyp_flu1_rho_rho=hyp_coef_flu1__tt_jrhorho.imag
    hyp_flu1_t_t=hyp_coef_flu1__tt_jrhorho.real

    #13
    hyp_flu1_cstt=hyp_coef_flu1__phiphi_jcstt.imag
    hyp_flu1_phiphi=hyp_coef_flu1__phiphi_jcstt.real

    #14

    euc_flu1_x_t=euc_coef_flu1_x__x_jt.imag
    euc_flu1_x_x=euc_coef_flu1_x__x_jt.real


    euc_flu1_rho_rho=euc_coef_flu1__tt_jrhorho.imag
    euc_flu1_t_t=euc_coef_flu1__tt_jrhorho.real


    euc_flu1_cstt=euc_coef_flu1__phiphi_jcstt.imag
    euc_flu1_phiphi=euc_coef_flu1__phiphi_jcstt.real
    ################################
    hyp_flu2_x_t=hyp_coef_flu2_x__x_jt.imag
    hyp_flu2_x_x=hyp_coef_flu2_x__x_jt.real


    hyp_flu2_rho_rho=hyp_coef_flu2__tt_jrhorho.imag
    hyp_flu2_t_t=hyp_coef_flu2__tt_jrhorho.real


    hyp_flu2_cstt=hyp_coef_flu2__phiphi_jcstt.imag
    hyp_flu2_phiphi=hyp_coef_flu2__phiphi_jcstt.real



    euc_flu2_x_t=euc_coef_flu2_x__x_jt.imag
    euc_flu2_x_x=euc_coef_flu2_x__x_jt.real


    euc_flu2_rho_rho=euc_coef_flu2__tt_jrhorho.imag
    euc_flu2_t_t=euc_coef_flu2__tt_jrhorho.real


    euc_flu2_cstt=euc_coef_flu2__phiphi_jcstt.imag
    euc_flu2_phiphi=euc_coef_flu2__phiphi_jcstt.real

    ##################################
    hyp_flu3_x_t=hyp_coef_flu3_x__x_jt.imag
    hyp_flu3_x_x=hyp_coef_flu3_x__x_jt.real


    hyp_flu3_rho_rho=hyp_coef_flu3__tt_jrhorho.imag
    hyp_flu3_t_t=hyp_coef_flu3__tt_jrhorho.real


    hyp_flu3_cstt=hyp_coef_flu3__phiphi_jcstt.imag
    hyp_flu3_phiphi=hyp_coef_flu3__phiphi_jcstt.real



    euc_flu3_x_t=euc_coef_flu3_x__x_jt.imag
    euc_flu3_x_x=euc_coef_flu3_x__x_jt.real


    euc_flu3_rho_rho=euc_coef_flu3__tt_jrhorho.imag
    euc_flu3_t_t=euc_coef_flu3__tt_jrhorho.real


    euc_flu3_cstt=euc_coef_flu3__phiphi_jcstt.imag
    euc_flu3_phiphi=euc_coef_flu3__phiphi_jcstt.real





    #29
    res_batch=res_batch__1j_LAMBDA.real
    Lambda_cosmological_constant_=res_batch__1j_LAMBDA.imag
    





    

    #do_print_stuff=True
    do_print_stuff=False

    #do_print_stuff2=True
    do_print_stuff2=False


    do_print_stuff3=False
    inputs={}

    do_print_stuff4=False

    do_print_stuff5=False
    ######################################################
    # correction on g_phi,phi
    #below
    if g_n_phi_phi==0 or np.isinf(g_n_phi_phi):
        g_n_phi_phi=1

    if g_v_phi_phi==0 or np.isinf(g_v_phi_phi):
        g_v_phi_phi=1

    if np.isnan(g_v_phi_phi) or np.isnan(g_n_phi_phi):
        g_n_phi_phi=1
        g_v_phi_phi=1



    inputs["g_n_t_t"]=g_n_t_t
    inputs["g_n_t_X"]=g_n_t_X
    inputs["g_n_X_X"]=g_n_X_X
    inputs["g_n_rho_rho"]=g_n_rho_rho
    inputs["g_n_phi_phi"]=g_n_phi_phi


    inputs["g_v_t_t"]=g_v_t_t
    inputs["g_v_t_X"]=g_v_t_X
    inputs["g_v_X_X"]=g_v_X_X
    inputs["g_v_rho_rho"]=g_v_rho_rho
    inputs["g_v_phi_phi"]=g_v_phi_phi

    inputs["sqrt_lambda_minus"]=sqrt_lambda_minus
    inputs["sqrt_lambda_plus"]=sqrt_lambda_plus



    ########################################################
    # we compute G_doubly covariant!!
    G_doubly_cov=np.array( [ [ G__t_t   , G__t_x   , G__t_rho   , 0 ],\
                             [ G__t_x   , G__x_x   , G__x_rho   , 0 ],\
                             [ G__t_rho , G__x_rho , G__rho_rho , 0 ],\
                             [ 0        , 0        , 0          , G__phi_phi ],\
    ])
    #######################################################
    # below are the numpy equivalents of lines / rows
    
    g_n_mat=np.array([[inputs["g_n_t_t"],inputs["g_n_t_X"],0,0],\
                      [inputs["g_n_t_X"],inputs["g_n_X_X"],0,0],\
                      [0                ,0                ,inputs["g_n_rho_rho"],0],\
                      [0                ,0                ,0                    ,inputs["g_n_phi_phi"]]])

    g_v_mat=np.array([[inputs["g_v_t_t"],inputs["g_v_t_X"],0,0],\
                      [inputs["g_v_t_X"],inputs["g_v_X_X"],0,0],\
                      [0                ,0                ,inputs["g_v_rho_rho"],0],\
                      [0                ,0                ,0                    ,inputs["g_v_phi_phi"]]])



    lambda_plus=(-(A-1)+np.sqrt((A-1)**2+4))/2.
    lambda_minus=(-(A-1)-np.sqrt((A-1)**2+4))/2.



    tW=np.array([[meshed_cos_theta,0,meshed_sin_theta*1./sqrt_lambda_plus],\
                 [0,1,0],\
                 [-meshed_sin_theta*sqrt_lambda_plus,0,meshed_cos_theta],\
                 ])

    return_to_starting_block=True
    while return_to_starting_block:

        #print('choose tw4 ---> make a function which creates such matrices \n line 1640')



        #choose_tW_matrix, below

        
        tW_4,\
            tW_4_rot_no_curv,\
            tW_4_boo_no_curv=rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu1_x_t,\
                                                                     hyp_flu1_x_x,\
                                                                     hyp_flu1_rho_rho,\
                                                                     hyp_flu1_t_t,\
                                                                     hyp_flu1_cstt,\
                                                                     hyp_flu1_phiphi,\
                                                                     euc_flu1_x_t,\
                                                                     euc_flu1_x_x,\
                                                                     euc_flu1_rho_rho,\
                                                                     euc_flu1_t_t,\
                                                                     euc_flu1_cstt,\
                                                                     euc_flu1_phiphi,\
                                                                     inputs,\
        )

        tW_4_second_fluid,\
            tW_4_second_fluid_rot_no_curv,\
            tW_4_second_fluid_boo_no_curv=rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu2_x_t,\
                                                                                  hyp_flu2_x_x,\
                                                                                  hyp_flu2_rho_rho,\
                                                                                  hyp_flu2_t_t,\
                                                                                  hyp_flu2_cstt,\
                                                                                  hyp_flu2_phiphi,\
                                                                                  euc_flu2_x_t,\
                                                                                  euc_flu2_x_x,\
                                                                                  euc_flu2_rho_rho,\
                                                                                  euc_flu2_t_t,\
                                                                                  euc_flu2_cstt,\
                                                                                  euc_flu2_phiphi,\
                                                                                  inputs,\
        )

        tW_4_third_fluid,\
            tW_4_third_fluid_rot_no_curv,\
            tW_4_third_fluid_boo_no_curv=rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu3_x_t,\
                                                                                 hyp_flu3_x_x,\
                                                                                 hyp_flu3_rho_rho,\
                                                                                 hyp_flu3_t_t,\
                                                                                 hyp_flu3_cstt,\
                                                                                 hyp_flu3_phiphi,\
                                                                                 euc_flu3_x_t,\
                                                                                 euc_flu3_x_x,\
                                                                                 euc_flu3_rho_rho,\
                                                                                 euc_flu3_t_t,\
                                                                                 euc_flu3_cstt,\
                                                                                 euc_flu3_phiphi,\
                                                                                 inputs,\
        )



        #below is the g_munu diagonal version
        thediag=np.diag([lambda_plus,lambda_minus,1])
        thediag_4=np.diag([lambda_plus,lambda_minus,1,g_v_phi_phi])
        thediag_4_no_curve=np.diag([1,-1,1,1])#no_curv

        
        
        product=np.dot(tW.T,np.dot(thediag,tW))

        product_4_4__ini_fluid__tw4=np.dot(tW_4.T,np.dot(thediag_4,tW_4)) # this should be the same as the_diag_4
        product_4_4__2nd_fluid__tw4=np.dot(tW_4_second_fluid.T,np.dot(thediag_4,tW_4_second_fluid)) # this should be the same as the_diag_4
        product_4_4__3rd_fluid__tw4=np.dot(tW_4_third_fluid.T,np.dot(thediag_4,tW_4_third_fluid)) # this should be the same as the_diag_4

        

        ####    g_v_t_X=v*(1-meshed_f)

        N_plus=np.sqrt((lambda_plus-1)**2+g_v_t_X**2)
        N_minus=np.sqrt((lambda_minus-1)**2+g_v_t_X**2)

        #################################
        
        #eigen_vect_g_doublcovariant=EVGDCov
        #{EVGDCov=theta matrix} is the matrix of eigen vectors of g_{mu,nu}

        EVGDCov=np.array([[(lambda_plus-1.0)/N_plus,(lambda_minus-1.0)/N_minus,0,0],\
                         [g_v_t_X/(1.0*N_plus),g_v_t_X/(1.0*N_minus),0,0],\
                         [0,0,1.,0],\
                         [0,0,0,1.]])
        the_product_should_be_diagonal=np.dot(EVGDCov.T,np.dot(g_v_mat,EVGDCov))

        ##################################

        # the following is contravariant (each nested array is not a desirable vector rather this is component 1 of the first array and then comp 1 of the second, etc.
        e_not_prime=np.array([[-1,0,0,0],\
                              [g_v_t_X,1,0,0],\
                              [0,0,1,0],\
                              [0,0,0,1/np.sqrt(g_v_phi_phi) ],\
        ])


        # this line below check the orthonormality 
        the_product_should_be_minkowski_metric=np.dot(e_not_prime.T,np.dot(g_v_mat,e_not_prime))
        minkowski_metric=np.diag([-1,1,1,1])

        ####################################################


        e_prime=np.dot(EVGDCov,np.dot(tW_4,np.dot(EVGDCov.T,e_not_prime)))
        e_prime_2nd=np.dot(EVGDCov,np.dot(tW_4_second_fluid,np.dot(EVGDCov.T,e_not_prime)))
        e_prime_3rd=np.dot(EVGDCov,np.dot(tW_4_third_fluid,np.dot(EVGDCov.T,e_not_prime))) 
        # these two vectors are contravariant components

        e_prime_3rd_cov=np.dot(g_v_mat,e_prime_3rd)# in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures
        e_prime_2nd_cov=np.dot(g_v_mat,e_prime_2nd)# same



        # e_prime is described as sets of lines , each dissecting interesting vectors (vectors as columns)
        #e_prime here is contravariant
        #below we compute the covariant vectors in a single matrix

        
        e_prime_cov=np.dot(g_v_mat,e_prime)# in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures



        # we compute the mixed components of Einstein tensor
        G_einst_mixed_plus_lambda_delta=np.dot(g_n_mat,G_doubly_cov+Lambda_cosmological_constant_*g_v_mat )

        # in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures



        
        # at this stage e_prime_cov== e_p_mat.T
        #so let us check the orthonormality of these vectors


        this_should_be_minkowski__e_prime_computed_from_scrap=np.dot(e_prime_cov.T,np.dot(g_n_mat,e_prime_cov))

        this_should_be_minkowski__e_prime_2nd_computed_from_scrap=np.dot(e_prime_2nd_cov.T,np.dot(g_n_mat,e_prime_2nd_cov))


        this_should_be_minkowski_metr_as_well=np.dot(e_prime.T,np.dot(g_v_mat,e_prime))

        this_is_minkowskian_too=np.dot(e_prime.T,e_prime_cov)

        #####################"
        #############################################




        #############################################



        basis_vectors_3fluids=[[e_prime_cov,e_prime],[e_prime_2nd_cov,e_prime_2nd],[e_prime_3rd_cov,e_prime_3rd]]
        big_matrix_3flu,gee_vector_3flu=compute_big_matrix_for_fluids(basis_vectors_3fluids,G_einst_mixed_plus_lambda_delta)


        if np.any(np.isnan(big_matrix_3flu)) or np.any(np.isinf(big_matrix_3flu)) or \
           np.any(np.isnan(gee_vector_3flu)) or np.any(np.isinf(gee_vector_3flu)):
            return_to_starting_block=True

            hyp_flu1_x_t=hyp_flu1_x_t/3.
            hyp_flu1_x_x=hyp_flu1_x_x/3.
            hyp_flu1_rho_rho=hyp_flu1_rho_rho/3.
            hyp_flu1_t_t=hyp_flu1_t_t/3.
            hyp_flu1_cstt=hyp_flu1_cstt/3.
            hyp_flu1_phiphi=hyp_flu1_phiphi/3.

            hyp_flu2_x_t=hyp_flu2_x_t/3.
            hyp_flu2_x_x=hyp_flu2_x_x/3.
            hyp_flu2_rho_rho=hyp_flu2_rho_rho/3.
            hyp_flu2_t_t=hyp_flu2_t_t/3.
            hyp_flu2_cstt=hyp_flu2_cstt/3.
            hyp_flu2_phiphi=hyp_flu2_phiphi/3.

            hyp_flu3_x_t=hyp_flu3_x_t/3.
            hyp_flu3_x_x=hyp_flu3_x_x/3.
            hyp_flu3_rho_rho=hyp_flu3_rho_rho/3.
            hyp_flu3_t_t=hyp_flu3_t_t/3.
            hyp_flu3_cstt=hyp_flu3_cstt/3.
            hyp_flu3_phiphi=hyp_flu3_phiphi/3.
            # above if inf or nan we decrease values all coef

        else:
            return_to_starting_block=False
    ##############################################################


    rcond_here=None
    #rcond_here=1e-10

    densities_pressures_3fluids,\
        residuals_3fluids,\
        matrix_rank_3fluids,\
        sing_values_matrix_3fluids=np.linalg.lstsq(big_matrix_3flu, gee_vector_3flu,rcond=rcond_here)
    
    ##############################################################################################################################
    



    
    res=list(densities_pressures_3fluids)
    
    return bidule




####################################################################################################################################################################################


def element_wise__compute__theta_gamma___myfunc(g_n_t__t_jX,\
                                                                         g_n__rho2_jX2,\
                                                                         g__v_jn__phiphi,\
                                                                         g_v_t__t_jX,\
                                                                         g_v__rho2_jX2,\
                                                                         meshed__cos_jsin__theta,\
                                                                         sqrt_lambda__pl_jmin,\
                                                                         G__t_t_jA,\
                                                                         G__t__x_jrho,\
                                                                         G__x__x_jrho,\
                                                                         G_x__rho2_jphi2,\
                                                                         hyp_coef_flu1_x__x_jt,\
                                                                         hyp_coef_flu1__tt_jrhorho,\
                                                                         hyp_coef_flu1__phiphi_jcstt,\
                                                                         euc_coef_flu1_x__x_jt,\
                                                                         euc_coef_flu1__tt_jrhorho,\
                                                                         euc_coef_flu1__phiphi_jcstt,\
                                                                         hyp_coef_flu2_x__x_jt,\
                                                                         hyp_coef_flu2__tt_jrhorho,\
                                                                         hyp_coef_flu2__phiphi_jcstt,\
                                                                         euc_coef_flu2_x__x_jt,\
                                                                         euc_coef_flu2__tt_jrhorho,\
                                                                         euc_coef_flu2__phiphi_jcstt,\
                                                                         hyp_coef_flu3_x__x_jt,\
                                                                         hyp_coef_flu3__tt_jrhorho,\
                                                                         hyp_coef_flu3__phiphi_jcstt,\
                                                                         euc_coef_flu3_x__x_jt,\
                                                                         euc_coef_flu3__tt_jrhorho,\
                                                                         euc_coef_flu3__phiphi_jcstt,\
):
    
    Lambda_cosmological_constant=0
    #1.11e-52 #(m^(-2))


    do_plot=False

    #1
    g_n_t_X=g_n_t__t_jX.imag
    g_n_t_t=g_n_t__t_jX.real

    #2
    g_n_X_X=g_n__rho2_jX2.imag
    g_n_rho_rho=g_n__rho2_jX2.real


    #3
    g_n_phi_phi=g__v_jn__phiphi.imag
    g_v_phi_phi=g__v_jn__phiphi.real


    #4
    g_v_t_X=g_v_t__t_jX.imag
    g_v_t_t=g_v_t__t_jX.real

    #5
    g_v_X_X=g_v__rho2_jX2.imag
    g_v_rho_rho=g_v__rho2_jX2.real

    #5
    meshed_sin_theta=meshed__cos_jsin__theta.imag
    meshed_cos_theta=meshed__cos_jsin__theta.real

    #6
    sqrt_lambda_minus=sqrt_lambda__pl_jmin.imag
    sqrt_lambda_plus=sqrt_lambda__pl_jmin.real

    #7
    A=G__t_t_jA.imag
    G__t_t=G__t_t_jA.real

    #8
    G__t_rho=G__t__x_jrho.imag
    G__t_x=G__t__x_jrho.real

    #9
    G__x_rho=G__x__x_jrho.imag
    G__x_x=G__x__x_jrho.real

    #10
    G__phi_phi=G_x__rho2_jphi2.imag
    G__rho_rho=G_x__rho2_jphi2.real

    # there are seven tt tx trho , xx xrho, rhorho, phiphi
    #11


    hyp_flu1_x_t=hyp_coef_flu1_x__x_jt.imag
    hyp_flu1_x_x=hyp_coef_flu1_x__x_jt.real

    #12
    hyp_flu1_rho_rho=hyp_coef_flu1__tt_jrhorho.imag
    hyp_flu1_t_t=hyp_coef_flu1__tt_jrhorho.real

    #13
    hyp_flu1_cstt=hyp_coef_flu1__phiphi_jcstt.imag
    hyp_flu1_phiphi=hyp_coef_flu1__phiphi_jcstt.real

    #14

    euc_flu1_x_t=euc_coef_flu1_x__x_jt.imag
    euc_flu1_x_x=euc_coef_flu1_x__x_jt.real


    euc_flu1_rho_rho=euc_coef_flu1__tt_jrhorho.imag
    euc_flu1_t_t=euc_coef_flu1__tt_jrhorho.real


    euc_flu1_cstt=euc_coef_flu1__phiphi_jcstt.imag
    euc_flu1_phiphi=euc_coef_flu1__phiphi_jcstt.real
    ################################
    hyp_flu2_x_t=hyp_coef_flu2_x__x_jt.imag
    hyp_flu2_x_x=hyp_coef_flu2_x__x_jt.real


    hyp_flu2_rho_rho=hyp_coef_flu2__tt_jrhorho.imag
    hyp_flu2_t_t=hyp_coef_flu2__tt_jrhorho.real


    hyp_flu2_cstt=hyp_coef_flu2__phiphi_jcstt.imag
    hyp_flu2_phiphi=hyp_coef_flu2__phiphi_jcstt.real



    euc_flu2_x_t=euc_coef_flu2_x__x_jt.imag
    euc_flu2_x_x=euc_coef_flu2_x__x_jt.real


    euc_flu2_rho_rho=euc_coef_flu2__tt_jrhorho.imag
    euc_flu2_t_t=euc_coef_flu2__tt_jrhorho.real


    euc_flu2_cstt=euc_coef_flu2__phiphi_jcstt.imag
    euc_flu2_phiphi=euc_coef_flu2__phiphi_jcstt.real

    ##################################
    hyp_flu3_x_t=hyp_coef_flu3_x__x_jt.imag
    hyp_flu3_x_x=hyp_coef_flu3_x__x_jt.real


    hyp_flu3_rho_rho=hyp_coef_flu3__tt_jrhorho.imag
    hyp_flu3_t_t=hyp_coef_flu3__tt_jrhorho.real


    hyp_flu3_cstt=hyp_coef_flu3__phiphi_jcstt.imag
    hyp_flu3_phiphi=hyp_coef_flu3__phiphi_jcstt.real



    euc_flu3_x_t=euc_coef_flu3_x__x_jt.imag
    euc_flu3_x_x=euc_coef_flu3_x__x_jt.real


    euc_flu3_rho_rho=euc_coef_flu3__tt_jrhorho.imag
    euc_flu3_t_t=euc_coef_flu3__tt_jrhorho.real


    euc_flu3_cstt=euc_coef_flu3__phiphi_jcstt.imag
    euc_flu3_phiphi=euc_coef_flu3__phiphi_jcstt.real





    #29
    #res_batch=res_batch__1j_LAMBDA.real
    Lambda_cosmological_constant_=Lambda_cosmological_constant
                                  








    #do_print_stuff=True
    do_print_stuff=False

    #do_print_stuff2=True
    do_print_stuff2=False


    do_print_stuff3=False
    inputs={}

    do_print_stuff4=False

    do_print_stuff5=False
    ######################################################
    # correction on g_phi,phi
    #below
    if g_n_phi_phi==0 or np.isinf(g_n_phi_phi):
        g_n_phi_phi=1

    if g_v_phi_phi==0 or np.isinf(g_v_phi_phi):
        g_v_phi_phi=1

    if np.isnan(g_v_phi_phi) or np.isnan(g_n_phi_phi):
        g_n_phi_phi=1
        g_v_phi_phi=1



    inputs["g_n_t_t"]=g_n_t_t
    inputs["g_n_t_X"]=g_n_t_X
    inputs["g_n_X_X"]=g_n_X_X
    inputs["g_n_rho_rho"]=g_n_rho_rho
    inputs["g_n_phi_phi"]=g_n_phi_phi


    inputs["g_v_t_t"]=g_v_t_t
    inputs["g_v_t_X"]=g_v_t_X
    inputs["g_v_X_X"]=g_v_X_X
    inputs["g_v_rho_rho"]=g_v_rho_rho
    inputs["g_v_phi_phi"]=g_v_phi_phi

    inputs["sqrt_lambda_minus"]=sqrt_lambda_minus
    inputs["sqrt_lambda_plus"]=sqrt_lambda_plus



    ########################################################
    # we compute G_doubly covariant
    G_doubly_cov=np.array( [ [ G__t_t   , G__t_x   , G__t_rho   , 0 ],\
                             [ G__t_x   , G__x_x   , G__x_rho   , 0 ],\
                             [ G__t_rho , G__x_rho , G__rho_rho , 0 ],\
                             [ 0        , 0        , 0          , G__phi_phi ],\
    ])
    #######################################################
    # below are the numpy equivalents of lines / rows

    g_n_mat=np.array([[inputs["g_n_t_t"],inputs["g_n_t_X"],0,0],\
                      [inputs["g_n_t_X"],inputs["g_n_X_X"],0,0],\
                      [0                ,0                ,inputs["g_n_rho_rho"],0],\
                      [0                ,0                ,0                    ,inputs["g_n_phi_phi"]]])

    g_v_mat=np.array([[inputs["g_v_t_t"],inputs["g_v_t_X"],0,0],\
                      [inputs["g_v_t_X"],inputs["g_v_X_X"],0,0],\
                      [0                ,0                ,inputs["g_v_rho_rho"],0],\
                      [0                ,0                ,0                    ,inputs["g_v_phi_phi"]]])



    lambda_plus=(-(A-1)+np.sqrt((A-1)**2+4))/2.
    lambda_minus=(-(A-1)-np.sqrt((A-1)**2+4))/2.


    tW=np.array([[meshed_cos_theta,0,meshed_sin_theta*1./sqrt_lambda_plus],\
                 [0,1,0],\
                 [-meshed_sin_theta*sqrt_lambda_plus,0,meshed_cos_theta],\
                 ])

    return_to_starting_block=True
    while return_to_starting_block:

        #print('choose tw4 ---> make a function which creates such matrices \n line 1640')



        #choose_tW_matrix
        

        

        tW_4,\
            tW_4_rot_no_curv,\
            tW_4_boo_no_curv=rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu1_x_t,\
                                                                     hyp_flu1_x_x,\
                                                                     hyp_flu1_rho_rho,\
                                                                     hyp_flu1_t_t,\
                                                                     hyp_flu1_cstt,\
                                                                     hyp_flu1_phiphi,\
                                                                     euc_flu1_x_t,\
                                                                     euc_flu1_x_x,\
                                                                     euc_flu1_rho_rho,\
                                                                     euc_flu1_t_t,\
                                                                     euc_flu1_cstt,\
                                                                     euc_flu1_phiphi,\
                                                                     inputs,\
        )

        tW_4_second_fluid,\
            tW_4_second_fluid_rot_no_curv,\
            tW_4_second_fluid_boo_no_curv=rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu2_x_t,\
                                                                                  hyp_flu2_x_x,\
                                                                                  hyp_flu2_rho_rho,\
                                                                                  hyp_flu2_t_t,\
                                                                                  hyp_flu2_cstt,\
                                                                                  hyp_flu2_phiphi,\
                                                                                  euc_flu2_x_t,\
                                                                                  euc_flu2_x_x,\
                                                                                  euc_flu2_rho_rho,\
                                                                                  euc_flu2_t_t,\
                                                                                  euc_flu2_cstt,\
                                                                                  euc_flu2_phiphi,\
                                                                                  inputs,\
        )

        tW_4_third_fluid,\
            tW_4_third_fluid_rot_no_curv,\
            tW_4_third_fluid_boo_no_curv=rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu3_x_t,\
                                                                                 hyp_flu3_x_x,\
                                                                                 hyp_flu3_rho_rho,\
                                                                                 hyp_flu3_t_t,\
                                                                                 hyp_flu3_cstt,\
                                                                                 hyp_flu3_phiphi,\
                                                                                 euc_flu3_x_t,\
                                                                                 euc_flu3_x_x,\
                                                                                 euc_flu3_rho_rho,\
                                                                                 euc_flu3_t_t,\
                                                                                 euc_flu3_cstt,\
                                                                                 euc_flu3_phiphi,\
                                                                                 inputs,\
        )



        #below is the g_munu diagonal version
        thediag=np.diag([lambda_plus,lambda_minus,1])
        thediag_4=np.diag([lambda_plus,lambda_minus,1,g_v_phi_phi])
        thediag_4_no_curve=np.diag([1,-1,1,1])#no_curv



        product=np.dot(tW.T,np.dot(thediag,tW))

        product_4_4__ini_fluid__tw4=np.dot(tW_4.T,np.dot(thediag_4,tW_4)) # this should be the same as the_diag_4
        product_4_4__2nd_fluid__tw4=np.dot(tW_4_second_fluid.T,np.dot(thediag_4,tW_4_second_fluid)) # this should be the same as the_diag_4
        product_4_4__3rd_fluid__tw4=np.dot(tW_4_third_fluid.T,np.dot(thediag_4,tW_4_third_fluid)) # this should be the same as the_diag_4


        N_plus=np.sqrt((lambda_plus-1)**2+g_v_t_X**2)
        N_minus=np.sqrt((lambda_minus-1)**2+g_v_t_X**2)

        #################################
        #eigen_vect_g_doublcovariant=EVGDCov
        #{EVGDCov=theta matrix} is the matrix of eigen vectors of g_{mu,nu}

        EVGDCov=np.array([[(lambda_plus-1.0)/N_plus,(lambda_minus-1.0)/N_minus,0,0],\
                         [g_v_t_X/(1.0*N_plus),g_v_t_X/(1.0*N_minus),0,0],\
                         [0,0,1.,0],\
                         [0,0,0,1.]])
        the_product_should_be_diagonal=np.dot(EVGDCov.T,np.dot(g_v_mat,EVGDCov))

        ##################################

        # the following is contravariant (each nested array is not a desirable vector rather this is component 1 of the first array and then comp 1 of the second, etc.
        e_not_prime=np.array([[-1,0,0,0],\
                              [g_v_t_X,1,0,0],\
                              [0,0,1,0],\
                              [0,0,0,1/np.sqrt(g_v_phi_phi) ],\
        ])


        # this line below check the orthonormality 
        the_product_should_be_minkowski_metric=np.dot(e_not_prime.T,np.dot(g_v_mat,e_not_prime))
        
        
        minkowski_metric=np.diag([-1,1,1,1])
        
        ####################################################

        
        e_prime=np.dot(EVGDCov,np.dot(tW_4,np.dot(EVGDCov.T,e_not_prime)))
        e_prime_2nd=np.dot(EVGDCov,np.dot(tW_4_second_fluid,np.dot(EVGDCov.T,e_not_prime)))
        e_prime_3rd=np.dot(EVGDCov,np.dot(tW_4_third_fluid,np.dot(EVGDCov.T,e_not_prime))) 
        # these two vectors are contravariant components

        e_prime_3rd_cov=np.dot(g_v_mat,e_prime_3rd)# in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures
        e_prime_2nd_cov=np.dot(g_v_mat,e_prime_2nd)# same here



        # e_prime is described as sets of lines , each dissecting interesting vectors (vectors as columns)
        #e_prime here is contravariant
        #below we compute the covariant vectors in a single matrix


        e_prime_cov=np.dot(g_v_mat,e_prime)# in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures



        # we compute the mixed components of Einstein tensor
        G_einst_mixed_plus_lambda_delta=np.dot(g_n_mat,G_doubly_cov+Lambda_cosmological_constant_*g_v_mat )

        # in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures




        # at this stage e_prime_cov== e_p_mat.T
        #so let us check the orthonormality of these vectors


        this_should_be_minkowski__e_prime_computed_from_scrap=np.dot(e_prime_cov.T,np.dot(g_n_mat,e_prime_cov))

        this_should_be_minkowski__e_prime_2nd_computed_from_scrap=np.dot(e_prime_2nd_cov.T,np.dot(g_n_mat,e_prime_2nd_cov))


        this_should_be_minkowski_metr_as_well=np.dot(e_prime.T,np.dot(g_v_mat,e_prime))

        this_is_minkowskian_too=np.dot(e_prime.T,e_prime_cov)

        #####################"



        basis_vectors_3fluids=[[e_prime_cov,e_prime],[e_prime_2nd_cov,e_prime_2nd],[e_prime_3rd_cov,e_prime_3rd]]
        big_matrix_3flu,gee_vector_3flu=compute_big_matrix_for_fluids(basis_vectors_3fluids,G_einst_mixed_plus_lambda_delta)


        
        big_contra_ee_matrix,matrix__e_t__or__u__fluids=compute_big_matrix_for_fluids___display_properties(basis_vectors_3fluids,G_einst_mixed_plus_lambda_delta)

        if np.any(np.isnan(big_matrix_3flu)) or np.any(np.isinf(big_matrix_3flu)) or \
           np.any(np.isnan(gee_vector_3flu)) or np.any(np.isinf(gee_vector_3flu)):
            return_to_starting_block=True

            hyp_flu1_x_t=hyp_flu1_x_t/3.
            hyp_flu1_x_x=hyp_flu1_x_x/3.
            hyp_flu1_rho_rho=hyp_flu1_rho_rho/3.
            hyp_flu1_t_t=hyp_flu1_t_t/3.
            hyp_flu1_cstt=hyp_flu1_cstt/3.
            hyp_flu1_phiphi=hyp_flu1_phiphi/3.

            hyp_flu2_x_t=hyp_flu2_x_t/3.
            hyp_flu2_x_x=hyp_flu2_x_x/3.
            hyp_flu2_rho_rho=hyp_flu2_rho_rho/3.
            hyp_flu2_t_t=hyp_flu2_t_t/3.
            hyp_flu2_cstt=hyp_flu2_cstt/3.
            hyp_flu2_phiphi=hyp_flu2_phiphi/3.

            hyp_flu3_x_t=hyp_flu3_x_t/3.
            hyp_flu3_x_x=hyp_flu3_x_x/3.
            hyp_flu3_rho_rho=hyp_flu3_rho_rho/3.
            hyp_flu3_t_t=hyp_flu3_t_t/3.
            hyp_flu3_cstt=hyp_flu3_cstt/3.
            hyp_flu3_phiphi=hyp_flu3_phiphi/3.
            # above si inf ou nan on diminue la valeur de tous les coefficients


        else:
            return_to_starting_block=False
    ##############################################################

    
    rcond_here=None
    #rcond_here=1e-10
    densities_pressures_3fluids,\
        residuals_3fluids,\
        matrix_rank_3fluids,\
        sing_values_matrix_3fluids=np.linalg.lstsq(big_matrix_3flu, gee_vector_3flu,rcond=rcond_here)



    big_contra_ee_matrix=np.array(big_contra_ee_matrix)

    #ci dessous cov
    matrix__e_t__or__u__fluids=    np.array(matrix__e_t__or__u__fluids)
    nb_of_fluids=3
    rhos=[]
    pressures=[]
    g_v_in_vector=[g_v_t_t,\
                   g_v_t_X,\
                   0,\
                   0,\
                   g_v_t_X,\
                   g_v_X_X,\
                   0,\
                   0,\
                   0,\
                   0,\
                   g_v_rho_rho,\
                   0,\
                   0,\
                   0,\
                   0,\
                   g_v_phi_phi]

    for int_i in range(nb_of_fluids):

        big_contra_ee_matri__one_fluid=big_contra_ee_matrix[:,int_i*4:(int_i+1)*4]
        
        density_pressure__one_fluid=densities_pressures_3fluids[int_i*4:(int_i+1)*4]
        

        u_vector_one_fluid=matrix__e_t__or__u__fluids[int_i,:]
        uu_tensor_cov=[u_vector_one_fluid[i]*u_vector_one_fluid[j] for i in range(len(u_vector_one_fluid)) for j in range(len(u_vector_one_fluid))]


        #SET tensor, fluid
        T_tensor=np.dot(density_pressure__one_fluid,big_contra_ee_matri__one_fluid.T)
        rhos.append(np.dot(T_tensor,uu_tensor_cov))


        pressures.append(np.dot(T_tensor,np.array(uu_tensor_cov)+np.array(g_v_in_vector)))

    ###################################################################################
    return rhos[0]+1j*pressures[0],\
        rhos[1]+1j*pressures[1],\
        rhos[2]+1j*pressures[2]
    



#############################################################################################################



#######################################################################################################################################################################################

#####################################################
def element_wise__compute__pressures_densities_recalc___myfunc(g_n_t__t_jX,\
                                                                         g_n__rho2_jX2,\
                                                                         g__v_jn__phiphi,\
                                                                         g_v_t__t_jX,\
                                                                         g_v__rho2_jX2,\
                                                                         meshed__cos_jsin__theta,\
                                                                         sqrt_lambda__pl_jmin,\
                                                                         G__t_t_jA,\
                                                                         G__t__x_jrho,\
                                                                         G__x__x_jrho,\
                                                                         G_x__rho2_jphi2,\
                                                                         hyp_coef_flu1_x__x_jt,\
                                                                         hyp_coef_flu1__tt_jrhorho,\
                                                                         hyp_coef_flu1__phiphi_jcstt,\
                                                                         euc_coef_flu1_x__x_jt,\
                                                                         euc_coef_flu1__tt_jrhorho,\
                                                                         euc_coef_flu1__phiphi_jcstt,\
                                                                         hyp_coef_flu2_x__x_jt,\
                                                                         hyp_coef_flu2__tt_jrhorho,\
                                                                         hyp_coef_flu2__phiphi_jcstt,\
                                                                         euc_coef_flu2_x__x_jt,\
                                                                         euc_coef_flu2__tt_jrhorho,\
                                                                         euc_coef_flu2__phiphi_jcstt,\
                                                                         hyp_coef_flu3_x__x_jt,\
                                                                         hyp_coef_flu3__tt_jrhorho,\
                                                                         hyp_coef_flu3__phiphi_jcstt,\
                                                                         euc_coef_flu3_x__x_jt,\
                                                                         euc_coef_flu3__tt_jrhorho,\
                                                                         euc_coef_flu3__phiphi_jcstt,\
):
    
    Lambda_cosmological_constant=0
    #1.11e-52 #(m^(-2))


    do_plot=False

    #1
    g_n_t_X=g_n_t__t_jX.imag
    g_n_t_t=g_n_t__t_jX.real

    #2
    g_n_X_X=g_n__rho2_jX2.imag
    g_n_rho_rho=g_n__rho2_jX2.real


    #3
    g_n_phi_phi=g__v_jn__phiphi.imag
    g_v_phi_phi=g__v_jn__phiphi.real


    #4
    g_v_t_X=g_v_t__t_jX.imag
    g_v_t_t=g_v_t__t_jX.real

    #5
    g_v_X_X=g_v__rho2_jX2.imag
    g_v_rho_rho=g_v__rho2_jX2.real

    #5
    meshed_sin_theta=meshed__cos_jsin__theta.imag
    meshed_cos_theta=meshed__cos_jsin__theta.real

    #6
    sqrt_lambda_minus=sqrt_lambda__pl_jmin.imag
    sqrt_lambda_plus=sqrt_lambda__pl_jmin.real

    #7
    A=G__t_t_jA.imag
    G__t_t=G__t_t_jA.real

    #8
    G__t_rho=G__t__x_jrho.imag
    G__t_x=G__t__x_jrho.real

    #9
    G__x_rho=G__x__x_jrho.imag
    G__x_x=G__x__x_jrho.real

    #10
    G__phi_phi=G_x__rho2_jphi2.imag
    G__rho_rho=G_x__rho2_jphi2.real

    # there are seven tt tx trho , xx xrho, rhorho, phiphi
    #11


    hyp_flu1_x_t=hyp_coef_flu1_x__x_jt.imag
    hyp_flu1_x_x=hyp_coef_flu1_x__x_jt.real

    #12
    hyp_flu1_rho_rho=hyp_coef_flu1__tt_jrhorho.imag
    hyp_flu1_t_t=hyp_coef_flu1__tt_jrhorho.real

    #13
    hyp_flu1_cstt=hyp_coef_flu1__phiphi_jcstt.imag
    hyp_flu1_phiphi=hyp_coef_flu1__phiphi_jcstt.real

    #14

    euc_flu1_x_t=euc_coef_flu1_x__x_jt.imag
    euc_flu1_x_x=euc_coef_flu1_x__x_jt.real


    euc_flu1_rho_rho=euc_coef_flu1__tt_jrhorho.imag
    euc_flu1_t_t=euc_coef_flu1__tt_jrhorho.real


    euc_flu1_cstt=euc_coef_flu1__phiphi_jcstt.imag
    euc_flu1_phiphi=euc_coef_flu1__phiphi_jcstt.real
    ################################
    hyp_flu2_x_t=hyp_coef_flu2_x__x_jt.imag
    hyp_flu2_x_x=hyp_coef_flu2_x__x_jt.real


    hyp_flu2_rho_rho=hyp_coef_flu2__tt_jrhorho.imag
    hyp_flu2_t_t=hyp_coef_flu2__tt_jrhorho.real


    hyp_flu2_cstt=hyp_coef_flu2__phiphi_jcstt.imag
    hyp_flu2_phiphi=hyp_coef_flu2__phiphi_jcstt.real



    euc_flu2_x_t=euc_coef_flu2_x__x_jt.imag
    euc_flu2_x_x=euc_coef_flu2_x__x_jt.real


    euc_flu2_rho_rho=euc_coef_flu2__tt_jrhorho.imag
    euc_flu2_t_t=euc_coef_flu2__tt_jrhorho.real


    euc_flu2_cstt=euc_coef_flu2__phiphi_jcstt.imag
    euc_flu2_phiphi=euc_coef_flu2__phiphi_jcstt.real

    ##################################
    hyp_flu3_x_t=hyp_coef_flu3_x__x_jt.imag
    hyp_flu3_x_x=hyp_coef_flu3_x__x_jt.real


    hyp_flu3_rho_rho=hyp_coef_flu3__tt_jrhorho.imag
    hyp_flu3_t_t=hyp_coef_flu3__tt_jrhorho.real


    hyp_flu3_cstt=hyp_coef_flu3__phiphi_jcstt.imag
    hyp_flu3_phiphi=hyp_coef_flu3__phiphi_jcstt.real



    euc_flu3_x_t=euc_coef_flu3_x__x_jt.imag
    euc_flu3_x_x=euc_coef_flu3_x__x_jt.real


    euc_flu3_rho_rho=euc_coef_flu3__tt_jrhorho.imag
    euc_flu3_t_t=euc_coef_flu3__tt_jrhorho.real


    euc_flu3_cstt=euc_coef_flu3__phiphi_jcstt.imag
    euc_flu3_phiphi=euc_coef_flu3__phiphi_jcstt.real





    #29
    #res_batch=res_batch__1j_LAMBDA.real
    Lambda_cosmological_constant_=Lambda_cosmological_constant
                                  








    #do_print_stuff=True
    do_print_stuff=False

    #do_print_stuff2=True
    do_print_stuff2=False


    do_print_stuff3=False
    inputs={}

    do_print_stuff4=False

    do_print_stuff5=False
    ######################################################
    # correction on g_phi,phi
    #below
    if g_n_phi_phi==0 or np.isinf(g_n_phi_phi):
        g_n_phi_phi=1

    if g_v_phi_phi==0 or np.isinf(g_v_phi_phi):
        g_v_phi_phi=1

    if np.isnan(g_v_phi_phi) or np.isnan(g_n_phi_phi):
        g_n_phi_phi=1
        g_v_phi_phi=1



    inputs["g_n_t_t"]=g_n_t_t
    inputs["g_n_t_X"]=g_n_t_X
    inputs["g_n_X_X"]=g_n_X_X
    inputs["g_n_rho_rho"]=g_n_rho_rho
    inputs["g_n_phi_phi"]=g_n_phi_phi


    inputs["g_v_t_t"]=g_v_t_t
    inputs["g_v_t_X"]=g_v_t_X
    inputs["g_v_X_X"]=g_v_X_X
    inputs["g_v_rho_rho"]=g_v_rho_rho
    inputs["g_v_phi_phi"]=g_v_phi_phi

    inputs["sqrt_lambda_minus"]=sqrt_lambda_minus
    inputs["sqrt_lambda_plus"]=sqrt_lambda_plus



    ########################################################
    # we compute G_doubly covariant!!
    G_doubly_cov=np.array( [ [ G__t_t   , G__t_x   , G__t_rho   , 0 ],\
                             [ G__t_x   , G__x_x   , G__x_rho   , 0 ],\
                             [ G__t_rho , G__x_rho , G__rho_rho , 0 ],\
                             [ 0        , 0        , 0          , G__phi_phi ],\
    ])
    #######################################################
    # below are the numpy equivalents of lines / rows

    g_n_mat=np.array([[inputs["g_n_t_t"],inputs["g_n_t_X"],0,0],\
                      [inputs["g_n_t_X"],inputs["g_n_X_X"],0,0],\
                      [0                ,0                ,inputs["g_n_rho_rho"],0],\
                      [0                ,0                ,0                    ,inputs["g_n_phi_phi"]]])

    g_v_mat=np.array([[inputs["g_v_t_t"],inputs["g_v_t_X"],0,0],\
                      [inputs["g_v_t_X"],inputs["g_v_X_X"],0,0],\
                      [0                ,0                ,inputs["g_v_rho_rho"],0],\
                      [0                ,0                ,0                    ,inputs["g_v_phi_phi"]]])



    lambda_plus=(-(A-1)+np.sqrt((A-1)**2+4))/2.
    lambda_minus=(-(A-1)-np.sqrt((A-1)**2+4))/2.



    tW=np.array([[meshed_cos_theta,0,meshed_sin_theta*1./sqrt_lambda_plus],\
                 [0,1,0],\
                 [-meshed_sin_theta*sqrt_lambda_plus,0,meshed_cos_theta],\
                 ])

    return_to_starting_block=True
    while return_to_starting_block:

        #print('choose tw4 ---> make a function which creates such matrices \n line 1640')
        #choose_tW_matrix


        tW_4,\
            tW_4_rot_no_curv,\
            tW_4_boo_no_curv=rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu1_x_t,\
                                                                     hyp_flu1_x_x,\
                                                                     hyp_flu1_rho_rho,\
                                                                     hyp_flu1_t_t,\
                                                                     hyp_flu1_cstt,\
                                                                     hyp_flu1_phiphi,\
                                                                     euc_flu1_x_t,\
                                                                     euc_flu1_x_x,\
                                                                     euc_flu1_rho_rho,\
                                                                     euc_flu1_t_t,\
                                                                     euc_flu1_cstt,\
                                                                     euc_flu1_phiphi,\
                                                                     inputs,\
        )

        tW_4_second_fluid,\
            tW_4_second_fluid_rot_no_curv,\
            tW_4_second_fluid_boo_no_curv=rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu2_x_t,\
                                                                                  hyp_flu2_x_x,\
                                                                                  hyp_flu2_rho_rho,\
                                                                                  hyp_flu2_t_t,\
                                                                                  hyp_flu2_cstt,\
                                                                                  hyp_flu2_phiphi,\
                                                                                  euc_flu2_x_t,\
                                                                                  euc_flu2_x_x,\
                                                                                  euc_flu2_rho_rho,\
                                                                                  euc_flu2_t_t,\
                                                                                  euc_flu2_cstt,\
                                                                                  euc_flu2_phiphi,\
                                                                                  inputs,\
        )

        tW_4_third_fluid,\
            tW_4_third_fluid_rot_no_curv,\
            tW_4_third_fluid_boo_no_curv=rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu3_x_t,\
                                                                                 hyp_flu3_x_x,\
                                                                                 hyp_flu3_rho_rho,\
                                                                                 hyp_flu3_t_t,\
                                                                                 hyp_flu3_cstt,\
                                                                                 hyp_flu3_phiphi,\
                                                                                 euc_flu3_x_t,\
                                                                                 euc_flu3_x_x,\
                                                                                 euc_flu3_rho_rho,\
                                                                                 euc_flu3_t_t,\
                                                                                 euc_flu3_cstt,\
                                                                                 euc_flu3_phiphi,\
                                                                                 inputs,\
        )



        #below is the g_munu diagonal version
        thediag=np.diag([lambda_plus,lambda_minus,1])
        thediag_4=np.diag([lambda_plus,lambda_minus,1,g_v_phi_phi])
        thediag_4_no_curve=np.diag([1,-1,1,1])#no_curv



        product=np.dot(tW.T,np.dot(thediag,tW))

        product_4_4__ini_fluid__tw4=np.dot(tW_4.T,np.dot(thediag_4,tW_4)) # this should be the same as the_diag_4
        product_4_4__2nd_fluid__tw4=np.dot(tW_4_second_fluid.T,np.dot(thediag_4,tW_4_second_fluid)) # this should be the same as the_diag_4
        product_4_4__3rd_fluid__tw4=np.dot(tW_4_third_fluid.T,np.dot(thediag_4,tW_4_third_fluid)) # this should be the same as the_diag_4


        N_plus=np.sqrt((lambda_plus-1)**2+g_v_t_X**2)
        N_minus=np.sqrt((lambda_minus-1)**2+g_v_t_X**2)

        #################################

        #eigen_vect_g_doublcovariant=EVGDCov
        #{EVGDCov=theta matrix} is the matrix of eigen vectors of g_{mu,nu}

        EVGDCov=np.array([[(lambda_plus-1.0)/N_plus,(lambda_minus-1.0)/N_minus,0,0],\
                         [g_v_t_X/(1.0*N_plus),g_v_t_X/(1.0*N_minus),0,0],\
                         [0,0,1.,0],\
                         [0,0,0,1.]])
        the_product_should_be_diagonal=np.dot(EVGDCov.T,np.dot(g_v_mat,EVGDCov))

        ##################################

        # the following is contravariant (each nested array is not a desirable vector rather this is component 1 of the first array and then comp 1 of the second, etc.
        e_not_prime=np.array([[-1,0,0,0],\
                              [g_v_t_X,1,0,0],\
                              [0,0,1,0],\
                              [0,0,0,1/np.sqrt(g_v_phi_phi) ],\
        ])


        # this line below check the orthonormality 
        the_product_should_be_minkowski_metric=np.dot(e_not_prime.T,np.dot(g_v_mat,e_not_prime))
        minkowski_metric=np.diag([-1,1,1,1])
        
        ####################################################

        
        e_prime=np.dot(EVGDCov,np.dot(tW_4,np.dot(EVGDCov.T,e_not_prime)))
        e_prime_2nd=np.dot(EVGDCov,np.dot(tW_4_second_fluid,np.dot(EVGDCov.T,e_not_prime)))
        e_prime_3rd=np.dot(EVGDCov,np.dot(tW_4_third_fluid,np.dot(EVGDCov.T,e_not_prime))) 
        # these two vectors are contravariant components

        e_prime_3rd_cov=np.dot(g_v_mat,e_prime_3rd)# in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures
        e_prime_2nd_cov=np.dot(g_v_mat,e_prime_2nd)# same below



        # e_prime is described as sets of lines , each dissecting interesting vectors (vectors as columns)
        #e_prime here is contravariant
        #below we compute the covariant vectors in a single matrix


        e_prime_cov=np.dot(g_v_mat,e_prime)# in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures



        # we compute the mixed components of Einstein tensor
        G_einst_mixed_plus_lambda_delta=np.dot(g_n_mat,G_doubly_cov+Lambda_cosmological_constant_*g_v_mat )

        # in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures




        # at this stage e_prime_cov== e_p_mat.T



        this_should_be_minkowski__e_prime_computed_from_scrap=np.dot(e_prime_cov.T,np.dot(g_n_mat,e_prime_cov))

        this_should_be_minkowski__e_prime_2nd_computed_from_scrap=np.dot(e_prime_2nd_cov.T,np.dot(g_n_mat,e_prime_2nd_cov))


        this_should_be_minkowski_metr_as_well=np.dot(e_prime.T,np.dot(g_v_mat,e_prime))

        this_is_minkowskian_too=np.dot(e_prime.T,e_prime_cov)

        #####################"



        basis_vectors_3fluids=[[e_prime_cov,e_prime],[e_prime_2nd_cov,e_prime_2nd],[e_prime_3rd_cov,e_prime_3rd]]
        big_matrix_3flu,gee_vector_3flu=compute_big_matrix_for_fluids(basis_vectors_3fluids,G_einst_mixed_plus_lambda_delta)


        
        big_contra_ee_matrix,matrix__e_t__or__u__fluids=compute_big_matrix_for_fluids___display_properties(basis_vectors_3fluids,G_einst_mixed_plus_lambda_delta)



    
        if np.any(np.isnan(big_matrix_3flu)) or np.any(np.isinf(big_matrix_3flu)) or \
           np.any(np.isnan(gee_vector_3flu)) or np.any(np.isinf(gee_vector_3flu)):
            return_to_starting_block=True

            hyp_flu1_x_t=hyp_flu1_x_t/3.
            hyp_flu1_x_x=hyp_flu1_x_x/3.
            hyp_flu1_rho_rho=hyp_flu1_rho_rho/3.
            hyp_flu1_t_t=hyp_flu1_t_t/3.
            hyp_flu1_cstt=hyp_flu1_cstt/3.
            hyp_flu1_phiphi=hyp_flu1_phiphi/3.

            hyp_flu2_x_t=hyp_flu2_x_t/3.
            hyp_flu2_x_x=hyp_flu2_x_x/3.
            hyp_flu2_rho_rho=hyp_flu2_rho_rho/3.
            hyp_flu2_t_t=hyp_flu2_t_t/3.
            hyp_flu2_cstt=hyp_flu2_cstt/3.
            hyp_flu2_phiphi=hyp_flu2_phiphi/3.

            hyp_flu3_x_t=hyp_flu3_x_t/3.
            hyp_flu3_x_x=hyp_flu3_x_x/3.
            hyp_flu3_rho_rho=hyp_flu3_rho_rho/3.
            hyp_flu3_t_t=hyp_flu3_t_t/3.
            hyp_flu3_cstt=hyp_flu3_cstt/3.
            hyp_flu3_phiphi=hyp_flu3_phiphi/3.
            # above si inf ou nan on diminue la valeur de tous les coefficients


        else:
            return_to_starting_block=False
    ##############################################################


    rcond_here=None
    #rcond_here=1e-10
    densities_pressures_3fluids,\
        residuals_3fluids,\
        matrix_rank_3fluids,\
        sing_values_matrix_3fluids=np.linalg.lstsq(big_matrix_3flu, gee_vector_3flu,rcond=rcond_here)



    big_contra_ee_matrix=np.array(big_contra_ee_matrix)

    matrix__e_t__or__u__fluids=    np.array(matrix__e_t__or__u__fluids)
    nb_of_fluids=3
    rhos=[]
    pressures=[]
    g_v_in_vector=[g_v_t_t,\
                   g_v_t_X,\
                   0,\
                   0,\
                   g_v_t_X,\
                   g_v_X_X,\
                   0,\
                   0,\
                   0,\
                   0,\
                   g_v_rho_rho,\
                   0,\
                   0,\
                   0,\
                   0,\
                   g_v_phi_phi]

    for int_i in range(nb_of_fluids):

        big_contra_ee_matri__one_fluid=big_contra_ee_matrix[:,int_i*4:(int_i+1)*4]
        
        density_pressure__one_fluid=densities_pressures_3fluids[int_i*4:(int_i+1)*4]
        

        u_vector_one_fluid=matrix__e_t__or__u__fluids[int_i,:]
        uu_tensor_cov=[u_vector_one_fluid[i]*u_vector_one_fluid[j] for i in range(len(u_vector_one_fluid)) for j in range(len(u_vector_one_fluid))]


        #SET tensor fluid
        T_tensor=np.dot(density_pressure__one_fluid,big_contra_ee_matri__one_fluid.T)
        rhos.append(np.dot(T_tensor,uu_tensor_cov))


        
        pressures.append(np.dot(T_tensor,np.array(uu_tensor_cov)+np.array(g_v_in_vector)))
    return rhos[0]+1j*pressures[0],\
        rhos[1]+1j*pressures[1],\
        rhos[2]+1j*pressures[2]
    



#############################################################################################################


#####################################################

#######################################################################################################################################################################################################

##############################################################################################################################################
def element_wise__compute__pressures_pX__densities__output_them___myfunc(g_n_t__t_jX,\
                                                                         g_n__rho2_jX2,\
                                                                         g__v_jn__phiphi,\
                                                                         g_v_t__t_jX,\
                                                                         g_v__rho2_jX2,\
                                                                         meshed__cos_jsin__theta,\
                                                                         sqrt_lambda__pl_jmin,\
                                                                         G__t_t_jA,\
                                                                         G__t__x_jrho,\
                                                                         G__x__x_jrho,\
                                                                         G_x__rho2_jphi2,\
                                                                         hyp_coef_flu1_x__x_jt,\
                                                                         hyp_coef_flu1__tt_jrhorho,\
                                                                         hyp_coef_flu1__phiphi_jcstt,\
                                                                         euc_coef_flu1_x__x_jt,\
                                                                         euc_coef_flu1__tt_jrhorho,\
                                                                         euc_coef_flu1__phiphi_jcstt,\
                                                                         hyp_coef_flu2_x__x_jt,\
                                                                         hyp_coef_flu2__tt_jrhorho,\
                                                                         hyp_coef_flu2__phiphi_jcstt,\
                                                                         euc_coef_flu2_x__x_jt,\
                                                                         euc_coef_flu2__tt_jrhorho,\
                                                                         euc_coef_flu2__phiphi_jcstt,\
                                                                         hyp_coef_flu3_x__x_jt,\
                                                                         hyp_coef_flu3__tt_jrhorho,\
                                                                         hyp_coef_flu3__phiphi_jcstt,\
                                                                         euc_coef_flu3_x__x_jt,\
                                                                         euc_coef_flu3__tt_jrhorho,\
                                                                         euc_coef_flu3__phiphi_jcstt,\
):
    
    Lambda_cosmological_constant=0
    #1.11e-52 #(m^(-2))


    do_plot=False

    #1
    g_n_t_X=g_n_t__t_jX.imag
    g_n_t_t=g_n_t__t_jX.real

    #2
    g_n_X_X=g_n__rho2_jX2.imag
    g_n_rho_rho=g_n__rho2_jX2.real


    #3
    g_n_phi_phi=g__v_jn__phiphi.imag
    g_v_phi_phi=g__v_jn__phiphi.real



    #4
    g_v_t_X=g_v_t__t_jX.imag
    g_v_t_t=g_v_t__t_jX.real

    #5
    g_v_X_X=g_v__rho2_jX2.imag
    g_v_rho_rho=g_v__rho2_jX2.real

    #5
    meshed_sin_theta=meshed__cos_jsin__theta.imag
    meshed_cos_theta=meshed__cos_jsin__theta.real

    #6
    sqrt_lambda_minus=sqrt_lambda__pl_jmin.imag
    sqrt_lambda_plus=sqrt_lambda__pl_jmin.real

    #7
    A=G__t_t_jA.imag
    G__t_t=G__t_t_jA.real

    #8
    G__t_rho=G__t__x_jrho.imag
    G__t_x=G__t__x_jrho.real

    #9
    G__x_rho=G__x__x_jrho.imag
    G__x_x=G__x__x_jrho.real

    #10
    G__phi_phi=G_x__rho2_jphi2.imag
    G__rho_rho=G_x__rho2_jphi2.real

    # there are seven tt tx trho , xx xrho, rhorho, phiphi
    #11


    hyp_flu1_x_t=hyp_coef_flu1_x__x_jt.imag
    hyp_flu1_x_x=hyp_coef_flu1_x__x_jt.real

    #12
    hyp_flu1_rho_rho=hyp_coef_flu1__tt_jrhorho.imag
    hyp_flu1_t_t=hyp_coef_flu1__tt_jrhorho.real

    #13
    hyp_flu1_cstt=hyp_coef_flu1__phiphi_jcstt.imag
    hyp_flu1_phiphi=hyp_coef_flu1__phiphi_jcstt.real

    #14

    euc_flu1_x_t=euc_coef_flu1_x__x_jt.imag
    euc_flu1_x_x=euc_coef_flu1_x__x_jt.real


    euc_flu1_rho_rho=euc_coef_flu1__tt_jrhorho.imag
    euc_flu1_t_t=euc_coef_flu1__tt_jrhorho.real


    euc_flu1_cstt=euc_coef_flu1__phiphi_jcstt.imag
    euc_flu1_phiphi=euc_coef_flu1__phiphi_jcstt.real
    ################################
    hyp_flu2_x_t=hyp_coef_flu2_x__x_jt.imag
    hyp_flu2_x_x=hyp_coef_flu2_x__x_jt.real


    hyp_flu2_rho_rho=hyp_coef_flu2__tt_jrhorho.imag
    hyp_flu2_t_t=hyp_coef_flu2__tt_jrhorho.real


    hyp_flu2_cstt=hyp_coef_flu2__phiphi_jcstt.imag
    hyp_flu2_phiphi=hyp_coef_flu2__phiphi_jcstt.real



    euc_flu2_x_t=euc_coef_flu2_x__x_jt.imag
    euc_flu2_x_x=euc_coef_flu2_x__x_jt.real


    euc_flu2_rho_rho=euc_coef_flu2__tt_jrhorho.imag
    euc_flu2_t_t=euc_coef_flu2__tt_jrhorho.real


    euc_flu2_cstt=euc_coef_flu2__phiphi_jcstt.imag
    euc_flu2_phiphi=euc_coef_flu2__phiphi_jcstt.real

    ##################################
    hyp_flu3_x_t=hyp_coef_flu3_x__x_jt.imag
    hyp_flu3_x_x=hyp_coef_flu3_x__x_jt.real


    hyp_flu3_rho_rho=hyp_coef_flu3__tt_jrhorho.imag
    hyp_flu3_t_t=hyp_coef_flu3__tt_jrhorho.real


    hyp_flu3_cstt=hyp_coef_flu3__phiphi_jcstt.imag
    hyp_flu3_phiphi=hyp_coef_flu3__phiphi_jcstt.real



    euc_flu3_x_t=euc_coef_flu3_x__x_jt.imag
    euc_flu3_x_x=euc_coef_flu3_x__x_jt.real


    euc_flu3_rho_rho=euc_coef_flu3__tt_jrhorho.imag
    euc_flu3_t_t=euc_coef_flu3__tt_jrhorho.real


    euc_flu3_cstt=euc_coef_flu3__phiphi_jcstt.imag
    euc_flu3_phiphi=euc_coef_flu3__phiphi_jcstt.real





    #29
    #res_batch=res_batch__1j_LAMBDA.real
    Lambda_cosmological_constant_=Lambda_cosmological_constant
                                  








    #do_print_stuff=True
    do_print_stuff=False

    #do_print_stuff2=True
    do_print_stuff2=False


    do_print_stuff3=False
    inputs={}

    do_print_stuff4=False

    do_print_stuff5=False
    ######################################################
    # correction on g_phi,phi
    #below
    if g_n_phi_phi==0 or np.isinf(g_n_phi_phi):
        g_n_phi_phi=1

    if g_v_phi_phi==0 or np.isinf(g_v_phi_phi):
        g_v_phi_phi=1

    if np.isnan(g_v_phi_phi) or np.isnan(g_n_phi_phi):
        g_n_phi_phi=1
        g_v_phi_phi=1



    inputs["g_n_t_t"]=g_n_t_t
    inputs["g_n_t_X"]=g_n_t_X
    inputs["g_n_X_X"]=g_n_X_X
    inputs["g_n_rho_rho"]=g_n_rho_rho
    inputs["g_n_phi_phi"]=g_n_phi_phi


    inputs["g_v_t_t"]=g_v_t_t
    inputs["g_v_t_X"]=g_v_t_X
    inputs["g_v_X_X"]=g_v_X_X
    inputs["g_v_rho_rho"]=g_v_rho_rho
    inputs["g_v_phi_phi"]=g_v_phi_phi

    inputs["sqrt_lambda_minus"]=sqrt_lambda_minus
    inputs["sqrt_lambda_plus"]=sqrt_lambda_plus



    ########################################################
    # we compute G_doubly covariant!!
    G_doubly_cov=np.array( [ [ G__t_t   , G__t_x   , G__t_rho   , 0 ],\
                             [ G__t_x   , G__x_x   , G__x_rho   , 0 ],\
                             [ G__t_rho , G__x_rho , G__rho_rho , 0 ],\
                             [ 0        , 0        , 0          , G__phi_phi ],\
    ])
    #######################################################
    # below are the numpy equivalents of lines / rows

    g_n_mat=np.array([[inputs["g_n_t_t"],inputs["g_n_t_X"],0,0],\
                      [inputs["g_n_t_X"],inputs["g_n_X_X"],0,0],\
                      [0                ,0                ,inputs["g_n_rho_rho"],0],\
                      [0                ,0                ,0                    ,inputs["g_n_phi_phi"]]])

    g_v_mat=np.array([[inputs["g_v_t_t"],inputs["g_v_t_X"],0,0],\
                      [inputs["g_v_t_X"],inputs["g_v_X_X"],0,0],\
                      [0                ,0                ,inputs["g_v_rho_rho"],0],\
                      [0                ,0                ,0                    ,inputs["g_v_phi_phi"]]])



    lambda_plus=(-(A-1)+np.sqrt((A-1)**2+4))/2.
    lambda_minus=(-(A-1)-np.sqrt((A-1)**2+4))/2.

    
    tW=np.array([[meshed_cos_theta,0,meshed_sin_theta*1./sqrt_lambda_plus],\
                 [0,1,0],\
                 [-meshed_sin_theta*sqrt_lambda_plus,0,meshed_cos_theta],\
                 ])

    return_to_starting_block=True
    while return_to_starting_block:

        #print('choose tw4 ---> make a function which creates such matrices \n line 1640')
        


        #choose_tW_matrix

        
        tW_4,\
            tW_4_rot_no_curv,\
            tW_4_boo_no_curv=rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu1_x_t,\
                                                                     hyp_flu1_x_x,\
                                                                     hyp_flu1_rho_rho,\
                                                                     hyp_flu1_t_t,\
                                                                     hyp_flu1_cstt,\
                                                                     hyp_flu1_phiphi,\
                                                                     euc_flu1_x_t,\
                                                                     euc_flu1_x_x,\
                                                                     euc_flu1_rho_rho,\
                                                                     euc_flu1_t_t,\
                                                                     euc_flu1_cstt,\
                                                                     euc_flu1_phiphi,\
                                                                     inputs,\
        )

        tW_4_second_fluid,\
            tW_4_second_fluid_rot_no_curv,\
            tW_4_second_fluid_boo_no_curv=rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu2_x_t,\
                                                                                  hyp_flu2_x_x,\
                                                                                  hyp_flu2_rho_rho,\
                                                                                  hyp_flu2_t_t,\
                                                                                  hyp_flu2_cstt,\
                                                                                  hyp_flu2_phiphi,\
                                                                                  euc_flu2_x_t,\
                                                                                  euc_flu2_x_x,\
                                                                                  euc_flu2_rho_rho,\
                                                                                  euc_flu2_t_t,\
                                                                                  euc_flu2_cstt,\
                                                                                  euc_flu2_phiphi,\
                                                                                  inputs,\
        )

        tW_4_third_fluid,\
            tW_4_third_fluid_rot_no_curv,\
            tW_4_third_fluid_boo_no_curv=rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu3_x_t,\
                                                                                 hyp_flu3_x_x,\
                                                                                 hyp_flu3_rho_rho,\
                                                                                 hyp_flu3_t_t,\
                                                                                 hyp_flu3_cstt,\
                                                                                 hyp_flu3_phiphi,\
                                                                                 euc_flu3_x_t,\
                                                                                 euc_flu3_x_x,\
                                                                                 euc_flu3_rho_rho,\
                                                                                 euc_flu3_t_t,\
                                                                                 euc_flu3_cstt,\
                                                                                 euc_flu3_phiphi,\
                                                                                 inputs,\
        )



        #below is the g_munu diagonal version
        thediag=np.diag([lambda_plus,lambda_minus,1])
        thediag_4=np.diag([lambda_plus,lambda_minus,1,g_v_phi_phi])
        thediag_4_no_curve=np.diag([1,-1,1,1])#no_curv

        
        
        product=np.dot(tW.T,np.dot(thediag,tW))

        product_4_4__ini_fluid__tw4=np.dot(tW_4.T,np.dot(thediag_4,tW_4)) # this should be the same as the_diag_4
        product_4_4__2nd_fluid__tw4=np.dot(tW_4_second_fluid.T,np.dot(thediag_4,tW_4_second_fluid)) # this should be the same as the_diag_4
        product_4_4__3rd_fluid__tw4=np.dot(tW_4_third_fluid.T,np.dot(thediag_4,tW_4_third_fluid)) # this should be the same as the_diag_4

        ##############################
        N_plus=np.sqrt((lambda_plus-1)**2+g_v_t_X**2)
        N_minus=np.sqrt((lambda_minus-1)**2+g_v_t_X**2)

        #################################
        
        #eigen_vect_g_doublcovariant=EVGDCov
        #{EVGDCov=theta matrix} is the matrix of eigen vectors of g_{mu,nu}

        EVGDCov=np.array([[(lambda_plus-1.0)/N_plus,(lambda_minus-1.0)/N_minus,0,0],\
                         [g_v_t_X/(1.0*N_plus),g_v_t_X/(1.0*N_minus),0,0],\
                         [0,0,1.,0],\
                         [0,0,0,1.]])
        the_product_should_be_diagonal=np.dot(EVGDCov.T,np.dot(g_v_mat,EVGDCov))
        
        ##################################
        

        # the following is contravariant (each nested array is not a desirable vector rather this is component 1 of the first array and then comp 1 of the second, etc.
        e_not_prime=np.array([[-1,0,0,0],\
                              [g_v_t_X,1,0,0],\
                              [0,0,1,0],\
                              [0,0,0,1/np.sqrt(g_v_phi_phi) ],\
        ])

        
        # this line below check the orthonormality 
        the_product_should_be_minkowski_metric=np.dot(e_not_prime.T,np.dot(g_v_mat,e_not_prime))

        minkowski_metric=np.diag([-1,1,1,1])

        ####################################################


        e_prime=np.dot(EVGDCov,np.dot(tW_4,np.dot(EVGDCov.T,e_not_prime)))
        e_prime_2nd=np.dot(EVGDCov,np.dot(tW_4_second_fluid,np.dot(EVGDCov.T,e_not_prime)))
        e_prime_3rd=np.dot(EVGDCov,np.dot(tW_4_third_fluid,np.dot(EVGDCov.T,e_not_prime))) 
        # these two vectors are contravariant components

        e_prime_3rd_cov=np.dot(g_v_mat,e_prime_3rd)# in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures
        e_prime_2nd_cov=np.dot(g_v_mat,e_prime_2nd)# same old as the line above 



        # e_prime is described as sets of lines , each dissecting interesting vectors (vectors as columns)
        #e_prime here is contravariant
        #below we compute the covariant vectors in a single matrix


        
        
        
        e_prime_cov=np.dot(g_v_mat,e_prime)# in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures


        
        # we compute the mixed components of Einstein tensor
        G_einst_mixed_plus_lambda_delta=np.dot(g_n_mat,G_doubly_cov+Lambda_cosmological_constant_*g_v_mat )
        
        # in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures





        this_should_be_minkowski__e_prime_computed_from_scrap=np.dot(e_prime_cov.T,np.dot(g_n_mat,e_prime_cov))

        this_should_be_minkowski__e_prime_2nd_computed_from_scrap=np.dot(e_prime_2nd_cov.T,np.dot(g_n_mat,e_prime_2nd_cov))


        this_should_be_minkowski_metr_as_well=np.dot(e_prime.T,np.dot(g_v_mat,e_prime))

        this_is_minkowskian_too=np.dot(e_prime.T,e_prime_cov)

        #####################"


        basis_vectors_3fluids=[[e_prime_cov,e_prime],[e_prime_2nd_cov,e_prime_2nd],[e_prime_3rd_cov,e_prime_3rd]]
        big_matrix_3flu,gee_vector_3flu=compute_big_matrix_for_fluids(basis_vectors_3fluids,G_einst_mixed_plus_lambda_delta)


        if np.any(np.isnan(big_matrix_3flu)) or np.any(np.isinf(big_matrix_3flu)) or \
           np.any(np.isnan(gee_vector_3flu)) or np.any(np.isinf(gee_vector_3flu)):
            return_to_starting_block=True

            hyp_flu1_x_t=hyp_flu1_x_t/3.
            hyp_flu1_x_x=hyp_flu1_x_x/3.
            hyp_flu1_rho_rho=hyp_flu1_rho_rho/3.
            hyp_flu1_t_t=hyp_flu1_t_t/3.
            hyp_flu1_cstt=hyp_flu1_cstt/3.
            hyp_flu1_phiphi=hyp_flu1_phiphi/3.

            hyp_flu2_x_t=hyp_flu2_x_t/3.
            hyp_flu2_x_x=hyp_flu2_x_x/3.
            hyp_flu2_rho_rho=hyp_flu2_rho_rho/3.
            hyp_flu2_t_t=hyp_flu2_t_t/3.
            hyp_flu2_cstt=hyp_flu2_cstt/3.
            hyp_flu2_phiphi=hyp_flu2_phiphi/3.

            hyp_flu3_x_t=hyp_flu3_x_t/3.
            hyp_flu3_x_x=hyp_flu3_x_x/3.
            hyp_flu3_rho_rho=hyp_flu3_rho_rho/3.
            hyp_flu3_t_t=hyp_flu3_t_t/3.
            hyp_flu3_cstt=hyp_flu3_cstt/3.
            hyp_flu3_phiphi=hyp_flu3_phiphi/3.
            # above si inf ou nan on diminue la valeur de tous les coefficients


        else:
            return_to_starting_block=False
    ##############################################################


    #rcond_here=1e-10
    rcond_here=None
    densities_pressures_3fluids,\
        residuals_3fluids,\
        matrix_rank_3fluids,\
        sing_values_matrix_3fluids=np.linalg.lstsq(big_matrix_3flu, gee_vector_3flu,rcond=rcond_here)

    ##############################################################################################################################
    res=list(densities_pressures_3fluids)
    return res[0]+1j*res[1],\
        res[4]+1j*res[5],\
        res[8]+1j*res[9]
    



#############################################################################################################


#######################################################################################################################################################################################################

##############################################################################################################################################################

##############################################################################################################################################
def element_wise__all3fluids__w_EOS___output_it___myfunc(g_n_t__t_jX,\
                                                                       g_n__rho2_jX2,\
                                                                       g__v_jn__phiphi,\
                                                                       g_v_t__t_jX,\
                                                                       g_v__rho2_jX2,\
                                                                       meshed__cos_jsin__theta,\
                                                                       sqrt_lambda__pl_jmin,\
                                                                       G__t_t_jA,\
                                                                       G__t__x_jrho,\
                                                                       G__x__x_jrho,\
                                                                       G_x__rho2_jphi2,\
                                                                       hyp_coef_flu1_x__x_jt,\
                                                                       hyp_coef_flu1__tt_jrhorho,\
                                                                       hyp_coef_flu1__phiphi_jcstt,\
                                                                       euc_coef_flu1_x__x_jt,\
                                                                       euc_coef_flu1__tt_jrhorho,\
                                                                       euc_coef_flu1__phiphi_jcstt,\
                                                                       hyp_coef_flu2_x__x_jt,\
                                                                       hyp_coef_flu2__tt_jrhorho,\
                                                                       hyp_coef_flu2__phiphi_jcstt,\
                                                                       euc_coef_flu2_x__x_jt,\
                                                                       euc_coef_flu2__tt_jrhorho,\
                                                                       euc_coef_flu2__phiphi_jcstt,\
                                                                       hyp_coef_flu3_x__x_jt,\
                                                                       hyp_coef_flu3__tt_jrhorho,\
                                                                       hyp_coef_flu3__phiphi_jcstt,\
                                                                       euc_coef_flu3_x__x_jt,\
                                                                       euc_coef_flu3__tt_jrhorho,\
                                                                       euc_coef_flu3__phiphi_jcstt,\
):

    Lambda_cosmological_constant=0
    #1.11e-52 #(m^(-2))

    do_plot=False

    #1
    g_n_t_X=g_n_t__t_jX.imag
    g_n_t_t=g_n_t__t_jX.real

    #2
    g_n_X_X=g_n__rho2_jX2.imag
    g_n_rho_rho=g_n__rho2_jX2.real


    #3
    g_n_phi_phi=g__v_jn__phiphi.imag
    g_v_phi_phi=g__v_jn__phiphi.real



    #4
    g_v_t_X=g_v_t__t_jX.imag
    g_v_t_t=g_v_t__t_jX.real

    #5
    g_v_X_X=g_v__rho2_jX2.imag
    g_v_rho_rho=g_v__rho2_jX2.real

    #5
    meshed_sin_theta=meshed__cos_jsin__theta.imag
    meshed_cos_theta=meshed__cos_jsin__theta.real

    #6
    sqrt_lambda_minus=sqrt_lambda__pl_jmin.imag
    sqrt_lambda_plus=sqrt_lambda__pl_jmin.real

    #7
    A=G__t_t_jA.imag
    G__t_t=G__t_t_jA.real

    #8
    G__t_rho=G__t__x_jrho.imag
    G__t_x=G__t__x_jrho.real

    #9
    G__x_rho=G__x__x_jrho.imag
    G__x_x=G__x__x_jrho.real

    #10
    G__phi_phi=G_x__rho2_jphi2.imag
    G__rho_rho=G_x__rho2_jphi2.real

    # there are seven tt tx trho , xx xrho, rhorho, phiphi
    #11


    hyp_flu1_x_t=hyp_coef_flu1_x__x_jt.imag
    hyp_flu1_x_x=hyp_coef_flu1_x__x_jt.real

    #12
    hyp_flu1_rho_rho=hyp_coef_flu1__tt_jrhorho.imag
    hyp_flu1_t_t=hyp_coef_flu1__tt_jrhorho.real

    #13
    hyp_flu1_cstt=hyp_coef_flu1__phiphi_jcstt.imag
    hyp_flu1_phiphi=hyp_coef_flu1__phiphi_jcstt.real

    #14

    euc_flu1_x_t=euc_coef_flu1_x__x_jt.imag
    euc_flu1_x_x=euc_coef_flu1_x__x_jt.real


    euc_flu1_rho_rho=euc_coef_flu1__tt_jrhorho.imag
    euc_flu1_t_t=euc_coef_flu1__tt_jrhorho.real


    euc_flu1_cstt=euc_coef_flu1__phiphi_jcstt.imag
    euc_flu1_phiphi=euc_coef_flu1__phiphi_jcstt.real
    ################################
    hyp_flu2_x_t=hyp_coef_flu2_x__x_jt.imag
    hyp_flu2_x_x=hyp_coef_flu2_x__x_jt.real


    hyp_flu2_rho_rho=hyp_coef_flu2__tt_jrhorho.imag
    hyp_flu2_t_t=hyp_coef_flu2__tt_jrhorho.real


    hyp_flu2_cstt=hyp_coef_flu2__phiphi_jcstt.imag
    hyp_flu2_phiphi=hyp_coef_flu2__phiphi_jcstt.real



    euc_flu2_x_t=euc_coef_flu2_x__x_jt.imag
    euc_flu2_x_x=euc_coef_flu2_x__x_jt.real


    euc_flu2_rho_rho=euc_coef_flu2__tt_jrhorho.imag
    euc_flu2_t_t=euc_coef_flu2__tt_jrhorho.real


    euc_flu2_cstt=euc_coef_flu2__phiphi_jcstt.imag
    euc_flu2_phiphi=euc_coef_flu2__phiphi_jcstt.real

    ##################################
    hyp_flu3_x_t=hyp_coef_flu3_x__x_jt.imag
    hyp_flu3_x_x=hyp_coef_flu3_x__x_jt.real


    hyp_flu3_rho_rho=hyp_coef_flu3__tt_jrhorho.imag
    hyp_flu3_t_t=hyp_coef_flu3__tt_jrhorho.real


    hyp_flu3_cstt=hyp_coef_flu3__phiphi_jcstt.imag
    hyp_flu3_phiphi=hyp_coef_flu3__phiphi_jcstt.real



    euc_flu3_x_t=euc_coef_flu3_x__x_jt.imag
    euc_flu3_x_x=euc_coef_flu3_x__x_jt.real


    euc_flu3_rho_rho=euc_coef_flu3__tt_jrhorho.imag
    euc_flu3_t_t=euc_coef_flu3__tt_jrhorho.real


    euc_flu3_cstt=euc_coef_flu3__phiphi_jcstt.imag
    euc_flu3_phiphi=euc_coef_flu3__phiphi_jcstt.real





    #29
    #res_batch=res_batch__1j_LAMBDA.real
    Lambda_cosmological_constant_=Lambda_cosmological_constant
                                  








    #do_print_stuff=True
    do_print_stuff=False

    #do_print_stuff2=True
    do_print_stuff2=False


    do_print_stuff3=False
    inputs={}

    do_print_stuff4=False

    do_print_stuff5=False
    ######################################################
    # correction on g_phi,phi
    #below
    if g_n_phi_phi==0 or np.isinf(g_n_phi_phi):
        g_n_phi_phi=1

    if g_v_phi_phi==0 or np.isinf(g_v_phi_phi):
        g_v_phi_phi=1

    if np.isnan(g_v_phi_phi) or np.isnan(g_n_phi_phi):
        g_n_phi_phi=1
        g_v_phi_phi=1



    inputs["g_n_t_t"]=g_n_t_t
    inputs["g_n_t_X"]=g_n_t_X
    inputs["g_n_X_X"]=g_n_X_X
    inputs["g_n_rho_rho"]=g_n_rho_rho
    inputs["g_n_phi_phi"]=g_n_phi_phi


    inputs["g_v_t_t"]=g_v_t_t
    inputs["g_v_t_X"]=g_v_t_X
    inputs["g_v_X_X"]=g_v_X_X
    inputs["g_v_rho_rho"]=g_v_rho_rho
    inputs["g_v_phi_phi"]=g_v_phi_phi

    inputs["sqrt_lambda_minus"]=sqrt_lambda_minus
    inputs["sqrt_lambda_plus"]=sqrt_lambda_plus



    ########################################################
    # we compute G_doubly covariant!!
    G_doubly_cov=np.array( [ [ G__t_t   , G__t_x   , G__t_rho   , 0 ],\
                             [ G__t_x   , G__x_x   , G__x_rho   , 0 ],\
                             [ G__t_rho , G__x_rho , G__rho_rho , 0 ],\
                             [ 0        , 0        , 0          , G__phi_phi ],\
    ])
    #######################################################
    # below are the numpy equivalents of lines / rows
    
    g_n_mat=np.array([[inputs["g_n_t_t"],inputs["g_n_t_X"],0,0],\
                      [inputs["g_n_t_X"],inputs["g_n_X_X"],0,0],\
                      [0                ,0                ,inputs["g_n_rho_rho"],0],\
                      [0                ,0                ,0                    ,inputs["g_n_phi_phi"]]])

    g_v_mat=np.array([[inputs["g_v_t_t"],inputs["g_v_t_X"],0,0],\
                      [inputs["g_v_t_X"],inputs["g_v_X_X"],0,0],\
                      [0                ,0                ,inputs["g_v_rho_rho"],0],\
                      [0                ,0                ,0                    ,inputs["g_v_phi_phi"]]])



    lambda_plus=(-(A-1)+np.sqrt((A-1)**2+4))/2.
    lambda_minus=(-(A-1)-np.sqrt((A-1)**2+4))/2.



    tW=np.array([[meshed_cos_theta,0,meshed_sin_theta*1./sqrt_lambda_plus],\
                 [0,1,0],\
                 [-meshed_sin_theta*sqrt_lambda_plus,0,meshed_cos_theta],\
                 ])

    return_to_starting_block=True
    while return_to_starting_block:

        #print('choose tw4 ---> make a function which creates such matrices \n line 1640')
        tW_4,\
            tW_4_rot_no_curv,\
            tW_4_boo_no_curv=rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu1_x_t,\
                                                                     hyp_flu1_x_x,\
                                                                     hyp_flu1_rho_rho,\
                                                                     hyp_flu1_t_t,\
                                                                     hyp_flu1_cstt,\
                                                                     hyp_flu1_phiphi,\
                                                                     euc_flu1_x_t,\
                                                                     euc_flu1_x_x,\
                                                                     euc_flu1_rho_rho,\
                                                                     euc_flu1_t_t,\
                                                                     euc_flu1_cstt,\
                                                                     euc_flu1_phiphi,\
                                                                     inputs,\
        )

        tW_4_second_fluid,\
            tW_4_second_fluid_rot_no_curv,\
            tW_4_second_fluid_boo_no_curv=rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu2_x_t,\
                                                                                  hyp_flu2_x_x,\
                                                                                  hyp_flu2_rho_rho,\
                                                                                  hyp_flu2_t_t,\
                                                                                  hyp_flu2_cstt,\
                                                                                  hyp_flu2_phiphi,\
                                                                                  euc_flu2_x_t,\
                                                                                  euc_flu2_x_x,\
                                                                                  euc_flu2_rho_rho,\
                                                                                  euc_flu2_t_t,\
                                                                                  euc_flu2_cstt,\
                                                                                  euc_flu2_phiphi,\
                                                                                  inputs,\
        )

        tW_4_third_fluid,\
            tW_4_third_fluid_rot_no_curv,\
            tW_4_third_fluid_boo_no_curv=rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu3_x_t,\
                                                                                 hyp_flu3_x_x,\
                                                                                 hyp_flu3_rho_rho,\
                                                                                 hyp_flu3_t_t,\
                                                                                 hyp_flu3_cstt,\
                                                                                 hyp_flu3_phiphi,\
                                                                                 euc_flu3_x_t,\
                                                                                 euc_flu3_x_x,\
                                                                                 euc_flu3_rho_rho,\
                                                                                 euc_flu3_t_t,\
                                                                                 euc_flu3_cstt,\
                                                                                 euc_flu3_phiphi,\
                                                                                 inputs,\
        )



        #below is the g_munu diagonal version
        thediag=np.diag([lambda_plus,lambda_minus,1])
        thediag_4=np.diag([lambda_plus,lambda_minus,1,g_v_phi_phi])
        thediag_4_no_curve=np.diag([1,-1,1,1])#no_curv

        product=np.dot(tW.T,np.dot(thediag,tW))

        product_4_4__ini_fluid__tw4=np.dot(tW_4.T,np.dot(thediag_4,tW_4)) # this should be the same as the_diag_4
        product_4_4__2nd_fluid__tw4=np.dot(tW_4_second_fluid.T,np.dot(thediag_4,tW_4_second_fluid)) # this should be the same as the_diag_4
        product_4_4__3rd_fluid__tw4=np.dot(tW_4_third_fluid.T,np.dot(thediag_4,tW_4_third_fluid)) # this should be the same as the_diag_4


        ####    g_v_t_X=v*(1-meshed_f)

        N_plus=np.sqrt((lambda_plus-1)**2+g_v_t_X**2)
        N_minus=np.sqrt((lambda_minus-1)**2+g_v_t_X**2)

        #################################
        
        #eigen_vect_g_doublcovariant=EVGDCov
        #{EVGDCov=theta matrix} is the matrix of eigen vectors of g_{mu,nu}

        EVGDCov=np.array([[(lambda_plus-1.0)/N_plus,(lambda_minus-1.0)/N_minus,0,0],\
                         [g_v_t_X/(1.0*N_plus),g_v_t_X/(1.0*N_minus),0,0],\
                         [0,0,1.,0],\
                         [0,0,0,1.]])
        the_product_should_be_diagonal=np.dot(EVGDCov.T,np.dot(g_v_mat,EVGDCov))

        ##################################

        # the following is contravariant (each nested array is not a desirable vector rather this is component 1 of the first array and then comp 1 of the second, etc.
        e_not_prime=np.array([[-1,0,0,0],\
                              [g_v_t_X,1,0,0],\
                              [0,0,1,0],\
                              [0,0,0,1/np.sqrt(g_v_phi_phi) ],\
        ])

        the_product_should_be_minkowski_metric=np.dot(e_not_prime.T,np.dot(g_v_mat,e_not_prime))
        minkowski_metric=np.diag([-1,1,1,1])
        e_prime=np.dot(EVGDCov,np.dot(tW_4,np.dot(EVGDCov.T,e_not_prime)))
        e_prime_2nd=np.dot(EVGDCov,np.dot(tW_4_second_fluid,np.dot(EVGDCov.T,e_not_prime)))
        e_prime_3rd=np.dot(EVGDCov,np.dot(tW_4_third_fluid,np.dot(EVGDCov.T,e_not_prime))) 
        # these two vectors are contravariant components

        e_prime_3rd_cov=np.dot(g_v_mat,e_prime_3rd)# in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures
        e_prime_2nd_cov=np.dot(g_v_mat,e_prime_2nd)# same



        # e_prime is described as sets of lines , each dissecting interesting vectors (vectors as columns)
        #e_prime here is contravariant
        #below we compute the covariant vectors in a single matrix


        e_prime_cov=np.dot(g_v_mat,e_prime)# in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures



        # we compute the mixed components of Einstein tensor
        G_einst_mixed_plus_lambda_delta=np.dot(g_n_mat,G_doubly_cov+Lambda_cosmological_constant_*g_v_mat )

        # in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures





        this_should_be_minkowski__e_prime_computed_from_scrap=np.dot(e_prime_cov.T,np.dot(g_n_mat,e_prime_cov))

        this_should_be_minkowski__e_prime_2nd_computed_from_scrap=np.dot(e_prime_2nd_cov.T,np.dot(g_n_mat,e_prime_2nd_cov))


        this_should_be_minkowski_metr_as_well=np.dot(e_prime.T,np.dot(g_v_mat,e_prime))

        this_is_minkowskian_too=np.dot(e_prime.T,e_prime_cov)

        #####################"
        ####################################################################





        ################################################

        #################################################
        #################################################

        #############################################




        #############################################



        basis_vectors_3fluids=[[e_prime_cov,e_prime],[e_prime_2nd_cov,e_prime_2nd],[e_prime_3rd_cov,e_prime_3rd]]
        big_matrix_3flu,gee_vector_3flu=compute_big_matrix_for_fluids(basis_vectors_3fluids,G_einst_mixed_plus_lambda_delta)


        if np.any(np.isnan(big_matrix_3flu)) or np.any(np.isinf(big_matrix_3flu)) or \
           np.any(np.isnan(gee_vector_3flu)) or np.any(np.isinf(gee_vector_3flu)):
            return_to_starting_block=True

            hyp_flu1_x_t=hyp_flu1_x_t/3.
            hyp_flu1_x_x=hyp_flu1_x_x/3.
            hyp_flu1_rho_rho=hyp_flu1_rho_rho/3.
            hyp_flu1_t_t=hyp_flu1_t_t/3.
            hyp_flu1_cstt=hyp_flu1_cstt/3.
            hyp_flu1_phiphi=hyp_flu1_phiphi/3.

            hyp_flu2_x_t=hyp_flu2_x_t/3.
            hyp_flu2_x_x=hyp_flu2_x_x/3.
            hyp_flu2_rho_rho=hyp_flu2_rho_rho/3.
            hyp_flu2_t_t=hyp_flu2_t_t/3.
            hyp_flu2_cstt=hyp_flu2_cstt/3.
            hyp_flu2_phiphi=hyp_flu2_phiphi/3.

            hyp_flu3_x_t=hyp_flu3_x_t/3.
            hyp_flu3_x_x=hyp_flu3_x_x/3.
            hyp_flu3_rho_rho=hyp_flu3_rho_rho/3.
            hyp_flu3_t_t=hyp_flu3_t_t/3.
            hyp_flu3_cstt=hyp_flu3_cstt/3.
            hyp_flu3_phiphi=hyp_flu3_phiphi/3.
            # above si inf ou nan on diminue la valeur de tous les coefficients


        else:
            return_to_starting_block=False
    ##############################################################


    rcond_here=None
    #rcond_here=1e-10
    densities_pressures_3fluids,\
        residuals_3fluids,\
        matrix_rank_3fluids,\
        sing_values_matrix_3fluids=np.linalg.lstsq(big_matrix_3flu, gee_vector_3flu,rcond=rcond_here)


    v_p=np.dot(big_matrix_3flu,densities_pressures_3fluids) # vector predicted
    v_r=gee_vector_3flu
    
    denom=np.array([el if el !=0 else 1 for el in list(np.abs(v_p)+np.abs(v_r)) ])
    diff_sur_som=np.abs(v_p-v_r)/denom
    

    res=list(densities_pressures_3fluids)


    if res[0]!=0:
        w_1=(res[1]+res[2]+res[3])/(1.0*res[0])
    else:
        w_1=0

    if res[4]!=0:
        w_2=(res[5]+res[6]+res[7])/(1.0*res[4])
    else:
        w_2=0

    if res[8]!=0:
        w_3=(res[9]+res[10]+res[11])/(1.0*res[8])
    else:
        w_3=0
    MW=np.max(np.abs([w_1,w_2,w_3])) # max of equation of states



    
    return MW



##################################################################################################################################################

###########################################################################################################################################################

##############################################################################################################################################
def element_wise__compute_p_rho__p_phi_pressures__output_them___myfunc(g_n_t__t_jX,\
                                                                       g_n__rho2_jX2,\
                                                                       g__v_jn__phiphi,\
                                                                       g_v_t__t_jX,\
                                                                       g_v__rho2_jX2,\
                                                                       meshed__cos_jsin__theta,\
                                                                       sqrt_lambda__pl_jmin,\
                                                                       G__t_t_jA,\
                                                                       G__t__x_jrho,\
                                                                       G__x__x_jrho,\
                                                                       G_x__rho2_jphi2,\
                                                                       hyp_coef_flu1_x__x_jt,\
                                                                       hyp_coef_flu1__tt_jrhorho,\
                                                                       hyp_coef_flu1__phiphi_jcstt,\
                                                                       euc_coef_flu1_x__x_jt,\
                                                                       euc_coef_flu1__tt_jrhorho,\
                                                                       euc_coef_flu1__phiphi_jcstt,\
                                                                       hyp_coef_flu2_x__x_jt,\
                                                                       hyp_coef_flu2__tt_jrhorho,\
                                                                       hyp_coef_flu2__phiphi_jcstt,\
                                                                       euc_coef_flu2_x__x_jt,\
                                                                       euc_coef_flu2__tt_jrhorho,\
                                                                       euc_coef_flu2__phiphi_jcstt,\
                                                                       hyp_coef_flu3_x__x_jt,\
                                                                       hyp_coef_flu3__tt_jrhorho,\
                                                                       hyp_coef_flu3__phiphi_jcstt,\
                                                                       euc_coef_flu3_x__x_jt,\
                                                                       euc_coef_flu3__tt_jrhorho,\
                                                                       euc_coef_flu3__phiphi_jcstt,\
):

    Lambda_cosmological_constant=0
    #1.11e-52 #(m^(-2))
    
    do_plot=False

    #1
    g_n_t_X=g_n_t__t_jX.imag
    g_n_t_t=g_n_t__t_jX.real

    #2
    g_n_X_X=g_n__rho2_jX2.imag
    g_n_rho_rho=g_n__rho2_jX2.real


    #3
    g_n_phi_phi=g__v_jn__phiphi.imag
    g_v_phi_phi=g__v_jn__phiphi.real



    #4
    g_v_t_X=g_v_t__t_jX.imag
    g_v_t_t=g_v_t__t_jX.real

    #5
    g_v_X_X=g_v__rho2_jX2.imag
    g_v_rho_rho=g_v__rho2_jX2.real

    #5
    meshed_sin_theta=meshed__cos_jsin__theta.imag
    meshed_cos_theta=meshed__cos_jsin__theta.real

    #6
    sqrt_lambda_minus=sqrt_lambda__pl_jmin.imag
    sqrt_lambda_plus=sqrt_lambda__pl_jmin.real

    #7
    A=G__t_t_jA.imag
    G__t_t=G__t_t_jA.real

    #8
    G__t_rho=G__t__x_jrho.imag
    G__t_x=G__t__x_jrho.real

    #9
    G__x_rho=G__x__x_jrho.imag
    G__x_x=G__x__x_jrho.real

    #10
    G__phi_phi=G_x__rho2_jphi2.imag
    G__rho_rho=G_x__rho2_jphi2.real

    # there are seven tt tx trho , xx xrho, rhorho, phiphi
    #11


    hyp_flu1_x_t=hyp_coef_flu1_x__x_jt.imag
    hyp_flu1_x_x=hyp_coef_flu1_x__x_jt.real

    #12
    hyp_flu1_rho_rho=hyp_coef_flu1__tt_jrhorho.imag
    hyp_flu1_t_t=hyp_coef_flu1__tt_jrhorho.real

    #13
    hyp_flu1_cstt=hyp_coef_flu1__phiphi_jcstt.imag
    hyp_flu1_phiphi=hyp_coef_flu1__phiphi_jcstt.real

    #14

    euc_flu1_x_t=euc_coef_flu1_x__x_jt.imag
    euc_flu1_x_x=euc_coef_flu1_x__x_jt.real


    euc_flu1_rho_rho=euc_coef_flu1__tt_jrhorho.imag
    euc_flu1_t_t=euc_coef_flu1__tt_jrhorho.real


    euc_flu1_cstt=euc_coef_flu1__phiphi_jcstt.imag
    euc_flu1_phiphi=euc_coef_flu1__phiphi_jcstt.real
    ################################
    hyp_flu2_x_t=hyp_coef_flu2_x__x_jt.imag
    hyp_flu2_x_x=hyp_coef_flu2_x__x_jt.real


    hyp_flu2_rho_rho=hyp_coef_flu2__tt_jrhorho.imag
    hyp_flu2_t_t=hyp_coef_flu2__tt_jrhorho.real


    hyp_flu2_cstt=hyp_coef_flu2__phiphi_jcstt.imag
    hyp_flu2_phiphi=hyp_coef_flu2__phiphi_jcstt.real



    euc_flu2_x_t=euc_coef_flu2_x__x_jt.imag
    euc_flu2_x_x=euc_coef_flu2_x__x_jt.real


    euc_flu2_rho_rho=euc_coef_flu2__tt_jrhorho.imag
    euc_flu2_t_t=euc_coef_flu2__tt_jrhorho.real


    euc_flu2_cstt=euc_coef_flu2__phiphi_jcstt.imag
    euc_flu2_phiphi=euc_coef_flu2__phiphi_jcstt.real

    ##################################
    hyp_flu3_x_t=hyp_coef_flu3_x__x_jt.imag
    hyp_flu3_x_x=hyp_coef_flu3_x__x_jt.real


    hyp_flu3_rho_rho=hyp_coef_flu3__tt_jrhorho.imag
    hyp_flu3_t_t=hyp_coef_flu3__tt_jrhorho.real


    hyp_flu3_cstt=hyp_coef_flu3__phiphi_jcstt.imag
    hyp_flu3_phiphi=hyp_coef_flu3__phiphi_jcstt.real



    euc_flu3_x_t=euc_coef_flu3_x__x_jt.imag
    euc_flu3_x_x=euc_coef_flu3_x__x_jt.real


    euc_flu3_rho_rho=euc_coef_flu3__tt_jrhorho.imag
    euc_flu3_t_t=euc_coef_flu3__tt_jrhorho.real


    euc_flu3_cstt=euc_coef_flu3__phiphi_jcstt.imag
    euc_flu3_phiphi=euc_coef_flu3__phiphi_jcstt.real





    #29
    #res_batch=res_batch__1j_LAMBDA.real
    Lambda_cosmological_constant_=Lambda_cosmological_constant
                                  
    







    #do_print_stuff=True
    do_print_stuff=False

    #do_print_stuff2=True
    do_print_stuff2=False


    do_print_stuff3=False
    inputs={}

    do_print_stuff4=False

    do_print_stuff5=False
    ######################################################
    # correction on g_phi,phi
    #below
    if g_n_phi_phi==0 or np.isinf(g_n_phi_phi):
        g_n_phi_phi=1

    if g_v_phi_phi==0 or np.isinf(g_v_phi_phi):
        g_v_phi_phi=1

    if np.isnan(g_v_phi_phi) or np.isnan(g_n_phi_phi):
        g_n_phi_phi=1
        g_v_phi_phi=1



    inputs["g_n_t_t"]=g_n_t_t
    inputs["g_n_t_X"]=g_n_t_X
    inputs["g_n_X_X"]=g_n_X_X
    inputs["g_n_rho_rho"]=g_n_rho_rho
    inputs["g_n_phi_phi"]=g_n_phi_phi


    inputs["g_v_t_t"]=g_v_t_t
    inputs["g_v_t_X"]=g_v_t_X
    inputs["g_v_X_X"]=g_v_X_X
    inputs["g_v_rho_rho"]=g_v_rho_rho
    inputs["g_v_phi_phi"]=g_v_phi_phi

    inputs["sqrt_lambda_minus"]=sqrt_lambda_minus
    inputs["sqrt_lambda_plus"]=sqrt_lambda_plus



    ########################################################
    # we compute G_doubly covariant!!
    G_doubly_cov=np.array( [ [ G__t_t   , G__t_x   , G__t_rho   , 0 ],\
                             [ G__t_x   , G__x_x   , G__x_rho   , 0 ],\
                             [ G__t_rho , G__x_rho , G__rho_rho , 0 ],\
                             [ 0        , 0        , 0          , G__phi_phi ],\
    ])
    #######################################################
    # below are the numpy equivalents of lines / rows
    
    g_n_mat=np.array([[inputs["g_n_t_t"],inputs["g_n_t_X"],0,0],\
                      [inputs["g_n_t_X"],inputs["g_n_X_X"],0,0],\
                      [0                ,0                ,inputs["g_n_rho_rho"],0],\
                      [0                ,0                ,0                    ,inputs["g_n_phi_phi"]]])

    g_v_mat=np.array([[inputs["g_v_t_t"],inputs["g_v_t_X"],0,0],\
                      [inputs["g_v_t_X"],inputs["g_v_X_X"],0,0],\
                      [0                ,0                ,inputs["g_v_rho_rho"],0],\
                      [0                ,0                ,0                    ,inputs["g_v_phi_phi"]]])



    lambda_plus=(-(A-1)+np.sqrt((A-1)**2+4))/2.
    lambda_minus=(-(A-1)-np.sqrt((A-1)**2+4))/2.



    tW=np.array([[meshed_cos_theta,0,meshed_sin_theta*1./sqrt_lambda_plus],\
                 [0,1,0],\
                 [-meshed_sin_theta*sqrt_lambda_plus,0,meshed_cos_theta],\
                 ])

    return_to_starting_block=True
    while return_to_starting_block:

        #print('choose tw4 ---> make a function which creates such matrices \n line 1640')
        tW_4,\
            tW_4_rot_no_curv,\
            tW_4_boo_no_curv=rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu1_x_t,\
                                                                     hyp_flu1_x_x,\
                                                                     hyp_flu1_rho_rho,\
                                                                     hyp_flu1_t_t,\
                                                                     hyp_flu1_cstt,\
                                                                     hyp_flu1_phiphi,\
                                                                     euc_flu1_x_t,\
                                                                     euc_flu1_x_x,\
                                                                     euc_flu1_rho_rho,\
                                                                     euc_flu1_t_t,\
                                                                     euc_flu1_cstt,\
                                                                     euc_flu1_phiphi,\
                                                                     inputs,\
        )

        tW_4_second_fluid,\
            tW_4_second_fluid_rot_no_curv,\
            tW_4_second_fluid_boo_no_curv=rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu2_x_t,\
                                                                                  hyp_flu2_x_x,\
                                                                                  hyp_flu2_rho_rho,\
                                                                                  hyp_flu2_t_t,\
                                                                                  hyp_flu2_cstt,\
                                                                                  hyp_flu2_phiphi,\
                                                                                  euc_flu2_x_t,\
                                                                                  euc_flu2_x_x,\
                                                                                  euc_flu2_rho_rho,\
                                                                                  euc_flu2_t_t,\
                                                                                  euc_flu2_cstt,\
                                                                                  euc_flu2_phiphi,\
                                                                                  inputs,\
        )

        tW_4_third_fluid,\
            tW_4_third_fluid_rot_no_curv,\
            tW_4_third_fluid_boo_no_curv=rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu3_x_t,\
                                                                                 hyp_flu3_x_x,\
                                                                                 hyp_flu3_rho_rho,\
                                                                                 hyp_flu3_t_t,\
                                                                                 hyp_flu3_cstt,\
                                                                                 hyp_flu3_phiphi,\
                                                                                 euc_flu3_x_t,\
                                                                                 euc_flu3_x_x,\
                                                                                 euc_flu3_rho_rho,\
                                                                                 euc_flu3_t_t,\
                                                                                 euc_flu3_cstt,\
                                                                                 euc_flu3_phiphi,\
                                                                                 inputs,\
        )



        #below is the g_munu diagonal version
        thediag=np.diag([lambda_plus,lambda_minus,1])
        thediag_4=np.diag([lambda_plus,lambda_minus,1,g_v_phi_phi])
        thediag_4_no_curve=np.diag([1,-1,1,1])#no_curv

        product=np.dot(tW.T,np.dot(thediag,tW))

        product_4_4__ini_fluid__tw4=np.dot(tW_4.T,np.dot(thediag_4,tW_4)) # this should be the same as the_diag_4
        product_4_4__2nd_fluid__tw4=np.dot(tW_4_second_fluid.T,np.dot(thediag_4,tW_4_second_fluid)) # this should be the same as the_diag_4
        product_4_4__3rd_fluid__tw4=np.dot(tW_4_third_fluid.T,np.dot(thediag_4,tW_4_third_fluid)) # this should be the same as the_diag_4


        ####    g_v_t_X=v*(1-meshed_f)

        N_plus=np.sqrt((lambda_plus-1)**2+g_v_t_X**2)
        N_minus=np.sqrt((lambda_minus-1)**2+g_v_t_X**2)

        #################################
        
        #eigen_vect_g_doublcovariant=EVGDCov
        #{EVGDCov=theta matrix} is the matrix of eigen vectors of g_{mu,nu}

        EVGDCov=np.array([[(lambda_plus-1.0)/N_plus,(lambda_minus-1.0)/N_minus,0,0],\
                         [g_v_t_X/(1.0*N_plus),g_v_t_X/(1.0*N_minus),0,0],\
                         [0,0,1.,0],\
                         [0,0,0,1.]])
        the_product_should_be_diagonal=np.dot(EVGDCov.T,np.dot(g_v_mat,EVGDCov))

        ##################################

        # the following is contravariant (each nested array is not a desirable vector rather this is component 1 of the first array and then comp 1 of the second, etc.
        e_not_prime=np.array([[-1,0,0,0],\
                              [g_v_t_X,1,0,0],\
                              [0,0,1,0],\
                              [0,0,0,1/np.sqrt(g_v_phi_phi) ],\
        ])

        the_product_should_be_minkowski_metric=np.dot(e_not_prime.T,np.dot(g_v_mat,e_not_prime))
        minkowski_metric=np.diag([-1,1,1,1])
        e_prime=np.dot(EVGDCov,np.dot(tW_4,np.dot(EVGDCov.T,e_not_prime)))
        e_prime_2nd=np.dot(EVGDCov,np.dot(tW_4_second_fluid,np.dot(EVGDCov.T,e_not_prime)))
        e_prime_3rd=np.dot(EVGDCov,np.dot(tW_4_third_fluid,np.dot(EVGDCov.T,e_not_prime))) 
        # these two vectors are contravariant components

        e_prime_3rd_cov=np.dot(g_v_mat,e_prime_3rd)# in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures
        e_prime_2nd_cov=np.dot(g_v_mat,e_prime_2nd)# same



        # e_prime is described as sets of lines , each dissecting interesting vectors (vectors as columns)
        #e_prime here is contravariant
        #below we compute the covariant vectors in a single matrix


        e_prime_cov=np.dot(g_v_mat,e_prime)# in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures



        # we compute the mixed components of Einstein tensor
        G_einst_mixed_plus_lambda_delta=np.dot(g_n_mat,G_doubly_cov+Lambda_cosmological_constant_*g_v_mat )

        # in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures





        this_should_be_minkowski__e_prime_computed_from_scrap=np.dot(e_prime_cov.T,np.dot(g_n_mat,e_prime_cov))

        this_should_be_minkowski__e_prime_2nd_computed_from_scrap=np.dot(e_prime_2nd_cov.T,np.dot(g_n_mat,e_prime_2nd_cov))


        this_should_be_minkowski_metr_as_well=np.dot(e_prime.T,np.dot(g_v_mat,e_prime))

        this_is_minkowskian_too=np.dot(e_prime.T,e_prime_cov)

        #####################"
        ####################################################################





        ################################################

        #################################################
        #################################################

        #############################################




        #############################################



        basis_vectors_3fluids=[[e_prime_cov,e_prime],[e_prime_2nd_cov,e_prime_2nd],[e_prime_3rd_cov,e_prime_3rd]]
        big_matrix_3flu,gee_vector_3flu=compute_big_matrix_for_fluids(basis_vectors_3fluids,G_einst_mixed_plus_lambda_delta)


        if np.any(np.isnan(big_matrix_3flu)) or np.any(np.isinf(big_matrix_3flu)) or \
           np.any(np.isnan(gee_vector_3flu)) or np.any(np.isinf(gee_vector_3flu)):
            return_to_starting_block=True

            hyp_flu1_x_t=hyp_flu1_x_t/3.
            hyp_flu1_x_x=hyp_flu1_x_x/3.
            hyp_flu1_rho_rho=hyp_flu1_rho_rho/3.
            hyp_flu1_t_t=hyp_flu1_t_t/3.
            hyp_flu1_cstt=hyp_flu1_cstt/3.
            hyp_flu1_phiphi=hyp_flu1_phiphi/3.

            hyp_flu2_x_t=hyp_flu2_x_t/3.
            hyp_flu2_x_x=hyp_flu2_x_x/3.
            hyp_flu2_rho_rho=hyp_flu2_rho_rho/3.
            hyp_flu2_t_t=hyp_flu2_t_t/3.
            hyp_flu2_cstt=hyp_flu2_cstt/3.
            hyp_flu2_phiphi=hyp_flu2_phiphi/3.

            hyp_flu3_x_t=hyp_flu3_x_t/3.
            hyp_flu3_x_x=hyp_flu3_x_x/3.
            hyp_flu3_rho_rho=hyp_flu3_rho_rho/3.
            hyp_flu3_t_t=hyp_flu3_t_t/3.
            hyp_flu3_cstt=hyp_flu3_cstt/3.
            hyp_flu3_phiphi=hyp_flu3_phiphi/3.
            # above si inf ou nan on diminue la valeur de tous les coefficients


        else:
            return_to_starting_block=False
    ##############################################################


    rcond_here=None
    #rcond_here=1e-10
    densities_pressures_3fluids,\
        residuals_3fluids,\
        matrix_rank_3fluids,\
        sing_values_matrix_3fluids=np.linalg.lstsq(big_matrix_3flu, gee_vector_3flu,rcond=rcond_here)


    v_p=np.dot(big_matrix_3flu,densities_pressures_3fluids) # vector predicted
    v_r=gee_vector_3flu
    
    denom=np.array([el if el !=0 else 1 for el in list(np.abs(v_p)+np.abs(v_r)) ])
    diff_sur_som=np.abs(v_p-v_r)/denom
    

    res=list(densities_pressures_3fluids)

    return res[2]+1j*res[3],\
        res[6]+1j*res[7],\
        res[10]+1j*res[11]



##################################################################################################################################################

##############################################################################################################################################
def element_wise__compute_relat_error__dif_over_sum__output_them___myfunc(g_n_t__t_jX,\
                                                                       g_n__rho2_jX2,\
                                                                       g__v_jn__phiphi,\
                                                                       g_v_t__t_jX,\
                                                                       g_v__rho2_jX2,\
                                                                       meshed__cos_jsin__theta,\
                                                                       sqrt_lambda__pl_jmin,\
                                                                       G__t_t_jA,\
                                                                       G__t__x_jrho,\
                                                                       G__x__x_jrho,\
                                                                       G_x__rho2_jphi2,\
                                                                       hyp_coef_flu1_x__x_jt,\
                                                                       hyp_coef_flu1__tt_jrhorho,\
                                                                       hyp_coef_flu1__phiphi_jcstt,\
                                                                       euc_coef_flu1_x__x_jt,\
                                                                       euc_coef_flu1__tt_jrhorho,\
                                                                       euc_coef_flu1__phiphi_jcstt,\
                                                                       hyp_coef_flu2_x__x_jt,\
                                                                       hyp_coef_flu2__tt_jrhorho,\
                                                                       hyp_coef_flu2__phiphi_jcstt,\
                                                                       euc_coef_flu2_x__x_jt,\
                                                                       euc_coef_flu2__tt_jrhorho,\
                                                                       euc_coef_flu2__phiphi_jcstt,\
                                                                       hyp_coef_flu3_x__x_jt,\
                                                                       hyp_coef_flu3__tt_jrhorho,\
                                                                       hyp_coef_flu3__phiphi_jcstt,\
                                                                       euc_coef_flu3_x__x_jt,\
                                                                       euc_coef_flu3__tt_jrhorho,\
                                                                       euc_coef_flu3__phiphi_jcstt,\
):

    Lambda_cosmological_constant=0
    #1.11e-52 #(m^(-2))

    do_plot=False

    #1
    g_n_t_X=g_n_t__t_jX.imag
    g_n_t_t=g_n_t__t_jX.real

    #2
    g_n_X_X=g_n__rho2_jX2.imag
    g_n_rho_rho=g_n__rho2_jX2.real


    #3
    g_n_phi_phi=g__v_jn__phiphi.imag
    g_v_phi_phi=g__v_jn__phiphi.real



    #4
    g_v_t_X=g_v_t__t_jX.imag
    g_v_t_t=g_v_t__t_jX.real

    #5
    g_v_X_X=g_v__rho2_jX2.imag
    g_v_rho_rho=g_v__rho2_jX2.real

    #5
    meshed_sin_theta=meshed__cos_jsin__theta.imag
    meshed_cos_theta=meshed__cos_jsin__theta.real

    #6
    sqrt_lambda_minus=sqrt_lambda__pl_jmin.imag
    sqrt_lambda_plus=sqrt_lambda__pl_jmin.real

    #7
    A=G__t_t_jA.imag
    G__t_t=G__t_t_jA.real

    #8
    G__t_rho=G__t__x_jrho.imag
    G__t_x=G__t__x_jrho.real

    #9
    G__x_rho=G__x__x_jrho.imag
    G__x_x=G__x__x_jrho.real

    #10
    G__phi_phi=G_x__rho2_jphi2.imag
    G__rho_rho=G_x__rho2_jphi2.real

    # there are seven tt tx trho , xx xrho, rhorho, phiphi
    #11


    hyp_flu1_x_t=hyp_coef_flu1_x__x_jt.imag
    hyp_flu1_x_x=hyp_coef_flu1_x__x_jt.real

    #12
    hyp_flu1_rho_rho=hyp_coef_flu1__tt_jrhorho.imag
    hyp_flu1_t_t=hyp_coef_flu1__tt_jrhorho.real

    #13
    hyp_flu1_cstt=hyp_coef_flu1__phiphi_jcstt.imag
    hyp_flu1_phiphi=hyp_coef_flu1__phiphi_jcstt.real

    #14

    euc_flu1_x_t=euc_coef_flu1_x__x_jt.imag
    euc_flu1_x_x=euc_coef_flu1_x__x_jt.real


    euc_flu1_rho_rho=euc_coef_flu1__tt_jrhorho.imag
    euc_flu1_t_t=euc_coef_flu1__tt_jrhorho.real


    euc_flu1_cstt=euc_coef_flu1__phiphi_jcstt.imag
    euc_flu1_phiphi=euc_coef_flu1__phiphi_jcstt.real
    ################################
    hyp_flu2_x_t=hyp_coef_flu2_x__x_jt.imag
    hyp_flu2_x_x=hyp_coef_flu2_x__x_jt.real


    hyp_flu2_rho_rho=hyp_coef_flu2__tt_jrhorho.imag
    hyp_flu2_t_t=hyp_coef_flu2__tt_jrhorho.real


    hyp_flu2_cstt=hyp_coef_flu2__phiphi_jcstt.imag
    hyp_flu2_phiphi=hyp_coef_flu2__phiphi_jcstt.real



    euc_flu2_x_t=euc_coef_flu2_x__x_jt.imag
    euc_flu2_x_x=euc_coef_flu2_x__x_jt.real


    euc_flu2_rho_rho=euc_coef_flu2__tt_jrhorho.imag
    euc_flu2_t_t=euc_coef_flu2__tt_jrhorho.real


    euc_flu2_cstt=euc_coef_flu2__phiphi_jcstt.imag
    euc_flu2_phiphi=euc_coef_flu2__phiphi_jcstt.real

    ##################################
    hyp_flu3_x_t=hyp_coef_flu3_x__x_jt.imag
    hyp_flu3_x_x=hyp_coef_flu3_x__x_jt.real


    hyp_flu3_rho_rho=hyp_coef_flu3__tt_jrhorho.imag
    hyp_flu3_t_t=hyp_coef_flu3__tt_jrhorho.real


    hyp_flu3_cstt=hyp_coef_flu3__phiphi_jcstt.imag
    hyp_flu3_phiphi=hyp_coef_flu3__phiphi_jcstt.real



    euc_flu3_x_t=euc_coef_flu3_x__x_jt.imag
    euc_flu3_x_x=euc_coef_flu3_x__x_jt.real


    euc_flu3_rho_rho=euc_coef_flu3__tt_jrhorho.imag
    euc_flu3_t_t=euc_coef_flu3__tt_jrhorho.real


    euc_flu3_cstt=euc_coef_flu3__phiphi_jcstt.imag
    euc_flu3_phiphi=euc_coef_flu3__phiphi_jcstt.real





    #29
    #res_batch=res_batch__1j_LAMBDA.real
    Lambda_cosmological_constant_=Lambda_cosmological_constant
                                  
    







    #do_print_stuff=True
    do_print_stuff=False

    #do_print_stuff2=True
    do_print_stuff2=False


    do_print_stuff3=False
    inputs={}

    do_print_stuff4=False

    do_print_stuff5=False
    ######################################################
    # correction on g_phi,phi
    #below
    if g_n_phi_phi==0 or np.isinf(g_n_phi_phi):
        g_n_phi_phi=1

    if g_v_phi_phi==0 or np.isinf(g_v_phi_phi):
        g_v_phi_phi=1

    if np.isnan(g_v_phi_phi) or np.isnan(g_n_phi_phi):
        g_n_phi_phi=1
        g_v_phi_phi=1



    inputs["g_n_t_t"]=g_n_t_t
    inputs["g_n_t_X"]=g_n_t_X
    inputs["g_n_X_X"]=g_n_X_X
    inputs["g_n_rho_rho"]=g_n_rho_rho
    inputs["g_n_phi_phi"]=g_n_phi_phi


    inputs["g_v_t_t"]=g_v_t_t
    inputs["g_v_t_X"]=g_v_t_X
    inputs["g_v_X_X"]=g_v_X_X
    inputs["g_v_rho_rho"]=g_v_rho_rho
    inputs["g_v_phi_phi"]=g_v_phi_phi

    inputs["sqrt_lambda_minus"]=sqrt_lambda_minus
    inputs["sqrt_lambda_plus"]=sqrt_lambda_plus



    ########################################################
    # we compute G_doubly covariant!!
    G_doubly_cov=np.array( [ [ G__t_t   , G__t_x   , G__t_rho   , 0 ],\
                             [ G__t_x   , G__x_x   , G__x_rho   , 0 ],\
                             [ G__t_rho , G__x_rho , G__rho_rho , 0 ],\
                             [ 0        , 0        , 0          , G__phi_phi ],\
    ])
    #######################################################
    # below are the numpy equivalents of lines / rows
    
    g_n_mat=np.array([[inputs["g_n_t_t"],inputs["g_n_t_X"],0,0],\
                      [inputs["g_n_t_X"],inputs["g_n_X_X"],0,0],\
                      [0                ,0                ,inputs["g_n_rho_rho"],0],\
                      [0                ,0                ,0                    ,inputs["g_n_phi_phi"]]])

    g_v_mat=np.array([[inputs["g_v_t_t"],inputs["g_v_t_X"],0,0],\
                      [inputs["g_v_t_X"],inputs["g_v_X_X"],0,0],\
                      [0                ,0                ,inputs["g_v_rho_rho"],0],\
                      [0                ,0                ,0                    ,inputs["g_v_phi_phi"]]])



    lambda_plus=(-(A-1)+np.sqrt((A-1)**2+4))/2.
    lambda_minus=(-(A-1)-np.sqrt((A-1)**2+4))/2.



    tW=np.array([[meshed_cos_theta,0,meshed_sin_theta*1./sqrt_lambda_plus],\
                 [0,1,0],\
                 [-meshed_sin_theta*sqrt_lambda_plus,0,meshed_cos_theta],\
                 ])

    return_to_starting_block=True
    while return_to_starting_block:

        #print('choose tw4 ---> make a function which creates such matrices \n line 1640')
        tW_4,\
            tW_4_rot_no_curv,\
            tW_4_boo_no_curv=rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu1_x_t,\
                                                                     hyp_flu1_x_x,\
                                                                     hyp_flu1_rho_rho,\
                                                                     hyp_flu1_t_t,\
                                                                     hyp_flu1_cstt,\
                                                                     hyp_flu1_phiphi,\
                                                                     euc_flu1_x_t,\
                                                                     euc_flu1_x_x,\
                                                                     euc_flu1_rho_rho,\
                                                                     euc_flu1_t_t,\
                                                                     euc_flu1_cstt,\
                                                                     euc_flu1_phiphi,\
                                                                     inputs,\
        )

        tW_4_second_fluid,\
            tW_4_second_fluid_rot_no_curv,\
            tW_4_second_fluid_boo_no_curv=rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu2_x_t,\
                                                                                  hyp_flu2_x_x,\
                                                                                  hyp_flu2_rho_rho,\
                                                                                  hyp_flu2_t_t,\
                                                                                  hyp_flu2_cstt,\
                                                                                  hyp_flu2_phiphi,\
                                                                                  euc_flu2_x_t,\
                                                                                  euc_flu2_x_x,\
                                                                                  euc_flu2_rho_rho,\
                                                                                  euc_flu2_t_t,\
                                                                                  euc_flu2_cstt,\
                                                                                  euc_flu2_phiphi,\
                                                                                  inputs,\
        )

        tW_4_third_fluid,\
            tW_4_third_fluid_rot_no_curv,\
            tW_4_third_fluid_boo_no_curv=rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu3_x_t,\
                                                                                 hyp_flu3_x_x,\
                                                                                 hyp_flu3_rho_rho,\
                                                                                 hyp_flu3_t_t,\
                                                                                 hyp_flu3_cstt,\
                                                                                 hyp_flu3_phiphi,\
                                                                                 euc_flu3_x_t,\
                                                                                 euc_flu3_x_x,\
                                                                                 euc_flu3_rho_rho,\
                                                                                 euc_flu3_t_t,\
                                                                                 euc_flu3_cstt,\
                                                                                 euc_flu3_phiphi,\
                                                                                 inputs,\
        )



        #below is the g_munu diagonal version
        thediag=np.diag([lambda_plus,lambda_minus,1])
        thediag_4=np.diag([lambda_plus,lambda_minus,1,g_v_phi_phi])
        thediag_4_no_curve=np.diag([1,-1,1,1])#no_curv

        product=np.dot(tW.T,np.dot(thediag,tW))

        product_4_4__ini_fluid__tw4=np.dot(tW_4.T,np.dot(thediag_4,tW_4)) # this should be the same as the_diag_4
        product_4_4__2nd_fluid__tw4=np.dot(tW_4_second_fluid.T,np.dot(thediag_4,tW_4_second_fluid)) # this should be the same as the_diag_4
        product_4_4__3rd_fluid__tw4=np.dot(tW_4_third_fluid.T,np.dot(thediag_4,tW_4_third_fluid)) # this should be the same as the_diag_4


        ####    g_v_t_X=v*(1-meshed_f)

        N_plus=np.sqrt((lambda_plus-1)**2+g_v_t_X**2)
        N_minus=np.sqrt((lambda_minus-1)**2+g_v_t_X**2)

        #################################
        
        #eigen_vect_g_doublcovariant=EVGDCov
        #{EVGDCov=theta matrix} is the matrix of eigen vectors of g_{mu,nu}

        EVGDCov=np.array([[(lambda_plus-1.0)/N_plus,(lambda_minus-1.0)/N_minus,0,0],\
                         [g_v_t_X/(1.0*N_plus),g_v_t_X/(1.0*N_minus),0,0],\
                         [0,0,1.,0],\
                         [0,0,0,1.]])
        the_product_should_be_diagonal=np.dot(EVGDCov.T,np.dot(g_v_mat,EVGDCov))

        ##################################

        # the following is contravariant (each nested array is not a desirable vector rather this is component 1 of the first array and then comp 1 of the second, etc.
        e_not_prime=np.array([[-1,0,0,0],\
                              [g_v_t_X,1,0,0],\
                              [0,0,1,0],\
                              [0,0,0,1/np.sqrt(g_v_phi_phi) ],\
        ])

        the_product_should_be_minkowski_metric=np.dot(e_not_prime.T,np.dot(g_v_mat,e_not_prime))
        minkowski_metric=np.diag([-1,1,1,1])
        e_prime=np.dot(EVGDCov,np.dot(tW_4,np.dot(EVGDCov.T,e_not_prime)))
        e_prime_2nd=np.dot(EVGDCov,np.dot(tW_4_second_fluid,np.dot(EVGDCov.T,e_not_prime)))
        e_prime_3rd=np.dot(EVGDCov,np.dot(tW_4_third_fluid,np.dot(EVGDCov.T,e_not_prime))) 
        # these two vectors are contravariant components

        e_prime_3rd_cov=np.dot(g_v_mat,e_prime_3rd)# in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures
        e_prime_2nd_cov=np.dot(g_v_mat,e_prime_2nd)# same



        # e_prime is described as sets of lines , each dissecting interesting vectors (vectors as columns)
        #e_prime here is contravariant
        #below we compute the covariant vectors in a single matrix


        e_prime_cov=np.dot(g_v_mat,e_prime)# in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures



        # we compute the mixed components of Einstein tensor
        G_einst_mixed_plus_lambda_delta=np.dot(g_n_mat,G_doubly_cov+Lambda_cosmological_constant_*g_v_mat )

        # in e_prime_cov as in e_prime, vectors are in columns not in rows whereas numpy is interested in  lines as nested structures





        this_should_be_minkowski__e_prime_computed_from_scrap=np.dot(e_prime_cov.T,np.dot(g_n_mat,e_prime_cov))

        this_should_be_minkowski__e_prime_2nd_computed_from_scrap=np.dot(e_prime_2nd_cov.T,np.dot(g_n_mat,e_prime_2nd_cov))


        this_should_be_minkowski_metr_as_well=np.dot(e_prime.T,np.dot(g_v_mat,e_prime))

        this_is_minkowskian_too=np.dot(e_prime.T,e_prime_cov)

        #####################"
        ####################################################################





        ################################################

        #################################################
        #################################################

        #############################################




        #############################################

        basis_vectors_3fluids=[[e_prime_cov,e_prime],[e_prime_2nd_cov,e_prime_2nd],[e_prime_3rd_cov,e_prime_3rd]]
        big_matrix_3flu,gee_vector_3flu=compute_big_matrix_for_fluids(basis_vectors_3fluids,G_einst_mixed_plus_lambda_delta)


        if np.any(np.isnan(big_matrix_3flu)) or np.any(np.isinf(big_matrix_3flu)) or \
           np.any(np.isnan(gee_vector_3flu)) or np.any(np.isinf(gee_vector_3flu)):
            return_to_starting_block=True

            hyp_flu1_x_t=hyp_flu1_x_t/3.
            hyp_flu1_x_x=hyp_flu1_x_x/3.
            hyp_flu1_rho_rho=hyp_flu1_rho_rho/3.
            hyp_flu1_t_t=hyp_flu1_t_t/3.
            hyp_flu1_cstt=hyp_flu1_cstt/3.
            hyp_flu1_phiphi=hyp_flu1_phiphi/3.

            hyp_flu2_x_t=hyp_flu2_x_t/3.
            hyp_flu2_x_x=hyp_flu2_x_x/3.
            hyp_flu2_rho_rho=hyp_flu2_rho_rho/3.
            hyp_flu2_t_t=hyp_flu2_t_t/3.
            hyp_flu2_cstt=hyp_flu2_cstt/3.
            hyp_flu2_phiphi=hyp_flu2_phiphi/3.

            hyp_flu3_x_t=hyp_flu3_x_t/3.
            hyp_flu3_x_x=hyp_flu3_x_x/3.
            hyp_flu3_rho_rho=hyp_flu3_rho_rho/3.
            hyp_flu3_t_t=hyp_flu3_t_t/3.
            hyp_flu3_cstt=hyp_flu3_cstt/3.
            hyp_flu3_phiphi=hyp_flu3_phiphi/3.
            # above si inf ou nan on diminue la valeur de tous les coefficients


        else:
            return_to_starting_block=False
    ##############################################################




    rcond_here=None
    #rcond_here=1e-10
    densities_pressures_3fluids,\
        residuals_3fluids,\
        matrix_rank_3fluids,\
        sing_values_matrix_3fluids=np.linalg.lstsq(big_matrix_3flu, gee_vector_3flu,rcond=rcond_here)


    v_p=np.dot(big_matrix_3flu,densities_pressures_3fluids) # vector predicted
    v_r=gee_vector_3flu
    
    denom=np.array([el if el !=0 else 1 for el in list(np.abs(v_p)+np.abs(v_r)) ])
    diff_sur_som=np.abs(v_p-v_r)/denom
    

    res=list(densities_pressures_3fluids)

    return diff_sur_som[np.argmax(np.abs(v_r))],matrix_rank_3fluids



##################################################################################################################################################


##################################################################
def minus_min_of_density_plus_pressure(x0,g_n_t__t_jX,\
                                       g_n__rho2_jX2,\
                                       g__v_jn__phiphi,\
                                       g_v_t__t_jX,\
                                       g_v__rho2_jX2,\
                                       meshed__cos_jsin__theta,\
                                       sqrt_lambda__pl_jmin,\
                                       G__t_t_jA,\
                                       G__t__x_jrho,\
                                       G__x__x_jrho,\
                                       G_x__rho2_jphi2,\
                                       no_plot=True,\
):

    if not no_plot:
        print('ENTRY in minus min of density')
        input()

    [hyp_flu1_x_t,\
     hyp_flu1_x_x,\
     hyp_flu1_rho_rho,\
     hyp_flu1_t_t,\
     hyp_flu1_cstt,\
     hyp_flu1_phiphi,\
     euc_flu1_x_t,\
     euc_flu1_x_x,\
     euc_flu1_rho_rho,\
     euc_flu1_t_t,\
     euc_flu1_cstt,\
     euc_flu1_phiphi,\
     hyp_flu2_x_t,\
     hyp_flu2_x_x,\
     hyp_flu2_rho_rho,\
     hyp_flu2_t_t,\
     hyp_flu2_cstt,\
     hyp_flu2_phiphi,\
     euc_flu2_x_t,\
     euc_flu2_x_x,\
     euc_flu2_rho_rho,\
     euc_flu2_t_t,\
     euc_flu2_cstt,\
     euc_flu2_phiphi,\
     hyp_flu3_x_t,\
     hyp_flu3_x_x,\
     hyp_flu3_rho_rho,\
     hyp_flu3_t_t,\
     hyp_flu3_cstt,\
     hyp_flu3_phiphi,\
     euc_flu3_x_t,\
     euc_flu3_x_x,\
     euc_flu3_rho_rho,\
     euc_flu3_t_t,\
     euc_flu3_cstt,\
     euc_flu3_phiphi]=list(x0)

    # there are seven tt tx trho , xx xrho, rhorho, phiphi
    #11
    hyp_coef_flu1_x__x_jt=1j*hyp_flu1_x_t
    hyp_coef_flu1_x__x_jt+=hyp_flu1_x_x

    #12
    hyp_coef_flu1__tt_jrhorho=1j*hyp_flu1_rho_rho
    hyp_coef_flu1__tt_jrhorho+=hyp_flu1_t_t

    #13
    hyp_coef_flu1__phiphi_jcstt=1j*hyp_flu1_cstt
    hyp_coef_flu1__phiphi_jcstt+=hyp_flu1_phiphi

    #14
    euc_coef_flu1_x__x_jt=1j*euc_flu1_x_t
    euc_coef_flu1_x__x_jt+=euc_flu1_x_x

    #15
    euc_coef_flu1__tt_jrhorho=1j*euc_flu1_rho_rho
    euc_coef_flu1__tt_jrhorho+=euc_flu1_t_t

    #16
    euc_coef_flu1__phiphi_jcstt=1j*euc_flu1_cstt
    euc_coef_flu1__phiphi_jcstt+=euc_flu1_phiphi

    ######################"
    #17
    hyp_coef_flu2_x__x_jt=1j*hyp_flu2_x_t
    hyp_coef_flu2_x__x_jt+=hyp_flu2_x_x

    #18
    hyp_coef_flu2__tt_jrhorho=1j*hyp_flu2_rho_rho
    hyp_coef_flu2__tt_jrhorho+=hyp_flu2_t_t

    #19
    hyp_coef_flu2__phiphi_jcstt=1j*hyp_flu2_cstt
    hyp_coef_flu2__phiphi_jcstt+=hyp_flu2_phiphi

    #20
    euc_coef_flu2_x__x_jt=1j*euc_flu2_x_t
    euc_coef_flu2_x__x_jt+=euc_flu2_x_x

    #21
    euc_coef_flu2__tt_jrhorho=1j*euc_flu2_rho_rho
    euc_coef_flu2__tt_jrhorho+=euc_flu2_t_t

    #22
    euc_coef_flu2__phiphi_jcstt=1j*euc_flu2_cstt
    euc_coef_flu2__phiphi_jcstt+=euc_flu2_phiphi
    ########################################

    ######################"
    #23
    hyp_coef_flu3_x__x_jt=1j*hyp_flu3_x_t
    hyp_coef_flu3_x__x_jt+=hyp_flu3_x_x

    #24
    hyp_coef_flu3__tt_jrhorho=1j*hyp_flu3_rho_rho
    hyp_coef_flu3__tt_jrhorho+=hyp_flu3_t_t

    #25
    hyp_coef_flu3__phiphi_jcstt=1j*hyp_flu3_cstt
    hyp_coef_flu3__phiphi_jcstt+=hyp_flu3_phiphi

    #26
    euc_coef_flu3_x__x_jt=1j*euc_flu3_x_t
    euc_coef_flu3_x__x_jt+=euc_flu3_x_x

    #27
    euc_coef_flu3__tt_jrhorho=1j*euc_flu3_rho_rho
    euc_coef_flu3__tt_jrhorho+=euc_flu3_t_t

    #28
    euc_coef_flu3__phiphi_jcstt=1j*euc_flu3_cstt
    euc_coef_flu3__phiphi_jcstt+=euc_flu3_phiphi
    ########################################



    #29

    res_batch=1
    res_batch__1j_LAMBDA=1j*Lambda_cosmological_constant
    res_batch__1j_LAMBDA+=res_batch



    ##########################################################################
    ##########################################################################

    #1


    g__v_jn__phiphi[(g__v_jn__phiphi.imag==0)|(g__v_jn__phiphi.imag==np.inf)]=1
    g__v_jn__phiphi[(g__v_jn__phiphi.real==0)|(g__v_jn__phiphi.real==np.inf)]=1
    
    nans_1_of_the_2=np.isnan(g__v_jn__phiphi.real)|np.isnan(g__v_jn__phiphi.imag)
    g__v_jn__phiphi[nans_1_of_the_2]=1+1j*1
    




    output=element_wise__compute_pressures_densities___vfunc(g_n_t__t_jX,\
                                                             g_n__rho2_jX2,\
                                                             g__v_jn__phiphi,\
                                                             g_v_t__t_jX,\
                                                             g_v__rho2_jX2,\
                                                             meshed__cos_jsin__theta,\
                                                             sqrt_lambda__pl_jmin,\
                                                             G__t_t_jA,\
                                                             G__t__x_jrho,\
                                                             G__x__x_jrho,\
                                                             G_x__rho2_jphi2,\
                                                             hyp_coef_flu1_x__x_jt,\
                                                             hyp_coef_flu1__tt_jrhorho,\
                                                             hyp_coef_flu1__phiphi_jcstt,\
                                                             euc_coef_flu1_x__x_jt,\
                                                             euc_coef_flu1__tt_jrhorho,\
                                                             euc_coef_flu1__phiphi_jcstt,\
                                                             hyp_coef_flu2_x__x_jt,\
                                                             hyp_coef_flu2__tt_jrhorho,\
                                                             hyp_coef_flu2__phiphi_jcstt,\
                                                             euc_coef_flu2_x__x_jt,\
                                                             euc_coef_flu2__tt_jrhorho,\
                                                             euc_coef_flu2__phiphi_jcstt,\
                                                             hyp_coef_flu3_x__x_jt,\
                                                             hyp_coef_flu3__tt_jrhorho,\
                                                             hyp_coef_flu3__phiphi_jcstt,\
                                                             euc_coef_flu3_x__x_jt,\
                                                             euc_coef_flu3__tt_jrhorho,\
                                                             euc_coef_flu3__phiphi_jcstt,\
                                                             res_batch__1j_LAMBDA,\
    )
    print("MAX  {  minus MIN {density + pressure} }   =   "+str(np.max(output)))
    return np.max(output)


    #################################################################################################
#########################################################################################################
##################################################################
def complete_map__minus_min_of_density_plus_pressure(x0,g_n_t__t_jX,\
                                                     g_n__rho2_jX2,\
                                                     g__v_jn__phiphi,\
                                                     g_v_t__t_jX,\
                                                     g_v__rho2_jX2,\
                                                     meshed__cos_jsin__theta,\
                                                     sqrt_lambda__pl_jmin,\
                                                     G__t_t_jA,\
                                                     G__t__x_jrho,\
                                                     G__x__x_jrho,\
                                                     G_x__rho2_jphi2,\
                                                     no_plot=True,\
):

    if not no_plot:
        print('ENTRY in minus min of density')
        input()

    [hyp_flu1_x_t,\
     hyp_flu1_x_x,\
     hyp_flu1_rho_rho,\
     hyp_flu1_t_t,\
     hyp_flu1_cstt,\
     hyp_flu1_phiphi,\
     euc_flu1_x_t,\
     euc_flu1_x_x,\
     euc_flu1_rho_rho,\
     euc_flu1_t_t,\
     euc_flu1_cstt,\
     euc_flu1_phiphi,\
     hyp_flu2_x_t,\
     hyp_flu2_x_x,\
     hyp_flu2_rho_rho,\
     hyp_flu2_t_t,\
     hyp_flu2_cstt,\
     hyp_flu2_phiphi,\
     euc_flu2_x_t,\
     euc_flu2_x_x,\
     euc_flu2_rho_rho,\
     euc_flu2_t_t,\
     euc_flu2_cstt,\
     euc_flu2_phiphi,\
     hyp_flu3_x_t,\
     hyp_flu3_x_x,\
     hyp_flu3_rho_rho,\
     hyp_flu3_t_t,\
     hyp_flu3_cstt,\
     hyp_flu3_phiphi,\
     euc_flu3_x_t,\
     euc_flu3_x_x,\
     euc_flu3_rho_rho,\
     euc_flu3_t_t,\
     euc_flu3_cstt,\
     euc_flu3_phiphi]=list(x0)

    # there are seven tt tx trho , xx xrho, rhorho, phiphi
    #11
    hyp_coef_flu1_x__x_jt=1j*hyp_flu1_x_t
    hyp_coef_flu1_x__x_jt+=hyp_flu1_x_x

    #12
    hyp_coef_flu1__tt_jrhorho=1j*hyp_flu1_rho_rho
    hyp_coef_flu1__tt_jrhorho+=hyp_flu1_t_t

    #13
    hyp_coef_flu1__phiphi_jcstt=1j*hyp_flu1_cstt
    hyp_coef_flu1__phiphi_jcstt+=hyp_flu1_phiphi

    #14
    euc_coef_flu1_x__x_jt=1j*euc_flu1_x_t
    euc_coef_flu1_x__x_jt+=euc_flu1_x_x

    #15
    euc_coef_flu1__tt_jrhorho=1j*euc_flu1_rho_rho
    euc_coef_flu1__tt_jrhorho+=euc_flu1_t_t

    #16
    euc_coef_flu1__phiphi_jcstt=1j*euc_flu1_cstt
    euc_coef_flu1__phiphi_jcstt+=euc_flu1_phiphi

    ######################"
    #17
    hyp_coef_flu2_x__x_jt=1j*hyp_flu2_x_t
    hyp_coef_flu2_x__x_jt+=hyp_flu2_x_x

    #18
    hyp_coef_flu2__tt_jrhorho=1j*hyp_flu2_rho_rho
    hyp_coef_flu2__tt_jrhorho+=hyp_flu2_t_t

    #19
    hyp_coef_flu2__phiphi_jcstt=1j*hyp_flu2_cstt
    hyp_coef_flu2__phiphi_jcstt+=hyp_flu2_phiphi

    #20
    euc_coef_flu2_x__x_jt=1j*euc_flu2_x_t
    euc_coef_flu2_x__x_jt+=euc_flu2_x_x

    #21
    euc_coef_flu2__tt_jrhorho=1j*euc_flu2_rho_rho
    euc_coef_flu2__tt_jrhorho+=euc_flu2_t_t

    #22
    euc_coef_flu2__phiphi_jcstt=1j*euc_flu2_cstt
    euc_coef_flu2__phiphi_jcstt+=euc_flu2_phiphi
    ########################################

    ######################"
    #23
    hyp_coef_flu3_x__x_jt=1j*hyp_flu3_x_t
    hyp_coef_flu3_x__x_jt+=hyp_flu3_x_x

    #24
    hyp_coef_flu3__tt_jrhorho=1j*hyp_flu3_rho_rho
    hyp_coef_flu3__tt_jrhorho+=hyp_flu3_t_t

    #25
    hyp_coef_flu3__phiphi_jcstt=1j*hyp_flu3_cstt
    hyp_coef_flu3__phiphi_jcstt+=hyp_flu3_phiphi

    #26
    euc_coef_flu3_x__x_jt=1j*euc_flu3_x_t
    euc_coef_flu3_x__x_jt+=euc_flu3_x_x

    #27
    euc_coef_flu3__tt_jrhorho=1j*euc_flu3_rho_rho
    euc_coef_flu3__tt_jrhorho+=euc_flu3_t_t

    #28
    euc_coef_flu3__phiphi_jcstt=1j*euc_flu3_cstt
    euc_coef_flu3__phiphi_jcstt+=euc_flu3_phiphi
    ########################################



    #29

    res_batch=1
    res_batch__1j_LAMBDA=1j*Lambda_cosmological_constant
    res_batch__1j_LAMBDA+=res_batch



    ##########################################################################
    ##########################################################################

    #1







    output=element_wise__compute_pressures_densities___vfunc(g_n_t__t_jX,\
                                                             g_n__rho2_jX2,\
                                                             g__v_jn__phiphi,\
                                                             g_v_t__t_jX,\
                                                             g_v__rho2_jX2,\
                                                             meshed__cos_jsin__theta,\
                                                             sqrt_lambda__pl_jmin,\
                                                             G__t_t_jA,\
                                                             G__t__x_jrho,\
                                                             G__x__x_jrho,\
                                                             G_x__rho2_jphi2,\
                                                             hyp_coef_flu1_x__x_jt,\
                                                             hyp_coef_flu1__tt_jrhorho,\
                                                             hyp_coef_flu1__phiphi_jcstt,\
                                                             euc_coef_flu1_x__x_jt,\
                                                             euc_coef_flu1__tt_jrhorho,\
                                                             euc_coef_flu1__phiphi_jcstt,\
                                                             hyp_coef_flu2_x__x_jt,\
                                                             hyp_coef_flu2__tt_jrhorho,\
                                                             hyp_coef_flu2__phiphi_jcstt,\
                                                             euc_coef_flu2_x__x_jt,\
                                                             euc_coef_flu2__tt_jrhorho,\
                                                             euc_coef_flu2__phiphi_jcstt,\
                                                             hyp_coef_flu3_x__x_jt,\
                                                             hyp_coef_flu3__tt_jrhorho,\
                                                             hyp_coef_flu3__phiphi_jcstt,\
                                                             euc_coef_flu3_x__x_jt,\
                                                             euc_coef_flu3__tt_jrhorho,\
                                                             euc_coef_flu3__phiphi_jcstt,\
                                                             res_batch__1j_LAMBDA,\
    )
    print("MAX  {  minus MIN {density + pressure} }   =   "+str(np.max(output)))
    return output


    #################################################################################################


#######################################################################################################################################################################################

                        

##################################################################

def complete_map__dens_press(x0,g_n_t__t_jX,\
                             g_n__rho2_jX2,\
                             g__v_jn__phiphi,\
                             g_v_t__t_jX,\
                             g_v__rho2_jX2,\
                             meshed__cos_jsin__theta,\
                             sqrt_lambda__pl_jmin,\
                             G__t_t_jA,\
                             G__t__x_jrho,\
                             G__x__x_jrho,\
                             G_x__rho2_jphi2,\
                             no_plot=True,\
):

    if not no_plot:
        print('ENTRY in minus min of density')
        input()

    [hyp_flu1_x_t,\
     hyp_flu1_x_x,\
     hyp_flu1_rho_rho,\
     hyp_flu1_t_t,\
     hyp_flu1_cstt,\
     hyp_flu1_phiphi,\
     euc_flu1_x_t,\
     euc_flu1_x_x,\
     euc_flu1_rho_rho,\
     euc_flu1_t_t,\
     euc_flu1_cstt,\
     euc_flu1_phiphi,\
     hyp_flu2_x_t,\
     hyp_flu2_x_x,\
     hyp_flu2_rho_rho,\
     hyp_flu2_t_t,\
     hyp_flu2_cstt,\
     hyp_flu2_phiphi,\
     euc_flu2_x_t,\
     euc_flu2_x_x,\
     euc_flu2_rho_rho,\
     euc_flu2_t_t,\
     euc_flu2_cstt,\
     euc_flu2_phiphi,\
     hyp_flu3_x_t,\
     hyp_flu3_x_x,\
     hyp_flu3_rho_rho,\
     hyp_flu3_t_t,\
     hyp_flu3_cstt,\
     hyp_flu3_phiphi,\
     euc_flu3_x_t,\
     euc_flu3_x_x,\
     euc_flu3_rho_rho,\
     euc_flu3_t_t,\
     euc_flu3_cstt,\
     euc_flu3_phiphi]=list(x0)

    # there are seven tt tx trho , xx xrho, rhorho, phiphi
    #11
    hyp_coef_flu1_x__x_jt=1j*hyp_flu1_x_t
    hyp_coef_flu1_x__x_jt+=hyp_flu1_x_x

    #12
    hyp_coef_flu1__tt_jrhorho=1j*hyp_flu1_rho_rho
    hyp_coef_flu1__tt_jrhorho+=hyp_flu1_t_t

    #13
    hyp_coef_flu1__phiphi_jcstt=1j*hyp_flu1_cstt
    hyp_coef_flu1__phiphi_jcstt+=hyp_flu1_phiphi

    #14
    euc_coef_flu1_x__x_jt=1j*euc_flu1_x_t
    euc_coef_flu1_x__x_jt+=euc_flu1_x_x

    #15
    euc_coef_flu1__tt_jrhorho=1j*euc_flu1_rho_rho
    euc_coef_flu1__tt_jrhorho+=euc_flu1_t_t

    #16
    euc_coef_flu1__phiphi_jcstt=1j*euc_flu1_cstt
    euc_coef_flu1__phiphi_jcstt+=euc_flu1_phiphi

    ######################"
    #17
    hyp_coef_flu2_x__x_jt=1j*hyp_flu2_x_t
    hyp_coef_flu2_x__x_jt+=hyp_flu2_x_x

    #18
    hyp_coef_flu2__tt_jrhorho=1j*hyp_flu2_rho_rho
    hyp_coef_flu2__tt_jrhorho+=hyp_flu2_t_t

    #19
    hyp_coef_flu2__phiphi_jcstt=1j*hyp_flu2_cstt
    hyp_coef_flu2__phiphi_jcstt+=hyp_flu2_phiphi

    #20
    euc_coef_flu2_x__x_jt=1j*euc_flu2_x_t
    euc_coef_flu2_x__x_jt+=euc_flu2_x_x

    #21
    euc_coef_flu2__tt_jrhorho=1j*euc_flu2_rho_rho
    euc_coef_flu2__tt_jrhorho+=euc_flu2_t_t

    #22
    euc_coef_flu2__phiphi_jcstt=1j*euc_flu2_cstt
    euc_coef_flu2__phiphi_jcstt+=euc_flu2_phiphi
    ########################################

    ######################"
    #23
    hyp_coef_flu3_x__x_jt=1j*hyp_flu3_x_t
    hyp_coef_flu3_x__x_jt+=hyp_flu3_x_x

    #24
    hyp_coef_flu3__tt_jrhorho=1j*hyp_flu3_rho_rho
    hyp_coef_flu3__tt_jrhorho+=hyp_flu3_t_t

    #25
    hyp_coef_flu3__phiphi_jcstt=1j*hyp_flu3_cstt
    hyp_coef_flu3__phiphi_jcstt+=hyp_flu3_phiphi

    #26
    euc_coef_flu3_x__x_jt=1j*euc_flu3_x_t
    euc_coef_flu3_x__x_jt+=euc_flu3_x_x

    #27
    euc_coef_flu3__tt_jrhorho=1j*euc_flu3_rho_rho
    euc_coef_flu3__tt_jrhorho+=euc_flu3_t_t

    #28
    euc_coef_flu3__phiphi_jcstt=1j*euc_flu3_cstt
    euc_coef_flu3__phiphi_jcstt+=euc_flu3_phiphi
    ########################################



    #29

    res_batch=1
    res_batch__1j_LAMBDA=1j*Lambda_cosmological_constant
    res_batch__1j_LAMBDA+=res_batch



    ##########################################################################
    ##########################################################################

    #1




        
    p_rho__p_phi_1,\
        p_rho__p_phi_2,\
        p_rho__p_phi_3=element_wise__compute_p_rho__p_phi_pressures__output_them___vfunc(g_n_t__t_jX,\
                                                                                         g_n__rho2_jX2,\
                                                                                         g__v_jn__phiphi,\
                                                                                         g_v_t__t_jX,\
                                                                                         g_v__rho2_jX2,\
                                                                                         meshed__cos_jsin__theta,\
                                                                                         sqrt_lambda__pl_jmin,\
                                                                                         G__t_t_jA,\
                                                                                         G__t__x_jrho,\
                                                                                         G__x__x_jrho,\
                                                                                         G_x__rho2_jphi2,\
                                                                                         hyp_coef_flu1_x__x_jt,\
                                                                                         hyp_coef_flu1__tt_jrhorho,\
                                                                                         hyp_coef_flu1__phiphi_jcstt,\
                                                                                         euc_coef_flu1_x__x_jt,\
                                                                                         euc_coef_flu1__tt_jrhorho,\
                                                                                         euc_coef_flu1__phiphi_jcstt,\
                                                                                         hyp_coef_flu2_x__x_jt,\
                                                                                         hyp_coef_flu2__tt_jrhorho,\
                                                                                         hyp_coef_flu2__phiphi_jcstt,\
                                                                                         euc_coef_flu2_x__x_jt,\
                                                                                         euc_coef_flu2__tt_jrhorho,\
                                                                                         euc_coef_flu2__phiphi_jcstt,\
                                                                                         hyp_coef_flu3_x__x_jt,\
                                                                                         hyp_coef_flu3__tt_jrhorho,\
                                                                                         hyp_coef_flu3__phiphi_jcstt,\
                                                                                         euc_coef_flu3_x__x_jt,\
                                                                                         euc_coef_flu3__tt_jrhorho,\
                                                                                         euc_coef_flu3__phiphi_jcstt,\
    )

    rho__p_x_1,\
        rho__p_x_2,\
        rho__p_x_3=element_wise__compute__pressures_pX__densities__output_them___vfunc(g_n_t__t_jX,\
                                                                                       g_n__rho2_jX2,\
                                                                                        g__v_jn__phiphi,\
                                                                                       g_v_t__t_jX,\
                                                                                       g_v__rho2_jX2,\
                                                                                       meshed__cos_jsin__theta,\
                                                                                       sqrt_lambda__pl_jmin,\
                                                                                       G__t_t_jA,\
                                                                                       G__t__x_jrho,\
                                                                                       G__x__x_jrho,\
                                                                                       G_x__rho2_jphi2,\
                                                                                       hyp_coef_flu1_x__x_jt,\
                                                                                       hyp_coef_flu1__tt_jrhorho,\
                                                                                       hyp_coef_flu1__phiphi_jcstt,\
                                                                                       euc_coef_flu1_x__x_jt,\
                                                                                       euc_coef_flu1__tt_jrhorho,\
                                                                                       euc_coef_flu1__phiphi_jcstt,\
                                                                                       hyp_coef_flu2_x__x_jt,\
                                                                                       hyp_coef_flu2__tt_jrhorho,\
                                                                                       hyp_coef_flu2__phiphi_jcstt,\
                                                                                       euc_coef_flu2_x__x_jt,\
                                                                                       euc_coef_flu2__tt_jrhorho,\
                                                                                       euc_coef_flu2__phiphi_jcstt,\
                                                                                       hyp_coef_flu3_x__x_jt,\
                                                                                       hyp_coef_flu3__tt_jrhorho,\
                                                                                       hyp_coef_flu3__phiphi_jcstt,\
                                                                                       euc_coef_flu3_x__x_jt,\
                                                                                       euc_coef_flu3__tt_jrhorho,\
                                                                                       euc_coef_flu3__phiphi_jcstt,\
    )


    diff_over_sum,ranks=element_wise__compute_relat_error__dif_over_sum__output_them___vfunc(g_n_t__t_jX,\
                                                                                             g_n__rho2_jX2,\
                                                                                             g__v_jn__phiphi,\
                                                                                             g_v_t__t_jX,\
                                                                                             g_v__rho2_jX2,\
                                                                                             meshed__cos_jsin__theta,\
                                                                                             sqrt_lambda__pl_jmin,\
                                                                                             G__t_t_jA,\
                                                                                             G__t__x_jrho,\
                                                                                             G__x__x_jrho,\
                                                                                             G_x__rho2_jphi2,\
                                                                                             hyp_coef_flu1_x__x_jt,\
                                                                                             hyp_coef_flu1__tt_jrhorho,\
                                                                                             hyp_coef_flu1__phiphi_jcstt,\
                                                                                             euc_coef_flu1_x__x_jt,\
                                                                                             euc_coef_flu1__tt_jrhorho,\
                                                                                             euc_coef_flu1__phiphi_jcstt,\
                                                                                             hyp_coef_flu2_x__x_jt,\
                                                                                             hyp_coef_flu2__tt_jrhorho,\
                                                                                             hyp_coef_flu2__phiphi_jcstt,\
                                                                                             euc_coef_flu2_x__x_jt,\
                                                                                             euc_coef_flu2__tt_jrhorho,\
                                                                                             euc_coef_flu2__phiphi_jcstt,\
                                                                                             hyp_coef_flu3_x__x_jt,\
                                                                                             hyp_coef_flu3__tt_jrhorho,\
                                                                                             hyp_coef_flu3__phiphi_jcstt,\
                                                                                             euc_coef_flu3_x__x_jt,\
                                                                                             euc_coef_flu3__tt_jrhorho,\
                                                                                             euc_coef_flu3__phiphi_jcstt,\
    )



    MW=element_wise__all3fluids__w_EOS___output_it___vfunc(g_n_t__t_jX,\
                                                                                             g_n__rho2_jX2,\
                                                                                             g__v_jn__phiphi,\
                                                                                             g_v_t__t_jX,\
                                                                                             g_v__rho2_jX2,\
                                                                                             meshed__cos_jsin__theta,\
                                                                                             sqrt_lambda__pl_jmin,\
                                                                                             G__t_t_jA,\
                                                                                             G__t__x_jrho,\
                                                                                             G__x__x_jrho,\
                                                                                             G_x__rho2_jphi2,\
                                                                                             hyp_coef_flu1_x__x_jt,\
                                                                                             hyp_coef_flu1__tt_jrhorho,\
                                                                                             hyp_coef_flu1__phiphi_jcstt,\
                                                                                             euc_coef_flu1_x__x_jt,\
                                                                                             euc_coef_flu1__tt_jrhorho,\
                                                                                             euc_coef_flu1__phiphi_jcstt,\
                                                                                             hyp_coef_flu2_x__x_jt,\
                                                                                             hyp_coef_flu2__tt_jrhorho,\
                                                                                             hyp_coef_flu2__phiphi_jcstt,\
                                                                                             euc_coef_flu2_x__x_jt,\
                                                                                             euc_coef_flu2__tt_jrhorho,\
                                                                                             euc_coef_flu2__phiphi_jcstt,\
                                                                                             hyp_coef_flu3_x__x_jt,\
                                                                                             hyp_coef_flu3__tt_jrhorho,\
                                                                                             hyp_coef_flu3__phiphi_jcstt,\
                                                                                             euc_coef_flu3_x__x_jt,\
                                                                                             euc_coef_flu3__tt_jrhorho,\
                                                                                             euc_coef_flu3__phiphi_jcstt,\
    )
    # we recalculate densities for each fluid / and calculate isotropic pressure
    
    theta__gamma_1,\
        theta__gamma_2,\
        theta__gamma_3=element_wise__compute__theta_gamma___vfunc(g_n_t__t_jX,\
                                                                                  g_n__rho2_jX2,\
                                                                                  g__v_jn__phiphi,\
                                                                                  g_v_t__t_jX,\
                                                                                  g_v__rho2_jX2,\
                                                                                  meshed__cos_jsin__theta,\
                                                                                  sqrt_lambda__pl_jmin,\
                                                                                  G__t_t_jA,\
                                                                                  G__t__x_jrho,\
                                                                                  G__x__x_jrho,\
                                                                                  G_x__rho2_jphi2,\
                                                                                  hyp_coef_flu1_x__x_jt,\
                                                                                  hyp_coef_flu1__tt_jrhorho,\
                                                                                  hyp_coef_flu1__phiphi_jcstt,\
                                                                                  euc_coef_flu1_x__x_jt,\
                                                                                  euc_coef_flu1__tt_jrhorho,\
                                                                                  euc_coef_flu1__phiphi_jcstt,\
                                                                                  hyp_coef_flu2_x__x_jt,\
                                                                                  hyp_coef_flu2__tt_jrhorho,\
                                                                                  hyp_coef_flu2__phiphi_jcstt,\
                                                                                  euc_coef_flu2_x__x_jt,\
                                                                                  euc_coef_flu2__tt_jrhorho,\
                                                                                  euc_coef_flu2__phiphi_jcstt,\
                                                                                  hyp_coef_flu3_x__x_jt,\
                                                                                  hyp_coef_flu3__tt_jrhorho,\
                                                                                  hyp_coef_flu3__phiphi_jcstt,\
                                                                                  euc_coef_flu3_x__x_jt,\
                                                                                  euc_coef_flu3__tt_jrhorho,\
                                                                                  euc_coef_flu3__phiphi_jcstt,\
    )

           
 
    return rho__p_x_1,p_rho__p_phi_1,\
        rho__p_x_2,p_rho__p_phi_2,\
        rho__p_x_3,p_rho__p_phi_3,\
        diff_over_sum,\
        ranks,\
        MW,\





#######################################################################################################################################################################################

################################################################################################
def choose_tW_matrix(meshed_f_r__ones_theta_real,sqrt_lambda_minus,meshed_cos_theta,meshed_sin_theta,sqrt_lambda_plus,meshed_f_r_over_r__ones_theta_real,second_basis_choice,big_N):


    # this is not the real theta /// meshed_cos_theta /// not meshed_cos_theta_real because: this rather is the theta by which we rotate in the eigenspace of g_{mu,nu}
                                         
    if second_basis_choice==0:
        #the following line gives the tW_4 but for the boost:
        tW_4_boost=np.array([[1,0                                                        ,0                                                        ,0],\
                             [0,np.cosh(meshed_f_r__ones_theta_real)                     ,np.sinh(meshed_f_r__ones_theta_real)*1./sqrt_lambda_minus,0],\
                             [0,np.sinh(meshed_f_r__ones_theta_real)*1.*sqrt_lambda_minus,np.cosh(meshed_f_r__ones_theta_real)                     ,0],\
                             [0,0                                                        ,0                                                        ,1],\
                             ])
        tW_4_second_fluid=tW_4_boost
    elif second_basis_choice==1:
        #the following line gives the tW_4 but instead of theta for (theta+pi/4):
        tW_4_pi_over_4=np.array([[np.sqrt(2)/2.*(meshed_cos_theta-meshed_sin_theta),0,np.sqrt(2)/2.*(meshed_sin_theta+meshed_cos_theta)*1./sqrt_lambda_plus,0],\
                                 [0,1,0,0],\
                                 [-np.sqrt(2)/2.*(meshed_sin_theta+meshed_cos_theta)*sqrt_lambda_plus,0,np.sqrt(2)/2.*(meshed_cos_theta-meshed_sin_theta),0],\
                                 [0,0,0,1],\
        ])
        tW_4_second_fluid=tW_4_pi_over_4
    elif second_basis_choice==2:
        ####################################
        tW_4_boost=np.array([[1,0                                                        ,0                                                        ,0],\
                             [0,np.cosh(meshed_f_r__ones_theta_real)                     ,np.sinh(meshed_f_r__ones_theta_real)*1./sqrt_lambda_minus,0],\
                             [0,np.sinh(meshed_f_r__ones_theta_real)*1.*sqrt_lambda_minus,np.cosh(meshed_f_r__ones_theta_real)                     ,0],\
                             [0,0                                                        ,0                                                        ,1],\
        ])
        tW_4_pi_over_4=np.array([[np.sqrt(2)/2.*(meshed_cos_theta-meshed_sin_theta),0,np.sqrt(2)/2.*(meshed_sin_theta+meshed_cos_theta)*1./sqrt_lambda_plus,0],\
                                 [0,1,0,0],\
                                 [-np.sqrt(2)/2.*(meshed_sin_theta+meshed_cos_theta)*sqrt_lambda_plus,0,np.sqrt(2)/2.*(meshed_cos_theta-meshed_sin_theta),0],\
                                 [0,0,0,1],\
        ])

        tW_4_second_fluid=np.dot(tW_4_pi_over_4,tW_4_boost)

        ####################################
    elif second_basis_choice==3:
        ####################################
        tW_4_boost=np.array([[1,0                                                        ,0                                                        ,0],\
                             [0,np.cosh(10*meshed_f_r__ones_theta_real)                     ,np.sinh(10*meshed_f_r__ones_theta_real)*1./sqrt_lambda_minus,0],\
                             [0,np.sinh(10*meshed_f_r__ones_theta_real)*1.*sqrt_lambda_minus,np.cosh(10*meshed_f_r__ones_theta_real)                     ,0],\
                             [0,0                                                        ,0                                                        ,1],\
        ])
        tW_4_pi_over_4=np.array([[np.sqrt(2)/2.*(meshed_cos_theta-meshed_sin_theta),0,np.sqrt(2)/2.*(meshed_sin_theta+meshed_cos_theta)*1./sqrt_lambda_plus,0],\
                                 [0,1,0,0],\
                                 [-np.sqrt(2)/2.*(meshed_sin_theta+meshed_cos_theta)*sqrt_lambda_plus,0,np.sqrt(2)/2.*(meshed_cos_theta-meshed_sin_theta),0],\
                                 [0,0,0,1],\
        ])

        tW_4_second_fluid=np.dot(tW_4_pi_over_4,tW_4_boost)

        ####################################
    elif second_basis_choice==4:
        ####################################
        tW_4_boost=np.array([[1,0                                                        ,0                                                        ,0],\
                             [0,np.cosh(10*meshed_f_r__ones_theta_real)                     ,np.sinh(10*meshed_f_r__ones_theta_real)*1./sqrt_lambda_minus,0],\
                             [0,np.sinh(10*meshed_f_r__ones_theta_real)*1.*sqrt_lambda_minus,np.cosh(10*meshed_f_r__ones_theta_real)                     ,0],\
                             [0,0                                                        ,0                                                        ,1],\
        ])
        tW_4_pi_over_3=np.array([[0.5*(meshed_cos_theta-np.sqrt(3)*meshed_sin_theta),0,0.5*(meshed_sin_theta+np.sqrt(3)*meshed_cos_theta)*1./sqrt_lambda_plus,0],\
                                 [0,1,0,0],\
                                 [-0.5*(meshed_sin_theta+np.sqrt(3)*meshed_cos_theta)*sqrt_lambda_plus,0,0.5*(meshed_cos_theta-np.sqrt(3)*meshed_sin_theta),0],\
                                 [0,0,0,1],\
        ])

        tW_4_second_fluid=np.dot(tW_4_pi_over_3,tW_4_boost)

        ####################################
    elif second_basis_choice==5:

        ####################################
        tW_4_boost=np.array([[1,0                                                        ,0                                                        ,0],\
                             [0,np.cosh(10*meshed_f_r_over_r__ones_theta_real)                     ,np.sinh(10*meshed_f_r_over_r__ones_theta_real)*1./sqrt_lambda_minus,0],\
                             [0,np.sinh(10*meshed_f_r_over_r__ones_theta_real)*1.*sqrt_lambda_minus,np.cosh(10*meshed_f_r_over_r__ones_theta_real)                     ,0],\
                             [0,0                                                        ,0                                                        ,1],\
        ])

        #big_N=5

        cos_theta_0=np.cos(np.pi/big_N)
        sin_theta_0=np.sin(np.pi/big_N)

        tW_4_pi_over_n=np.array([[(cos_theta_0*meshed_cos_theta-sin_theta_0*meshed_sin_theta),0,(cos_theta_0*meshed_sin_theta+sin_theta_0*meshed_cos_theta)*1./sqrt_lambda_plus,0],\
                                 [0,1,0,0],\
                                 [-(cos_theta_0*meshed_sin_theta+sin_theta_0*meshed_cos_theta)*sqrt_lambda_plus,0,(cos_theta_0*meshed_cos_theta-sin_theta_0*meshed_sin_theta),0],\
                                 [0,0,0,1],\
        ])

        tW_4_second_fluid=np.dot(tW_4_pi_over_n,tW_4_boost)

        ####################################
    return tW_4_second_fluid
############################################################
def print_np_array(array,txt):
                            
    np.set_printoptions(linewidth=150)#[source]
    print(array)
    print("\n -----\n "+txt+'\n -------------\n ')
    
    np.set_printoptions(linewidth=75)#[source]
    return
########################################################################
##################################################
def compute_big_matrix_for_fluids___display_properties(basis_vectors,G_einst_mixed_plus_Lambda_delta):


    # HERE we want to compute rho and pressure etc
    # thus we beforehand compute
    #terms like (e_beta,alpha)^mu    *      (e_beta,alpha)^nu , for each fluid

    nb_of_fluids=len(basis_vectors)


    big_matrix=[]
    vect_u_cov=[]
    
    for mu in range(4):

        for nu in range(4):

            
            row=[] # row for big matrix
            
            for i in range(nb_of_fluids):
                # on va former une matrice 4*nb_of_fluids x 16 (pour 2 fluides: 8 x 16 ;pour 3 fluides 12*16)

                

                #basis vector indexing (so far)
                #fluid nb / cov 0 -- contra 1 / component mu--nu/ vector index (a time vector , a X vector, a rho vector etc.)
                
                for j in range(4): # this is the vector index
                    ##ini

                    #en fait que contravariante e / big matrix
                    row.append(basis_vectors[i][1][nu,j]*basis_vectors[i][1][mu,j])
                    
            big_matrix.append(row)
    matrix__e_t__or__u__fluids=[]
    for i in range(nb_of_fluids):
        row=[]
        for mu in range(4):

        
            
            #en fait que cov
            row.append(basis_vectors[i][0][mu,0])
        matrix__e_t__or__u__fluids.append(row)
    
    return big_matrix,matrix__e_t__or__u__fluids
###################################################

##################################################
def compute_big_matrix_for_fluids(basis_vectors,G_einst_mixed_plus_Lambda_delta):
    # HERE we compute
    #(e_beta,alpha)^mu    *      (e_beta,alpha)_nu

    nb_of_fluids=len(basis_vectors)


    big_matrix=[]
    
    Gee_vector=[]


    
    G_plus_lambda_delta__over_8_pi=G_einst_mixed_plus_Lambda_delta/(8*np.pi)
    for mu in range(4):
        for nu in range(mu+1):
            Gee_vector.append(G_plus_lambda_delta__over_8_pi[mu,nu])
            
            row=[] # row for big matrix
            for i in range(nb_of_fluids):
                # on va former une matrice 4*nb_of_fluids x 10 (pour 2 fluides: 8 x 10 ;pour 3 fluides 12*10)

                

                #basis vector indexing (so far)
                #fluid nb / cov 0 -- contra 1 / component mu--nu/ vector index (a time vector , a X vector, a rho vector etc.)
                
                for j in range(4): # this is the vector index

                    row.append(basis_vectors[i][0][nu,j]*basis_vectors[i][1][mu,j])
            big_matrix.append(row)
            
    return big_matrix,Gee_vector
###################################################
##################################################
def compute_close_to_identity_matrix_for_fluids(basis_vectors,G_einst_mixed):
    # (e_beta0,alpha0)_mu * (e_beta,alpha)^mu    +      (e_beta,alpha)_nu    *   (e_beta0,alpha0)^nu   

    nb_of_fluids=len(basis_vectors)


    big_matrix=[]
    
    Gee_vector=[]
    #G_einst_mixed
    
    for mu in range(4):
        for nu in range(mu+1):
            Gee_vector.append(G_einst_mixed[mu,nu])
            
            row=[] # row for big matrix
            for i in range(nb_of_fluids):
                # on va former une matrice 4*nb_of_fluids x 10 (pour 2 fluides: 8 x 10 ;pour 3 fluides 12*10)

                

                #basis vector indexing (so far)
                #fluid nb / cov 0 -- contra 1 / component mu--nu/ vector index (a time vector , a X vector, a rho vector etc.)
                
                for j in range(4): # this is the vector index

                    row.append(basis_vectors[i][0][nu,j]*basis_vectors[i][1][mu,j])
            big_matrix.append(row)
            
    return big_matrix,Gee_vector
###################################################

if 1:
        if 1:
                if 1:
                        if 1:
                                        

                                #############################################################################################
                                def complete_by_continuity(mask_where_we_can_replace_by_whatever_we_like,epsilon_minus_less_nan):
                        
                                        original_nans=np.isnan(epsilon_minus_less_nan)
                                        should_still_be_nans= ((original_nans==1) & (mask_where_we_can_replace_by_whatever_we_like==0))



                                        epsilon_minus_less_nan__mean_continuity = ndimage.generic_filter(epsilon_minus_less_nan, np.nanmean, size=3, mode='constant', cval=np.NaN)
                                        epsilon_minus_less_nan__mean_continuity[should_still_be_nans==1]=np.float('NaN')
                                        return epsilon_minus_less_nan__mean_continuity

                        
                                #####################################################################################################################
                                def plot_array(quantity,theta_mesh, phi_mesh,v_max,v_min,v,r_max,r_min,r,quantity_txt='',supplementary_array=[],\
                                               supplementary_array_txt=''):
                                        
                                        fig = plt.figure(num=None, figsize=(15, 13), dpi=80, facecolor='w', edgecolor='k')
                                        ax = fig.gca(projection='3d')
                                        surf = ax.plot_surface(theta_mesh, phi_mesh, quantity, rstride=1, cstride=1, cmap='hot', linewidth=0, antialiased=False)
                                        if len(supplementary_array)!=0:
                                                
                                                surf2 = ax.plot_surface(theta_mesh, phi_mesh, supplementary_array, rstride=1, cstride=1, cmap='cool', linewidth=0, antialiased=False)
                                        ax.set_xlabel('theta')
                                        ax.set_ylabel('phi')
                                        ax.set_zlabel(quantity_txt)

                                        text_to_show='v_max = '+v_max.__str__()+"\n v = "+v.__str__()+'\n v_min = '+v_min.__str__()+\
                                                '\n \n r_max = '+r_max.__str__()+' \n r = '+r.__str__()+'\n r_min = '+r_min.__str__()

                                        text_to_show+='\n \n '+quantity_txt+'___MAX = '+np.max(quantity.flatten()).__str__()+'\n '+quantity_txt+'___MIN = '+np.min(quantity.flatten()).__str__()
                                        ax.text2D(0.05, 0.95, text_to_show, transform=ax.transAxes)
                                        plt.title(quantity_txt)

                                        fig.colorbar(surf, shrink=0.5, aspect=5)
                                        if len(supplementary_array)!=0:
                                                fig.colorbar(surf2, shrink=0.5, aspect=5)
                                        plt.show()
                                        return
                                #############################################################################################
                                #####################################################################################################################
                                def plot_array__theta_r(quantity,theta_mesh, r_mesh,v_max,v_min,v,r_max,r_min,r,quantity_txt='',supplementary_array=[],\
                                                        supplementary_array_txt='', symlog_z=False):
                                        
                                        fig = plt.figure(num=None, figsize=(15, 13), dpi=80, facecolor='w', edgecolor='k')
                                        ax = fig.gca(projection='3d')
                                        surf = ax.plot_surface(theta_mesh, r_mesh, quantity, rstride=1, cstride=1, cmap='hot', linewidth=0, antialiased=False)
                                        if len(supplementary_array)!=0:
                                                
                                                surf2 = ax.plot_surface(theta_mesh, r_mesh, supplementary_array, rstride=1, cstride=1, cmap='cool', linewidth=0, antialiased=False)
                                        ax.set_xlabel('theta')
                                        ax.set_ylabel('r')
                                        ax.set_zlabel(quantity_txt)

                                        if symlog_z:
                                            ax.set_zscale('symlog')
                                        text_to_show='v_max = '+v_max.__str__()+"\n v = "+v.__str__()+'\n v_min = '+v_min.__str__()
                                        if 'varies' in str(r):
                                            text_to_show+='\n \n r_max = '+r_max.__str__()+'\n r_min = '+r_min.__str__()
                                        else:
                                            text_to_show+='\n \n r_max = '+r_max.__str__()+' \n r = '+r.__str__()+'\n r_min = '+r_min.__str__()

                                        text_to_show+='\n \n '+quantity_txt+'___MAX = '+np.max(quantity.flatten()).__str__()+'\n '+quantity_txt+'___MIN = '+np.min(quantity.flatten()).__str__()
                                        ax.text2D(0.05, 0.95, text_to_show, transform=ax.transAxes)
                                        plt.title(quantity_txt)

                                        fig.colorbar(surf, shrink=0.5, aspect=5)
                                        if len(supplementary_array)!=0:
                                                fig.colorbar(surf2, shrink=0.5, aspect=5)
                                        plt.show()
                                        return
                                #############################################################################################

                                def complete_by_continuity(mask_where_we_can_replace_by_whatever_we_like,epsilon_minus_less_nan):
                        
                                        original_nans=np.isnan(epsilon_minus_less_nan)
                                        should_still_be_nans= ((original_nans==1) & (mask_where_we_can_replace_by_whatever_we_like==0))



                                        epsilon_minus_less_nan__mean_continuity = ndimage.generic_filter(epsilon_minus_less_nan, np.nanmean, size=3, mode='constant', cval=np.NaN)
                                        epsilon_minus_less_nan__mean_continuity[should_still_be_nans==1]=np.float('NaN')
                                        return epsilon_minus_less_nan__mean_continuity



###################################################################
def compute_things_with_r_and_thetas(theta,r_range):
    
    ones_theta=np.ones(theta.shape) 

    ones_r=np.ones(r_range.shape) 



    r_max=np.max(r_range)
    r_min=np.min(r_range)


    theta_mesh,r_mesh=np.meshgrid(theta,r_range)

    ###############################################
    check_dim_thetas=False
    ###############################################


    cos_theta=np.cos(theta)
    sin_theta=np.sin(theta)
    sin_theta_squared=sin_theta**2
    cos_theta_squared=cos_theta**2
    sin_half_theta=np.sin(theta/2.)
    sin_half_theta_squared=sin_half_theta**2

    cos_theta_squared=cos_theta_squared[:,np.newaxis]
    sin_theta_squared=sin_theta_squared[:,np.newaxis]
    sin_theta=sin_theta[:,np.newaxis]
    cos_theta=cos_theta[:,np.newaxis]
    sin_half_theta=sin_half_theta[:,np.newaxis]
    sin_half_theta_squared=sin_half_theta_squared[:,np.newaxis]


    sin_2theta=np.sin(2*theta)[:,np.newaxis]

    ones_theta=ones_theta[:,np.newaxis]
    ones_r=ones_r[:,np.newaxis]

    ones_theta_and_r=np.dot(ones_r,ones_theta.T)
    meshed_sin_half_theta_squared=np.dot(ones_r,sin_half_theta_squared.T)
    meshed_sin_theta=np.dot(ones_r,sin_theta.T)
    meshed_cos_theta=np.dot(ones_r,cos_theta.T)

    return sin_theta,cos_theta,ones_theta,r_mesh,cos_theta_squared,sin_theta_squared,sin_half_theta,sin_half_theta_squared,ones_theta_and_r,\
            meshed_sin_half_theta_squared,meshed_sin_theta,meshed_cos_theta,theta_mesh



###################################################################

def compute_f_and_derivatives(r,R,sigma):

    T_plus=np.tanh(sigma*(r+R))
    T_minu=np.tanh(sigma*(r-R))
    tanh_sigma_R=np.tanh(sigma*R)
    f=(T_plus-T_minu)/(2*tanh_sigma_R)
    f_r=-sigma/(2*tanh_sigma_R)*(T_plus**2-T_minu**2)
    f_rr=-sigma**2/tanh_sigma_R*(T_plus*(1-T_plus**2)-T_minu*(1-T_minu**2))

    f_r_over_r=f_r/r

    f=f[:,np.newaxis]
    f_r=f_r[:,np.newaxis]
    f_rr=f_rr[:,np.newaxis]
    f_r_over_r=f_r_over_r[:,np.newaxis]
    return f,f_r,f_rr,f_r_over_r

####################################################################################

def mix_derivatives_of_f__and_theta(f,f_r,f_rr,f_r_over_r,sin_theta,cos_theta,ones_theta,cos_theta_squared,sin_theta_squared):
        


            
    sin_theta_dot_f_r=np.dot(f_r,sin_theta.T)
    cos_theta_dot_f_r=np.dot(f_r,cos_theta.T)
    meshed_f_r=np.dot(f_r,ones_theta.T)
    meshed_f_r_over_r=np.dot(f_r_over_r,ones_theta.T)
    meshed_f_rr=np.dot(f_rr,ones_theta.T)


    cos_theta_dot_sin_theta=cos_theta*sin_theta

    cos_theta_dot_sin_theta_dot__f_rr_minus_f_r_over_r=np.dot(f_rr-f_r_over_r,cos_theta_dot_sin_theta.T)
    

    cos_theta_squared_dot__f_rr_minus_f_r_over_r=np.dot(f_rr-f_r_over_r,cos_theta_squared.T)
    sin_theta_squared_dot__f_rr_minus_f_r_over_r=np.dot(f_rr-f_r_over_r,sin_theta_squared.T)



    ##################################
    # on nomme g_{\mu,\nu}*e'^{\mu}=e'_{\nu}, avec e' une base orthonormée de vecteurs au sens e'^\mu=e'_{\alpha}^\mu with \alpha \in \{t,X,\rho,\phi\}
    #and <e'_{\alpha} | e'_{\alpha'}>= <  e'_{\alpha}^\mu  g_{\mu\nu} e'_{\alpha'}^\nu  > =\eta^{\alpha \alpha'} = [the minkowski metric]


    meshed_f=np.dot(f,ones_theta.T)


    return meshed_f,cos_theta_squared_dot__f_rr_minus_f_r_over_r,sin_theta_squared_dot__f_rr_minus_f_r_over_r,meshed_f_r,meshed_f_r_over_r,meshed_f_rr
###########################################################################
def e_prime_computation(sin_half_theta_squared_over_N_plus_squared,\
                        lambda_plus,sqrt_lambda_plus,A,v,meshed_f,sin_theta_over_N_plus,meshed_f_r,meshed_cos_theta,theta_real_mesh,r_mesh):


                
    #e_p stands for e_prime /// e_p_t stands for the time vector then there is the component e_p_t_mu with mu in {t,X,rho,phi}
    # these are covariant components
    e_p_t_t=1+2*np.multiply(np.multiply(sin_half_theta_squared_over_N_plus_squared,-lambda_plus+2-A),np.multiply(A,lambda_plus-1)+A-1)
    e_p_t_X=-2*np.multiply(np.multiply(sin_half_theta_squared_over_N_plus_squared,-lambda_plus+2-A),  np.multiply(v*(1-meshed_f),lambda_plus  ))
    e_p_t_rho=np.multiply(np.multiply(sin_theta_over_N_plus,-lambda_plus+2-A),  -sqrt_lambda_plus)
    e_p_t_phi=0*meshed_f_r
    
    
    
    
    e_p_X_t=np.multiply(v*(1-meshed_f),(1-2*np.multiply(sin_half_theta_squared_over_N_plus_squared,-np.multiply(A,lambda_plus-1)-A+1)))
    e_p_X_X=1.-2.*np.multiply(np.multiply(sin_half_theta_squared_over_N_plus_squared,1.-A),lambda_plus)
    e_p_X_rho=np.multiply(np.multiply(sin_theta_over_N_plus,  v*(1-meshed_f)   ),  -sqrt_lambda_plus)
    e_p_X_phi=0*meshed_f_r
    
    
    e_p_rho_t=np.multiply(sin_theta_over_N_plus,np.multiply(1./sqrt_lambda_plus,-np.multiply(A,lambda_plus-1.)-A+1))
    e_p_rho_X=np.multiply(np.multiply(sin_theta_over_N_plus,v*(1-meshed_f)),sqrt_lambda_plus)
    e_p_rho_rho=meshed_cos_theta
    e_p_rho_phi=0*meshed_f_r
    
    
    # to compute the real values of rho (coordinate)
    real_rho_values=np.multiply(np.sin(theta_real_mesh),r_mesh)
    
    e_p_phi_t=0*meshed_f_r
    e_p_phi_X=0*meshed_f_r
    e_p_phi_rho=0*meshed_f_r
    e_p_phi_phi=real_rho_values
    ###############################################
    return e_p_t_t,\
        e_p_t_X,\
        e_p_t_rho,\
        e_p_t_phi,\
        e_p_X_t,\
        e_p_X_X,\
        e_p_X_rho,\
        e_p_X_phi,\
        e_p_rho_t,\
        e_p_rho_X,\
        e_p_rho_rho,\
        e_p_rho_phi,\
        e_p_phi_t,\
        e_p_phi_X,\
        e_p_phi_rho,\
        e_p_phi_phi,\
        real_rho_values
        
#######################################################################
def compute_composites_warp_v__meshed_f__and_theta(v,meshed_f,meshed_sin_half_theta_squared,meshed_sin_theta):

                
    A=1-v**2*(1-meshed_f)**2
    #########################################
    #ci dessous on écrit e'_t les 4 composantes d'un vecteur de genre temps (selon lequel sera choisi la vitesse du fluide)

    lambda_plus= (-A+1+np.sqrt((A-1)**2+4))/2.
    sqrt_lambda_plus=np.sqrt(lambda_plus)
    lambda_minus= (-A+1-np.sqrt((A-1)**2+4))/2.
    sqrt_lambda_minus=np.sqrt(-lambda_minus)

    N_plus_squared=(lambda_plus-1)**2+v**2*(1-meshed_f)**2
    N_plus=np.sqrt(N_plus_squared)

    
    sin_half_theta_squared_over_N_plus_squared=np.multiply(1./N_plus_squared,meshed_sin_half_theta_squared)
    sin_theta_over_N_plus=np.multiply(1./N_plus,meshed_sin_theta)
    return A, lambda_plus,sqrt_lambda_plus,sqrt_lambda_minus,N_plus_squared,N_plus,sin_half_theta_squared_over_N_plus_squared,sin_theta_over_N_plus


####################################################################
def compute_doublycov_doublycontra_metrics_comp(ones_theta_and_r,v,meshed_f,A,real_rho_values):

    #doubly contravariant below
    g_n_t_t=-1.0*ones_theta_and_r
    g_n_t_X=v*(1.0-meshed_f)
    g_n_X_X=A
    g_n_rho_rho=ones_theta_and_r

    g_n_phi_phi=1./real_rho_values**2
    
    #doubly covariant below
    g_v_t_t=-A
    g_v_t_X=v*(1-meshed_f)
    g_v_X_X=ones_theta_and_r
    g_v_rho_rho=ones_theta_and_r

    g_v_phi_phi=real_rho_values**2

    return g_n_t_t,\
        g_n_t_X,\
        g_n_X_X,\
        g_n_rho_rho,\
        g_n_phi_phi,\
        g_v_t_t,\
        g_v_t_X,\
        g_v_X_X,\
        g_v_rho_rho,\
        g_v_phi_phi
####################################################################
def compute_einstein_tensor_components_doublyCov(meshed_f_r_over_r__ones_theta_real,\
                                                 sin_theta_real_squared_dot__f_rr_minus_f_r_over_r,\
                                                 meshed_sin_squared_theta_real,\
                                                 meshed_f_r__ones_theta_real,\
                                                 v,\
                                                 meshed_f__ones_theta_real,\
                                                 meshed_cos_theta_real_times_meshed_sin_theta_real,\
                                                 meshed_f_rr__ones_theta_real,\
                                                 v_t,\
                                                 meshed_sin_theta_real,\
                                                 meshed_cos_theta_real,\
                                                 cos_theta_real_squared_dot__f_rr_minus_f_r_over_r,\
                                                 real_rho_values,\
):
    ###################""

                    
    two_f_r_over_r__plus_sin_squared_theta_real__dot_f_rr_minus_f_r_over_r=2*meshed_f_r_over_r__ones_theta_real+sin_theta_real_squared_dot__f_rr_minus_f_r_over_r
    
    G__t_t=-1.0/4.0*np.multiply(np.multiply(meshed_sin_squared_theta_real,meshed_f_r__ones_theta_real**2),v**2+3*v**4*np.multiply(1-meshed_f__ones_theta_real,1-meshed_f__ones_theta_real))+\
        v**2*np.multiply(1-meshed_f__ones_theta_real,two_f_r_over_r__plus_sin_squared_theta_real__dot_f_rr_minus_f_r_over_r)
    
    G__t_x=1.0/2.0*v*two_f_r_over_r__plus_sin_squared_theta_real__dot_f_rr_minus_f_r_over_r-3.0/4.0*v**3*np.multiply(1-meshed_f__ones_theta_real,\
                                                                                                                     np.multiply(meshed_sin_squared_theta_real,meshed_f_r__ones_theta_real))
    
    G__x_x=-3.0/4.0*v**2*np.multiply(meshed_sin_squared_theta_real,meshed_f_r__ones_theta_real)
    
    G__t_rho=np.multiply(meshed_cos_theta_real_times_meshed_sin_theta_real,\
                         np.multiply(np.multiply(meshed_f_r__ones_theta_real,meshed_f_r__ones_theta_real)*v**3,1-meshed_f__ones_theta_real)+\
                         (-1/2.0*v)*np.multiply(meshed_f_rr__ones_theta_real-meshed_f_r_over_r__ones_theta_real,1+v**2*(1-meshed_f__ones_theta_real)**2))+\
                         1/2.0*v*v_t*np.multiply(np.multiply(meshed_sin_theta_real,meshed_f_r__ones_theta_real),1-meshed_f__ones_theta_real)
    G__x_rho=np.multiply(meshed_cos_theta_real_times_meshed_sin_theta_real,\
                        np.multiply(meshed_f_r__ones_theta_real,meshed_f_r__ones_theta_real)*v**2+\
                        (-1/2.0)*np.multiply(meshed_f_rr__ones_theta_real-meshed_f_r_over_r__ones_theta_real,v**2*(1-meshed_f__ones_theta_real)**2))+\
                        1/2.0*v_t*np.multiply(meshed_sin_theta_real,meshed_f_r__ones_theta_real)
    G__rho_rho=np.multiply((-1/4.-3/4.*np.multiply(meshed_cos_theta_real,meshed_cos_theta_real))*v**2,np.multiply(meshed_f_r__ones_theta_real,meshed_f_r__ones_theta_real))+\
               v**2*np.multiply(1-meshed_f__ones_theta_real,meshed_f_r_over_r__ones_theta_real+cos_theta_real_squared_dot__f_rr_minus_f_r_over_r)\
               -v_t*np.multiply(meshed_cos_theta_real,meshed_f_r__ones_theta_real)
    G__phi_phi=np.multiply(real_rho_values**2,G__rho_rho)
    return  G__t_t,\
        G__t_x,\
        G__x_x,\
        G__t_rho,\
        G__x_rho,\
        G__rho_rho,\
        G__phi_phi
#############################################################################################
def choose_filename_saving(prefix,suffix):


    mypath='/home/wilwil/Documents/2016-09-18-important-folders/to-the-stars-or-not/2018-08-20--three--fluids_NEC'
    #Fuurai no Shiren GB2 - Going to Tabigarasu
    
    good_numbers = [int(f.split(prefix)[1].split(suffix)[0]) for f in listdir(mypath) if isfile(join(mypath, f)) and prefix in f]

    index=1
    while index in good_numbers:
        index+=1
    saving_filename= prefix+index.__str__()+suffix
        
    return saving_filename

#################################################################################################
################################################################################################
def rotation_euc_hyp__g_v__choice_tW_matrix(hyp_flu3_x_t,\
                                            hyp_flu3_x_x,\
                                            hyp_flu3_rho_rho,\
                                            hyp_flu3_t_t,\
                                            hyp_flu3_cstt,\
                                            hyp_flu3_phi_phi,\
                                            euc_flu3_x_t,\
                                            euc_flu3_x_x,\
                                            euc_flu3_rho_rho,\
                                            euc_flu3_t_t,\
                                            euc_flu3_cstt,\
                                            euc_flu3_phi_phi,\
                                            inputs,\
):

    
                                            
    gamma=inputs["g_v_t_t"]*hyp_flu3_t_t\
           +inputs["g_v_t_X"]*hyp_flu3_x_t\
           +inputs["g_v_X_X"]*hyp_flu3_x_x\
           +inputs["g_v_rho_rho"]*hyp_flu3_rho_rho\
           +inputs["g_v_phi_phi"]*hyp_flu3_phi_phi\
           +hyp_flu3_cstt

    
    
    theta=inputs["g_v_t_t"]*euc_flu3_t_t\
           +inputs["g_v_t_X"]*euc_flu3_x_t\
           +inputs["g_v_X_X"]*euc_flu3_x_x\
           +inputs["g_v_rho_rho"]*euc_flu3_rho_rho\
           +inputs["g_v_phi_phi"]*euc_flu3_phi_phi\
           +euc_flu3_cstt

    
    
    ############ 
    sqrt_lambda_minus=inputs["sqrt_lambda_minus"]
    sqrt_lambda_plus=inputs["sqrt_lambda_plus"]
    ############""
    np_cosh_gamma=np.cosh(gamma)
    
    if np.isinf(np_cosh_gamma):
        while np.isinf(np.cosh(gamma)):
            gamma=gamma/100.
    
    
    tW_4_boost=np.array([[1,0                                  ,0                                  ,0],\
                         [0,np.cosh(gamma)                     ,np.sinh(gamma)*1./sqrt_lambda_minus,0],\
                         [0,np.sinh(gamma)*1.*sqrt_lambda_minus,np.cosh(gamma)                     ,0],\
                         [0,0                                  ,0                                  ,1],\
    ])

    tW_4_boost_no_curv=np.array([[1,0                                  ,0                                  ,0],\
                                 [0,np.cosh(gamma)                     ,np.sinh(gamma),0],\
                                 [0,np.sinh(gamma)*1.,np.cosh(gamma)                     ,0],\
                                 [0,0                                  ,0                                  ,1],\
    ])


    

    #ancient below
    tW_4_rotat=np.array([[np.cos(theta),0,(np.sin(theta))*1./sqrt_lambda_plus,0],\
                         [0,1,0,0],\
                         [-(np.sin(theta))*sqrt_lambda_plus,0,(np.cos(theta)),0],\
                         [0,0,0,1],\
    ])


    tW_4_rotat_no_curv=np.array([[np.cos(theta),0,(np.sin(theta))*1.,0],\
                                 [0,1,0,0],\
                                 [-(np.sin(theta)),0,(np.cos(theta)),0],\
                                 [0,0,0,1],\
    ])


    tW_4_fluid=np.dot(tW_4_rotat,tW_4_boost)

    return tW_4_fluid,tW_4_rotat_no_curv,tW_4_boost_no_curv


#############################################################################################################
def extract_right_dicts_for_analysis(list_of_old_dict_res,v,v_t,sigma=8):



    exact_v_dict_mean=[dict_b for dict_b in list_of_old_dict_res if 'warp_speed' in dict_b and 'metric-fun' in dict_b \
                       and dict_b['warp_acceleration']==v_t and dict_b['sigma']==sigma and 'rank' in dict_b and "LAMBDA_into_account" in dict_b and dict_b['warp_speed']==v \
    ]

    return exact_v_dict_mean
#####################################################################
def  plot_warpspeed_against_NEC_violation(list_of_old_dict_res):
    #############
    v_list=[]
    v_t_list=[]
    fun_min=[]
    LAMBDA_into_account=[]
    for dict_b in list_of_old_dict_res:
        for key in dict_b.keys():

            
            if "warp" in key.lower() or "fun" in key.lower(): 
                print(key+'   :   '+str(dict_b[key]))
        if 'LAMBDA_into_account' in dict_b.keys() and 'timestamp' in dict_b.keys() and 'sigma' in dict_b.keys():
            if dict_b['sigma']==8 and  dict_b['warp_acceleration']==1.1e-16:
        
                v_list.append(dict_b['warp_speed'])

                v_t_list.append(dict_b['warp_acceleration'])
                fun_min.append(dict_b['fun'])
                LAMBDA_into_account.append(dict_b['LAMBDA_into_account'])

    data = {'v': v_list, 'v_t': v_t_list,'fun_min':fun_min,\
            "LAMBDA_into_account": LAMBDA_into_account,\
    }
    df=pd.DataFrame.from_dict(data)
    new_series=df.groupby('v')['fun_min'].min()
    new_df=pd.DataFrame(new_series)
    

    plt.figure()

    new_df.plot(logx=True,logy=True)
    plt.show()
    return

##########################################################################
def filter_out_useless(list_of_old_dict_res2):
    #check the key of last saved data
    # keep only data wth the same keys (not less)
    
    keys=list_of_old_dict_res2[0].keys()
    output_list_of_old_dict=[list_of_old_dict_res2[a] for a in range(len(list_of_old_dict_res2)) if 'warp_speed' in list_of_old_dict_res2[a].keys() and \
                             'warp_acceleration' in list_of_old_dict_res2[a].keys() and \
                             'LAMBDA_into_account' in list_of_old_dict_res2[a].keys() and \
                             'timestamp' in list_of_old_dict_res2[a].keys() and \
                             'sigma' in list_of_old_dict_res2[a].keys() and \
                             'metric-fun' in  list_of_old_dict_res2[a].keys()]

    return output_list_of_old_dict

###############
#######################################################################################
def seek_for_the_correct_adresses_of_files(cwd=''):
    #we search for a specific arborescence
    list_of_dir__bits=['/three-fluids-accel-data',\
                       '/two-fluids-accel-data',\
                       '/two-fluids-decel-data',\
                       '/three-fluids-decel-data',\
    ]
    list_of_absolute_files_adresses=[]
    for dir__bit in list_of_dir__bits:

        list_of_files=os.listdir(cwd+dir__bit)
        list_of_files__param_warpdrive_=[el for el in list_of_files if "param_warpdrive_" in el]
        for el in list_of_files__param_warpdrive_:
            
            list_of_absolute_files_adresses.append(cwd+dir__bit+'/'+el)
    return list_of_absolute_files_adresses
###################################################################################


def from_list_of_dict_to_pd_dataframe(two_flu__accel,interesting_columns):

    dict_for_df={}
    for coln_ind in range(len(interesting_columns)):
        coln=interesting_columns[coln_ind]
        dict_for_df[coln]=[two_flu__accel[a][coln] for a in range(len(two_flu__accel))]
    return pd.DataFrame(dict_for_df)


###############################################################################
def print_full_pandas(x):

    pd.set_option('display.max_rows', 500)
    pd.set_option('display.max_columns', 500)
    pd.set_option('display.width', 1500)
    pd.set_option('max_colwidth',150)
    
    print(x)
    pd.reset_option('display.max_rows')
    pd.reset_option('display.max_columns')
    pd.reset_option('display.width')
    pd.reset_option('max_colwidth')
    return

##############################################################################
def extract_the_various_parameters(list_of_absolute_files_adresses,threshold_timestamp=''):

    complete_dataframe=[]
    complete_list_of_dicts=[]
    iteration=0
    print("concatenate and drop duplicates...")
    for el in list_of_absolute_files_adresses:
        iteration+=1
        print(str(iteration)+'//'+str(len(list_of_absolute_files_adresses)))
        loading_filename=el

        #we read each data file
        with open(loading_filename, 'r') as fd:
            list_of_old_dict_res=json.load(fd)

        #first filtering step
        #  m a i nly it means removing first run (that do not have the proper characteristics)
        list_of_old_dict_res__filtered=filter_out_useless(list_of_old_dict_res)

        #the second filtering is all about filtering what has been produced before such date
        #second filtering step
        if len(threshold_timestamp)!=0:
            list_of_old_dict_res__filtered2=[ el for el in list_of_old_dict_res__filtered if el["timestamp"]>=threshold_timestamp]
        else:
            list_of_old_dict_res__filtered2=list_of_old_dict_res__filtered

        #we collect dicts that could be useful
        complete_list_of_dicts+=list_of_old_dict_res__filtered2
        #########################################################
        #### first we need to extract a dataframe with at least the following keys

        # and then remove the dulicates
        the_keys=['warp_speed', 'warp_acceleration', 'LAMBDA_into_account', 'timestamp', 'sigma',  'metric-fun']

        dict_for_creating_a_dataframe={}
        for key in the_keys:
            dict_for_creating_a_dataframe[key]=[list_of_old_dict_res__filtered2[a][key] for a in range(len(list_of_old_dict_res__filtered2))]
            
        df_partial=pd.DataFrame(dict_for_creating_a_dataframe)

        if isinstance(complete_dataframe,list):
            complete_dataframe=df_partial
        elif isinstance(complete_dataframe,pd.DataFrame):
            complete_dataframe=complete_dataframe.append(df_partial)
             
        else:
            print("isinstance(complete_dataframe,pd.DataFrame): not true")
            print("isinstance(complete_dataframe,list): not true")
            print('line 10486')
            raise
    
        

    complete_dataframe.reset_index(drop=True,inplace=True)
    complete_dataframe=complete_dataframe.drop_duplicates()

    list_of_good_indices=list(complete_dataframe.index)
    list_of_correct_timestamps=list(complete_dataframe["timestamp"])


    interesting_extraction__from_complete_list_of_dicts=[complete_list_of_dicts[a] for a in range(len(complete_list_of_dicts)) if a in list_of_good_indices ]
    interesting_timestamps=[interesting_extraction__from_complete_list_of_dicts[a]["timestamp"] for a in range(len(interesting_extraction__from_complete_list_of_dicts))]

    if not interesting_timestamps==list_of_correct_timestamps:
        print("not interesting_timestamps==list_of_correct_timestamps: \n line 10537 \n -------------- \n PB!!!")
        raise

    
    return interesting_extraction__from_complete_list_of_dicts
#######################################################################
        
####################################################################
if __name__ == "__main__":




    
    ###########################################################################################
    #objectives of this processing below seek good parameters (identical / different)


    #current working directory (CWD)
    cwd = os.getcwd()



    if 1:
        # necessary to filter past occurences

        with open(cwd+'/2020-02-25---shortlist---extracted_list_of_dicts__param_wardrive_1.json', 'r') as fd:
            list_of_old_dict_res__ref=json.load(fd)
            

        
        timestamps_ref_list=[list_of_old_dict_res__ref[a]["timestamp"] for a in range(len(list_of_old_dict_res__ref))]
        timestamps_ref_list.sort()
        last_ref_timestamp=timestamps_ref_list[-1]
        first_ref_timestamp=timestamps_ref_list[0]
        
    ########################################
    
    
    list_of_absolute_files_adresses=seek_for_the_correct_adresses_of_files(cwd=cwd)
    print('-----------------')
    for el in list_of_absolute_files_adresses:
        print(el)
    print(" \n -------- \n list_of_absolute_files_adresses \n line 5117")
    print('--------------')
    input()

    #this below is reference --- all data accumulated before this should not be included in the final results

    
    filename='2020-02-25___param_warpdrive_1.json'
    
    with open(cwd+'/'+filename, 'r') as fd:
        list_of_old_dict_res=json.load(fd)

    timestamps=[list_of_old_dict_res[a]['timestamp'] for a in range(len(list_of_old_dict_res))]
    timestamps.sort()

    
    #all timestamps that are before the first that we found in this list are to be rejected
    first_timestamp=timestamps[0]
    
    print('------------------------')
    print(first_timestamp)
    print("timest \n line 10499")
    print('------------------------')
    
    
    total_list_dict=extract_the_various_parameters(list_of_absolute_files_adresses,threshold_timestamp=last_ref_timestamp)


    total_list_dict=list_of_old_dict_res__ref+total_list_dict
    




    ####################"
    # analysis of 2 fluids Accel
    if 1:
        print(total_list_dict[0].keys())
        print("list_of_old_dict_res[0].keys() \n line 10550 \n ------------ \n ")

        two_flu__accel=[total_list_dict[a] for a in range(len(total_list_dict)) if total_list_dict[a]['warp_acceleration']>0 and\
                        '2flu' in total_list_dict[a]['metric-fun']  and "accel" in total_list_dict[a]['metric-fun']  ]
        
        two_flu__accel_metricFun=[total_list_dict[a]['metric-fun'] for a in range(len(total_list_dict)) if total_list_dict[a]['warp_acceleration']>0 ]
        print(list(set(two_flu__accel_metricFun)))
        print("list(set(two_flu__accel_metricFun)) \n line 10574 \n -------- \n ")
        interesting_columns=['warp_speed', 'timestamp', 'metric-fun']

        df=from_list_of_dict_to_pd_dataframe(two_flu__accel,interesting_columns)
        df.sort_values('timestamp',inplace=True)
        print_full_pandas(df)
        print("df \n line 10569 \n ----------\n ")
        
        
        timestamps=[list_of_old_dict_res[a]['timestamp'] for a in range(len(list_of_old_dict_res))]
    
    ######################


    
    ########################
    ###########################

    filename='ADD_output__filtered_interesting__param_warpdrive_1.json'

    path_cwd=os.getcwd()
    complete_path_filename=path_cwd+'/'+"OUTPUTS--of--analyze__drop_duplicates/"+filename
    
    
    print(len(total_list_dict))
    print("len(total_list_dict) \n line 10692 \n ---- \n ")
    
    with open(complete_path_filename, 'w') as fd:
        fd.write(json.dumps(total_list_dict))    #  dicts in a list here

    with open(complete_path_filename, 'r') as fd:
        list_of_dicts_reloaded2=json.load(fd)
    if not np.all([total_list_dict[a]==list_of_dicts_reloaded2[a] for a in range(len(total_list_dict))]):
        
        print(np.all([total_list_dict[a]==list_of_dicts_reloaded2[a] for a in range(len(total_list_dict))]))
        print("np.all([total_list_dict[a]==list_of_dicts_reloaded2[a] for a in range(len(total_list_dict))]) \n line 10577")
        print('written and not well reloaded!! \n line 10576')
        raise
    print('written and reloaded!! \n line 10576')
    

#!/usr/bin/python
# -*- coding: utf-8 -*-

import datetime
import matplotlib.pyplot as plt

import shutil
import copy

import matplotlib as mpl
import os, sys

import time, sys
import numpy as np
import matplotlib
matplotlib.use('QT4Agg')

from matplotlib.font_manager import FontProperties
import cmath as mth
import math
import time
import _pickle as cPickle
import pickle as Pickle

import matplotlib.mlab as mlab
import scipy
import scipy.fftpack
import scipy.stats.stats
import scipy.special
import scipy.linalg
import scipy.signal

import ctypes
import random

import locale
import operator
from matplotlib.ticker import ScalarFormatter
import re
import decimal
locale.setlocale(locale.LC_NUMERIC, "")
import urllib
import string
import pandas as pd
from scipy import stats

import os

from matplotlib import rcParams
import datetime


from cmath import *

from collections import Counter

import copy

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.colors as colors


import numpy as np
from scipy import ndimage


from scipy.optimize import minimize


from numpy import linalg as LA

import scipy as scipy
import numpy
print(numpy.version.version)
print("numpy.version.version")


import json

import os, shutil
from os import listdir
from os.path import isfile, join
print('--------------')
print(scipy.__version__)
print("scipy.__version__")
print('--------------')
import datetime      

try:
    import cPickle as pickle
except ImportError:  # python 3.x
    import pickle



from analyze__drop_duplicates import seek_for_the_correct_adresses_of_files,filter_out_useless


##################################################################################
##############################################################################
def extract_the_various_parameters__but_also_expunge(list_of_absolute_files_adresses,threshold_timestamp=''):

    complete_dataframe=[]
    complete_list_of_dicts=[]
    iteration=0
    print("concatenate and drop duplicates...")

    print('expunging things...')

    print(list_of_absolute_files_adresses)
    print("list_of_absolute_files_adresses \n line 127 \n ------- \n ")
    
    for el in list_of_absolute_files_adresses:
        iteration+=1
        print(str(iteration)+'//'+str(len(list_of_absolute_files_adresses)))
        loading_filename=el

        #we read each data file
        with open(loading_filename, 'r') as fd:
            list_of_old_dict_res=json.load(fd)


        ################################################
        list_of_old_dict_res=expunge__list_of_old_dicts(list_of_old_dict_res)

        bidule_save="/home/wilwil/Documents/2016-09-18-important-folders/"+\
                     "to-the-stars-or-not/2020-02-24---open-source--trials--accel-and-decel---2-3--fluids-nec/three-fluids-accel-data/2020-02-26-param_warpdrive_1.json"
        with open(bidule_save, 'w') as fd:
            fd.write(json.dumps(list_of_old_dict_res))    #  dicts in a list here
            
        with open(bidule_save, 'r') as fd:
            list_of_old_dict_res2=json.load(fd)
        #################################################

        #first filtering step
        #  m a i nly it means removing first run (that do not have the proper characteristics)
        print(len(list_of_old_dict_res))
        print("len(list_of_old_dict_res) \n line 156 \n ---- \n ")
        
        list_of_old_dict_res__filtered=filter_out_useless(list_of_old_dict_res)
        print(len(list_of_old_dict_res__filtered))
        print("LEN list_of_old_dict_res__filtered \n line 16 \n ----- \n ")
     
        #the second filtering is all about filtering what has been produced before such date
        #second filtering step
        if len(threshold_timestamp)!=0:
            list_of_old_dict_res__filtered2=[ el for el in list_of_old_dict_res__filtered if el["timestamp"]>=threshold_timestamp]
        else:
            list_of_old_dict_res__filtered2=list_of_old_dict_res__filtered

        #we collect dicts that could be useful
        complete_list_of_dicts+=list_of_old_dict_res__filtered2
        #########################################################
        #### first we need to extract a dataframe with at least the following keys
        # and then remove the duplicates
        
        the_keys=['warp_speed', 'warp_acceleration', 'LAMBDA_into_account', 'timestamp', 'sigma', 'metric-fun']

        dict_for_creating_a_dataframe={}
        for key in the_keys:
            dict_for_creating_a_dataframe[key]=[list_of_old_dict_res__filtered2[a][key] for a in range(len(list_of_old_dict_res__filtered2))]
            
        df_partial=pd.DataFrame(dict_for_creating_a_dataframe)

        if isinstance(complete_dataframe,list):
            complete_dataframe=df_partial
        elif isinstance(complete_dataframe,pd.DataFrame):
            complete_dataframe=complete_dataframe.append(df_partial)
             
        else:
            print("isinstance(complete_dataframe,pd.DataFrame): not true")
            print("isinstance(complete_dataframe,list): not true")
            print('line 10486')
            raise
    
        

    complete_dataframe.reset_index(drop=True,inplace=True)
    complete_dataframe=complete_dataframe.drop_duplicates()

    list_of_good_indices=list(complete_dataframe.index)
    list_of_correct_timestamps=list(complete_dataframe["timestamp"])


    interesting_extraction__from_complete_list_of_dicts=[complete_list_of_dicts[a] for a in range(len(complete_list_of_dicts)) if a in list_of_good_indices ]
    interesting_timestamps=[interesting_extraction__from_complete_list_of_dicts[a]["timestamp"] for a in range(len(interesting_extraction__from_complete_list_of_dicts))]

    if not interesting_timestamps==list_of_correct_timestamps:
        print("not interesting_timestamps==list_of_correct_timestamps: \n line 10537 \n -------------- \n PB!!!")
        raise

    
    return interesting_extraction__from_complete_list_of_dicts
#######################################################################


#################################################################################
def expunge__list_of_old_dicts(list_of_old_dict_res__ref):
    Okay_list=["LAMBDA_into_account",\
               "message",\
               "metric-fun",\
               "sigma",\
               "timestamp",\
               "warp_acceleration",\
               "warp_speed",\
               "x",\
    ]
    Not_okay=['current_NEC',\
              'current_EOS',\
              'direc',\
              "min-ranks",\
    ]
    for a in range(len(list_of_old_dict_res__ref)):
        for key in list(list_of_old_dict_res__ref[a].keys()):

            if key in Not_okay or (key not in Okay_list):
                del list_of_old_dict_res__ref[a][key]
                print(key in list(list_of_old_dict_res__ref[a].keys()))
                print("key in list(list_of_old_dict_res__ref[a].keys()) \n line 156 should be false! \n ")
                
                if key in list(list_of_old_dict_res__ref[a].keys()):
                    print('following condition should be false!!')
                    print(key in list(list_of_old_dict_res__ref[a].keys()))
                    raise

    return         list_of_old_dict_res__ref 

####################################################################
if __name__ == "__main__":




    
    ###########################################################################################
    #objective of this process below: seek good parameters
    # extract parameters and find identical/different param
    

    #current working directory (CWD)
    cwd = os.getcwd()


    #############################################" 
    if 1:
        # necessary to filter past occurences

        
        
        with open(cwd+'/2020-02-25---shortlist---extracted_list_of_dicts__param_wardrive_1.json', 'r') as fd:
            list_of_old_dict_res__ref=json.load(fd)
            
        list_of_old_dict_res__ref=expunge__list_of_old_dicts(list_of_old_dict_res__ref)
        
        
        bidule_new=cwd+'/2020-02-25---shortlist---extracted_list_of_dicts__param_wardrive_1.json'
        with open(bidule_new, 'w') as fd:
            fd.write(json.dumps(list_of_old_dict_res__ref))    #  dicts in a list here

        with open(bidule_new, 'r') as fd:
            list_of_old_dict_res__ref2=json.load(fd)
        print('end of it !!')
        print('---------------- \n line 178 \n ---- \n ')
        
    if 1:
        timestamps_ref_list=[list_of_old_dict_res__ref[a]["timestamp"] for a in range(len(list_of_old_dict_res__ref))]
        timestamps_ref_list.sort()
        last_ref_timestamp=timestamps_ref_list[-1]
        first_ref_timestamp=timestamps_ref_list[0]
        
    ########################################
    
    
    list_of_absolute_files_adresses=seek_for_the_correct_adresses_of_files(cwd=cwd)
    print(list_of_absolute_files_adresses)
    print("list_of_absolute_files_adresses \n line 212 \n --- \n ")
    
    for el in list_of_absolute_files_adresses:
        print(el)
    

    #this below is reference --- all data accumulated before this should not be included in the final results
    filename='2020-02-25___param_warpdrive_1.json'


    filename_n='2020-02-25___param_warpdrive_1.json'
    with open(cwd+'/'+filename, 'r') as fd:
        list_of_old_dict_res=json.load(fd)

    list_of_old_dict_res=expunge__list_of_old_dicts(list_of_old_dict_res)

    with open(cwd+'/'+filename_n, 'w') as fd:
        fd.write(json.dumps(list_of_old_dict_res__ref))    #  dicts in a list here
        
    with open(cwd+'/'+filename_n, 'r') as fd:
        list_of_old_dict_res__ref2=json.load(fd)

    
    ###############################
    timestamps=[list_of_old_dict_res[a]['timestamp'] for a in range(len(list_of_old_dict_res))]
    timestamps.sort()

    
    #all timestamps that are before [the first that we found in this list] are to be rejected
    first_timestamp=timestamps[0]
    

    total_list_dict=extract_the_various_parameters__but_also_expunge(list_of_absolute_files_adresses,threshold_timestamp=last_ref_timestamp)


    ####################################################

    #expunge DIRECTORIES!!
    list_of_directories_to_expunge=["OUTPUTS--plots--regular--each-case_latex_nonrankdeficient",\
                                    "OUTPUTS--plots--regular--each-case",\
                                    "2020-02-12--OUTPUTS--plots--regular--loglog-latex",\
                                    "2019-11-24--OUTPUTS--plots--regular--loglog",\
                                    "OUTPUTS--plots--regular--each-case_latex",\
                                    "OUTPUTS-OF--analyze_kip_threshold_date_2flu",\
                                    "OUTPUTS--plots--regular--each-case---error-minus-sinh",\
                                    "OUTPUTS--of--analyze__explore_keep_only_best__all_indicators",\
                                    "OUTPUTS--plots--regular--each-case___with_error_minus_sinh",\
                                    "OUTPUTS--of--analyze__explore_keep_only_best__all_indicators__list_of_dicts",\
                                    "OUTPUTS--of--analyse-keep-min-rank-optimum",\
                                    "OUTPUTS--of--analyze__drop_duplicates",\
                                    "OUTPUTS--of--dict_of_dict__2list_of_dicts",\
    ]

    print(cwd+'/')
    print("cwd+'/' \n line 383 \n -")


    for folder in [cwd+'/' + el for el in list_of_directories_to_expunge]:
        for filename in os.listdir(folder):
            if "gitignore" not in filename: 
                file_path = os.path.join(folder, filename)
                try:
                    if os.path.isfile(file_path) or os.path.islink(file_path):
                        os.unlink(file_path)
                    elif os.path.isdir(file_path):
                        shutil.rmtree(file_path)
                except Exception as e:
                    print('Failed to delete %s. Reason: %s' % (file_path, e))
    
    

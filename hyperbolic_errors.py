import numpy as np
import matplotlib
import matplotlib.pyplot as plt
#############################



from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})

rc('text', usetex=True)

numbers=np.arange(-50,50,0.1)

numbers=list(numbers)
numbers.extend([np.power(np.float(10),np.float(-1*ind_)) for ind_ in range(30)])

numbers.extend(list(np.arange(10,25,0.1)))
numbers.extend(list(np.arange(-10,-25,0.1)))
numbers.sort()

error_hyperbolic=np.array([np.cosh(nb)**2 - np.sinh(nb)**2-1 for nb in numbers])
numbers=np.array(numbers)
###########################################

cond_exceeded=(np.abs(error_hyperbolic)>1e-1)
number_that_exceeded_the_cond= numbers[cond_exceeded]



fig, ax = plt.subplots()

ax.plot(numbers,error_hyperbolic)

plt.xticks(np.arange(np.min(numbers), np.max(numbers), step=10.))
bound=17.33
plt.axvline(x=bound,c='r')
plt.axvline(x=-1*bound,c='r')

plt.yscale('symlog', linthreshy=0.01)
#########

ax.set(xlabel='x', ylabel=r'$\cosh^2(x)-\sinh^2(x)-1$')





ax.grid()

fig.savefig("test.png")
plt.show()



#!/usr/bin/python
# -*- coding: utf-8 -*-

import datetime
import matplotlib.pyplot as plt

plt.gca()

import copy

import matplotlib as mpl
import os, sys

import time, sys
import numpy as np
import matplotlib


from matplotlib.font_manager import FontProperties
import cmath as mth
import math
import time
import _pickle as cPickle
import pickle as Pickle

import matplotlib.mlab as mlab
import scipy
import scipy.fftpack
import scipy.stats.stats
import scipy.special
import scipy.linalg
import scipy.signal

import ctypes
import random

import locale
import operator
from matplotlib.ticker import ScalarFormatter
import re
import decimal
locale.setlocale(locale.LC_NUMERIC, "")
import urllib
import string
import pandas as pd
from scipy import stats

import os

from matplotlib import rcParams
import datetime


from cmath import *

from collections import Counter

import copy

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.colors as colors


import numpy as np
from scipy import ndimage


from scipy.optimize import minimize

from numpy import linalg as LA

import scipy as scipy
import numpy
print(numpy.version.version)
print("numpy.version.version")


import json

from os import listdir
from os.path import isfile, join
print('--------------')
print(scipy.__version__)
print("scipy.__version__")
print('--------------')
import datetime      
from matplotlib import rc
from matplotlib import rcdefaults

from matplotlib.ticker import NullFormatter  
from matplotlib.ticker import MaxNLocator
from matplotlib.offsetbox import AnchoredText
import matplotlib as mpl
import matplotlib.pyplot as plt 
import matplotlib.ticker

from matplotlib import rc
import sys
import gc
import matplotlib as mpl
import matplotlib.colors as colors
#####################################################################################
class MidpointNormalize(colors.Normalize):
	"""
	Normalise the colorbar so that diverging bars work there way either side from a prescribed midpoint value)

	e.g. im=ax1.imshow(array, norm=MidpointNormalize(midpoint=0.,vmin=-100, vmax=100))
	"""
	def __init__(self, vmin=None, vmax=None, midpoint=None, clip=False):
		self.midpoint = midpoint
		colors.Normalize.__init__(self, vmin, vmax, clip)

	def __call__(self, value, clip=None):
		# I'm ignoring masked values and all kinds of edge cases to make a
		# simple example...
		x, y = [self.vmin, self.midpoint, self.vmax], [0, 0.5, 1]
		return np.ma.masked_array(np.interp(value, x, y), np.isnan(value))

####################################################################################

def create_counterpart_everywhere(list_instead_of_dict2):


    all_keys=[]
    for ind_ in range(len(list_instead_of_dict2)):
        all_keys.extend(list(list_instead_of_dict2[ind_].keys()))
    all_keys=list(set(all_keys))
    list_of_index_where_somethings_amiss=[]
    for a in range(len(list_instead_of_dict2)): # we extract each dict
        the_dict=list_instead_of_dict2[a]
        
        if np.any([el not in list(the_dict.keys()) for el in all_keys]):
            el_list_missing=[el for el in all_keys if el not in list(the_dict.keys())]

            # en fait on complete quand on ne trouve pas la bonne clef, par une donnée (explicitement une donnée labelisée comme manquante)
            for el in el_list_missing:
                the_dict[el]=''
        list_instead_of_dict2[a]=the_dict
    #counterparts have been created for each key of list_b
    return list_instead_of_dict2
##########################################################################################################################


##########################################################################################################################
def create_some_new_features(list_instead_of_dict2,compute_max_std_min_mean_for_all_these_keys):
    
    for dict_b__ind in range(len(list_instead_of_dict2)):
        dict_b=list_instead_of_dict2[dict_b__ind]

        
        ###############
        #below we seek to
        #extract radius and theta of  [pressure max, pressure min], [density max, density min], NECV biggest violation,

            
        DIFF8OVER8SUM=np.array(dict_b["diff_over_sum"])

        DIFF8OVER8SUM8MODELED=np.array(dict_b['diff_over_sum__modeled'])

        einstein_tens_largest__map=np.array(dict_b["einstein_tens_largest__map"])

        einst_mixed_largest_value__map=np.array(dict_b["einst_mixed_largest_value__map"])

        sing_val_mat_6__map=np.array(dict_b["sing_val_mat_6"])
        



        ind__argmin_sing_val_6 = np.unravel_index(np.argmin(sing_val_mat_6__map, axis=None), sing_val_mat_6__map.shape)

        min_singular_value = np.min(np.array(dict_b["sing_val_mat_6"]))
        dict_b["min_singular_value__smallest_sv7"]=min_singular_value
        dict_b["einst_tens_largest__smallest_sv7"]=einstein_tens_largest__map[ind__argmin_sing_val_6]
        dict_b["einst_mixed_largest_value__smallest_sv7"]=einst_mixed_largest_value__map[ind__argmin_sing_val_6]
        ##################
        
        if '--2flu' not in dict_b["metric-fun"]:
            
            max_density= np.max([np.abs(np.array(dict_b["rho__p_x_1___real"])),\
                                 np.abs(np.array(dict_b["rho__p_x_2___real"])),\
                                 np.abs(np.array(dict_b["rho__p_x_3___real"]))])
            
            mat0=np.abs(np.array(dict_b["rho__p_x_1___real"]))
            ind__argmax_density__1 = np.unravel_index(np.argmax(mat0, axis=None), mat0.shape)
            max_1=mat0[ind__argmax_density__1]

            mat0=np.abs(np.array(dict_b["rho__p_x_2___real"]))
            ind__argmax_density__2 = np.unravel_index(np.argmax(mat0, axis=None), mat0.shape)
            max_2=mat0[ind__argmax_density__2]

            mat0=np.abs(np.array(dict_b["rho__p_x_3___real"]))
            ind__argmax_density__3 = np.unravel_index(np.argmax(mat0, axis=None), mat0.shape)
            max_3=mat0[ind__argmax_density__3]

            if max_density==max_3:
                ind__argmax_density=ind__argmax_density__3
            elif max_density==max_2:
                ind__argmax_density=ind__argmax_density__2
            elif max_density==max_1:
                ind__argmax_density=ind__argmax_density__1
            else:
                print('max_density==max_ NTH, not found !!! line 12849 \n ------ \n  "latex x y labels" 3 fluids')

            ################
            abs_rho1=np.abs(np.array(dict_b["rho__p_x_1___real"]))
            abs_rho2=np.abs(np.array(dict_b["rho__p_x_2___real"]))
            abs_rho3=np.abs(np.array(dict_b["rho__p_x_3___real"]))

            abs_rho1__min_sing_val=abs_rho1[ind__argmin_sing_val_6]
            abs_rho2__min_sing_val=abs_rho2[ind__argmin_sing_val_6]
            abs_rho3__min_sing_val=abs_rho3[ind__argmin_sing_val_6]
            
            max_abs_rho__min_sing_val=np.max([abs_rho1__min_sing_val,\
                                              abs_rho2__min_sing_val,\
                                              abs_rho3__min_sing_val,\
            ])

            
            
                                              
                
        else:
            max_density= np.max([np.abs(np.array(dict_b["rho__p_x_1___real"])),\
                                 np.abs(np.array(dict_b["rho__p_x_2___real"]))])
            
            mat0=np.abs(np.array(dict_b["rho__p_x_1___real"]))
            ind__argmax_density__1 = np.unravel_index(np.argmax(mat0, axis=None), mat0.shape)
            max_1=mat0[ind__argmax_density__1]

            mat0=np.abs(np.array(dict_b["rho__p_x_2___real"]))
            ind__argmax_density__2 = np.unravel_index(np.argmax(mat0, axis=None), mat0.shape)
            max_2=mat0[ind__argmax_density__2]

            if max_density==max_2:
                ind__argmax_density=ind__argmax_density__2
            elif max_density==max_1:
                ind__argmax_density=ind__argmax_density__1
            else:
                print('max_density==max_ NTH, not found !!! line 12849 \n ------ \n  "latex x y labels" 2 fluids!!')
            
            ########
            # do the same for 2 fluids / vs min of 6th sing val 
            abs_rho1=np.abs(np.array(dict_b["rho__p_x_1___real"]))
            abs_rho2=np.abs(np.array(dict_b["rho__p_x_2___real"]))
            

            abs_rho1__min_sing_val=abs_rho1[ind__argmin_sing_val_6]
            abs_rho2__min_sing_val=abs_rho2[ind__argmin_sing_val_6]
            
            
            max_abs_rho__min_sing_val=np.max([abs_rho1__min_sing_val,\
                                              abs_rho2__min_sing_val,\
            ])
            



        dict_b["max_abs_rho__min_sing_val"]=max_abs_rho__min_sing_val
        
        dict_b["eins_mixed_largest___for_max_abs_density"]=einst_mixed_largest_value__map[ind__argmax_density]
        dict_b["sing_val_mat_6___for_max_abs_density"]=sing_val_mat_6__map[ind__argmax_density]

        dict_b["max_abs_density"]=max_density 


        dict_b["upper_bound___for_max_abs_density"]=7*dict_b["eins_mixed_largest___for_max_abs_density"]*(dict_b["sing_val_mat_6___for_max_abs_density"])**(-1)


        dict_b["RATIO_upper_bound__over__max_abs_density"]=(7.0*dict_b["eins_mixed_largest___for_max_abs_density"]*(dict_b["sing_val_mat_6___for_max_abs_density"])**(-1))/max_density


        dict_b["einst_mixed_largest_value__map__absolute_max"]=np.nanmax(einst_mixed_largest_value__map)

        ####################
        try:
            dict_b["diff_ov_sum_modeled__located_a_max_diff_ov_sum"]=DIFF8OVER8SUM8MODELED[ind]


        except:
            dict_b["diff_ov_sum_modeled__located_a_max_diff_ov_sum"]=""


        #################
        


        DIFF8OVER8SUM=np.array(dict_b["diff_over_sum"])

        DIFF8OVER8SUM8MODELED=np.array(dict_b['diff_over_sum__modeled'])



        

        ind = np.unravel_index(np.argmax(DIFF8OVER8SUM, axis=None), DIFF8OVER8SUM.shape)
        ####################
        try:
            dict_b["diff_ov_sum_modeled__located_a_max_diff_ov_sum"]=DIFF8OVER8SUM8MODELED[ind]


        except:
            dict_b["diff_ov_sum_modeled__located_a_max_diff_ov_sum"]=""

            


        ###############
        try:
            #########
            table=np.array(dict_b["sin_theta_errors"])
            dict_b["sin_theta_errors__biggest_RD_EinstTens"]=table[ind]
            sin_theta_err_brd_e=table[ind]
            
            table=np.array(dict_b["sing_val"])
            dict_b["sing_val__biggest_RD_EinstTens"]=table[ind]
            sing_val_brd_e=table[ind]


            
            
            upper_boundary=sys.float_info.epsilon*2*sing_val_brd_e/np.sqrt(1-sin_theta_err_brd_e**2)+sin_theta_err_brd_e/np.sqrt(1-sin_theta_err_brd_e**2)*sing_val_brd_e**2
            dict_b["up_bo__biggestRDEinTens"]=upper_boundary
        except:
            dict_b["sin_theta_errors__biggest_RD_EinstTens"]=""
            dict_b["sing_val__biggest_RD_EinstTens"]=""
            dict_b["up_bo__biggestRDEinTens"]=""
            
            print(type(dict_b["sin_theta_errors"]))
            print("TYPE np.array(dict_b['sin_theta_errors']) \n line 12748 \n ---------- \n ")
            
            print(ind)
            print('ind \n line 12750 \n ----------- \n ')

            print(DIFF8OVER8SUM)
            print("DIFF8OVER8SUM \n line 12752 \n ----------- \n ")

            input()


        ###################
        for key_b in compute_max_std_min_mean_for_all_these_keys:
            
            if isinstance(dict_b[key_b],type(np.array([]))) or \
               isinstance(dict_b[key_b],type(list([]))):
                dict_b["np.nanmax("+key_b+")"]=np.nanmax(dict_b[key_b])
                dict_b["np.nanmin("+key_b+")"]=np.nanmin(dict_b[key_b])
                dict_b["np.nanstd("+key_b+")"]=np.nanstd(dict_b[key_b])
                dict_b["np.nanmean("+key_b+")"]=np.nanmean(dict_b[key_b])
            else:
                dict_b["np.nanmax("+key_b+")"]=''
                dict_b["np.nanmin("+key_b+")"]=''
                dict_b["np.nanstd("+key_b+")"]=''
                dict_b["np.nanmean("+key_b+")"]=''
        list_instead_of_dict2[dict_b__ind]=dict_b
    return list_instead_of_dict2
##########################################################################################################################

#############################################################
def retreat_output_list_of_dict(list_of_dict):


    # we want to extract the sum of positive mass
    # the sum of negative mass, and all sorts of quantities
    ########################################"""
    ########################################"""
    for dict_b in list_of_dict:
        dict_b['max_abs__max_abs_u_squared_plus_one']=np.max(np.abs(dict_b['max_abs_u_squared_plus_one']))
        dict_b['max_abs__max_abs_of_velocities_components']=np.max(np.abs(dict_b['max_abs_of_velocities_components']))

        dict_b['max__sing_val_mat_6']=np.max(dict_b['sing_val_mat_6'])
        dict_b['min__sing_val_mat_6']=np.min(dict_b['sing_val_mat_6'])
        dict_b['mean__sing_val_mat_6']=np.mean(dict_b['sing_val_mat_6'])
        dict_b['std__sing_val_mat_6']=np.std(dict_b['sing_val_mat_6'])

        
        if "--2flu" in dict_b["metric-fun"]:
            # 2 fluids
            dict_b['vol_dens_neg_sum__TOT']=dict_b['vol_dens_2_neg_sum']+dict_b['vol_dens_1_neg_sum']
            dict_b['vol_dens_pos_sum__TOT']=dict_b['vol_dens_2_pos_sum']+dict_b['vol_dens_1_pos_sum']

            dict_b['hyperbolic-relation--max-deviation']=np.max([np.abs(dict_b['gamma___2__j_relation___imag']),\
                                                                 np.abs(dict_b['gamma___1__j_relation___imag']),\
                                                                 ])
            dict_b['max-gamma']=np.max([np.abs(dict_b['gamma___2__j_relation___real']),\
                                        np.abs(dict_b['gamma___1__j_relation___real']),\
            ])

        else:
                    
            # 3 fluids
            dict_b['vol_dens_neg_sum__TOT']=dict_b['vol_dens_2_neg_sum']+dict_b['vol_dens_1_neg_sum']+dict_b['vol_dens_3_neg_sum']
            dict_b['vol_dens_pos_sum__TOT']=dict_b['vol_dens_2_pos_sum']+dict_b['vol_dens_1_pos_sum']+dict_b['vol_dens_3_pos_sum']

            dict_b['hyperbolic-relation--max-deviation']=np.max([np.abs(dict_b['gamma___3__j_relation___imag']),\
                                                                 np.abs(dict_b['gamma___2__j_relation___imag']),\
                                                                 np.abs(dict_b['gamma___1__j_relation___imag']),\
                                                                 ])
            dict_b['max-gamma']=np.max([np.abs(dict_b['gamma___3__j_relation___real']),\
                                        np.abs(dict_b['gamma___2__j_relation___real']),\
                                        np.abs(dict_b['gamma___1__j_relation___real']),\
            ])

            


    
    return list_of_dict
###########################################################################
###########################################################################
def create_new_list_of_features(list_fo_dict):
    
    
    all_keys=[]
    for ind_ in range(len(list_fo_dict)):
        
        all_keys.extend(list(list_fo_dict[ind_].keys()))
    all_keys=list(set(all_keys))
        
    return all_keys

################################################################################
################################################################################
def plot_one_label__all_cases_latex(label,dict_of_dfs__accel_decel__two_three__each_case_label,addendum='',plt='',sub_directory="",\
                                    dico_results_numeric_article={},\
                                    save_res_art=False,\
):


    rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})


    fig = plt.figure()

    ax = fig.add_subplot(1, 1, 1)

    dict_colors={}
    it_colors=0
    colors_set=['r','g','b','k']
    shift_set=range(len(colors_set))
    dict_shift={}
    

    for key in list(dict_of_dfs__accel_decel__two_three__each_case_label.keys()):
        dict_colors[key]=colors_set[it_colors]
        dict_shift[key]=shift_set[it_colors]
        it_colors+=1

    non_all_same_sign=["density_meanplotting"]


    concatenate_all_ys=[]
    

    ############
    #compute correct x ticks
    complete_x_list=[]
    for key in list(dict_of_dfs__accel_decel__two_three__each_case_label.keys()):
        ylabel=label

        latex_use=False
        df=dict_of_dfs__accel_decel__two_three__each_case_label[key]
        df=df.sort_values("warp_speed")
        x=list(df["warp_speed"])#_s)
        complete_x_list.extend(x)

    complete_x_list=list(set(complete_x_list))
    complete_x_list.sort()


    dict_from_x_str_to_xticks={r'$'+('%.3e' % complete_x_list[el_ind])+r'$':el_ind  for el_ind in range(len(complete_x_list))}

    dict_from_x_str_to_xticks___values={complete_x_list[el_ind]:el_ind  for el_ind in range(len(complete_x_list))}

    
    x_str_sticks=[r'$'+('%.3e' % el)+r'$'  for el in x]
    plt.xticks([ dict_from_x_str_to_xticks[el_b] for el_b in x_str_sticks],tuple(x_str_sticks),rotation=90)
        

    if save_res_art:
        dico_results_numeric_article[label]={"dict_from_x_str_to_xticks":dict_from_x_str_to_xticks}
    #########################
    for key in list(dict_of_dfs__accel_decel__two_three__each_case_label.keys()):

        print("key   "+str(key) + ' line 470 ')
        

        #####################################
        ylabel=label
        print("ylabel   "+str(ylabel)+' line 475  \n ----- \n ')

        latex_use=False
        df=dict_of_dfs__accel_decel__two_three__each_case_label[key]
        df=df.sort_values("warp_speed")

           
        
        if ylabel=='map--mean-rank':
            ylabel=r'map average of $\mathbf{[e \otimes e]}$ rank'
            xlabel=r'warp speeds (v/c)'

            key_label=key

            y_ticks=[2,3,4,5,6,7,8,9,10,11]
            y_ticks=np.array(y_ticks)/10.0+6
            plt.yticks(y_ticks)
            ax=plt.gca()
            ax.set_ylim(np.min(y_ticks),np.max(y_ticks))
            
            y=list(df[label])
            
        elif ylabel=='np.max(diff_sur_som)':

            ylabel=r'map max of RD'
            xlabel=r'warp speeds (v/c)'

            key_label=key
        
            y=list(df[label])
        elif ylabel.lower()=='current_eos'  :

            ylabel=r'MAEOS'
            xlabel=r'warp speeds (v/c)'

            key_label=key
        
            y=list(df[label]/3.)
        elif ylabel.lower()=='max-gamma'  :


            ylabel=r'$max(|y_{h}|)$'
            xlabel=r'warp speeds (v/c)'

            key_label=key
        
            y=list(df[label])
        elif ylabel.lower()=='hyperbolic-relation--max-deviation'  :


            ylabel=r'$\max( \Delta_{ h }( y_{h,m}) ) $'
            xlabel=r'warp speeds (v/c)'

            key_label=key
        
            y=list(df[label])
        elif ylabel.lower()=='\max(SCQ)'.lower() or ('SCQ'.lower() in ylabel.lower() and 'max' in ylabel.lower()) or ('np_max_condition_schwarzschild' in label)   or ('max' in label.lower() and 'condition' in label.lower() and 'schwarzschild'  in ylabel.lower()):
            # schwarzschld condition quantity

            ylabel=r'max(SCQ)'
            xlabel=r'warp speeds (v/c)'

            key_label=key
        
            y=list(df[label])


            
        elif ylabel.lower()=='vol_dens_neg_sum__TOT__sun_mass'.lower()  :

            ylabel=r'opposite of total negative mass (in solar mass)'

            xlabel=r'warp speeds (v/c)'
            key_label=key
            y=list(df[label])
        elif ylabel.lower()=='vol_dens_pos_sum__TOT__sun_mass'.lower()  :


            ylabel=r'total positive mass (in solar mass)'
            xlabel=r'warp speeds (v/c)'
            key_label=key
            y=list(df[label])
        elif ylabel.lower()=='current_NEC__density__kg_m_minus_3'.lower()  :


            ylabel=r'NECV'
            #\left[ kg m^{-3} \right]'

            xlabel=r'warp speeds (v/c)'
            key_label=key
            y=list(df[label])

        elif ylabel.lower()=='max_abs__max_abs_u_squared_plus_one'.lower()  :


            ylabel=r'$\max |u^2+1| $'
            xlabel=r'warp speeds (v/c)'
            key_label=key
            y=list(df[label])
        elif "RATIO_upper_bound__over__max_abs_density".lower() in ylabel.lower() : 
            ylabel=r'(upper bound)/(max abs. density)'
            xlabel=r'warp speeds (v/c)'
            key_label=key
            y=list(df[label])

        elif "density" in ylabel.lower() or "pressure" in ylabel.lower() or "einst" in ylabel.lower():
            ylabel=ylabel.replace('-',' ').replace('_',' ')
            xlabel=r'warp speeds (v/c)'
            key_label=key
            y=list(df[label])

            


        else:
            key_label=key
            xlabel='warp speeds (v/c)'
        
            y=list(df[label])
        #####################################
        if y==['' for el in y]:
            pass
                            
        ##################################################################################
        else:    
            

        
        

            concatenate_all_ys.extend(y)
            if label not in non_all_same_sign:
                try:
                    if y[0]<0:
                        y=[el*(-1.0) for el in y]
                except:
                    print(label)
                    print("label \n line 11029 \n ---------- \n ")
                    print(y)
                    print("y \n line 11098 \n ------- \n ")

                    raise

            means=np.array(list(df[label]))

            x=list(df["warp_speed"])
            
            if df["warp_acceleration"].unique().shape[0]>1:
                print(df["warp_acceleration"].unique().shape[0]>1)
                print('df["warp_acceleration"].unique().shape[0]>1')

                print(df["warp_acceleration"].unique().shape[0])
                print('df["warp_acceleration"].unique().shape[0]')
                print('line 10536')
                raise
            warp_accel_extracted=df["warp_acceleration"].unique()[0]





            ##############



            # loc works the same as it does with figures (though best doesn't work)
            # pad=5 will increase the size of padding between the border and text
            # borderpad=5 will increase the distance between the border and the axes
            # frameon=False will remove the box around the text

            if "--2flu" in df["metric-fun"].unique()[0]:
                nFluid_txt="2 fluids"

            else:
                nFluid_txt="3 fluids"
            nFluid__filename_txt=nFluid_txt.replace(' ','_') #"2fluids"
            if warp_accel_extracted>0:
                accel_txt="accel"

            else:
                accel_txt="decel"
            
            

            key_label_dict__fluid_nb={"A_2F":"A-2FM",\
                                      "D_2F":"D-2FM",\
                                      "A_3F":"A-3FM",\
                                      "D_3F":"D-3FM",\
            }
            dict_of_dfs__accel_decel__ONLYnrd__each_case_label={"A_2F":accel_two_fluids__df_nrd,\
                                                        "D_2F":decel_two_fluids__df_nrd,\
                                                        "A_3F":accel_three_fluids__df_nrd,\
                                                        "D_3F":decel_three_fluids__df_nrd}

            
            x_values_for_plot=np.array([dict_from_x_str_to_xticks___values[el] for el in x])

            if  label in ['nit',"nfev", "current_min_rank"]+non_all_same_sign or "map" in label.lower():
                ax.plot(x_values_for_plot+dict_shift[key]*0.1, y, ls='',marker='.',c=dict_colors[key],label=key_label_dict__fluid_nb[key_label])
            else:
                ax.semilogy(x_values_for_plot+dict_shift[key]*0.1, y, ls='',marker='.',c=dict_colors[key],label=key_label_dict__fluid_nb[key_label])
            
            for b_ind in range(len(x_values_for_plot)):

                ax.plot((x_values_for_plot[b_ind]+dict_shift[key]*0.1, x_values_for_plot[b_ind]+dict_shift[key]*0.1), (0, y[b_ind]), c=dict_colors[key])
        if save_res_art:
            y_save=[float(el) for el in y]
            x_save=[float(el) for el in x_values_for_plot]
                
            dico_results_numeric_article[label][key]={"xlabel":xlabel,\
                                                      "ylabel":ylabel,\
                                                      "y_values":list(y),\
                                                      "x_values":list(x_save),\
            }

    plt.gca().yaxis.set_minor_formatter(NullFormatter())
    mpl.rcParams['xtick.major.pad'] = 8
    mpl.rcParams['ytick.major.pad'] = 8
    
    if "RATIO_upper_bound__over__max_abs_density".lower() in ylabel.lower() or r'(upper bound)/(max abs. density)' in ylabel: 
        locmaj = matplotlib.ticker.LogLocator(base=10,numticks=4) 
        ax.yaxis.set_major_locator(locmaj) 
        ax.tick_params(axis='y', which='minor', bottom=False)

        max_y=np.max(y)
        min_y=np.min(y)

        max_int=int(np.ceil(np.log(max_y)/np.log(10)))
        min_int=int(np.floor(np.log(min_y)/np.log(10)))

        int_list_b=np.arange(min_int,max_int+1)
        ax.set_yticks([10**el for el in int_list_b])
        
    nan_max_y=np.nanmax(concatenate_all_ys)
    nan_min_y=np.nanmin(concatenate_all_ys)

    
    plt.xlabel(xlabel)    
    plt.ylabel(ylabel)
    plt.tight_layout()
    plt.grid(axis='y')


    if 'np' in label and \
       'max' in label and \
       "condition" in label and \
       "schwarzschild" in label:
        plt.hlines(1, 0, len(x),color='darksalmon')
    

    if "rank" in label.lower()  :
        legend = ax.legend(loc='lower left', shadow=True, fontsize='x-large')
        legend.get_frame().set_facecolor('C0')

    elif "diff_sur_som" in label.lower():

        legend = ax.legend(loc='best', shadow=True, fontsize='x-large')
        legend.get_frame().set_facecolor('C0')
    elif "max-gamma" in label.lower():
        pass
    else:
        legend = ax.legend(loc='best', shadow=True, fontsize='x-large')
        legend.get_frame().set_facecolor('C0')

    ###########################"
    ##########################"
    if len(addendum)!=0:
        filename_b=sub_directory+label.replace('(','_').replace(')','_').replace('.','_').replace('<','_').replace('/','_')+'___'+addendum+'.eps'
        plt.savefig(filename_b)
        print("saved   "+str(filename_b)+'  input any key')
        input()
    else:
        filename_b=sub_directory+label.replace('(','_').replace(')','_').replace('.','_').replace('<','_').replace('/','_')+'__all_accel__allfluidmixes.eps'
        plt.savefig(filename_b)
        print("saved  "+ str(filename_b)+'  input any key')
        input()
    plt.cla()
    plt.clf()
    plt.close('all')
    mpl.rcParams.update(mpl.rcParamsDefault)
    if save_res_art:
        return dico_results_numeric_article
    else:
        return


###########################################################################

#############################################################################
##########################################################################################################################################
################################################################################
def plot_one_label__all_cases(label,dict_of_dfs__accel_decel__two_three__each_case_label,addendum='',plt='',sub_directory=""):


 
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    dict_colors={}
    it_colors=0
    colors_set=['r','g','b','k']
    shift_set=range(len(colors_set))
    dict_shift={}
    
 
    for key in list(dict_of_dfs__accel_decel__two_three__each_case_label.keys()):
        dict_colors[key]=colors_set[it_colors]
        dict_shift[key]=shift_set[it_colors]
        it_colors+=1

    non_all_same_sign=["density_meanplotting"]


    concatenate_all_ys=[]


    ###################################
    # we seek how many cases are found for each warp velocity
    for key in list(dict_of_dfs__accel_decel__two_three__each_case_label.keys()):
        print(key)
    print('line 788')
    print(dict_of_dfs__accel_decel__two_three__each_case_label[key])
    print('key   :  '+key)
    print('line 791  \n line --')
    print(dict_of_dfs__accel_decel__two_three__each_case_label[key].columns)
    print("dict_of_dfs__accel_decel__two_three__each_case_label[key].columns \n line 793 \n ---- \n ")

    print(list(dict_of_dfs__accel_decel__two_three__each_case_label[key].columns))
    print("list(dict_of_dfs__accel_decel__two_three__each_case_label[key].columns) \n line 796 \n --- \n ")
    for col_bidule in list(dict_of_dfs__accel_decel__two_three__each_case_label[key].columns):
        print(col_bidule)
    print('line 799 \n ------- \n ')

    print(np.unique(dict_of_dfs__accel_decel__two_three__each_case_label[key]["warp_speed"]))
    print('np.unique(dict_of_dfs__accel_decel__two_three__each_case_label[key]["warp_speed"]) \n line 802 \n ----- \ ')
    print(len(np.unique(dict_of_dfs__accel_decel__two_three__each_case_label[key]["warp_speed"])))
    print('len(np.unique(dict_of_dfs__accel_decel__two_three__each_case_label[key]["warp_speed"])) \n line 804 \n ---- \n ')
    print(len(dict_of_dfs__accel_decel__two_three__each_case_label[key]["warp_speed"]))
    print('len( NON UNIK !! (dict_of_dfs__accel_decel__two_three__each_case_label[key]["warp_speed"])) \n line 804 \n ---- \n ')
    
    
    

    

    ############
    #compute correct x ticks
    complete_x_list=[]
    for key in list(dict_of_dfs__accel_decel__two_three__each_case_label.keys()):
        ylabel=label

        latex_use=False
        df=dict_of_dfs__accel_decel__two_three__each_case_label[key]
        df=df.sort_values("warp_speed")
        x=list(df["warp_speed"])
        complete_x_list.extend(x)

    complete_x_list=list(set(complete_x_list))
    complete_x_list.sort()
    dict_from_x_str_to_xticks={r'$'+('%.3e' % complete_x_list[el_ind])+r'$':el_ind  for el_ind in range(len(complete_x_list))}

    dict_from_x_str_to_xticks___values={complete_x_list[el_ind]:el_ind  for el_ind in range(len(complete_x_list))}

    
    x_str_sticks=[r'$'+('%.3e' % el)+r'$'  for el in x]
    plt.xticks([ dict_from_x_str_to_xticks[el_b] for el_b in x_str_sticks],tuple(x_str_sticks),rotation=90)
        

    
    #########################
    for key in list(dict_of_dfs__accel_decel__two_three__each_case_label.keys()):


        #####################################
        ylabel=label

        latex_use=False
        df=dict_of_dfs__accel_decel__two_three__each_case_label[key]
        df=df.sort_values("warp_speed")

        if ylabel=='map--mean-rank':
            ylabel=r'map average of matrix rank'


            xlabel=r'warp speeds (v/c)'

            key_label=key

            y_ticks=[2,3,4,5,6,7,8,9,10,11]
            y_ticks=np.array(y_ticks)/10.0+6
            plt.yticks(y_ticks)
            ax=plt.gca()
            ax.set_ylim(np.min(y_ticks),np.max(y_ticks))
            
            y=list(df[label])
        elif ylabel=='np.max(diff_sur_som)':

            ylabel=r'map max of RD'
            xlabel=r'warp speeds (v/c)'

            key_label=key
        
            y=list(df[label])
        elif ylabel.lower()=='current_eos'  :

            ylabel=r'MAEOS'
            xlabel=r'warp speeds (v/c)'

            key_label=key
        
            y=list(df[label]/3.)
        elif ylabel.lower()=='max-gamma'  :


            ylabel=r'max |y_h|'
            xlabel=r'warp speeds (v/c)'

            key_label=key
        
            y=list(df[label])
        elif ylabel.lower()=='hyperbolic-relation--max-deviation'  :

            ylabel=r'max Delta_h(y_{h,m})'
            xlabel=r'warp speeds (v/c)'

            key_label=key
        
            y=list(df[label])
        elif ylabel.lower()=='np.max(condition_schwarzschild)'  :
            # schwarzschld condition quantity
            ylabel=r'SCQ'
            xlabel=r'warp speeds (v/c)'

            key_label=key
        
            y=list(df[label])


            
        elif ylabel.lower()=='vol_dens_neg_sum__TOT__sun_mass'.lower()  :
            ylabel=r'opposite of total negative mass (in solar mass)'
            xlabel=r'warp speeds (v/c)'
            key_label=key
            y=list(df[label])
        elif ylabel.lower()=='vol_dens_pos_sum__TOT__sun_mass'.lower()  :

            ylabel=r'total positive mass (in solar mass)'
            xlabel=r'warp speeds (v/c)'
            key_label=key
            y=list(df[label])
        elif ylabel.lower()=='current_NEC__density__kg_m_minus_3'.lower()  :

            ylabel=r'NEC (kg/m^3)'
            xlabel=r'warp speeds (v/c)'
            key_label=key
            y=list(df[label])

        elif ylabel.lower()=='max_abs__max_abs_u_squared_plus_one'.lower()  :

            ylabel=r'max|u^2+1|'
            xlabel=r'warp speeds (v/c)'
            key_label=key
            y=list(df[label])



        else:
            key_label=key
            xlabel='warp speeds (v/c)'
        
            y=list(df[label])
        #####################################
        if y==['' for el in y]:
            if 0:
                print('-------------------------------')
            else:
                pass
                            
        ##################################################################################
        else:    


        
        

            concatenate_all_ys.extend(y)
            if label not in non_all_same_sign:
                try:
                    if y[0]<0:
                        y=[el*(-1.0) for el in y]
                except:
                    print(label)
                    print("label \n line 11029 \n ---------- \n ")
                    print(y)
                    print("y \n line 11098 \n ------- \n ")

                    raise

            means=np.array(list(df[label]))

            x=list(df["warp_speed"])

            if df["warp_acceleration"].unique().shape[0]>1:
                print(df["warp_acceleration"].unique().shape[0]>1)
                print('df["warp_acceleration"].unique().shape[0]>1')

                print(df["warp_acceleration"].unique().shape[0])
                print('df["warp_acceleration"].unique().shape[0]')
                print('line 10536')
                raise
            warp_accel_extracted=df["warp_acceleration"].unique()[0]





            ##############




            if "--2flu" in df["metric-fun"].unique()[0]:
                nFluid_txt="2 fluids"

            else:
                nFluid_txt="3 fluids"
            nFluid__filename_txt=nFluid_txt.replace(' ','_') 
            if warp_accel_extracted>0:
                accel_txt="accel"

            else:
                accel_txt="decel"


            x_values_for_plot=np.array([dict_from_x_str_to_xticks___values[el] for el in x])

            if  label in ['nit',"nfev", "current_min_rank"]+non_all_same_sign or "map" in label.lower():
                plt.plot(x_values_for_plot+dict_shift[key]*0.1, y, ls='',marker='.',c=dict_colors[key],label=key_label)


            else:

                plt.semilogy(x_values_for_plot+dict_shift[key]*0.1, y, ls='',marker='.',c=dict_colors[key],label=key_label)

            for b_ind in range(len(x_values_for_plot)):


                plt.plot((x_values_for_plot[b_ind]+dict_shift[key]*0.1, x_values_for_plot[b_ind]+dict_shift[key]*0.1), (0, y[b_ind]), c=dict_colors[key])


    
    plt.gca().yaxis.set_minor_formatter(NullFormatter())

    

    

    mpl.rcParams['xtick.major.pad'] = 8
    mpl.rcParams['ytick.major.pad'] = 8
    
    nan_max_y=np.nanmax(concatenate_all_ys)
    nan_min_y=np.nanmin(concatenate_all_ys)

    
    plt.xlabel(xlabel)    
    plt.ylabel(ylabel)
    plt.tight_layout()
    plt.grid(axis='y')


    if 'np' in label and \
       'max' in label and \
       "condition" in label and \
       "schwarzschild" in label:
        plt.hlines(1, 0, len(x),color='darksalmon')
    

    if "rank" in label.lower()  :
        legend = ax.legend(loc='lower left', shadow=True, fontsize='x-large')
        legend.get_frame().set_facecolor('C0')

    elif "diff_sur_som" in label.lower():

        legend = ax.legend(loc='best', shadow=True, fontsize='x-large')
        legend.get_frame().set_facecolor('C0')
    elif "max-gamma" in label.lower():
        pass
    else:
        legend = ax.legend(loc='best', shadow=True, fontsize='x-large')
        legend.get_frame().set_facecolor('C0')

    
    ###########################"
    ##########################"
    if len(addendum)!=0:
        plt.savefig(sub_directory+label.replace('(','_').replace(')','_').replace('.','_').replace('<','_').replace('/','_')+'___'+addendum+'.eps')
    else:
        plt.savefig(sub_directory+label.replace('(','_').replace(')','_').replace('.','_').replace('<','_').replace('/','_')+'__all_accel__allfluidmixes.eps')
    plt.cla()
    plt.clf()
    plt.close('all')
    if latex_use:
        rcdefaults()
    return
####

###########################################################################
def compute_normalization(txt):
    ###################
    c__speed_of_light=2.99792458e8
    G__gravitational_cstt=6.67408e-11

    if "rho__p_x_" in txt  and "___real" in txt or \
       "current_NEC_-_map" in txt :
        #density normalisation --> density in
        #kg_m_minus_3
        normalization=c__speed_of_light**2/G__gravitational_cstt
        #




    elif ("rho__p_x_" in txt  and "___imag" in txt) or ("p_rho__p_phi_"in txt) or\
         ("total_pressure__aver_pressures" in txt):
        #pressure normalisation --> prssure in
        #N/m^2 =Pa
        normalization=c__speed_of_light**4/G__gravitational_cstt

    else:
        normalization=1
        #################"
    return normalization
################################################################################
def dataframe__from_list_of_dict__and_list_cols(list_b,list_instead_of_dict2):


    
    
    dict_key_good={}
    ###################
    ##################
    for key in list_b:

        if 1:
            print(key)
            print("key \n line 10528 \n --------- \n ")
            
            for dict_b in list_instead_of_dict2: # we check all dicts -- each cases
                if key in list(dict_b.keys()):
                    dict_key_good[key]='ini'

                    if str(type(dict_b[key]))=="<class 'float'>":
                        dict_key_good[key]="True"

                    elif str(type(dict_b[key]))=="<class 'int'>":
                        dict_key_good[key]="True"
                    elif str(type(dict_b[key]))=="<class 'bool'>":
                        dict_key_good[key]="True"
                    elif str(type(dict_b[key]))=="<class 'numpy.float64'>":
                        dict_key_good[key]="True"
                    elif str(type(dict_b[key]))=="<class 'list'>":
                        dict_key_good[key]="False"




                        break
                    elif str(type(dict_b[key]))=="<class 'str'>":

                        dict_key_good[key]="True"
                    else:

                        print('key  :   '+str(key))
                        print(type(dict_b[key]))
                        print("type(dict_b[key]) \n line 10503 \n ---------- \n ")
                        if len(dict_b[key])>0:
                            print(len(dict_b[key]))
                            print("len(dict_b[key]) \n line 10513 \n ------- \n ")
                        print(dict_b[key])
                        print("dict_b[key] \n line 10515 \n ----------- \n ")
                        print("BIZZARE ! bizarre")
                        input()
    ##################"""

    ##################"""
    list_b=[el for el in list_b if dict_key_good[el]!="False"]


    #################################################

    ###################################
    #create a function for this
    list_of_list=[]
    for colname in list_b:
        list_of_list.append([])
        # one list for each element in list_b
    ###################################    
    for dict_b in list_instead_of_dict2:
        for ind_list in range(len(list_b)):
            list_of_list[ind_list].append(dict_b[list_b[ind_list]])
    dict_to_make_df={}
    for colname_ind in range(len(list_b)):
        dict_to_make_df[list_b[colname_ind]]=list_of_list[colname_ind]

    df=pd.DataFrame(dict_to_make_df)
    return df

##########################################################################

def build_and_save_path_easy_read(inputs_build_and_save_path,symlog=True,colored=True,cwd=""):
    

    if symlog and colored:
        base_path=cwd+"/2020-10-31--newplots-col-symlog"
        
    else:
        base_path=cwd+"/2020-10-30---new-plots"
        
    print(base_path)
    print("base_path \n line 1218 \n ----- \n ")
    #input()

    
    list_k=list(inputs_build_and_save_path.keys())
    list_k.sort()
    if 0:
        for key in list_k :
            print(str(key)+' ------> '+str(inputs_build_and_save_path[key]))
        print('-----------')
        print('line  137')

    if "rho__p_x_" in inputs_build_and_save_path["quantity_txt"] and \
       "real" in inputs_build_and_save_path["quantity_txt"]:
        quantity_dir="densities"
    elif "EOS".lower() in inputs_build_and_save_path["quantity_txt"].lower() :
        quantity_dir="EOS" # equation of state
    elif "total_pressure__aver_pressures_".lower() in inputs_build_and_save_path["quantity_txt"].lower() :#F1__map
        quantity_dir="tot_press_aver"
    elif "theta_mesh" in inputs_build_and_save_path["quantity_txt"] or \
         "r_mesh" in inputs_build_and_save_path["quantity_txt"]:
        quantity_dir=inputs_build_and_save_path["quantity_txt"]
    

    else:
        print("quantity_dir not defined \n line 145 \n ------ \n ")
        raise
       
    
    full_good_path_dir=base_path+'/'+inputs_build_and_save_path["ws"]+\
        '/'+inputs_build_and_save_path["wa"]+\
        '/'+quantity_dir
    
    full_good_path=full_good_path_dir+\
        '/'+inputs_build_and_save_path["quantity_txt"]
    
    
    os.makedirs(full_good_path_dir, exist_ok=True)
    return full_good_path
    

###########################################################################################

#########################################################################################
def build_and_save_path(inputs_build_and_save_path,symlog=True,colored=True):
    if symlog and colored:
        base_path=cwd+"/2020-10-31--newplots-col-symlog"

    else:
        base_path=cwd+"/2020-10-30---new-plots"
        
    print(base_path)
    print("base_path \n line 1212 \n ----- \n ")
    #input()
    list_k=list(inputs_build_and_save_path.keys())
    list_k.sort()
    
    if "rho__p_x_" in inputs_build_and_save_path["quantity_txt"] and \
       "real" in inputs_build_and_save_path["quantity_txt"]:
        quantity_dir="densities"
    elif "EOS".lower() in inputs_build_and_save_path["quantity_txt"].lower() :
        quantity_dir="EOS" # equation of state
    elif "total_pressure__aver_pressures_".lower() in inputs_build_and_save_path["quantity_txt"].lower() :#F1__map
        quantity_dir="tot_press_aver"
    elif "theta_mesh" in inputs_build_and_save_path["quantity_txt"] or \
         "r_mesh" in inputs_build_and_save_path["quantity_txt"]:
        quantity_dir=inputs_build_and_save_path["quantity_txt"]
    

    else:
        print("quantity_dir not defined \n line 145 \n ------ \n ")
        raise
    
    
    full_good_path_dir=base_path+'/'+inputs_build_and_save_path["ws"]+\
        '/'+inputs_build_and_save_path["wa"]+\
        '/'+quantity_dir
    
    full_good_path=full_good_path_dir+\
        '/'+inputs_build_and_save_path["quantity_txt"]
    
    
    os.makedirs(full_good_path_dir, exist_ok=True)
    return full_good_path
    

###########################################################################################

################################################################################################
def plot_heatmap_array__X_rho__easy_read(inputs,per_velocity=True,symlog=True,colored=True,short_test=False,cwd=''):


    dir_heatmaps=inputs['dir_heatmaps']
    quantity=inputs["quantity"]

    
    
    theta_mesh=inputs["theta_mesh"]
    r_mesh=inputs['r_mesh']
    X_mesh=inputs['X_mesh']
    rho_mesh=inputs['rho_mesh']
    normalization=inputs['normalization']

    v_max=inputs['v_max']
    v_min=inputs['v_min']
    v=inputs['v']
    r_max=inputs['r_max']
    r_min=inputs['r_min']
    r=inputs['r']
    
    quantity_txt=inputs['quantity_txt']

    
    theta_mesh=np.array(theta_mesh)
    r_mesh=np.array(r_mesh)
    quantity=np.array(quantity)

    
    quantity=quantity*normalization


    if short_test:
        print(np.max(quantity))
        print("np.max(dict_reloaded[txt]) \n line 9604")
        print(np.min(quantity))
        print("np.min(dict_reloaded[txt]) \n line 9606")


    
    
    plt.rcParams.update({"font.family": "serif",\
                         "font.serif": ["Palatino"],\
    })

    
    font = {'weight' : 'bold',\
            'size'   : 25,\
            'family' : 'normal',\
    }

    
    matplotlib.rc('font', **font)

    
    
    fig = plt.figure(num=None, figsize=(15, 13), dpi=80, facecolor='w', edgecolor='k')

    ax = fig.gca()


    quantity_no_inf=quantity[(quantity!=-np.inf)&(quantity!=np.inf)]
    q_max=np.nanmax(quantity_no_inf)
    q_min=np.nanmin(quantity_no_inf)
    lin_thres=np.nanmin(np.abs(quantity_no_inf))
    mid_val=0
    if lin_thres==0:
        lin_thres=np.nanmin(np.abs([el for el in quantity_no_inf.flatten() if el!=0]))

    print(q_max)
    print("qmax \n line 241 \n ---- \n ")
    print(q_min)
    print("qmin \n line 242 \n ---- \n ")
    print(lin_thres)
    print("lin_thres \n line 245 \n ---- \n ")
    
    quantity_finite=np.nan_to_num(quantity, copy=False, nan=0.0, posinf=q_max, neginf=q_min)

    
    
    
    print(np.isnan(quantity).any())
    print("quantity.isnan().any() \n line 242 \n ----   \n")
    print(np.isinf(quantity).any())
    print("quantity.isinf().any() \n line 244 \n ----   \n")
    print(np.isinf(quantity_no_inf).any())
    print("quantity_no_inf.isnan() \n line 246 \n ----   \n")

    print(np.isnan(quantity_finite).any())
    print("quantity_finite.isnan().any() \n line 242 \n ----   \n")
    print(np.isinf(quantity_finite).any())
    print("quantity_finite.isinf().any() \n line 244 \n ----   \n")
    
    cmap='plasma'
    linscale=4
    base=10
    if symlog and colored:
        pcol_obj=ax.pcolor(rho_mesh, X_mesh, quantity_finite,norm=colors.SymLogNorm(linthresh=lin_thres, linscale=linscale,vmin=q_min, vmax=q_max,base=base),cmap=cmap,shading='auto')#'gist_ncar')
        pcol_obj=ax.pcolor(-1.0*rho_mesh, X_mesh, quantity_finite,norm=colors.SymLogNorm(linthresh=lin_thres, linscale=linscale,vmin=q_min, vmax=q_max,base=base),cmap=cmap,shading='auto')#'gist_ncar')
        
        
    else:
        if q_min<0 and  q_max>0:
                pcol_obj=ax.pcolor(rho_mesh, X_mesh, quantity,norm=MidpointNormalize(midpoint=mid_val,vmin=q_min, vmax=q_max),cmap='RdBu')
                pcol_obj=ax.pcolor(-1.0*rho_mesh, X_mesh, quantity,norm=MidpointNormalize(midpoint=mid_val,vmin=q_min, vmax=q_max),cmap='RdBu')
        else:
                pcol_obj=ax.pcolor(rho_mesh, X_mesh, quantity,cmap='RdBu')
                pcol_obj=ax.pcolor(-1.0*rho_mesh, X_mesh, quantity,cmap='RdBu')
                
    ax.set_xlabel(r'$\rho$')
    ax.set_ylabel(r'$X$')
    text_to_show='v_max = '+v_max.__str__()+"\n v = "+v.__str__()+'\n v_min = '+v_min.__str__()
    if 'varies' in str(r):
        text_to_show+='\n \n r_max = '+r_max.__str__()+'\n r_min = '+r_min.__str__()
    else:
        text_to_show+='\n \n r_max = '+r_max.__str__()+' \n r = '+r.__str__()+'\n r_min = '+r_min.__str__()

    text_to_show+='\n \n '+quantity_txt+'___MAX = '+np.max(quantity.flatten()).__str__()+'\n '+quantity_txt+'___MIN = '+np.min(quantity.flatten()).__str__()
    plt.tight_layout()
    fig.colorbar(pcol_obj, shrink=0.9, aspect=5)
    
    ax.set_aspect('equal', 'box')
    inputs_build_and_save_path={"quantity_txt":quantity_txt,\
                                "ws":'ws_'+str(inputs['warp_speed']).replace('.','_'),\
                                "wa":'wa__'+str(inputs['warp_acceleration']).replace('.','_'),\
    }
    save_path=build_and_save_path_easy_read(inputs_build_and_save_path,symlog=symlog,colored=colored,cwd=cwd)
    plt.savefig(save_path,bbox_inches='tight',dpi=100)
    print('saved : '+save_path)
    plt.clf()
    plt.close('all')
    return

#####################################################################################################################
def compute_EOS_diverse_fluids(dictb):

    EOS_s=[]    
    for i in range(3):
        this_eos=(np.array(dictb["p_rho__p_phi_"+str(i+1)+"___real"])+np.array(dictb["p_rho__p_phi_"+str(i+1)+"___imag"])+    np.array(dictb["rho__p_x_"+str(i+1)+"___imag"]))/(3.*np.array(dictb["rho__p_x_"+str(i+1)+"___real"]))
        EOS_s.append(this_eos)
        dictb["EOS_"+str(i+1)]=this_eos
    u=[np.max(np.abs(EOS)) for EOS in EOS_s]
    print(u)
    print("[np.max(np.abs(EOS)) for EOS in EOS_s] \n line 1558 \n ----- \n ")
    

    
    return dictb
    

####################################################################
if __name__ == "__main__":
    # the objective here is to produce graphs

    saving_directory_for_saving_files_produced_here="OUTPUTS--plots--regular--each-case"
    cwd=os.getcwd()

    complete_path_OUTPUTS__of__this_script=cwd+'/'+saving_directory_for_saving_files_produced_here+'/'

    complete_path_OUTPUTS__of__this_script_latex=cwd+'/'+saving_directory_for_saving_files_produced_here+'_latex'+'/'
    complete_path_OUTPUTS__of__this_script_latex_nrd=cwd+'/'+saving_directory_for_saving_files_produced_here+'_latex_nonrankdeficient'+'/'

    ######################

    #####################

    complete_path_OUTPUTS__of__dict_of_dict__2list_of_dicts=cwd+'/'+"OUTPUTS-OF--analyze_kip_threshold_date_2flu/"
    list_files=os.listdir(complete_path_OUTPUTS__of__dict_of_dict__2list_of_dicts)
    print(list_files)
    print("list_files \n line 1172 \n ----- \n ")
    
    if "extracted_list_of_dicts__param_warpdrive_1.json" in list_files:
        new_filename_complete_filename=complete_path_OUTPUTS__of__dict_of_dict__2list_of_dicts+"extracted_list_of_dicts__param_warpdrive_1.json"

    print("opening "+new_filename_complete_filename)
    print('"opening "+new_filename_complete_filename \n line 1182 \n ----- \n ')
    

    
    with open(new_filename_complete_filename, 'r') as fd:
        list_instead_of_dict2=json.load(fd)
    print("opened "+new_filename_complete_filename)
    print('opened file!! ')


    ##################
    print(len(list_instead_of_dict2))
    print("len(list_instead_of_dict2) \n line 10985")
    timestamps= [list_instead_of_dict2[a]["timestamp"] for a in range(len(list_instead_of_dict2))]
    timestamps.sort()
    for el in timestamps:
        print(el)

    print('line 10988, waiting for input...')
    print('--------------------------------------------------')





        
    
    print('------------------------------------' )
    for key in list(list_instead_of_dict2[0].keys()):
        print(key)
    print(' key in list(list_instead_of_dict2[0].keys()): \n line 10778 \n -------- \n ')

    print('-------------------------------------- \n input any key!  \n ---- \n line 1220')


    for a_b in range(len(list_instead_of_dict2)):
        dictb=list_instead_of_dict2[a_b]
        
        if (dictb["warp_speed"]==1e-5 or dictb["warp_speed"]==1.0) and dictb["nb_of_fluids"]==3:
            print(dictb["warp_speed"])
            print('dictb["warp_speed"] \n line 1226 \n ----- \n  ')
            print(type(dictb["warp_speed"]))
            print('TYPE dictb["warp_speed"] \n line 1228 \n ---- \n ')
            
            print(dictb["warp_speed"]==1e-5)
            print('dictb["warp_speed"]==1e-5 \n line 1228 \n ---- \n ')


            dictb=compute_EOS_diverse_fluids(dictb)
            
            heatmaps_inputs={}
            heatmaps_inputs['dir_heatmaps']=cwd+'/2019-06-16----map_data_plots/'
            
            
            heatmaps_inputs['warp_speed']=dictb['warp_speed']
            heatmaps_inputs['warp_acceleration']=dictb['warp_acceleration']
            
            theta_mesh=np.array(dictb['theta_real_mesh'])
            r_mesh=np.array(dictb["r_mesh__one_theta_real"])
            
            X_mesh=np.multiply(r_mesh,np.cos(theta_mesh))
            rho_mesh=np.multiply(r_mesh,np.sin(theta_mesh))
            
            heatmaps_inputs['X_mesh']=X_mesh
            heatmaps_inputs['rho_mesh']=rho_mesh
            
            
            
            heatmaps_inputs['theta_mesh']=theta_mesh
            heatmaps_inputs['r_mesh']=r_mesh
            heatmaps_inputs['v_max']=""
            heatmaps_inputs['v_min']=""
            
            heatmaps_inputs['r_max']=np.max(rho_mesh)
            heatmaps_inputs['r_min']=np.min(rho_mesh)
            heatmaps_inputs['r']=''
            
            heatmaps_inputs['v']=heatmaps_inputs['warp_speed']
                

            

            for nb_f in range(3):
                
                key_rel="rho__p_x_"+str(nb_f+1)+"___real"
                print("key-reloaded  --->     "+key_rel)
                print(type(dictb[key_rel]))
                print("type(dictb[key_rel]) \n line 1240 \n ------ \n ")
                
                
                print(np.max(dictb[key_rel]))
                print("np.max(dictb[key_rel]) \n line 1244 \n ------- \n ")
                print(np.array(dictb[key_rel]).shape)
                print("dictb[key_rel].shape \n line 1246 \n ----- \n  input any key")


                ###########################################################################


            
                
                heatmaps_inputs['quantity']=dictb[key_rel]

                
                heatmaps_inputs['quantity_txt']=key_rel


                normalization=compute_normalization(key_rel)
                
                
                heatmaps_inputs['normalization']=normalization
                
                short_test=True
                plot_heatmap_array__X_rho__easy_read(heatmaps_inputs,per_velocity=True,symlog=False,colored=False,short_test=short_test,cwd=cwd)
                
                

                gc.collect()

                
                print(dictb['current_min_rank'])
                print("large_dict_of_best_dict[long_string_key]['current_min_rank'] \n line 7643 \n ------------ \n ")
                

                print('--------------------------------------------------------------------------------')
                print('################################################################################ \n line 7578')



                
                ###########################################################################
                

            for nb_f in range(3):

                    
                key_rel="EOS_"+str(nb_f+1)+""
                print("key-reloaded  --->     "+key_rel)
                print(type(dictb[key_rel]))
                print("type(dictb[key_rel]) \n line 1240 \n ------ \n ")
                
                
                print(np.max(dictb[key_rel]))
                print("np.max(dictb[key_rel]) \n line 1244 \n ------- \n ")
                print(np.array(dictb[key_rel]).shape)
                print("dictb[key_rel].shape \n line 1246 \n ----- \n  input any key")


                ###########################################################################


            
                
                heatmaps_inputs['quantity']=dictb[key_rel]

                
                heatmaps_inputs['quantity_txt']=key_rel


                normalization=compute_normalization(key_rel)
                
                
                heatmaps_inputs['normalization']=normalization
                
                short_test=True
                plot_heatmap_array__X_rho__easy_read(heatmaps_inputs,per_velocity=True,symlog=True,colored=True,short_test=short_test,cwd=cwd)
                plot_heatmap_array__X_rho__easy_read(heatmaps_inputs,per_velocity=True,symlog=False,colored=False,short_test=short_test,cwd=cwd)
                
                

                gc.collect()


                
                print(dictb['current_min_rank'])
                print("large_dict_of_best_dict[long_string_key]['current_min_rank'] \n line 7643 \n ------------ \n ")
                

                print('--------------------------------------------------------------------------------')
                print('################################################################################ \n line 7578')




                

            print('------------')
            
            print(dictb["density_max"])
            print('dictb["density_max"] \n line 1258 \n ----- \n ')
            print('------------')
            print('------------')

    print('FIN de ---> for a_b in range(len(list_instead_of_dict2)): \n input any key \n ---- \n ')
    
